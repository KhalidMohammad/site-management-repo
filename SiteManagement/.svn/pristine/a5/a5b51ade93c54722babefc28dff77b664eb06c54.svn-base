﻿@using SiteManagement.Models;
@using SiteManagement.Data;
@model SiteModel

<div>
    @using (Html.BeginForm("Save", "Site", FormMethod.Post, new { id = "frmSite", @class = "form-horizontal", target = "_blank" }))
    {
        @Html.HiddenFor(model => model.SiteId, new { id = "hdnSiteId" })

        <div class="form-group">
            @Html.CustomLabelFor(model => model.SiteCode, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.TextBoxFor(model => model.SiteCode, new { @class = "form-control" })
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.ValidationMessageFor(model => model.SiteCode, "", new { @class = "text-danger" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.RelatedSite, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.MultipleSelect2For(model => model.RelatedSite, ViewData["Sites"] as IEnumerable<SelectListItem>, "Select related sites", new { @class = "form-control" })
                </div>

            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.SiteName, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.TextBoxFor(model => model.SiteName, new { @class = "form-control" })
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.ValidationMessageFor(model => model.SiteName, "", new { @class = "text-danger" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.SiteCity, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.TextBoxFor(model => model.SiteCity, new { @class = "form-control" })
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.ValidationMessageFor(model => model.SiteCity, "", new { @class = "text-danger" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.SiteGovernorates, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.TextBoxFor(model => model.SiteGovernorates, new { @class = "form-control" })
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.ValidationMessageFor(model => model.SiteGovernorates, "", new { @class = "text-danger" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.Latitude, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.DecimalInputFor(model => model.Latitude, new { @class = "form-control", @min = -90, @max = 90 })
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.ValidationMessageFor(model => model.Latitude, "", new { @class = "text-danger" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.Longitude, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.DecimalInputFor(model => model.Longitude, new { @class = "form-control", @min = -180, @max = 180 })
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.ValidationMessageFor(model => model.Longitude, "", new { @class = "text-danger" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.StreetName, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.TextBoxFor(model => model.StreetName, new { @class = "form-control" })
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.ValidationMessageFor(model => model.StreetName, "", new { @class = "text-danger" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.IsActive, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.SwitchFor(model => Model.IsActive)
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.SiteType, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @if ((RoleTypeEnum)SessionManager.CurrentUser.UserType == RoleTypeEnum.SuperAdmin)
                {
                        @Html.EditableSelect2For(model => model.SiteType, ViewData["SiteTypes"] as IEnumerable<SelectListItem>
                    ,
                    (int)LookupTypeEnum.SiteType, Url.Action("AddNewLookup"), Url.Action("EditLookup"),
                    "Choose a Site", false, true, false, null, null, new { @class = "form-control" })
                    }
                    else
                    {
                        @Html.Select2For(model => model.SiteType, ViewData["SiteTypes"] as IEnumerable<SelectListItem>
                        ,
                        "Choose a Site", true, new { @class = "form-control" })
                    }
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.ValidationMessageFor(model => model.SiteType, "", new { @class = "text-danger" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.SiteNote, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.TextAreaFor(model => model.SiteNote, new { @class = "form-control" })
                </div>
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.ValidationMessageFor(model => model.SiteNote, "", new { @class = "text-danger" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.MunicipalityPermission, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.SwitchFor(model => Model.MunicipalityPermission, false, new { id = "chkIsMunicipalityPermission" })
                </div>
            </div>
        </div>

        <div class="form-group" id="divMunicipalityPermissionDate" style="display: none;">
            @Html.CustomLabelFor(model => model.MunicipalityPermissionDate, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.DatePickerFor(model => model.MunicipalityPermissionDate)
                </div>
                @*<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @Html.ValidationMessageFor(model => model.MunicipalityPermissionDate, "", new { @class = "text-danger" })
                    </div>*@
            </div>
        </div>

        <div class="form-group" id="divMunicipalityPermissionPicture" style="display: none;">
            @Html.CustomLabelFor(model => model.MunicipalityPermissionPictureJson, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.UploadPictureFor(model => model.MunicipalityPermissionPictureJson, Url.Action("DownloadFile", "File"))
                </div>
                @*<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @Html.ValidationMessageFor(model => model.MunicipalityPermissionPictureJson, "", new { @class = "text-danger" })
                    </div>*@
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.LocationSubType, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @if ((RoleTypeEnum)SessionManager.CurrentUser.UserType == RoleTypeEnum.SuperAdmin)
            {
                        @Html.EditableSelect2For(model => model.LocationSubType, ViewData["LocationSubTypes"] as IEnumerable<SelectListItem>
                ,
                (int)LookupTypeEnum.LocationSubType, Url.Action("AddNewLookup"), Url.Action("EditLookup"),
                "Choose a Site", false, true, false, null, null, new { @class = "form-control" })
                    }
                    else
                    {
                        @Html.Select2For(model => model.LocationSubType, ViewData["LocationSubTypes"] as IEnumerable<SelectListItem>
                    ,
                    "Choose a Location Sub Type", true, new { @class = "form-control" })
                    }
                </div>
                @*<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @Html.ValidationMessageFor(model => model.LocationSubType, "", new { @class = "text-danger" })
                    </div>*@
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.LocationSubTypeNote, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.TextAreaFor(model => model.LocationSubTypeNote, new { @class = "form-control" })
                </div>
                @*<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @Html.ValidationMessageFor(model => model.LocationSubTypeNote, "", new { @class = "text-danger" })
                    </div>*@
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.TXSiteType, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @if ((RoleTypeEnum)SessionManager.CurrentUser.UserType == RoleTypeEnum.SuperAdmin)
            {
                        @Html.EditableSelect2For(model => model.TXSiteType, ViewData["TXSiteTypes"] as IEnumerable<SelectListItem>
                ,
                (int)LookupTypeEnum.TXSiteType, Url.Action("AddNewLookup"), Url.Action("EditLookup"),
                "Choose a Site", false, true, false, null, null, new { @class = "form-control" })
                    }
                    else
                    {
                        @Html.Select2For(model => model.TXSiteType, ViewData["TXSiteTypes"] as IEnumerable<SelectListItem>
                    ,
                    "Choose a TX Site Type", true, new { @class = "form-control" })
                    }
                </div>
                @*<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @Html.ValidationMessageFor(model => model.TXSiteType, "", new { @class = "text-danger" })
                    </div>*@
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.PayRent, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.SwitchFor(model => model.PayRent)
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.OnAirDate, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.DatePickerFor(model => model.OnAirDate)
                </div>
                @*<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @Html.ValidationMessageFor(model => model.MunicipalityPermissionDate, "", new { @class = "text-danger" })
                    </div>*@
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.PhaseNumber, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.TextBoxFor(model => model.PhaseNumber, new { @class = "form-control" })
                </div>
            </div>
        </div>

        <div class="form-group">
            @Html.CustomLabelFor(model => model.SiteCodePrefix, new { @class = "control-label col-sm-2" })
            <div class="col-sm-10">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    @Html.TextBoxFor(model => model.SiteCodePrefix, new { @class = "form-control" })
                </div>
            </div>
        </div>
    }
</div>

<script>
    @Html.JsMinify(@<text>

    $(document).ready(function () {
        $.validator.unobtrusive.parse($("#frmSite"));
    });

    $("#chkIsMunicipalityPermission").change(function () {
        if ($(this).is(":checked")) {
            $("#divMunicipalityPermissionDate").show();
            $("#divMunicipalityPermissionPicture").show();
        }
        else {
            $("#divMunicipalityPermissionDate").hide();
            $("#divMunicipalityPermissionPicture").hide();
        }
    });

    // For the first time, if the form is in update mode
    if ($("#chkIsMunicipalityPermission").is(":checked")) {
        $("#divMunicipalityPermissionDate").show();
        $("#divMunicipalityPermissionPicture").show();
    }

    </text>)
</script>