﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SiteManagement.Business
{
    public static class ImportFileManager
    {
        public static byte[] GetElectricityBillTemplate()
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Create excel sheet document
                using (SpreadsheetDocument excelDoc = SpreadsheetDocument.Create(ms, SpreadsheetDocumentType.Workbook))
                {
                    ms.Seek(0, SeekOrigin.Begin);

                    // Create workbook part
                    WorkbookPart workbookPart = excelDoc.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();

                    // Create worksheet
                    SheetData sheetData = new SheetData();
                    worksheetPart.Worksheet = new Worksheet(sheetData);

                    // Create sheets
                    Sheets sheets = excelDoc.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

                    // Append a new worksheet and associate it with the workbook.
                    Sheet sheet = new Sheet()
                    {
                        Id = excelDoc.WorkbookPart.GetIdOfPart(worksheetPart),
                        SheetId = 1,
                        Name = "ElectricityBillSheet"
                    };

                    sheets.Append(sheet);

                    // Create header row
                    Row headerRow = new Row();

                    // Prepare header titles
                    List<string> columnNames = new List<string>
                                {
                                    "Electricity Meter Code",
                                    "Electricity Bill Code",
                                    "Old Reading",
                                    "New Reading",
                                    "Bill Date",
                                    "Consumption",
                                    "Amount"
                                };

                    // Create header columns
                    foreach (string column in columnNames)
                    {
                        // Create header cell
                        Cell cell = new Cell
                        {
                            DataType = CellValues.String,
                            CellValue = new CellValue(column),
                            StyleIndex = 2
                        };

                        headerRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(headerRow);

                    excelDoc.WorkbookPart.Workbook.Save();

                    excelDoc.Close();

                    return ms.ToArray();
                }
            }
        }

        public static List<ElectricityBill> ReadElectricityBillSheet(byte[] fileContnt)
        {
            List<ElectricityBill> electricityBills = new List<ElectricityBill>();

            using (MemoryStream stream = new MemoryStream(fileContnt))
            {
                using (SpreadsheetDocument spreadsheetDocument = SpreadsheetDocument.Open(stream, false))
                {
                    WorkbookPart workbookPart = spreadsheetDocument.WorkbookPart;

                    WorksheetPart worksheetPart = workbookPart.WorksheetParts.FirstOrDefault();

                    // Validate sheet
                    if (worksheetPart == null)
                    {
                        throw new ArgumentException("There is no sheet on this file");
                    }

                    SheetData sheetData = worksheetPart.Worksheet.Elements<SheetData>().First();

                    List<Row> rows = sheetData.Elements<Row>().ToList();

                    // Validate Rows
                    if (rows == null || rows.Count <= 1)
                    {
                        throw new ArgumentException("There is no data in this sheet");
                    }

                    // Validate Header
                    List<string> columnNames = new List<string>
                                {
                                    "Electricity Meter Code",
                                    "Electricity Bill Code",
                                    "Old Reading",
                                    "New Reading",
                                    "Bill Date",
                                    "Consumption",
                                    "Amount"
                                };

                    List<Cell> headerCells = rows[0].Elements<Cell>().ToList();

                    bool isHeaderValid = true;
                    List<string> headerInvalidCellsNames = new List<string>();
                    //for (int i = 0; i < columnNames.Count; i++) // This throws an exception if the headerCells count is less than the count of columnNames
                    for (int i = 0; i < headerCells.Count; i++)
                    {
                        string headerCellText = headerCells[i].CellValue.InnerText;
                        if (headerCellText != columnNames[i])
                        {
                            isHeaderValid = false;
                            headerInvalidCellsNames.Add(headerCellText);
                        }
                    }

                    if (!isHeaderValid)
                    {
                        if (headerCells.Count < columnNames.Count)
                        {
                            // Add the rest of not exist columns to invalid list
                            for (int i = headerCells.Count; i < columnNames.Count; i++)
                            {
                                headerInvalidCellsNames.Add(columnNames[i]);
                            }
                        }
                        string invalidCells = string.Join(",", headerInvalidCellsNames);
                        //throw new ArgumentException(string.Format("The following columns '{0}' not exist in your excel sheet", invalidCells));
                    }

                    // Read sheet data
                    try
                    {
                        for (int i = 1; i < rows.Count; i++)
                        {
                            List<Cell> rowCells = rows[i].Elements<Cell>().ToList();

                            electricityBills.Add(new ElectricityBill
                            {
                                ElectricityMeterCode = GetCellValue(spreadsheetDocument, rowCells[0]),
                                ElectricityBillCode = GetCellValue(spreadsheetDocument, rowCells[1]),
                                OldReading = GetCellValue(spreadsheetDocument, rowCells[2]),
                                NewReading = GetCellValue(spreadsheetDocument, rowCells[3]),
                                BillDate = CommonHelper.GetDateFromModel(SpreadsheetGradersHelper.GetCellContent(spreadsheetDocument, rowCells[4])), //CommonHelper.GetDateFromModel(GetCellValue(spreadsheetDocument, rowCells[3])), //TODO
                                Consumption = GetCellValue(spreadsheetDocument, rowCells[5]),
                                Amount = Convert.ToDecimal(GetCellValue(spreadsheetDocument, rowCells[6])),
                            });
                        }
                    }
                    catch (Exception)
                    {
                        throw new ArgumentException("Incorrect data");
                    }
                }
            }

            return electricityBills;
        }

        private static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            if (cell == null)
                return null;

            string cellText = cell.InnerText;

            if (String.IsNullOrEmpty(cellText))
                return null;

            if (cell.DataType != null)
            {
                switch (cell.DataType.Value)
                {
                    case CellValues.SharedString:
                        var sharedString = document.WorkbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
                        cellText = sharedString.SharedStringTable.ChildElements[Int32.Parse(cellText)].InnerText;
                        break;

                    case CellValues.Boolean:
                        cellText = cellText == "0" ? "FALSE" : "TRUE";
                        break;
                }
            }

            return cellText;
        }

        //private static string GetValue(string text, Cell cell, SpreadsheetDocument spreadsheetDocument)
        //{
        //    try
        //    {
        //        if (cell.DataType != null)
        //        {
        //            switch (cell.DataType.Value)
        //            {
        //                case CellValues.SharedString:
        //                    var sharedString = spreadsheetDocument.WorkbookPart.GetPartsOfType<SharedStringTablePart>().FirstOrDefault();
        //                    return sharedString.SharedStringTable.ChildElements[Int32.Parse(text)].InnerText;

        //                    break;
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        return text;
        //    }

        //    return text;
        //}
    }
}