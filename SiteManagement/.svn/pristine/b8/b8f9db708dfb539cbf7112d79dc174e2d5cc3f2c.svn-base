using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteManagement.Data
{
    [Table("Contract")]
    public partial class Contract : ISavedEntity, IApprovalEntity, IAuditEntity
    {
        [Key]
        public int ContractId { get; set; }

        public int SiteId { get; set; }

        public virtual Site Site { get; set; }

        [Column(TypeName = "date")]
        public DateTime ContractStartDate { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ContractEndDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal YearlyRent { get; set; }

        public bool MunicipalityPermission { get; set; }

        [StringLength(200)]
        public string ContractDocumentFilePath { get; set; }

        [NotMapped]
        public FileEntity ContractDocumentFile { get; set; }

        public int? PaymentTypeId { get; set; }

        public virtual Lookup PaymentType { get; set; }

        [StringLength(200)]
        public string QoshanFilePath { get; set; }

        [NotMapped]
        public FileEntity QoshanFile { get; set; }

        public bool IsMadeByUmniah { get; set; }

        public bool IsColocationClose { get; set; }

        public string ColocationCloseNote { get; set; }

        public bool IsActive { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ActiveDate { get; set; }

        public string ActiveNote { get; set; }

        public bool IsTerminated { get; set; }

        [Column(TypeName = "date")]
        public DateTime? TerminatedDate { get; set; }

        public string TerminatedNote { get; set; }

        [StringLength(200)]
        public string AquariumNumber { get; set; }

        [StringLength(200)]
        public string AquariumName { get; set; }

        [StringLength(200)]
        public string PieceNumber { get; set; }

        [StringLength(200)]
        public string Directorate { get; set; }

        [StringLength(200)]
        public string Town { get; set; }

        public string PlateNumber { get; set; }

        [Column(TypeName = "numeric")]
        public decimal MaarefTax { get; set; }

        public int? AcquiredById { get; set; }

        public virtual Lookup AcquiredBy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? SignatureDate { get; set; }

        public bool IsIncremental { get; set; }

        public decimal? IncrementalPercentage { get; set; }

        [StringLength(200)]
        public string TerminationLetterFilePath { get; set; }

        [NotMapped]
        public FileEntity TerminationLetterFile { get; set; }

        public virtual ICollection<ContractYearlyRent> ContractYearlyRents { get; set; }


        #region ISavedEntity Properties

        public bool FlagDeleted { get; set; }

        public int CreatedByUserId { get; set; }

        public virtual User CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UpdatedByUserId { get; set; }

        public virtual User UpdatedBy { get; set; }

        public DateTime LastUpdatedDate { get; set; }

        #endregion ISavedEntity Properties


        #region IApprovalEntity Properties

        public int? OriginalId { get; set; }

        public int ApprovalStatusId { get; set; }

        public int? PartialApprovedByUserId { get; set; }

        public virtual User PartialApprovedBy { get; set; }

        public DateTime? PartialApprovedDate { get; set; }

        public int? ApprovedByUserId { get; set; }

        public virtual User ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int Id
        {
            get
            {
                return ContractId;
            }
        }

        /// <summary>
        /// save contract yearly rents for logging only 
        /// </summary>
        public string ContractYearlyRentsString { get; set; }

        #endregion IApprovalEntity Properties
    }
}
