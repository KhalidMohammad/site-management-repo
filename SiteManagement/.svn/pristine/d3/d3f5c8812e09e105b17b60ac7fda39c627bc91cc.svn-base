﻿using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class ElectricityMeterConverter
    {

        public static ElectricityMeter ToDatabaseEntity(this ElectricityMeterModel model)
        {
            if (model != null)
            {
                return new ElectricityMeter
                {
                    ElectricityMeterCode = model.ElectricityMeterCode,
                    PipesFillingSupplierName = model.PipesFillingSupplierName,
                    ActivationDate = String.IsNullOrWhiteSpace(model.ActivationDate) ? null :  (DateTime?)Convert.ToDateTime(model.ActivationDate),
                    USenceSupplierName = model.USenceSupplierName,
                    ElectricityMeterDistributionId = model.ElectricityMeterDistribution,
                    ElectricityMeterSubTypeId = model.ElectricityMeterSubType,
                    ElectricityMeterPaymentSubTypeId = model.ElectricityMeterPaymentSubType,
                    ElectricityMeterId = model.ElectricityMeterId,
                    ElectricityMeterType = model.ElectricityMeterType,
                    EstimatePowerConsumption = model.EstimatePowerConsumption,
                    PipesFilling = model.PipesFilling,
                    SiteId = model.ElectricityMeterSiteId,
                    BoxInstallaion = model.BoxInstallaion,
                    IsActive = model.IsActive,
                    MeterShield = model.MeterShield,
                    USence = model.USence,
                    TechnologiesUsed = model.TechnologyUsed != null ? model.TechnologyUsed.Select(t =>
                    new ElectricityMeterTechnology
                    {
                        TechnologyUsedId = t
                    }).ToList() : null
                };
            }

            return null;
        }

        public static ElectricityMeterModel FromDatabaseEntity(this ElectricityMeter entity)
        {
            if (entity != null)
            {
                return new ElectricityMeterModel
                {
                    ElectricityMeterCode = entity.ElectricityMeterCode,
                    PipesFillingSupplierName = entity.PipesFillingSupplierName,
                    USenceSupplierName = entity.USenceSupplierName,
                    ActivationDate = Convert.ToString(entity.ActivationDate),
                    ElectricityMeterDistribution = entity.ElectricityMeterDistributionId,
                    ElectricityMeterSubType = entity.ElectricityMeterSubTypeId,
                    ElectricityMeterPaymentSubType = entity.ElectricityMeterPaymentSubTypeId,
                    ElectricityMeterId = entity.ElectricityMeterId,
                    ElectricityMeterType = entity.ElectricityMeterType,
                    EstimatePowerConsumption = entity.EstimatePowerConsumption,
                    PipesFilling = entity.PipesFilling,
                    ElectricityMeterSiteId = entity.SiteId,
                    BoxInstallaion = entity.BoxInstallaion,
                    IsActive = entity.IsActive,
                    MeterShield = entity.MeterShield,
                    USence = entity.USence,
                    TechnologyUsed = entity.TechnologiesUsed != null ? entity.TechnologiesUsed.Select(t => t.TechnologyUsedId).ToList() : null
                };
            }

            return null;
        }
    }
}