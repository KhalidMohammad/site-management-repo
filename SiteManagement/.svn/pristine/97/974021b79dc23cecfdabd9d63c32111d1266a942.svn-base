﻿@using SiteManagement.Data;
@using SiteManagement.Helpers;
@{
    ViewBag.Title = "Map";
    Layout = "~/Views/Shared/_MasterLayout.cshtml";
}

<div class="maps-main-div">
    <!--Add Site Button-->
    <div class="site-buttons-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="container">
            <a href="#search-collapse" class="btn btn-default btn-sm show-filter-btn" data-toggle="collapse" aria-expanded="true" aria-controls="search-collapse">
                Search Filter
            </a>

            <div id="addSite" class="umniah-btn circle-icon-container add-site-icon-container" data-toggle="tooltip" data-placement="left" title="Add Site">
                <div class="circle-icon add-site-icon">
                    <i>+</i>
                </div>
            </div>
            @if ((RoleTypeEnum)SessionManager.CurrentUser.RoleId == RoleTypeEnum.SuperAdmin)
            {
                <div id="importFile" class="umniah-btn circle-icon-container import-file-icon-container" data-toggle="tooltip" data-placement="left" title="Import File">
                    <div class="circle-icon import-file-icon">
                        <i class="fa fa-file-text-o"></i><br />
                        <i class="fa fa-upload"></i>
                    </div>
                </div>

                <div id="theftReport" class="umniah-btn circle-icon-container theft-report-icon-container" data-toggle="tooltip" data-placement="left" title="Theft Report">
                    <div class="circle-icon theft-report-icon">
                        <i class="fa fa-bar-chart"></i>
                    </div>
                </div>
            }
        </div>
    </div>

    <!--Filters-->
    <div id="search-collapse" class="search collapse in map-filter-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="container">

            <div class="search-collapse">

            </div>
            <div id="mapFilterFormContainer" class="map-filter-form-container row">
                @Html.Partial("~/Views/Site/_PartialSiteFilter.cshtml")
            </div>

            <div id="filterButtons" class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <a id="markerInfo" data-toggle="popover" data-trigger="hover" title="Marker Info">
                        <img src="@Url.Content("~/images/marker-info.png")">
                    </a>
                    <button type="button" id="btnFilterMap" class="btn btn-primary">Search</button>
                    <button type="button" id="btnExportFilterMap" class="btn btn-primary">Export</button>
                    <button type="button" id="btnClearFilterMap" class="btn btn-primary">Clear Filter</button>
                    <button type="button" id="btnAdvanceFilterMap" class="btn btn-primary">Advance Search</button>
                    @*<span class="glyphicon glyphicon-info-sign"></span>*@
                    <span><b>Number of Results: </b><span id="numberOfResults"></span></span>
                </div>
            </div>
        </div>
    </div>

    <div id="mapsContainer" class="maps-container col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @Html.Partial("~/Views/Site/_PartialMap.cshtml")
    </div>
</div>

<script>
    @Html.JsMinify(@<text>

    $('[data-toggle="tooltip"]').tooltip();

    /////////////////////////////////////////////////
    //****************** Marker Info ****************

    $('#markerInfo').popover({
        html: true,
        content: function () {
            var activeImage = $("<img>")
                                    .addClass("marker-info-img") // Add css style for this class
                                    .attr("src", "@SiteMarkerType.Blue_A.ToMapLocationPath()");

            var inActiveImage = $("<img>")
                                    .addClass("marker-info-img") // Add css style for this class
                                    .attr("src", "@SiteMarkerType.Red_A.ToMapLocationPath()");

            return $("<div>")
                        .append(activeImage).append("Active Site")
                        .append("<br/>")
                        .append(inActiveImage).append("Inactive Site");
        }
    });

    /////////////////////////////////////////////////
    //**************** Shared function **************

    function onSiteModalClosing() {
        // if the site is created or updated
        if (isDirty) {
            getMap();
        }
    };

    /////////////////////////////////////////////////
    //******************* Add Site ******************

    // On add site button click
    $("#addSite").click(function () {
        // This method will open a popup with partial site view
        $.ajax({
            type: 'post',
            url: '@Url.Action("NewSite", "Site")',
            data: null,
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        ModalManager.showModal(data.Content, 'New Site', null, {
                            top: 10,
                            bottom: 10,
                            fullscreen: true, onClosing:
                                function () {
                                    onSiteModalClosing();
                                }
                        });
                        break;
                    case 2:
                        ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    });

    /////////////////////////////////////////////////
    //****************** Edit Site ******************

    // On map location edit button click
    function editSite(siteId) {
        // This method will open a popup with partial site view filled with data
        $.ajax({
            type: 'post',
            url: '@Url.Action("GetSiteById", "Site")',
            data: { siteId: siteId },
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        var title = data.SiteName || 'New Site';
                        ModalManager.showModal(data.Content, title, null, {
                            top: 10,
                            bottom: 10,
                            fullscreen: true, onClosing:
                                    function () {
                                        onSiteModalClosing();
                                    }
                        });
                        break;
                    case 2:
                        ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    };

    /////////////////////////////////////////////////

    @if ((RoleTypeEnum)SessionManager.CurrentUser.RoleId == RoleTypeEnum.SuperAdmin)
    {
        <text>

    ////////////////////////////////////////
    //************ Import File *************

    $("#importFile").click(function () {
        $.ajax({
            type: 'post',
            url: '@Url.Action("Index", "ImportFile")',
            data: null,
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        var container = $("<div>").append("<br />").append(data.Content);
                        // Cache this modal on window to close it when import file on server side
                        importFileModal = ModalManager.showModal(container, 'Import File');
                        break;
                    case 2:
                        ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    });

    /////////////////////////////////////////
    //************* Theft Report ************

    $("#theftReport").click(function () {
        $.ajax({
            type: 'post',
            url: '@Url.Action("Index", "Report")',
            data: null,
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        var container = $("<div>").append("<br />").append(data.Content);
                        ModalManager.showModal(container, 'Theft Report');
                        break;
                    case 2:
                        ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    });

    </text>
    }

    /////////////////////////////////////////////////
    //************* Map Locations Filter ************

    var isFilterMapDirty = false;

    $("#btnFilterMap").click(function () {
        isFilterMapDirty = true;
        getMap();
    });

    $("#btnExportFilterMap").click(function () {
        isFilterMapDirty = true;
        exportMap();
    });

    $("#btnClearFilterMap").click(function () {
        $('#frmFilterMap')[0].reset();
        if (isFilterMapDirty) {
            isFilterMapDirty = false;
            getMap();
        }
    });

    function getMap() {
        if (isFilterMapDirty) {
            getFilterMapLocations();
        }
        else {
            getAllMapLocations();
        }
    };

    function getFilterMapLocations() {
        var formData = $("#frmFilterMap").serializeArray();

        $.ajax({
            type: 'post',
            url: '@Url.Action("GetFilterMapLocations", "Site")',
            data: formData,
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        if (data.IsHaveResult) {
                            $("#mapsContainer").html(data.Content);
                        }
                        else {
                            ModalManager.showWarningModal("There no locations match with this criteria");
                        }
                        break;
                    case 2:
                        ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    };

    function exportMap() {
        var formData = $("#frmFilterMap").serializeArray();

        $.ajax({
            type: 'post',
            url: '@Url.Action("ExportFilterMapLocations", "Site")',
            data: formData,
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        window.location = data.Url;
                        break;
                    case 2:
                        ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    };

    function getAllMapLocations() {
        $.ajax({
            type: 'post',
            url: '@Url.Action("GetAllMapLocations", "Site")',
            data: null,
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        $("#mapsContainer").html(data.Content);
                        break;
                    case 2:
                        ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    };

    $("#btnAdvanceFilterMap").click(function () {
        $.ajax({
            type: 'post',
            url: '@Url.Action("GetAdvanceFilterView", "Site")',
            data: null,
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        // Cache advance filter modal on window to close it after any search
                        advanceFilterModal = ModalManager.showModal(data.Content, 'Advance Filter');
                        //advanceFilterModal = ModalManager.showConfirmModal(data.Content, 'Advance Filter');
                        //advanceFilterModal = ModalManager.showModal("test", 'Advance Filter');
                        break;
                    case 2:
                        ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    });


    </text>)
</script>
