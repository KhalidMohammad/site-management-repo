﻿(function () {
    this.Ui = this.Ui || {};

    var ns = this.Ui;

    /***************************************************************************************************/
    /**************************************** Upload Picture File **************************************/

    /*
        options: { 
                    id:.., 
                    maximumFileSize: 524000000, 
                    file: { fileID:.., name:.., path:.., content:.. }, 
                    accept: "file_extension,audio/*,video/*,image/*,media_type", 
                    downloadUrl:.., 
                    hiddenField:.., 
                    isReadOnly: true/false, 
                }

        accept: "file_extension,audio/*,video/*,image/*,media_type"
            file_extension: A file extension starting with the STOP character, e.g: .gif, .jpg, .png, .doc
            media_type: A valid media type, with no parameters. Look at IANA Media Types for a complete list of standard media types

        IMPORTANT NOTE: (The needed js files to import before use this class)
          ajaxManager.js
          CommonHelper.js
          ModalHelper.js

    */
    ns.UploadPictureFile = (function (parent) {
        UploadPictureFile.prototype = new ns.UploadBase();
        UploadPictureFile.prototype.constructor = UploadPictureFile;

        function UploadPictureFile(options) {
            options.localizer = {
                BROWSER_UPLOAD_SUPPORT_MESSAGE: "Can\'t upload files in this browser",
                REMOVE_FILE_CONFIRMATION_MESSAGE: "Are you sure you want to remove the file",
                //WARNING: "Warning",
                EMPTY_FILE_VALIDATION_MESSAGE: "Empty file is not accepted",
                FILE_SIZE_VALIDATION_MESSAGE_1: "This file is too large, the maximum size is",
                FILE_SIZE_VALIDATION_MESSAGE_2: "you must select a smaller file",
                FILE_FORMAT_VALIDATION_MESSAGE: "File format is not supported",
                WAITING_READ_FILE_MESSAGE: "Please wait while read the file",
                //UPLOAD_ERROR_MESSAGE: "Error while uploading the file",
                UPLOAD: "Upload",
                CHANGE_PICTURE: "Change Picure"
            };

            this.fileBlock = $("<div>").addClass("upload-picture-file-block");

            this.uploadPictureContainer = $("<div>").addClass("upload-picture-container");

            this.pictureContainer = $("<div>")
                                        .addClass("picture-container")
                                        .addClass("hidden-container");

            this.picture = $("<img>")
                                .addClass("picture-image")
                                .addClass("img-responsive");            

            parent.call(this, options);
        }

        var setValue = function (file) {
            var me = this;

            if (file.content || file.thumbnail) {
                this.picture.attr("src", file.content || file.thumbnail);
                //this.fileName.html(file.name);

                showFile.call(this);
            }            
        };

        var showFile = function () {
            this.uploadPictureContainer.addClass("hidden-container");
            this.pictureContainer.removeClass("hidden-container");
        };

        var hideFile = function () {
            this.uploadPictureContainer.removeClass("hidden-container");
            this.pictureContainer.addClass("hidden-container");

        };

        var onValidationSuccess = function () {
            this.fileBlock.removeClass("control-validation-error");
        };

        var onValidationFail = function () {
            this.fileBlock.addClass("control-validation-error");
        };

        //#region ___________________________ Overridden Methods ___________________________

        UploadPictureFile.prototype.onBeforeUploadClick = function () {
            this.uploadButton.addClass("disabled");
        };

        UploadPictureFile.prototype.onAfterUploadClick = function () {
            this.uploadButton.removeClass("disabled");
        };

        UploadPictureFile.prototype.renderFile = function (file) {
            setValue.call(this, file);
        };

        UploadPictureFile.prototype.showValidationErrorMessage = function (errorMessage) {
            showLocalValidation.call(this, errorMessage);
        };

        UploadPictureFile.prototype.renderFileWithProgress = function (file) {
            setValue.call(this, file);

            hideLocalValidation.call(this);
        };

        UploadPictureFile.prototype.onAfterChange = function () {
            if (this.hiddenField) {
                var hiddenFiles = this.getFiles();

                var hiddenFile = hiddenFiles[hiddenFiles.length - 1];

                this.hiddenField.val(JSON.stringify(hiddenFile));
                this.hiddenField.valid();
            }
        };

        UploadPictureFile.prototype.removeFileFromUi = function () {
            this.picture.attr("src", "");
            //this.fileName.html("");
            hideFile.call(this);
        };

        UploadPictureFile.prototype.previewFile = function (fileContent) {

        };

        //#endregion ___________________________ Overridden Methods ___________________________


        //#region ___________________________ File Local Validation ___________________________

        var createLocalValidationInput = function (controlContainer) {
            this.localValidationField = $("<span>")
                                            .addClass("text-danger")
                                            .css("display", "none");

            controlContainer.append(this.localValidationField);
        };

        var showLocalValidation = function (validationMessage) {
            this.localValidationField.empty();

            this.localValidationField
                        .css("display", "block")
                        .append(validationMessage);

        };

        var hideLocalValidation = function () {
            this.localValidationField.css("display", "none");
        };

        //#endregion ___________________________ File Local Validation ___________________________

        //#region ___________________________ File Block Creation ___________________________

        var createFileBlock = function (container) {
            var me = this;

            container.append(this.fileBlock);

            // 1. Render the file input field (this field will be hidden on the screen)
            container.append(this.uploadField);

            // 2. Create upload picture container
            createUploadPictureContainer.call(this, container);

            // 3. Create picture container
            createPictureContainer.call(this, container);
        };

        var createUploadPictureContainer = function (container) {
            container.append(this.uploadPictureContainer);
            
            var uploadPictureWrapper = $("<div>").addClass("upload-picture-wrapper");

            this.uploadButton = $("<button type='button'>")
                                    .addClass("upload-picture-button")
                                    .addClass("btn btn-primary")
                                    .html(this.options.localizer["UPLOAD"]);
            
            // When user click on our customize button, internally the method fires the click event of uploadField control
            var me = this;
            this.uploadButton.click(function () {
                me.onUploadFileButtonClick(); // fire upload base method
            });

            uploadPictureWrapper.append(this.uploadButton);

            this.uploadPictureContainer.append(uploadPictureWrapper);
        };

        var createPictureContainer = function (container) {
            var me = this;

            container.append(this.pictureContainer);

            var pictureWrapper = $("<div>").addClass("picture-wrapper");

            this.pictureContainer.append(pictureWrapper);

            pictureWrapper.append(this.picture);

            var editPictureMenu = $("<div>").addClass("upload-picture-menu");

            // Create download icon
            var isDownloadable = this.options.downloadUrl && this.options.file.path;
            if (isDownloadable) {
                var downloadIcon = $("<span>")
                                        .addClass("upload-picture-download-file-icon")
                                        .addClass("fa fa-download");

                downloadIcon.click(function () {
                    var file = me.options.files[me.options.files.length - 1];
                    me.onDownloadFileClick(file);
                });

                editPictureMenu.append(downloadIcon);
            }

            if (!this.options.isReadOnly) {
                var removeFileIcon = $("<span>")
                                            .addClass("upload-picture-remove-file-icon")
                                            .addClass("fa fa-trash-o")
                                            .click(function () {
                                                var file = me.options.files[me.options.files.length - 1];
                                                me.onRemoveFileClick(file, function () {
                                                    me.removeFileFromUi();
                                                });
                                            });

                editPictureMenu.append(removeFileIcon);
                
                var changePicture = $("<div>")
                                        .addClass("upload-picture-change-picture-link")
                                        .append(this.options.localizer["CHANGE_PICTURE"])
                                        .click(function () {
                                            me.uploadButton.click();
                                        });

                editPictureMenu.append(changePicture);
            }

            pictureWrapper.append(editPictureMenu);

            this.pictureContainer.hover(function () {
                editPictureMenu.slideDown();
            }, function () {
                editPictureMenu.slideUp();
            });
        };

        //#endregion ___________________________ File Block Creation ___________________________

        UploadPictureFile.prototype.buildControl = function (controlContainer) {
            var container = $("<div>")
                                .addClass("upload-picture-file-container");

            // 1. Create file block
            createFileBlock.call(this, container);
            controlContainer.append(container);

            // 2. Create local hidden field
            createLocalValidationInput.call(this, controlContainer);

            // 3. Create hidden field
            if (this.options.hiddenField) {
                this.hiddenField = $(this.options.hiddenField);
                controlContainer.append(this.hiddenField);

                var me = this;
                this.hiddenField.bind("onSuccess", function () {
                    onValidationSuccess.call(me);
                });

                this.hiddenField.bind("onError", function () {
                    onValidationFail.call(me);
                });
            }

            if (this.options.file && Object.keys(this.options.file).length) {
                this.setFiles();
            }
        };

        return UploadPictureFile;
    })(ns.UploadBase);
})();
