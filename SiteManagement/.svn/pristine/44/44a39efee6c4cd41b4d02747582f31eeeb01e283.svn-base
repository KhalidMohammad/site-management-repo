﻿using Newtonsoft.Json;
using SiteManagement.Attributes.Authorization;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.Admin, RoleTypeEnum.SuperAdmin, RoleTypeEnum.Guest)]
    public class ContractController : AuthenticatedController
    {
        [HttpPost]
        public ActionResult GetSiteContracts(int siteId)
        {
            ActionResult result = null;

            try
            {
                ContractModel contractModel = new ContractModel();

                List<ContractModel> siteContractModels = new List<ContractModel>();

                SiteDataManager.CallDBContext(db =>
                {
                    List<Contract> contracts = db.Contracts.Where(o => o.SiteId == siteId && !o.FlagDeleted).ToList();
                    if (contracts != null)
                    {
                        siteContractModels = contracts.Select(o => o.FromDatabaseEntity()).ToList();
                    }
                });

                ViewData["PaymentTypes"] = GetPaymentTypes();
                ViewData["Acquiredby"] = GetAcquiredby();

                ViewData["contractGridData"] = JsonConvert.SerializeObject(siteContractModels);

                result = Json(new
                {
                    AddContractContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/AddContract.cshtml", contractModel),
                    ContractGridContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/ContractGrid.cshtml", null),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult SaveContract(ContractModel model)
        {
            ActionResult result = null;

            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    Contract contract = ContractConverter.ToDatabaseEntity(model);
                    //contract.ApprovlStatusId = SiteManager.GetInsertStatusId();
                    SiteManager.FillApprovalStatus(contract, contract.Site);
                    if (model.ContractId == 0)
                    {
                        FillEntitySavedData(contract, false);
                        model.ContractId = ContractManager.AddContract(contract);
                    }
                    else
                    {
                        if (contract.ApprovlStatusId != (int)ApprovalStatus.Approved)
                        {
                            FillEntitySavedData(contract, false);
                            contract.OriginalId = contract.ContractId;
                            contract.ContractId = 0;
                            model.ContractId = ContractManager.AddContract(contract);
                        }
                        else
                        {
                            FillEntitySavedData(contract, true);
                            ContractManager.UpdateContract(contract);
                        }
                    }

                    // Used only in contract grid
                    model.StartDate = Convert.ToString(model.Date.Start);
                    // Used only in contract grid
                    model.EndDate = Convert.ToString(model.Date.End);

                    string contractJsonData = JsonConvert.SerializeObject(model);

                    result = Json(new
                    {
                        ContractJsonData = contractJsonData,
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks on cancel button of edit contract
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAddContractView()
        {
            ActionResult result = null;

            try
            {
                ViewData["PaymentTypes"] = GetPaymentTypes();

                ContractModel model = new ContractModel();

                result = Json(new
                {
                    AddContractContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/AddContract.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks edit button on contracts grid
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetEditContractView(int contractId)
        {
            ActionResult result = null;

            try
            {
                ViewData["PaymentTypes"] = GetPaymentTypes();

                ContractModel model = new ContractModel();

                SiteDataManager.CallDBContext(db =>
                {
                    Contract contract = db.Contracts.Where(o => o.ContractId == contractId).FirstOrDefault();
                    if (contract != null)
                    {
                        model = contract.FromDatabaseEntity();
                    }
                });

                result = Json(new
                {
                    EditContractContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/EditContract.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks on delete button on contracts grid
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteContract(int contractId)
        {
            ActionResult result = null;

            try
            {
                SiteDataManager.CallDBContext(db =>
                {
                    Contract oldContract = db.Contracts.Find(contractId);
                    oldContract.FlagDeleted = true;
                    db.SaveChanges();
                });

                result = Json(new { MessageType = MessageType.Success }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult GetFilterMapLocations(ContractFilterModel contractFilterModel)
        {
            ActionResult result = null;

            try
            {
                List<MapModel> list = MapSearchCriteria.GetContractSearch(contractFilterModel);

                ViewData["mapList"] = JsonConvert.SerializeObject(list);

                result = Json(new
                {
                    IsHaveResult = list.Any(),
                    Content = list.Any() ? UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Site/_PartialMap.cshtml", null) : "",
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult ExportFilterMapLocations(ContractFilterModel contractFilterModel)
        {
            ActionResult result = null;

            try
            {
                byte[] fileContent = MapSearchCriteria.ExportContractSearch(contractFilterModel);
                TempData["ExportSheetResult"] = fileContent;

                result = Json(new
                {
                    Url = Url.Action("DownloadExportSheet"),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpGet]
        public FileResult DownloadExportSheet()
        {
            byte[] fileContent = TempData["ExportSheetResult"] as byte[];
            return File(fileContent, System.Net.Mime.MediaTypeNames.Application.Octet, "Filter Map By Contract.xlsx");
        }

        #region Private Methods

        private IEnumerable<SelectListItem> GetPaymentTypes()
        {
            return GetLookupSelectedItems(LookupTypeEnum.PaymentType);
        }

        private IEnumerable<SelectListItem> GetAcquiredby()
        {
            return GetLookupSelectedItems(LookupTypeEnum.Acquiredby);
        }
        #endregion Private Methods
    }
}