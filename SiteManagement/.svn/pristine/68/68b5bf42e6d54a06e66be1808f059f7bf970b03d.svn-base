﻿@using SiteManagement.Models;
@using SiteManagement.Data;
@model ElectricityMeterModel

<div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading" href="#editElectricityMeterCollapse">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#editElectricityMeterCollapse">Edit Electricity Meter</a>
                </h4>
            </div>
            <div id="editElectricityMeterCollapse" class="panel-collapse collapse in">
                <div class="panel-body collapse-body">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @Html.Partial("~/Views/ElectricityMeter/_PartialElectricityMeter.cshtml", Model)
                    </div>
                </div>
                <br />
                <div id="electricityMeterButtons" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button type="button" id="btnEditElectricityMeter" class="btn btn-primary">Edit</button>
                    <button type="button" id="btnCancelEditElectricityMeter" class="btn btn-primary">Cancel</button>
                    @if ((RoleTypeEnum)SessionManager.CurrentUser.RoleId == RoleTypeEnum.SuperAdmin)
                    {
                        <button type="button" id="btnShowElectricityMeterLog" class="btn btn-primary">Show Log</button>
                    }
                </div>
                <br />
            </div>
        </div>
    </div>
</div>

<script>
    @Html.JsMinify(@<text>

    $("#btnEditElectricityMeter").click(function () {
        onEditElectricityMeterButtonClick();
    });

    $("#btnCancelEditElectricityMeter").click(function () {
        onCancelEditElectricityMeterButtonClick();
    });

    // After edit an exist load add electricity meter view
    function onEditElectricityMeterButtonClick() {
        var isFormValid = $("#frmElectricityMeter").valid();

        if (!isFormValid) {
            // Show an error message
            ModalManager.showErrorModal("Please fill the required fields");
        } else {
            // Show edit electricity meter confirm message
            ModalManager.showConfirmModal("Are you sure you want to edit this electricity meter?", null, function (modal) {
                // For the first time, get siteId from site form and set it to electricity meter form
                var siteId = $("#hdnSiteId").val();
                $("#hdnElectricityMeterSiteId").val(siteId);

                $("#estimatePowerConsumption").removeAttr("disabled");
                var formData = $("#frmElectricityMeter").serializeArray();
                $("#estimatePowerConsumption").attr("disabled", "disabled");

                $.ajax({
                    type: 'post',
                    url: '@Url.Action("SaveElectricityMeter", "ElectricityMeter")',
                    data: formData,
                    success:
                        function (data) {
                            switch (data.MessageType) {
                                case 1:
                                    // Edit the updated electricity meter to electricity meter grid
                                    editElectricityMeterGridRow(JSON.parse(data.ElectricityMeterJsonData));

                                    // Enable all grid buttons
                                    enableElectricityMeterGridButtons(true);

                                    // After edit the exist electricity meter, load the add electricity meter view (change to add electricity meter mode)
                                    GetAddElectricityMeterView();

                                    // Show successfully updated message
                                    ModalManager.showInformationModal("The electricity meter successfully updated");
                                    break;
                                case 2:
                                    ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                                    break;
                                case 3:
                                    // Show server validation errors
                                    var validator = $("#frmElectricityMeter").data("validator");
                                    var errors = $.parseJSON(data.errors);
                                    validator.showErrors(errors);
                                    break;
                            }
                        },
                    error: function (data) {
                    },
                    beforeSend: function () {
                        $('body').jLoadingOverlay('');
                    },
                    complete: function () {
                        $('body').jLoadingOverlay('close');
                    }
                });

                modal.close();
            });
        }
    };

    function onCancelEditElectricityMeterButtonClick() {
        GetAddElectricityMeterView();
    };

    function GetAddElectricityMeterView() {
        $.ajax({
            type: 'post',
            url: '@Url.Action("GetAddElectricityMeterView", "ElectricityMeter")',
            data: null,
            success:
                function (data) {
                    switch (data.MessageType) {
                        case 1:
                            // Load the add electricity meter view (change to add electricity meter mode)
                            $("#electricityMeterContent").html(data.AddElectricityMeterContent);

                            // Enable all grid buttons
                            enableElectricityMeterGridButtons(true);
                            break;
                        case 2:
                            ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                            break;
                    }
                },
            error: function (data) {
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    };

    ////////////////////////////////////////////////////////////
    //******************* Electricity Meter ********************

    @if ((RoleTypeEnum)SessionManager.CurrentUser.RoleId == RoleTypeEnum.SuperAdmin)
    {
        <text>
    $("#btnShowElectricityMeterLog").click(function () {
        $.ajax({
            type: 'post',
            url: '@Url.Action("GetLog", "Log")',
            data: { logType: @((int)LogType.ElectricityMeter), id: $("#hdnElectricityMeterId").val() },
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        ModalManager.showModal(data.Content, "Electricity Meter Log");
                        break;
                    case 2:
                        ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    });
    </text>
    }

    </text>)
</script>