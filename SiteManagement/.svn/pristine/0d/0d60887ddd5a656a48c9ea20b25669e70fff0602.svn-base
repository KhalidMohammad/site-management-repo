﻿@using SiteManagement.Models;
@using SiteManagement.Data;
@model OwnerModel

<div>
    <div class="panel-group">
        <div class="panel panel-default">
            <div class="panel-heading" href="#editOwnerCollapse">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#editOwnerCollapse">Edit Owner</a>
                </h4>
            </div>
            <div id="editOwnerCollapse" class="panel-collapse collapse in">
                <div class="panel-body collapse-body">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        @Html.Partial("~/Views/Owner/_PartialOwner.cshtml", Model)
                    </div>
                </div>
                <br />
                <div id="ownerButtons" class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <button type="button" id="btnEditOwner" class="btn btn-primary">Edit</button>
                    <button type="button" id="btnCancelEditOwner" class="btn btn-primary">Cancel</button>
                    @if ((RoleTypeEnum)SessionManager.CurrentUser.RoleId == RoleTypeEnum.SuperAdmin)
                    {
                        <button type="button" id="btnShowOwnerLog" class="btn btn-primary">Show Log</button>
                    }
                </div>
                <br />
            </div>
        </div>
    </div>
</div>

<script>
    @Html.JsMinify(@<text>

    $("#btnEditOwner").click(function () {
        onEditOwnerButtonClick();
    });

    $("#btnCancelEditOwner").click(function () {
        onCancelEditOwnerButtonClick();
    });

    // After edit an exist load add owner view
    function onEditOwnerButtonClick() {
        var isFormValid = $("#frmOwner").valid();

        if (!isFormValid) {
            // Show an error message
            ModalHelper.showErrorModal("Please fill the required fields");
        } else {
            // Show edit owner confirm message
            ModalHelper.showConfirmModal("Are you sure you want to edit this owner?", function (modal) {
                // For the first time, get siteId from site form and set it to owner form
                var siteId = $("#hdnSiteId").val();
                $("#hdnOwnerSiteId").val(siteId);

                var formData = $("#frmOwner").serializeArray();

                $.ajax({
                    type: 'post',
                    url: '@Url.Action("SaveOwner", "Owner")',
                    data: formData,
                    success:
                        function (data) {
                            switch (data.MessageType) {
                                case 1:
                                    // Edit the updated owner to owner grid
                                    editOwnerGridRow(JSON.parse(data.OwnerJsonData));

                                    // Enable all grid buttons
                                    enableOwnerGridButtons(true);

                                    // After edit the exist owner, load the add owner view (change to add owner mode)
                                    GetAddOwnerView();

                                    // Show successfully updated message
                                    ModalHelper.showInformationModal("The owner successfully updated");
                                    break;
                                case 2:
                                    ModalHelper.showErrorModal("There is an error accured, please contact the system administrator");
                                    break;
                                case 3:
                                    // Show server validation errors
                                    var validator = $("#frmOwner").data("validator");
                                    var errors = $.parseJSON(data.errors);
                                    validator.showErrors(errors);
                                    break;
                            }
                        },
                    error: function (data) {
                    },
                    beforeSend: function () {
                        $('body').jLoadingOverlay('');
                    },
                    complete: function () {
                        $('body').jLoadingOverlay('close');
                    }
                });

                modal.close();
            });
        }
    };

    function onCancelEditOwnerButtonClick() {
        GetAddOwnerView();
    };

    function GetAddOwnerView() {
        $.ajax({
            type: 'post',
            url: '@Url.Action("GetAddOwnerView", "Owner")',
            data: null,
            success:
                function (data) {
                    switch (data.MessageType) {
                        case 1:
                            // Load the add owner view (change to add owner mode)
                            $("#ownerContent").html(data.AddOwnerContent);

                            // Enable all grid buttons
                            enableOwnerGridButtons(true);
                            break;
                        case 2:
                            ModalHelper.showErrorModal("There is an error accured, please contact the system administrator");
                            break;
                    }
                },
            error: function (data) {
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    };

    ////////////////////////////////////////////////////////////
    //************************ Owner Log ***********************

    @if ((RoleTypeEnum)SessionManager.CurrentUser.RoleId == RoleTypeEnum.SuperAdmin)
    {
        <text>
    $("#btnShowOwnerLog").click(function () {
        $.ajax({
            type: 'post',
            url: '@Url.Action("GetLog", "Log")',
            data: { logType: @((int)LogType.Owner), id: $("#hdnOwnerId").val() },
            success:
            function (data) {
                switch (data.MessageType) {
                    case 1:
                        ModalManager.showModal(data.Content, "Owner Log");
                        break;
                    case 2:
                        ModalHelper.showErrorModal("There is an error accured, please contact the system administrator");
                        break;
                }
            },
            beforeSend: function () {
                $('body').jLoadingOverlay('');
            },
            complete: function () {
                $('body').jLoadingOverlay('close');
            }
        });
    });
    </text>
    }

    </text>)
</script>