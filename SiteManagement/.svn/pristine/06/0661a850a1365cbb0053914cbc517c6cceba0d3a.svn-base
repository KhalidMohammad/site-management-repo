﻿(function () {
    this.Ui = this.Ui || {};

    var ns = this.Ui;

    /***************************************************************************************************/
    /****************************************** Editable Select2 ***************************************/

    /*
    options: { 
                selectInput:.., 
                lookupType:.., 
                dropdownOptions:.., 
                value:.., 
                placeholder:.., 
                isMultiple: true/false, 
                allowClear: true/false, 
                isReadOnly: true/false, 
                dir: "ltr"/"rtl",
                onChangeCallback:.., 
                onAddNewOptionUrl:.., 
                onEditOptionUrl:..,
            }
    */
    ns.EditableSelect2 = (function () {
        function EditableSelect2(options) {
            this.options = options;
            this.options.dir = this.options.dir || "ltr";

            if (this.options.isMultiple) {
                this.options.isMultiple = this.options.isMultiple.toString().toLowerCase() == "true";
            }

            if (this.options.allowClear) {
                this.options.allowClear = this.options.allowClear.toString().toLowerCase() == "true";
            }

            if (this.options.isReadOnly) {
                this.options.isReadOnly = this.options.isReadOnly.toString().toLowerCase() == "true";
            }

            if (this.options.dropdownOptions) {
                if (typeof (this.options.dropdownOptions) == "string") {
                    this.options.dropdownOptions = JSON.parse(this.options.dropdownOptions);
                }
            }

            this.container = $("<div>")
                                .addClass("select2-edit-container")
                                .addClass("editable-select2-container")
                                .addClass("col-lg-10 col-md-10 col-sm-10 col-xs-10");

            this.select = $(this.options.selectInput)
                                    .addClass("editable-select2")
                                    .addClass('ui-control-select2')
                                    .addClass("form-control");

            this.select2Options = {
                placeholder: this.options.placeholder,
                minimumResultsForSearch: 1,
                multiple: this.options.isMultiple,
                allowClear: this.options.isMultiple ? false : this.options.allowClear,
                disabled: this.options.isReadOnly,
                dir: this.options.dir,
                dropdownAutoWidth: true,
                width: '100%',
                theme: 'bootstrap',
                tags: false, //Important Note: Prevent select2 from dynamically create new options from text input by the user in the search box.                
            };

            this.isOptionsDirty = false;
        }

        var onChangeCallback = function (input) {
            var me = this;

            $(document).ready(function () {
                if (me.options.onChangeCallback) {
                    if (me.options.onChangeCallback instanceof Function) {
                        me.options.onChangeCallback(input);
                    }
                    else {
                        window[me.options.onChangeCallback](input);
                    }
                }
            });
        };

        var fixMultipleSelect2 = function () {
            // Fix bug in select2 when multiple option is true and it has a placeholder.
            // Bug Description: For the first time of selecting an item, there are two tags drawn - the text of selected value in addition to the text of placeholder - 
            // because select2 considers the placeholder as a previous selected value. 
            // Workaround Fix: The placeholder option value is removed then the changed event explicitly triggered to set select2 value with the user selected one.
            var values = this.select.val();
            if (values && values.length > 0) {
                if (!values[0]) {
                    values.shift();
                    this.select.val(values).trigger('change');
                }
            }
        };

        var createSelect2 = function () {
            this.select2Options.allowClear = this.select2Options.multiple ? false : this.options.allowClear;

            this.container.append(this.select);

            this.select.select2(this.select2Options);

            if (this.select2Options.multiple) {
                fixMultipleSelect2.call(this);
            }
        };

        var createAddNewOptionButton = function (controlContainer) {
            var me = this;

            var addNewOptionBtnContainer = $("<div>")
                                                .addClass("editable-select2-button-container")
                                                .addClass("col-lg-2 col-md-2 col-sm-2 col-xs-2");
            // Create add option button
            var addNewOptionBtn = $("<button type='button'>")
                                    .addClass("btn btn-primary")
                                    .addClass("editable-select2-button")
                                    .text("+")
                                    .click(function () {
                                        onAddNewOptionBtnClick.call(me);
                                    });

            addNewOptionBtnContainer.append(addNewOptionBtn);
            controlContainer.append(addNewOptionBtnContainer);
        };

        var registerEvents = function () {
            var me = this;

            this.select.change(function () {
                onChangeCallback.call(me, this);
                // Check if state of document to avoid call valid() method for the first time on setValue,
                //   if we don't set this check and there is a value for the first time so this will cause to stop the whole form validation.
                if (document.readyState === 'complete') {
                    $(this).valid();
                }
            });
        };

        var onAddOptionBtnClick = function (text) {
            var me = this;

            if (text) {
                $.ajax({
                    type: 'post',
                    url: this.options.onAddNewOptionUrl,
                    data: { lookupType: this.options.lookupType, text: text },
                    success:
                        function (data) {
                            switch (data.MessageType) {
                                case 1:
                                    me.isOptionsDirty = true;
                                    var newOption = { id: data.optionId, text: text };
                                    me.options.dropdownOptions.push(newOption);
                                    break;
                                case 2:
                                    ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                                    break;
                            }
                        },
                    error: function (data) {
                    },
                    beforeSend: function () {
                        $('body').jLoadingOverlay('');
                    },
                    complete: function () {
                        $('body').jLoadingOverlay('close');
                    }
                });
            }
        };

        var onEditOptionBtnClick = function (id, text) {
            var me = this;

            $.ajax({
                type: 'post',
                url: this.options.onEditOptionUrl,
                data: { lookupType: this.options.lookupType, id: id, text: text },
                success:
                    function (data) {
                        switch (data.MessageType) {
                            case 1:
                                me.isOptionsDirty = true;
                                for (var i = 0; i < me.options.dropdownOptions.length; i++) {
                                    if (me.options.dropdownOptions[i].id == id) {
                                        me.options.dropdownOptions[i].text = text;
                                        break;
                                    }
                                }
                                break;
                            case 2:
                                ModalManager.showErrorModal("There is an error accured, please contact the system administrator");
                                break;
                        }
                    },
                error: function (data) {
                },
                beforeSend: function () {
                    $('body').jLoadingOverlay('');
                },
                complete: function () {
                    $('body').jLoadingOverlay('close');
                }
            });
        };

        var onAddNewOptionBtnClick = function () {
            var me = this;

            var addEditOptionsContainer = $("<div>");

            // Create a div that contains all options
            var optionsContainer = $("<div>").addClass("row");

            var bindEditOptionBtnClick = function (id, optionEditButton, optionInputText) {
                optionEditButton.click(function () {
                    var text = optionInputText.val();
                    onEditOptionBtnClick.call(me, id, text);
                });
            };

            for (var i = 0; i < this.options.dropdownOptions.length; i++) {
                var dropdownOption = this.options.dropdownOptions[i];

                var optionContainer = $("<div>").addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12");

                // Create input text
                var optionInputTextContainer = $("<div>").addClass("col-lg-10 col-md-10 col-sm-10 col-xs-10");
                var optionInputText = $("<input type='text'>").addClass("form-control").val(dropdownOption.text);
                optionInputTextContainer.append(optionInputText);

                // Create Edit button
                var optionEditButtonContainer = $("<div>").addClass("col-lg-2 col-md-2 col-sm-2 col-xs-2");
                var optionEditButton = $("<button type='button'>").addClass("btn btn-primary").text("Edit");
                optionEditButtonContainer.append(optionEditButton);

                // Bind Edit button click
                bindEditOptionBtnClick.call(this, dropdownOption.id, optionEditButton, optionInputText);

                optionContainer.append(optionInputTextContainer).append(optionEditButtonContainer);

                optionsContainer.append(optionContainer).append("<br /><br />");
            }

            // Create add new option button
            var addNewOptionButtonContainer = $("<div>").addClass("row").addClass("add-new-option-inner-btn");

            var addNewOptionButton = $("<button type='button'>")
                                        .addClass("btn btn-primary")
                                        .text("+")
                                        .click(function () {
                                            var optionContainer = $("<div>").addClass("col-lg-12 col-md-12 col-sm-12 col-xs-12");

                                            // Create input text
                                            var optionInputTextContainer = $("<div>").addClass("col-lg-10 col-md-10 col-sm-10 col-xs-10");
                                            var optionInputText = $("<input type='text'>").addClass("form-control");
                                            optionInputTextContainer.append(optionInputText);

                                            // Create Add button
                                            var optionEditButtonContainer = $("<div>").addClass("col-lg-2 col-md-2 col-sm-2 col-xs-2");
                                            var optionAddButton = $("<button type='button'>")
                                                                        .addClass("btn btn-primary")
                                                                        .text("Add")
                                                                        .click(function () {
                                                                            onAddOptionBtnClick.call(me, optionInputText.val());
                                                                        });

                                            optionEditButtonContainer.append(optionAddButton);

                                            optionContainer.append(optionInputTextContainer).append(optionEditButtonContainer);

                                            optionsContainer.append(optionContainer).append("<br /><br />");
                                        });

            addNewOptionButtonContainer.append(addNewOptionButton);

            addEditOptionsContainer.append(optionsContainer).append("<br />").append(addNewOptionButtonContainer);

            var closeModalCallback = function (model) {
                refreshDropdown.call(me);
                model.close();
            };

            ModalManager.showModal(addEditOptionsContainer, "Add New Lookup", [
                { label: "Close", onClick: closeModalCallback }
            ], { onModalCloseButtonClick: closeModalCallback });
        };

        var refreshDropdown = function () {
            if (this.isOptionsDirty) {
                this.isOptionsDirty = false;
                this.reBuildControl({ data: this.options.dropdownOptions });
            }
        };

        EditableSelect2.prototype.buildControl = function (controlContainer) {
            controlContainer.append(this.container);

            createSelect2.call(this);

            if (this.options.value) {
                this.setValue(this.options.value);
            }

            this.select.bind("onSuccess", function () {
                $(this).removeClass('control-validation-error');
            });

            this.select.bind("onError", function () {
                $(this).addClass('control-validation-error');
            });

            registerEvents.call(this);

            createAddNewOptionButton.call(this, controlContainer);
        };

        EditableSelect2.prototype.reBuildControl = function (select2Options) {
            this.container.empty();
            this.select.unbind("change").empty();

            // Set the value to default before re-create a new control
            var emptyValue = this.select2Options.multiple ? [] : "";
            this.select.val(emptyValue);

            this.select2Options = $.extend(this.select2Options, select2Options);
            this.select2Options['allowClear'] = true;
            createSelect2.call(this);

            registerEvents.call(this);
        };

        EditableSelect2.prototype.isMultiple = function () {
            return this.select2Options.multiple;
        };

        EditableSelect2.prototype.getValue = function () {
            return this.select.val();
        };

        EditableSelect2.prototype.getValueArray = function () {
            var value = this.select.val();

            if (!this.select2Options.multiple) {
                value = value ? [value] : [];
            }

            return value;
        };

        EditableSelect2.prototype.setValue = function (value) {
            var isArray = value.constructor === Array;

            if (isArray && value.length == 0) {
                return false;
            }

            var isMultiple = this.select2Options.multiple;

            var selectedValue;
            if (isMultiple && !isArray) {
                selectedValue = [value];
            }
            else if (!isMultiple && isArray) {
                selectedValue = value[0];
            }
            else {
                selectedValue = value;
            }

            if (selectedValue) {
                this.select.val(selectedValue).trigger('change');
            }
        };

        return EditableSelect2;
    })();
})();