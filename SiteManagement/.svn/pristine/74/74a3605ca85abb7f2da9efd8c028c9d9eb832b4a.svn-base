using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteManagement.Data
{
    [Table("Owner")]
    public partial class Owner : ISavedEntity, IApprovalEntity, IAuditEntity
    {
        [Key]
        public int OwnerId { get; set; }

        public int SiteId { get; set; }

        public virtual Site Site { get; set; }

        [Required]
        public string FullName { get; set; }

        [Required]
        [StringLength(50)]
        public string PrimaryPhone { get; set; }

        [StringLength(50)]
        public string SecoundaryPhone { get; set; }

        [StringLength(50)]
        public string Email { get; set; }

        [StringLength(200)]
        public string BankName { get; set; }

        [StringLength(200)]
        public string BankBranch { get; set; }

        [StringLength(200)]
        public string AccountNumber { get; set; }

        [StringLength(200)]
        public string IBAN { get; set; }

        public bool IsPay { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Percentage { get; set; }

        public string Note { get; set; }

        [StringLength(200)]
        public string OwnerCode { get; set; }

        public bool IsInventoryOfLegacy { get; set; }

        [StringLength(200)]
        public string InventoryOfLegacyFilePath { get; set; }

        [NotMapped]
        public FileEntity InventoryOfLegacyFile { get; set; }

        public decimal PercentageOfMoneyReceived { get; set; }

        public int? NationalityId { get; set; }

        public virtual Nationality Nationality { get; set; }

        public string NationalId { get; set; }


        #region ISavedEntity Properties

        public bool FlagDeleted { get; set; }

        public int CreatedByUserId { get; set; }

        public virtual User CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UpdatedByUserId { get; set; }

        public virtual User UpdatedBy { get; set; }

        public DateTime LastUpdatedDate { get; set; }

        #endregion ISavedEntity Properties


        #region IApprovalEntity Properties

        public int? OriginalId { get; set; }

        public int ApprovalStatusId { get; set; }

        public int? PartialApprovedByUserId { get; set; }

        public virtual User PartialApprovedBy { get; set; }

        public DateTime? PartialApprovedDate { get; set; }

        public int? ApprovedByUserId { get; set; }

        public virtual User ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int Id
        {
            get
            {
                return OwnerId;
            }
        }

        #endregion IApprovalEntity Properties
    }
}
