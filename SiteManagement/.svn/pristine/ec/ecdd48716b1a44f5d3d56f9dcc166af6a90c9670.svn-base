﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class ContractConverter
    {

        public static Contract ToDatabaseEntity(this ContractModel model, out List<Data.ContractYearlyRent> rents)
        {
            rents = null;
            if (model != null)
            {
                rents = model.Rents.Select(r => new Data.ContractYearlyRent
                {
                    Amount = r.Amount,
                    ContractId = model.ContractId,
                    FromDate = r.StartDate,
                    ToDate = r.EndDate
                }).ToList();

                model.DocumentFile = !String.IsNullOrEmpty(model.DocumentFileJson) ? JsonConvert.DeserializeObject<FileModel>(model.DocumentFileJson) : null;
                model.QOSHAN = !String.IsNullOrEmpty(model.QOSHANJson) ? JsonConvert.DeserializeObject<FileModel>(model.QOSHANJson) : null;
                model.TerminationLetter = !String.IsNullOrEmpty(model.TerminationLetterJson) ? JsonConvert.DeserializeObject<FileModel>(model.TerminationLetterJson) : null;

                Contract entity = new Contract
                {
                    //ContractStartDate = CommonHelper.GetDateFromModel(model.Date.StartDate),
                    //ContractEndDate = CommonHelper.GetDateFromModel(model.EndDate),
                    ContractStartDate = CommonHelper.GetDateFromModel(model.Date.Start),
                    ContractEndDate = CommonHelper.GetDateFromModel(model.Date.End),
                    ContractId = model.ContractId,
                    MunicipalityPermission = model.MunicipalityPermission,
                    SiteId = model.ContractSiteId,
                    YearlyRent = model.YearlyRent,
                    PaymentTypeId = model.PaymentType,
                    IsMadeByUmniah = model.IsMadeByUmniah,
                    IsColocationClose = model.IsColocationClose,
                    ColocationCloseNote = model.ColocationCloseNote,
                    IsActive = model.IsActive,
                    AcquiredbyId = model.AcquiredbyId,
                    AquariumName = model.AquariumName,
                    AquariumNumber = model.AquariumNumber,
                    PieceNumber = model.PieceNumber,
                    Directorate = model.Directorate,
                    MaarefTax = model.MaarefTax,
                    PlateNumber = model.PlateNumber,
                    Town = model.Town,

                    IsIncremental = model.IsIncremental,
                    IncrementalPercentage = model.YearlyIncremental,

                    SigntuerDate = String.IsNullOrWhiteSpace(model.SigntuerDate) ? null : (DateTime?)CommonHelper.GetDateFromModel(model.SigntuerDate),


                    ActiveDate = !String.IsNullOrEmpty(model.ActiveDate) ?
                                                    CommonHelper.GetDateFromModel(model.ActiveDate) : (DateTime?)null,
                    ActiveNote = model.ActiveNote,
                    IsTerminated = model.IsTerminated,
                    TerminatedDate = !String.IsNullOrEmpty(model.TerminatedDate) ?
                                                    CommonHelper.GetDateFromModel(model.TerminatedDate) : (DateTime?)null,
                    TerminatedNote = model.TerminatedNote,
                    ContractDocumentFile = model.DocumentFile == null ? null : new FileEntity
                    {
                        Name = model.DocumentFile.Name,
                        Path = model.DocumentFile.Path,
                        Content = model.DocumentFile.Content
                    },
                    QoshanFile = model.QOSHAN != null ? new FileEntity
                    {
                        Name = model.QOSHAN.Name,
                        Path = model.QOSHAN.Path,
                        Content = model.QOSHAN.Content
                    } : null
                    ,
                    TerminationLetterFile = model.TerminationLetter != null ? new FileEntity
                    {
                        Name = model.TerminationLetter.Name,
                        Path = model.TerminationLetter.Path,
                        Content = model.TerminationLetter.Content
                    } : null
                };

                return entity;
            }

            return null;
        }

        public static ContractModel FromDatabaseEntity(this Contract entity, List<Data.ContractYearlyRent> rents)
        {
            if (entity != null)
            {
                ContractModel model = new ContractModel
                {
                    // Used only in contract grid
                    StartDate = Convert.ToString(entity.ContractStartDate),
                    // Used only in contract grid
                    EndDate = Convert.ToString(entity.ContractEndDate),
                    Date = new DateRangeModel
                    {
                        Start = Convert.ToString(entity.ContractStartDate),
                        End = Convert.ToString(entity.ContractEndDate),
                    },
                    ContractId = entity.ContractId,
                    MunicipalityPermission = entity.MunicipalityPermission,
                    ContractSiteId = entity.SiteId,
                    YearlyRent = entity.YearlyRent,
                    PaymentType = entity.PaymentTypeId != null ? entity.PaymentTypeId.Value : 0,
                    IsMadeByUmniah = entity.IsMadeByUmniah,
                    IsColocationClose = entity.IsColocationClose,
                    ColocationCloseNote = entity.ColocationCloseNote,
                    IsActive = entity.IsActive,
                    ActiveDate = entity.ActiveDate != null ? Convert.ToString(entity.ActiveDate) : null,
                    ActiveNote = entity.ActiveNote,
                    IsTerminated = entity.IsTerminated,
                    TerminatedDate = entity.TerminatedDate != null ? Convert.ToString(entity.TerminatedDate) : null,
                    TerminatedNote = entity.TerminatedNote,
                    IsIncremental = entity.IsIncremental,
                    YearlyIncremental = entity.IncrementalPercentage,

                    AcquiredbyId = entity.AcquiredbyId,
                    AquariumName = entity.AquariumName,
                    AquariumNumber = entity.AquariumNumber,
                    PieceNumber = entity.PieceNumber,
                    Directorate = entity.Directorate,
                    MaarefTax = entity.MaarefTax,
                    PlateNumber = entity.PlateNumber,
                    Town = entity.Town,
                    SigntuerDate = entity.SigntuerDate == null ? null : Convert.ToString(entity.SigntuerDate),

                    //YearlyRents = String.Join(Environment.NewLine,
                    //        rents.Select(r => String.Format("{0}-{1} {2}", r.FromDate.Year, r.ToDate.Year, r.Amount))),


                    DocumentFile = !String.IsNullOrEmpty(entity.ContractDocumentFilePath) ? new FileModel()
                    {
                        Name = FileManager.GetFileNameFromPath(entity.ContractDocumentFilePath),
                        Path = entity.ContractDocumentFilePath,
                        Content = null
                    } : null,
                    QOSHAN = !String.IsNullOrEmpty(entity.QoshanFilePath) ? new FileModel()
                    {
                        Name = FileManager.GetFileNameFromPath(entity.QoshanFilePath),
                        Path = entity.QoshanFilePath,
                        Content = null
                    } : null,

                    TerminationLetter = !String.IsNullOrEmpty(entity.TerminationLetterFilePath) ? new FileModel()
                    {
                        Name = FileManager.GetFileNameFromPath(entity.TerminationLetterFilePath),
                        Path = entity.TerminationLetterFilePath,
                        Content = null
                    } : null,
                };

                model.DocumentFileJson = JsonConvert.SerializeObject(model.DocumentFile);

                if (model.QOSHAN != null)
                {
                    model.QOSHANJson = JsonConvert.SerializeObject(model.QOSHAN);
                }

                if (model.TerminationLetter != null)
                {
                    model.TerminationLetterJson = JsonConvert.SerializeObject(model.TerminationLetter);
                }
                if (rents != null && rents.Count > 0)
                {
                    model.Rents = rents.Select(r => new ContractYearlyRentModel
                    {
                        Amount = r.Amount,
                        EndDate = r.ToDate,
                        StartDate = r.FromDate,
                        Label = String.Format("{0}-{1}", r.FromDate.Year, r.ToDate.Year)

                    }).ToList();
                }
                else
                    FillRents(model);
                return model;
            }

            return null;
        }

        private static void FillRents(ContractModel contract)
        {
            if (contract.IsIncremental)
            {
                DateTime endDate = CommonHelper.GetDateFromModel(contract.EndDate);
                DateTime from = CommonHelper.GetDateFromModel(contract.StartDate);
                DateTime to;
                while (from < endDate)
                {
                    to = from.AddYears(1) <= endDate ?
                                from.AddYears(1) : endDate;

                    contract.Rents.Add(new Models.ContractYearlyRentModel
                    {
                        StartDate = from,
                        EndDate = to,
                        Label = String.Format("{0}-{1}", from.Year, to.Year),

                    });

                    from = to;
                }
            }
        }

    }
}