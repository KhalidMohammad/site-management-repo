using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteManagement.Data
{
    [Table("ElectricityBill")]
    public partial class ElectricityBill : ISavedEntity
    {
        [Key]
        public int BillId { get; set; }

        public int ElectricityMeterId { get; set; }

        public virtual ElectricityMeter ElectricityMeter { get; set; }

        [Required]
        [StringLength(200)]
        public string ElectricityBillCode { get; set; }

        [Required]
        [StringLength(200)]
        public string OldReading { get; set; }

        [Required]
        [StringLength(200)]
        public string NewReading { get; set; }

        [Column(TypeName = "date")]
        public DateTime BillDate { get; set; }

        [Required]
        [StringLength(200)]
        public string Consumption { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Amount { get; set; }

        [NotMapped]
        public string ElectricityMeterCode { get; set; }

        #region ISavedEntity Properties

        public bool FlagDeleted { get; set; }

        public int CreatedByUserId { get; set; }

        public virtual User CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UpdatedByUserId { get; set; }

        public virtual User UpdatedBy { get; set; }

        public DateTime LastUpdatedDate { get; set; }

        #endregion ISavedEntity Properties
    }
}
