﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SiteManagement.Models;
using SiteManagement.Attributes.Authorization;
using SiteManagement.Data;
using System.Collections.Generic;
using Newtonsoft.Json;
using SiteManagement.Helpers;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.SuperAdmin)]
    public class UserController : AuthenticatedController
    {
        // GET: User
        public ActionResult ManageUsers()
        {
            List<UserModel> userModels = new List<UserModel>();

            SiteDataManager.CallDBContext(db =>
            {
                List<User> users = db.Users.ToList();
                //List<User> users = db.Users.ToList();
                if (users != null)
                {
                    userModels = users.Select(u => u.FromDatabaseEntity()).ToList();
                }
            });

            ViewData["UserRoles"] = GetRoles();
            ViewData["UserData"] = JsonConvert.SerializeObject(userModels);

            return View();
        }

        public ActionResult SaveUser(UserModel model)
        {
            ActionResult result = null;
            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    SiteDataManager.CallDBContext(db =>
                    {
                        User user = model.ToDatabaseEntity();

                        if (model.Id == 0)
                        {
                            db.Users.Add(user);
                            db.SaveChanges();
                            //Important Note: This line should be after save changes
                            model.Id = user.UserId;
                        }
                        else
                        {
                            User oldUser = db.Users.Find(user.UserId);

                            db.Entry(oldUser).CurrentValues.SetValues(user);
                            db.SaveChanges();
                        }
                    });

                    string userJsonData = JsonConvert.SerializeObject(model);

                    result = Json(new
                    {
                        UserJsonData = userJsonData,
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult DeleteUser(int userId)
        {
            ActionResult result = null;

            try
            {
                SiteDataManager.CallDBContext(db =>
                {
                    User user = db.Users.Find(userId);

                    db.Users.Remove(user);
                    db.SaveChanges();
                });

                result = Json(new { MessageType = MessageType.Success }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult NewUser()
        {
            ActionResult result = null;

            try
            {
                ViewData["UserRoles"] = GetRoles();

                UserModel model = new UserModel();

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/User/AddUser.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult EditUser(int userId)
        {
            ActionResult result = null;

            try
            {
                ViewData["UserRoles"] = GetRoles();

                UserModel model = new UserModel();

                SiteDataManager.CallDBContext(db =>
                {
                    User user = db.Users.Where(u => u.UserId == userId).First();
                    if (user != null)
                    {
                        model = user.FromDatabaseEntity();
                    }
                });

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/User/EditUser.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        #region Private Methods
        private IEnumerable<SelectListItem> GetRoles()
        {
            //TODO_N: Get the roles from lookup(from db)

            List<SelectListItem> roles = new List<SelectListItem>();

            //Enum.GetValues(typeof(RoleTypeEnum))
            roles.Add(new SelectListItem
            {
                Value = "1",
                Text = "Super Admin"
            });

            roles.Add(new SelectListItem {
                Value = "2", 
                Text = "Admin"
            });

            roles.Add(new SelectListItem
            {
                Value = "3",
                Text = "Guest"
            });

            return roles;
        }

        #endregion Private Methods
    }
}