﻿using SiteManagement.Data;
using SiteManagement.Tools;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SiteManagement.ConsolTools
{
    class Program
    {
        static void Main(string[] args)
        {
            btnAllMosquInfo_Click();

        }
        private static void btnAllMosquInfo_Click()
        {
            List<Site> allSites = new List<Data.Site>();

            SiteDataManager.CallDBContext(db =>
            {
                allSites = db.Sites.ToList();
            });

            List<int> ids = new List<int>();

            using (TempLocationDB db = new TempLocationDB())
            {
                ids = db.PlaceInfo.Where(p => p.externalId.HasValue && p.externalId.Value > 0).Select(p => p.externalId.Value).Distinct().ToList();

            }
            allSites = allSites.Where(s => !ids.Contains(s.SiteId)).ToList();

            foreach (var site in allSites.OrderByDescending(a => a.SiteId))
            {
                RetrieveAddressByType(site.Latitude, site.Longitude, "mosque", true, site.SiteId);
            }
        }
        private static List<PlaceInfo> RetrieveAddressByType(string lat, string lng, string type, bool callRecursive, int id = 0)
        {
            List<PlaceInfo> places = new List<PlaceInfo>();
            string requestUri = string.Format(Form1.baseUri, lat, lng, type);
            string statusCode = String.Empty;
            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                string result = wc.DownloadString(requestUri);
                var xmlElm = XElement.Parse(result);
                var status = (from elm in xmlElm.Descendants()
                              where
                                        elm.Name == "status"
                              select elm).FirstOrDefault();

                statusCode = status.Value.ToLower();
                Console.WriteLine(statusCode);

                if (statusCode == "ok")
                {
                    var results = (from elm in xmlElm.Descendants()
                                   where elm.Name == "result"
                                   select elm);

                    bool cont = true;
                    if (id > 0)
                    {
                        using (TempLocationDB db = new TempLocationDB())
                        {
                            cont = db.PlaceInfo.Count(p => p.externalId.HasValue && p.externalId.Value == id) < results.Count();
                        }

                    }
                    if (cont)
                        foreach (var locationResult in results)
                        {

                            var address = (from elm in locationResult.Descendants()
                                           where elm.Name == "vicinity"
                                           select elm).FirstOrDefault();

                            var name = (from elm in locationResult.Descendants()
                                        where elm.Name == "name"
                                        select elm).FirstOrDefault();


                            var loc = (from elm in locationResult.Descendants()
                                       where elm.Name == "location"
                                       select elm).FirstOrDefault();

                            var locationLat = (from elm in loc.Descendants()
                                               where elm.Name == "lat"
                                               select elm).FirstOrDefault();

                            var locationLong = (from elm in loc.Descendants()
                                                where elm.Name == "lng"
                                                select elm).FirstOrDefault();


                            PlaceInfo info = new PlaceInfo();
                            info.Address = address == null ? null : address.Value;
                            info.Name = name == null ? null : name.Value;
                            info.Lat = lat == null ? null : locationLat.Value;
                            info.Long = locationLong == null ? null : locationLong.Value;
                            if (id > 0)
                            {
                                info.externalId = id;
                            }

                            using (var db = new TempLocationDB())
                            {
                                if (!db.PlaceInfo.Any(p => p.Name == info.Name && p.Lat == info.Lat && p.Long == info.Long && (id == 0 || id == info.externalId)))
                                {
                                    db.PlaceInfo.Add(info);
                                    db.SaveChanges();
                                }
                            }
                            places.Add(info);
                        }
                }
            }
            if (callRecursive &&
                statusCode.ToUpperInvariant() == "OVER_QUERY_LIMIT")
            {
                Thread.Sleep(Convert.ToInt32(TimeSpan.FromSeconds(5).TotalMilliseconds));
                return RetrieveAddressByType(lat, lng, type, callRecursive, id);
            }
            if (statusCode.ToUpperInvariant().Contains("ZERO") && id > 0)
            {
                using (var db = new TempLocationDB())
                {
                    db.PlaceInfo.Add(new PlaceInfo { externalId = id });
                    db.SaveChanges();

                }
            }


            return places;
        }
    }
}
