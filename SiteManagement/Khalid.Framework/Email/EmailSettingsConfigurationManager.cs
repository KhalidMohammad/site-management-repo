﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Khalid.Framework.Email
{
    internal static class EmailSettingsConfigurationManager
    {
        private static readonly string WEB_CONFIG_SECTION_NAME = "EmailSettingsConfiguration";

        internal static SenderEmailConfiguration GeneralEmail
        {
            get;
            private set;
        }

        internal static SenderEmailConfiguration ErrorReportingEmail
        {
            get;
            private set;
        }

        internal static RecieverEmailConfiguration SupportEmail
        {
            get;
            private set;
        }

        internal static DebuggerEmailConfiguration DebuggerEmail
        {
            get;
            private set;
        }

        internal static string SMTPServer
        {
            get;
            private set;
        }

        static EmailSettingsConfigurationManager()
        {
            EmailSettingsConfigSectionHandler section = ConfigurationManager.GetSection(WEB_CONFIG_SECTION_NAME) as EmailSettingsConfigSectionHandler;
            GeneralEmail = section.GeneralEmailSettings;
            ErrorReportingEmail = section.ErrorReportingEmailSettings;
            SupportEmail = section.SupportEmailSettings;
            SMTPServer = section.SMTPServer;
            DebuggerEmail = section.DebuggerEmailSettings;
        }


    }
}
