﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace Khalid.Framework.Email
{
    internal class EmailConfiguration : ConfigurationElement
    {
        /// <summary>
        ///  The email address.
        /// </summary>
        [ConfigurationProperty("EmailAddress", IsRequired = true)]
        internal string EmailAddress
        {
            get
            {
                return this["EmailAddress"] as string;
            }
        }

    }
    
    internal class SenderEmailConfiguration : EmailConfiguration
    {
        /// <summary>
        /// The password of sender email.
        /// </summary>
        [ConfigurationProperty("Password", IsRequired = true)]
        internal string Password
        {
            get
            {
                return this["Password"] as string;
            }
        }
    }

    internal class RecieverEmailConfiguration : EmailConfiguration
    {

    }

    internal class DebuggerEmailConfiguration : EmailConfiguration
    {

    }

}
