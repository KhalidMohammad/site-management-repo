﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Khalid.Framework.Email
{
    /// <summary>
    /// Main Class for sending emails all over the system
    /// </summary>
    public static class EmailManager
    {
        /// <summary>
        /// SMPT server name.
        /// </summary>
        public static string SMTPServerName { get; private set; }

        /// <summary>
        /// sender email.
        /// </summary>
        public static string SenderEmail { get; private set; }

        /// <summary>
        /// sender password.
        /// </summary>
        public static string SenderPassword { get; private set; }

        /// <summary>
        /// Support email.
        /// </summary>
        public static string SupportEmail { get; private set; }

        /// <summary>
        /// Contains a Dictionary of Key as <see cref="ICDL.Interfaces.Enums.EmailTemplate"/> and the key is
        /// the value as html read from text file
        /// </summary>
        /// 


        static IDictionary<string, string> LanguageToEmailTemplates { get; set; }

        #region
        static EmailManager()
        {
            SMTPServerName = EmailSettingsConfigurationManager.SMTPServer;
            SenderEmail = EmailSettingsConfigurationManager.GeneralEmail.EmailAddress;
            SenderPassword = EmailSettingsConfigurationManager.GeneralEmail.Password;
            SupportEmail = EmailSettingsConfigurationManager.SupportEmail.EmailAddress;

            //TODO_khalid add file per language on configuration
            string filePath = Path.Combine(AppDomain.CurrentDomain.BaseDirectory,
                             "Files", "EmailTemplate", "GeneralTemplate.txt");

            if (!File.Exists(filePath))
            {
                throw new FileNotFoundException(filePath, new FileNotFoundException("you must provide email template"));
            }

            LanguageToEmailTemplates = new Dictionary<string, string>
            {
                {"en" , File.ReadAllText(filePath) }
            };


        }

        #endregion

        /// <summary>
        /// Sends an email - Overload -
        /// </summary>
        /// <param name="message"><see cref="System.Net.Mail.MailMessage"/></param>
        /// <param name="enableSSl">true to enable SSL on smtpClient</param>

        /// <summary>
        /// Send Email
        /// </summary>
        /// <param name="toEmailReceiver"></param>
        /// <param name="emailSubject"></param>
        /// <param name="emailMessage"></param>
        /// <param name="isMessageContainHtml"></param>
        /// <param name="ccEmailReceiver"></param>
        /// <param name="bccEmailReceiver"></param>
        /// <param name="isAsync"></param>
        /// <param name="languageCode">language code {ar , en} must add template file and file path for language in configuration</param>
        /// <returns></returns>

        public static void SendEmail(string toEmailReceiver, string emailSubject, string emailMessage,
            bool isMessageContainHtml = true,
            string ccEmailReceiver = null,
            string bccEmailReceiver = null,
            bool isAsync = true,
            string languageCode = "en",
            string excelFileName = null,
            Stream excelFileStream = null)
        {

#if DEBUG
            toEmailReceiver = EmailSettingsConfigurationManager.DebuggerEmail.EmailAddress;
            ccEmailReceiver = null;
            bccEmailReceiver = null;
#endif

            Action SendEmailAction = () =>
            {

                string emailBody = BuildMessageFromHtmlTemplate(emailMessage, languageCode);

                using (SMTP myMailer = new SMTP(SMTPServerName, SenderEmail, toEmailReceiver, emailSubject, emailBody,
                    String.IsNullOrWhiteSpace(ccEmailReceiver) ? "" : ccEmailReceiver, String.IsNullOrWhiteSpace(bccEmailReceiver) ? "" : bccEmailReceiver))
                {
                    try
                    {
                        //if (attachmentFilename != null)
                        //{
                        //    Attachment attachment = new Attachment(attachmentFilename, MediaTypeNames.Application.Octet);
                        //    ContentDisposition disposition = attachment.ContentDisposition;
                        //    disposition.CreationDate = File.GetCreationTime(attachmentFilename);
                        //    disposition.ModificationDate = File.GetLastWriteTime(attachmentFilename);
                        //    disposition.ReadDate = File.GetLastAccessTime(attachmentFilename);
                        //    disposition.FileName = Path.GetFileName(attachmentFilename);
                        //    disposition.Size = new FileInfo(attachmentFilename).Length;
                        //    disposition.DispositionType = DispositionTypeNames.Attachment;
                        //    message.Attachments.Add(attachment);
                        //}
                        if (null != excelFileName && excelFileStream != null)
                            myMailer.AddExcelAttachment(excelFileStream, excelFileName);

                        myMailer.SetSMTPCredentials(SenderEmail, SenderPassword);
                        myMailer.Send(isMessageContainHtml);
                    }
                    catch (Exception ex)
                    {
                    }
                }
            };

            if (isAsync)
            {
                Task.Factory.StartNew(SendEmailAction);
            }
            else
            {
                SendEmailAction();
            }
        }



        /// <summary>
        /// Builds the email message from Html Template
        /// </summary>
        /// <param name="message"><see cref="System.Net.Mail.MailMessage"/></param>
        /// <param name="template"><see cref="ICDL.Interfaces.Enums.EmailTemplate"/></param>
        /// <remarks>The email Template will be plugged in IF and only If th name exists of that template
        /// exsists in the supported templates and that template's Html is not null or empty.
        /// 
        /// If the template was not found, no html formatting will be done on the original message body</remarks>
        private static string BuildMessageFromHtmlTemplate(string emailBody, string languageCode)
        {
            string emailTemplates = LanguageToEmailTemplates[languageCode];

            string bodyText = string.Empty;

            string htmlBody = emailTemplates;

            if (htmlBody != null)
            {
                bodyText = emailBody;

                emailBody = htmlBody.Replace("[bodytext]",
                bodyText.Replace(Environment.NewLine, "<br />")); //replace the new line to line break in html

            }
            return emailBody;
        }

    }
}
