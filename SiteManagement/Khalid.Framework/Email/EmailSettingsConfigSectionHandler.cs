﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Khalid.Framework.Email
{
    internal class EmailSettingsConfigSectionHandler : ConfigurationSection
    {
        [ConfigurationProperty("GeneralEmail")]
        public SenderEmailConfiguration GeneralEmailSettings
        {
            get
            {
                return this["GeneralEmail"] as SenderEmailConfiguration;
            }
        }

        [ConfigurationProperty("ErrorReportingEmail")]
        public SenderEmailConfiguration ErrorReportingEmailSettings
        {
            get
            {
                return this["ErrorReportingEmail"] as SenderEmailConfiguration;
            }
        }

        [ConfigurationProperty("SupportEmail")]
        public RecieverEmailConfiguration SupportEmailSettings
        {
            get
            {
                return this["SupportEmail"] as RecieverEmailConfiguration;
            }
        }

        [ConfigurationProperty("DebuggarEmail")]
        public DebuggerEmailConfiguration DebuggerEmailSettings
        {
            get
            {
                return this["DebuggarEmail"] as DebuggerEmailConfiguration;
            }
        }

        [ConfigurationProperty("SMTPServer")]
        public string SMTPServer
        {
            get
            {
                return this["SMTPServer"] as string;
            }
        }

    }
}
