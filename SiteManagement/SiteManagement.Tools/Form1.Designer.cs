﻿namespace SiteManagement.Tools
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnFillInfo = new System.Windows.Forms.Button();
            this.btnGetMosquInfo = new System.Windows.Forms.Button();
            this.txtLong = new System.Windows.Forms.TextBox();
            this.txtLat = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.btnAllMosquInfo = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnFillInfo
            // 
            this.btnFillInfo.Location = new System.Drawing.Point(12, 49);
            this.btnFillInfo.Name = "btnFillInfo";
            this.btnFillInfo.Size = new System.Drawing.Size(207, 23);
            this.btnFillInfo.TabIndex = 0;
            this.btnFillInfo.Text = "fill site addrees info";
            this.btnFillInfo.UseVisualStyleBackColor = true;
            this.btnFillInfo.Click += new System.EventHandler(this.btnFillInfo_Click);
            // 
            // btnGetMosquInfo
            // 
            this.btnGetMosquInfo.Location = new System.Drawing.Point(12, 157);
            this.btnGetMosquInfo.Name = "btnGetMosquInfo";
            this.btnGetMosquInfo.Size = new System.Drawing.Size(207, 23);
            this.btnGetMosquInfo.TabIndex = 1;
            this.btnGetMosquInfo.Text = "get mosqu info";
            this.btnGetMosquInfo.UseVisualStyleBackColor = true;
            this.btnGetMosquInfo.Click += new System.EventHandler(this.btnGetMosquInfo_Click);
            // 
            // txtLong
            // 
            this.txtLong.Location = new System.Drawing.Point(51, 114);
            this.txtLong.Name = "txtLong";
            this.txtLong.Size = new System.Drawing.Size(168, 20);
            this.txtLong.TabIndex = 2;
            // 
            // txtLat
            // 
            this.txtLat.Location = new System.Drawing.Point(51, 88);
            this.txtLat.Name = "txtLat";
            this.txtLat.Size = new System.Drawing.Size(168, 20);
            this.txtLat.TabIndex = 3;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 88);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(18, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "lat";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 121);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(27, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "long";
            // 
            // btnAllMosquInfo
            // 
            this.btnAllMosquInfo.Location = new System.Drawing.Point(12, 205);
            this.btnAllMosquInfo.Name = "btnAllMosquInfo";
            this.btnAllMosquInfo.Size = new System.Drawing.Size(207, 23);
            this.btnAllMosquInfo.TabIndex = 6;
            this.btnAllMosquInfo.Text = "get all mosqu info";
            this.btnAllMosquInfo.UseVisualStyleBackColor = true;
            this.btnAllMosquInfo.Click += new System.EventHandler(this.btnAllMosquInfo_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.btnAllMosquInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtLat);
            this.Controls.Add(this.txtLong);
            this.Controls.Add(this.btnGetMosquInfo);
            this.Controls.Add(this.btnFillInfo);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnFillInfo;
        private System.Windows.Forms.Button btnGetMosquInfo;
        private System.Windows.Forms.TextBox txtLong;
        private System.Windows.Forms.TextBox txtLat;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnAllMosquInfo;
    }
}

