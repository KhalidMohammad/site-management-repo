﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Tools
{

    public class MosqueInfo
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Address { get; set; }

    }
    public class PlaceInfo
    {
        public int id { get; set; }

        public string Name { get; set; }

        public string Long { get; set; }

        public string Lat { get; set; }

        public string Address { get; set; }

        public int? externalId { get; set; }
    }

    public class TempLocationDB : DbContext
    {
        public TempLocationDB()
            : base("name=TempLocationDB")
        {
        }

        public virtual DbSet<PlaceInfo> PlaceInfo { get; set; }

        public virtual DbSet<MosqueInfo> Mosques { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<TempLocationDB>(null);

            base.OnModelCreating(modelBuilder);
        }
    }
}
