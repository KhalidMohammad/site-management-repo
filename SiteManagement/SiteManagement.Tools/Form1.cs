﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml.Linq;

namespace SiteManagement.Tools
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }



        private void btnFillInfo_Click(object sender, EventArgs e)
        {
            SiteDataManager.CallDBContext(db =>
            {
                foreach (var site in db.Sites)
                {
                    LocationHelper.FillSiteInfo(site, false);
                    Thread.Sleep(Convert.ToInt32(TimeSpan.FromSeconds(5).TotalMilliseconds));

                    //var info = new Lazy<locationInfo>(() => RetrieveFormatedAddress(site.Latitude, site.Longitude));
                    //if (IsEmptyOrRabish(site.SiteCity) && info.Value != null)
                    //{
                    //    site.SiteCity = info.Value.CityName;
                    //}
                    //if (IsEmptyOrRabish(site.SiteAddress) && info.Value != null)
                    //{
                    //    site.SiteAddress = info.Value.Address;
                    //}
                    //if (IsEmptyOrRabish(site.StreetName) && info.Value != null)
                    //{
                    //    site.StreetName = info.Value.StreetName;
                    //}
                    //if (IsEmptyOrRabish(site.SiteGovernorates) && info.Value != null)
                    //{
                    //    site.SiteGovernorates = info.Value.Governorate;
                    //}
                    db.SaveChanges();
                }

            });

        }

        private void btnGetMosquInfo_Click(object sender, EventArgs e)
        {
            if (!String.IsNullOrWhiteSpace(txtLat.Text) && !String.IsNullOrWhiteSpace(txtLong.Text))
                RetrieveAddressByType(txtLat.Text, txtLong.Text, "mosque", true);
        }

        private static readonly string AppKey = "AIzaSyCF9nJ2u-0V7xhDHc4A2PILDJRxpt9cIIA"; //"AIzaSyCFktrzmtVWdWISxiRx2xo5QDEErwBuStE";

        public static string baseUri = @"https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location={0},{1}&radius=50000&type={2}&key=" + AppKey;
        public static string searchbaseUri = @"https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location={0},{1}&radius=50000&name={2}&key=" + AppKey;

        private static List<PlaceInfo> FillAddressByName(string lat, string lng, string namesearch, bool callRecursive, int id = 0)
        {
            List<PlaceInfo> places = new List<PlaceInfo>();
            string requestUri = string.Format(searchbaseUri, lat, lng, namesearch);
            string statusCode = String.Empty;
            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                string result = wc.DownloadString(requestUri);
                var xmlElm = XElement.Parse(result);
                var status = (from elm in xmlElm.Descendants()
                              where
                                        elm.Name == "status"
                              select elm).FirstOrDefault(); 

                statusCode = status.Value.ToLower();
                Console.WriteLine(statusCode);
                if (statusCode == "ok")
                {
                    var results = (from elm in xmlElm.Descendants()
                                   where elm.Name == "result"
                                   select elm);

                    bool cont = true;
                    if (id > 0)
                    {
                        using (TempLocationDB db = new TempLocationDB())
                        {
                            cont = db.PlaceInfo.Count(p => p.externalId.HasValue && p.externalId.Value == id) < results.Count();
                        }

                    }
                    if (cont)
                        foreach (var locationResult in results)
                        {

                            var address = (from elm in locationResult.Descendants()
                                           where elm.Name == "vicinity"
                                           select elm).FirstOrDefault();

                            var name = (from elm in locationResult.Descendants()
                                        where elm.Name == "name"
                                        select elm).FirstOrDefault();


                            var loc = (from elm in locationResult.Descendants()
                                       where elm.Name == "location"
                                       select elm).FirstOrDefault();

                            var locationLat = (from elm in loc.Descendants()
                                               where elm.Name == "lat"
                                               select elm).FirstOrDefault();

                            var locationLong = (from elm in loc.Descendants()
                                                where elm.Name == "lng"
                                                select elm).FirstOrDefault();


                            PlaceInfo info = new PlaceInfo();
                            info.Address = address == null ? null : address.Value;
                            info.Name = name == null ? null : name.Value;
                            info.Lat = lat == null ? null : locationLat.Value;
                            info.Long = locationLong == null ? null : locationLong.Value;
                            if (id > 0)
                            {
                                info.externalId = id;
                            }

                            using (var db = new TempLocationDB())
                            {
                                if (!db.PlaceInfo.Any(p => p.Name == info.Name && p.Lat == info.Lat && p.Long == info.Long))
                                {
                                    db.PlaceInfo.Add(info);
                                }
                                else if (id > 0)
                                {
                                    db.PlaceInfo.Add(new PlaceInfo { externalId = id });
                                }
                                db.SaveChanges();
                            }
                            places.Add(info);
                        }
                }
            }
            if (callRecursive &&
                statusCode.ToUpperInvariant() == "OVER_QUERY_LIMIT")
            {
                Thread.Sleep(Convert.ToInt32(TimeSpan.FromSeconds(5).TotalMilliseconds));
                return RetrieveAddressByType(lat, lng, namesearch, callRecursive, id);
            }
            if (statusCode.ToUpperInvariant().Contains("ZERO") && id > 0)
            {
                using (var db = new TempLocationDB())
                {

                    db.PlaceInfo.Add(new PlaceInfo { externalId = id });
                    db.SaveChanges();

                }
            }


            return places;
        }

        private static List<PlaceInfo> RetrieveAddressByType(string lat, string lng, string type, bool callRecursive, int id = 0)
        {
            List<PlaceInfo> places = new List<PlaceInfo>();
            string requestUri = string.Format(baseUri, lat, lng, type);
            string statusCode = String.Empty;
            using (WebClient wc = new WebClient())
            {
                wc.Encoding = Encoding.UTF8;
                string result = wc.DownloadString(requestUri);
                var xmlElm = XElement.Parse(result);
                var status = (from elm in xmlElm.Descendants()
                              where
                                        elm.Name == "status"
                              select elm).FirstOrDefault();

                statusCode = status.Value.ToLower();
                Console.WriteLine(statusCode);
                if (statusCode == "ok")
                {
                    var results = (from elm in xmlElm.Descendants()
                                   where elm.Name == "result"
                                   select elm);

                    bool cont = true;
                    if (id > 0)
                    {
                        using (TempLocationDB db = new TempLocationDB())
                        {
                            cont = db.PlaceInfo.Count(p => p.externalId.HasValue && p.externalId.Value == id) < results.Count();
                        }

                    }
                    if (cont)
                        foreach (var locationResult in results)
                        {

                            var address = (from elm in locationResult.Descendants()
                                           where elm.Name == "vicinity"
                                           select elm).FirstOrDefault();

                            var name = (from elm in locationResult.Descendants()
                                        where elm.Name == "name"
                                        select elm).FirstOrDefault();


                            var loc = (from elm in locationResult.Descendants()
                                       where elm.Name == "location"
                                       select elm).FirstOrDefault();

                            var locationLat = (from elm in loc.Descendants()
                                               where elm.Name == "lat"
                                               select elm).FirstOrDefault();

                            var locationLong = (from elm in loc.Descendants()
                                                where elm.Name == "lng"
                                                select elm).FirstOrDefault();


                            PlaceInfo info = new PlaceInfo();
                            info.Address = address == null ? null : address.Value;
                            info.Name = name == null ? null : name.Value;
                            info.Lat = lat == null ? null : locationLat.Value;
                            info.Long = locationLong == null ? null : locationLong.Value;
                            if (id > 0)
                            {
                                info.externalId = id;
                            }

                            using (var db = new TempLocationDB())
                            {
                                if (!db.PlaceInfo.Any(p => p.Name == info.Name && p.Lat == info.Lat && p.Long == info.Long))
                                {
                                    db.PlaceInfo.Add(info);
                                }
                                else
                                {
                                    db.PlaceInfo.Add(new PlaceInfo { externalId = id });
                                }
                                db.SaveChanges();
                            }
                            places.Add(info);
                        }
                }
            }
            if (callRecursive &&
                statusCode.ToUpperInvariant() == "OVER_QUERY_LIMIT")
            {
                Thread.Sleep(Convert.ToInt32(TimeSpan.FromSeconds(5).TotalMilliseconds));
                return RetrieveAddressByType(lat, lng, type, callRecursive, id);
            }
            if (statusCode.ToUpperInvariant().Contains("ZERO") && id > 0)
            {
                using (var db = new TempLocationDB())
                {

                    db.PlaceInfo.Add(new PlaceInfo { externalId = id });
                    db.SaveChanges();

                }
            }


            return places;
        }

        private void btnAllMosquInfo_Click(object sender, EventArgs e)
        {
            List<Site> allSites = new List<Data.Site>();

            SiteDataManager.CallDBContext(db =>
            {
                allSites = db.Sites.ToList();
            });

            List<int> ids = new List<int>();

            using (TempLocationDB db = new TempLocationDB())
            {
                ids = db.PlaceInfo.Where(p => p.externalId.HasValue && p.externalId.Value > 0).Select(p => p.externalId.Value).Distinct().ToList();

            }
            allSites = allSites.Where(s => !ids.Contains(s.SiteId)).ToList();

            foreach (var site in allSites.OrderByDescending(a => a.SiteCity))
            {
                RetrieveAddressByType(site.Latitude, site.Longitude, "mosque", true, site.SiteId);
            }
        }

        public static void AllMosquInfo()
        {
            List<Site> allSites = new List<Data.Site>();

            SiteDataManager.CallDBContext(db =>
            {
                allSites = db.Sites.ToList();
            });

            List<int> ids = new List<int>();

            List<MosqueInfo> allMosque = new List<MosqueInfo>();

            using (TempLocationDB db = new TempLocationDB())
            {
                ids = db.PlaceInfo.Where(p => p.externalId.HasValue && p.externalId.Value > 0).Select(p => p.externalId.Value).Distinct().ToList();
                allMosque = db.Mosques.ToList();
            }
            allSites = allSites.Where(s => !ids.Contains(s.SiteId)).ToList();

            foreach (var site in allSites.OrderByDescending(a => a.SiteCity))
            {
                RetrieveAddressByType(site.Latitude, site.Longitude, "mosque", true, site.SiteId);
            }
        }

        public static void FillAllMosquInfo()
        {
            List<Site> allSites = new List<Data.Site>();

            SiteDataManager.CallDBContext(db =>
            {
                allSites = db.Sites.ToList();
            });

            List<int> ids = new List<int>();

            List<MosqueInfo> allMosque = new List<MosqueInfo>();

            using (TempLocationDB db = new TempLocationDB())
            {
                ids = db.PlaceInfo.Where(p => p.externalId.HasValue && p.externalId.Value > 0).Select(p => p.externalId.Value).Distinct().ToList();
                allMosque = db.Mosques.ToList();
            }
            //allSites = allSites.Where(s => !ids.Contains(s.SiteId)).ToList();

            foreach (MosqueInfo mosque in allMosque)
            {
                foreach (var site in allSites)
                {
                    var cc = FillAddressByName(site.Latitude, site.Longitude, mosque.Name, true, site.SiteId);
                    if (cc != null && cc.Count > 0)
                    {
                        break;
                    }
                }
            }
        }

        //static string baseUri = @"http://maps.google.com/maps/api/geocode/xml?latlng={0},{1}&sensor=false&creditntial=AIzaSyAyyPZ_62snfVRtTCgBTrUt7JJiPUzEK2U";
        //string location = string.Empty;

        //public static locationInfo RetrieveFormatedAddress(string lat, string lng)
        //{
        //    locationInfo info = new locationInfo();
        //    string requestUri = string.Format(baseUri, lat, lng);
        //    string statusCode = String.Empty;
        //    using (WebClient wc = new WebClient())
        //    {
        //        string result = wc.DownloadString(requestUri);
        //        var xmlElm = XElement.Parse(result);
        //        var status = (from elm in xmlElm.Descendants()
        //                      where
        //                                elm.Name == "status"
        //                      select elm).FirstOrDefault();

        //        statusCode = status.Value.ToLower();
        //        if (statusCode == "ok")
        //        {
        //            var address = (from elm in xmlElm.Descendants()
        //                           where elm.Name == "formatted_address"
        //                           select elm).FirstOrDefault();

        //            var city = (from elm in xmlElm.Descendants()
        //                        where elm.Name == "type" &&
        //                              elm.Value.Equals("locality", StringComparison.InvariantCultureIgnoreCase)
        //                        select elm).FirstOrDefault();

        //            if (city != null) city = city.Parent.Descendants().FirstOrDefault(p => p.Name == "long_name");

        //            var route = (from elm in xmlElm.Descendants()
        //                         where
        //                            elm.Name == "type" &&
        //                            elm.Value.Equals("route", StringComparison.InvariantCultureIgnoreCase)
        //                         select elm).FirstOrDefault();
        //            if (route != null) route = route.Parent.Descendants().FirstOrDefault(p => p.Name == "long_name");


        //            var governorate = (from elm in xmlElm.Descendants()
        //                               where
        //                                  elm.Name == "type" &&
        //                                  elm.Value.Equals("administrative_area_level_1", StringComparison.InvariantCultureIgnoreCase)
        //                               select elm).FirstOrDefault();

        //            if (governorate != null) governorate = governorate.Parent.Descendants().FirstOrDefault(p => p.Name == "long_name");

        //            info.Address = address == null ? null : address.Value;
        //            info.CityName = city == null ? null : city.Value;
        //            info.StreetName = route == null ? null : route.Value;
        //            info.Governorate = governorate == null ? null : governorate.Value;
        //        }
        //    }
        //    Thread.Sleep(Convert.ToInt32(TimeSpan.FromSeconds(5).TotalMilliseconds));
        //    if (statusCode.ToUpperInvariant() == "OVER_QUERY_LIMIT")
        //    {
        //        return RetrieveFormatedAddress(lat, lng);
        //    }
        //    return info;
        //}

        //private static bool IsEmptyOrRabish(string val)
        //{
        //    return String.IsNullOrWhiteSpace(val) || val.Equals("-") || val.Equals("_");
        //}
        //public static void FillSiteInfo(Site site)
        //{
        //    var info = new Lazy<locationInfo>(() => RetrieveFormatedAddress(site.Latitude, site.Longitude));
        //    if (IsEmptyOrRabish(site.SiteCity) && info.Value != null)
        //    {
        //        site.SiteCity = info.Value.CityName;
        //    }
        //    if (IsEmptyOrRabish(site.SiteAddress) && info.Value != null)
        //    {
        //        site.SiteAddress = info.Value.Address;
        //    }
        //    if (IsEmptyOrRabish(site.StreetName) && info.Value != null)
        //    {
        //        site.StreetName = info.Value.StreetName;
        //    }
        //    if (IsEmptyOrRabish(site.SiteGovernorates) && info.Value != null)
        //    {
        //        site.SiteGovernorates = info.Value.Governorate;
        //    }
        //}

    }

}
