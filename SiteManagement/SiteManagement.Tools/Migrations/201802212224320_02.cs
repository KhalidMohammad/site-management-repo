namespace SiteManagement.Tools.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _02 : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.PlaceInfoes", "externalId", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.PlaceInfoes", "externalId", c => c.Int(nullable: false));
        }
    }
}
