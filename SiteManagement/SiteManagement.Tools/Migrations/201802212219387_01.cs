namespace SiteManagement.Tools.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class _01 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PlaceInfoes", "externalId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PlaceInfoes", "externalId");
        }
    }
}
