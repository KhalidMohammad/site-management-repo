namespace SiteManagement.Tools.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class tttt : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MosqueInfoes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Address = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MosqueInfoes");
        }
    }
}
