﻿SET XACT_ABORT ON;

BEGIN TRAN

--************************************************************ dbo.Role ***********************************************************************
INSERT INTO [dbo].[Role]([RoleId], [RoleName]) SELECT 1,N'SuperAdmin' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Role] WHERE RoleId = 1)
INSERT INTO [dbo].[Role]([RoleId], [RoleName]) SELECT 2,N'Admin' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Role] WHERE RoleId = 2)
INSERT INTO [dbo].[Role]([RoleId], [RoleName]) SELECT 3,N'Guest' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Role] WHERE RoleId = 3)

--**********************************************************************************************************************************************
--************************************************************ dbo.LookupType ***********************************************************************
INSERT INTO [dbo].[LookupType]([LookupTypeId], [LookupTypeName]) SELECT 1,N'SiteType' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[LookupType] WHERE LookupTypeId = 1)
INSERT INTO [dbo].[LookupType]([LookupTypeId], [LookupTypeName]) SELECT 2,N'ElectricityMeterType' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[LookupType] WHERE LookupTypeId = 2)
INSERT INTO [dbo].[LookupType]([LookupTypeId], [LookupTypeName]) SELECT 3,N'LocationSubType' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[LookupType] WHERE LookupTypeId = 3)
INSERT INTO [dbo].[LookupType]([LookupTypeId], [LookupTypeName]) SELECT 4,N'TXSiteType' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[LookupType] WHERE LookupTypeId = 4)
INSERT INTO [dbo].[LookupType]([LookupTypeId], [LookupTypeName]) SELECT 5,N'PaymentType' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[LookupType] WHERE LookupTypeId = 5)
INSERT INTO [dbo].[LookupType]([LookupTypeId], [LookupTypeName]) SELECT 6,N'ElectricityMeterDistribution' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[LookupType] WHERE LookupTypeId = 6)
INSERT INTO [dbo].[LookupType]([LookupTypeId], [LookupTypeName]) SELECT 7,N'ElectricityMeterSubType' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[LookupType] WHERE LookupTypeId = 7)
INSERT INTO [dbo].[LookupType]([LookupTypeId], [LookupTypeName]) SELECT 8,N'ElectricityMeterPaymentSubType' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[LookupType] WHERE LookupTypeId = 8)
INSERT INTO [dbo].[LookupType]([LookupTypeId], [LookupTypeName]) SELECT 9,N'AcquiredBy' WHERE NOT EXISTS(SELECT 1 FROM [dbo].[LookupType] WHERE LookupTypeId = 9)

--**********************************************************************************************************************************************
--************************************************************ dbo.Lookup ***********************************************************************
SET IDENTITY_INSERT [dbo].[Lookup] ON;

INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 1,N'Individual', 1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 1)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 2,N'Hospital', 1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 2)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 3,N'MOE School', 1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 3)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 4,N'Army', 1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 4)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 5,N'Camp', 1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 5)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 6,N'Umniah', 2 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 6)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 7,N'Owner', 2 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 7)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 8,N'Temporary', 2 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 8)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 9,N'BSC', 4 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 9)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 10,N'NODE', 4 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 10)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 11,N'Terminal', 4 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 11)
INSERT INTO [dbo].[Lookup]([Id], [DisplayName], [LookupTypeId]) SELECT 12,N'Fiber', 4 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[Lookup] WHERE [Id] = 12)

SET IDENTITY_INSERT [dbo].[Lookup] OFF;

--**********************************************************************************************************************************************
--************************************************************ dbo.TechnologyUsed ***********************************************************************
SET IDENTITY_INSERT [dbo].[TechnologyUsed] ON;

INSERT INTO [dbo].[TechnologyUsed]([TechnologyUsedId], [DisplayName], [AverageCost]) SELECT 1,N'2G', 1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[TechnologyUsed] WHERE [TechnologyUsedId] = 1)
INSERT INTO [dbo].[TechnologyUsed]([TechnologyUsedId], [DisplayName], [AverageCost]) SELECT 2,N'3G', 3.4 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[TechnologyUsed] WHERE [TechnologyUsedId] = 2)
INSERT INTO [dbo].[TechnologyUsed]([TechnologyUsedId], [DisplayName], [AverageCost]) SELECT 3,N'4G TDD', 12 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[TechnologyUsed] WHERE [TechnologyUsedId] = 3)
INSERT INTO [dbo].[TechnologyUsed]([TechnologyUsedId], [DisplayName], [AverageCost]) SELECT 4,N'4G FDD', 2.1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[TechnologyUsed] WHERE [TechnologyUsedId] = 4)
INSERT INTO [dbo].[TechnologyUsed]([TechnologyUsedId], [DisplayName], [AverageCost]) SELECT 5,N'TU', 22 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[TechnologyUsed] WHERE [TechnologyUsedId] = 5)

SET IDENTITY_INSERT [dbo].[TechnologyUsed] OFF;

--**********************************************************************************************************************************************


--**********************************************************************************************************************************************
--************************************************************ dbo.user ***********************************************************************
INSERT INTO [user] (UserName, FullName, UserEmail, Password, RoleId, IsActive)
	select 'admin', 'admin', 'admin@co.co', '123', 1, 1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[user] WHERE username = 'admin')

INSERT INTO [user] (UserName, FullName, UserEmail, Password, RoleId, IsActive)
	select 'admin2', 'admin2', 'admin2@co.co', '123', 2, 1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[user] WHERE username = 'admin2')

INSERT INTO [user] (UserName, FullName, UserEmail, Password, RoleId, IsActive)
	select 'guest', 'guest', 'guest@co.co', '123', 3, 1 WHERE NOT EXISTS(SELECT 1 FROM [dbo].[user] WHERE username = 'guest')

--***********************************************************************************************************************

COMMIT TRAN