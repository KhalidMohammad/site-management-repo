﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    public static class SiteDataManager
    {
        //public static IEnumerable<TReturnValue> CallSiteDBContextEnumerable<TReturnValue>(Func<SiteData, IEnumerable<TReturnValue>> dbCall)
        //{
        //    return DataManager.CallDBContextEnumerable<SiteData, TReturnValue>(dbCall);
        //}
        public static void CallDBContext(Action<SiteData> dbCall)
        {
            DataManager.CallDBContext<SiteData>(dbCall);
        }

        //public static void UpdateApprovalState<TApprovalEntity>(int id, ApprovalStatus newStatus)
        //    where TApprovalEntity : class, IApprovalEntity
        //{
        //    CallDBContext(db =>
        //    {
        //        TApprovalEntity entity = db.Set<TApprovalEntity>().Find(id);
        //        entity.ApprovlStatusId = (int)newStatus;
        //        db.SaveChanges();
        //    });
        //}

        //public static bool ApprovalCondition(IApprovalEntity entity)
        //{
        //    return (entity.ApprovlStatusId == (int)ApprovalStatus.Pending ||
        //            entity.ApprovlStatusId == (int)ApprovalStatus.Editied);
        //}
    }
}
