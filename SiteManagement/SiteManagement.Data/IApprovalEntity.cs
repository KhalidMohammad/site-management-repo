﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    public interface IApprovalEntity
    {
        int? OriginalId { get; set; }

        int ApprovalStatusId { get; set; }

        int? PartialApprovedByUserId { get; set; }

        //User PartialApprovedBy { get; set; }

        DateTime? PartialApprovedDate { get; set; }

        int? ApprovedByUserId { get; set; }

        //User ApprovedBy { get; set; }

        DateTime? ApprovedDate { get; set; }
    }
}
