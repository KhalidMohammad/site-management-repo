﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    public static class LookupHelper
    {
        public static IEnumerable<Lookup> GetLookupByType(LookupTypeEnum type)
        {
            IEnumerable<Lookup> items = new List<Lookup>();
            SiteDataManager.CallDBContext(db =>
            {
                items = db.Lookups.Where(l => l.LookupTypeId == (int)type).ToList();
            });

            return items;
        }

    }
}
