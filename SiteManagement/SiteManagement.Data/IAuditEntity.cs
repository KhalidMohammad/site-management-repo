﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    public interface IAuditEntity
    {
        int Id { get; }
    }
}
