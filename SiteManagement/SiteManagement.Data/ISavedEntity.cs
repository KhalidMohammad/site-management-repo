﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    public interface ISavedEntity
    {
        bool FlagDeleted { get; set; }

        int CreatedByUserId { get; set; }
        
        //User CreatedBy { get; set; }

        DateTime CreatedDate { get; set; }

        int UpdatedByUserId { get; set; }

        //User UpdatedBy { get; set; }

        DateTime LastUpdatedDate { get; set; }
    }
}
