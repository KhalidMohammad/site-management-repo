﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    public enum ApprovalStatus
    {
        Pending = 1,

        PartialApprove = 2,

        Approved = 3,

        Editied = 4,

        Rejected = 5
    }

    public enum ApprovalType
    {
        Add = 1,
        Edit = 2
    }
}
