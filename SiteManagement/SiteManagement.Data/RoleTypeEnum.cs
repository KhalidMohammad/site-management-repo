﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    public enum RoleTypeEnum
    {
        Unknown = 0,
        SuperAdmin = 1,
        Admin = 2,
        Guest = 3,
    }
}
