namespace SiteManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addAuditLog : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ElectricityTheft");
            CreateTable(
                "dbo.AuditLog",
                c => new
                    {
                        AuditLogId = c.Int(nullable: false, identity: true),
                        ChangeSetId = c.Int(nullable: false),
                        UserID = c.String(nullable: false, maxLength: 50),
                        EventDateUTC = c.DateTime(nullable: false),
                        EventType = c.String(nullable: false, maxLength: 1),
                        TableName = c.String(nullable: false, maxLength: 100),
                        RecordId = c.String(nullable: false, maxLength: 100),
                        ColumnName = c.String(nullable: false, maxLength: 100),
                        OriginalValue = c.String(),
                        NewValue = c.String(),
                    })
                .PrimaryKey(t => t.AuditLogId);
            
            AlterColumn("dbo.ElectricityTheft", "TheftId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ElectricityTheft", "TheftId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.ElectricityTheft");
            AlterColumn("dbo.ElectricityTheft", "TheftId", c => c.Int(nullable: false));
            DropTable("dbo.AuditLog");
            AddPrimaryKey("dbo.ElectricityTheft", "TheftId");
        }
    }
}
