namespace SiteManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addyearlyrentlogging : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contract", "ContractYearlyRentsString", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Contract", "ContractYearlyRentsString");
        }
    }
}
