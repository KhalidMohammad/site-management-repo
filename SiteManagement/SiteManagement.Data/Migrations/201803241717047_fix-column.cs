namespace SiteManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class fixcolumn : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AuditLog", "EventDate", c => c.DateTime(nullable: false));
            AlterColumn("dbo.AuditLog", "RecordId", c => c.String(maxLength: 100));
            DropColumn("dbo.AuditLog", "EventDateUTC");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AuditLog", "EventDateUTC", c => c.DateTime(nullable: false));
            AlterColumn("dbo.AuditLog", "RecordId", c => c.String(nullable: false, maxLength: 100));
            DropColumn("dbo.AuditLog", "EventDate");
        }
    }
}
