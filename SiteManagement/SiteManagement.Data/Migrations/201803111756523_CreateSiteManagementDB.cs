namespace SiteManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateSiteManagementDB : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Contract",
                c => new
                    {
                        ContractId = c.Int(nullable: false, identity: true),
                        SiteId = c.Int(nullable: false),
                        ContractStartDate = c.DateTime(nullable: false, storeType: "date"),
                        ContractEndDate = c.DateTime(storeType: "date"),
                        YearlyRent = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        MunicipalityPermission = c.Boolean(nullable: false),
                        ContractDocumentFilePath = c.String(maxLength: 200),
                        PaymentTypeId = c.Int(),
                        QoshanFilePath = c.String(maxLength: 200),
                        IsMadeByUmniah = c.Boolean(nullable: false),
                        IsColocationClose = c.Boolean(nullable: false),
                        ColocationCloseNote = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        ActiveDate = c.DateTime(storeType: "date"),
                        ActiveNote = c.String(),
                        IsTerminated = c.Boolean(nullable: false),
                        TerminatedDate = c.DateTime(storeType: "date"),
                        TerminatedNote = c.String(),
                        AquariumNumber = c.String(maxLength: 200),
                        AquariumName = c.String(maxLength: 200),
                        PieceNumber = c.String(maxLength: 200),
                        Directorate = c.String(maxLength: 200),
                        Town = c.String(maxLength: 200),
                        PlateNumber = c.String(),
                        MaarefTax = c.Decimal(nullable: false, precision: 10, scale: 3, storeType: "numeric"),
                        AcquiredById = c.Int(),
                        SignatureDate = c.DateTime(storeType: "date"),
                        IsIncremental = c.Boolean(nullable: false),
                        IncrementalPercentage = c.Decimal(precision: 10, scale: 3),
                        TerminationLetterFilePath = c.String(maxLength: 200),
                        FlagDeleted = c.Boolean(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedByUserId = c.Int(nullable: false),
                        LastUpdatedDate = c.DateTime(nullable: false),
                        OriginalId = c.Int(),
                        ApprovalStatusId = c.Int(nullable: false),
                        PartialApprovedByUserId = c.Int(),
                        PartialApprovedDate = c.DateTime(),
                        ApprovedByUserId = c.Int(),
                        ApprovedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ContractId)
                .ForeignKey("dbo.Lookup", t => t.AcquiredById)
                .ForeignKey("dbo.User", t => t.ApprovedByUserId)
                .ForeignKey("dbo.User", t => t.CreatedByUserId)
                .ForeignKey("dbo.User", t => t.PartialApprovedByUserId)
                .ForeignKey("dbo.Lookup", t => t.PaymentTypeId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .ForeignKey("dbo.User", t => t.UpdatedByUserId)
                .Index(t => t.SiteId)
                .Index(t => t.PaymentTypeId)
                .Index(t => t.AcquiredById)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.UpdatedByUserId)
                .Index(t => t.PartialApprovedByUserId)
                .Index(t => t.ApprovedByUserId);
            
            CreateTable(
                "dbo.Lookup",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DisplayName = c.String(nullable: false, maxLength: 200),
                        LookupTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.LookupType", t => t.LookupTypeId)
                .Index(t => t.LookupTypeId);
            
            CreateTable(
                "dbo.LookupType",
                c => new
                    {
                        LookupTypeId = c.Int(nullable: false),
                        LookupTypeName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.LookupTypeId);
            
            CreateTable(
                "dbo.User",
                c => new
                    {
                        UserId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 200),
                        FullName = c.String(maxLength: 200),
                        UserEmail = c.String(maxLength: 200),
                        Password = c.String(nullable: false, maxLength: 200),
                        RoleId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.UserId)
                .ForeignKey("dbo.Role", t => t.RoleId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.Role",
                c => new
                    {
                        RoleId = c.Int(nullable: false),
                        RoleName = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.RoleId);
            
            CreateTable(
                "dbo.ContractYearlyRent",
                c => new
                    {
                        ContractId = c.Int(nullable: false),
                        FromDate = c.DateTime(nullable: false, storeType: "date"),
                        ToDate = c.DateTime(nullable: false, storeType: "date"),
                        Amount = c.Decimal(nullable: false, precision: 10, scale: 3, storeType: "numeric"),
                    })
                .PrimaryKey(t => new { t.ContractId, t.FromDate, t.ToDate })
                .ForeignKey("dbo.Contract", t => t.ContractId)
                .Index(t => t.ContractId);
            
            CreateTable(
                "dbo.Site",
                c => new
                    {
                        SiteId = c.Int(nullable: false, identity: true),
                        SiteCode = c.String(nullable: false, maxLength: 100),
                        SiteName = c.String(maxLength: 200),
                        SiteCity = c.String(maxLength: 200),
                        SiteGovernorates = c.String(maxLength: 200),
                        Latitude = c.String(nullable: false, maxLength: 50),
                        Longitude = c.String(nullable: false, maxLength: 50),
                        StreetName = c.String(maxLength: 200),
                        SiteAddress = c.String(maxLength: 200),
                        IsActive = c.Boolean(nullable: false),
                        SiteTypeId = c.Int(nullable: false),
                        SiteNote = c.String(),
                        MunicipalityPermission = c.Boolean(nullable: false),
                        LocationSubTypeId = c.Int(),
                        LocationSubTypeNote = c.String(),
                        MunicipalityPermissionDate = c.DateTime(storeType: "date"),
                        MunicipalityPermissionPicturePath = c.String(),
                        TXSiteTypeId = c.Int(),
                        PayRent = c.Boolean(nullable: false),
                        PhaseNumber = c.String(),
                        OnAirDate = c.DateTime(storeType: "date"),
                        SiteCodePrefix = c.String(),
                        FlagDeleted = c.Boolean(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedByUserId = c.Int(nullable: false),
                        LastUpdatedDate = c.DateTime(nullable: false),
                        OriginalId = c.Int(),
                        ApprovalStatusId = c.Int(nullable: false),
                        PartialApprovedByUserId = c.Int(),
                        PartialApprovedDate = c.DateTime(),
                        ApprovedByUserId = c.Int(),
                        ApprovedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.SiteId)
                .ForeignKey("dbo.User", t => t.ApprovedByUserId)
                .ForeignKey("dbo.User", t => t.CreatedByUserId)
                .ForeignKey("dbo.Lookup", t => t.LocationSubTypeId)
                .ForeignKey("dbo.User", t => t.PartialApprovedByUserId)
                .ForeignKey("dbo.Lookup", t => t.SiteTypeId)
                .ForeignKey("dbo.Lookup", t => t.TXSiteTypeId)
                .ForeignKey("dbo.User", t => t.UpdatedByUserId)
                .Index(t => t.SiteTypeId)
                .Index(t => t.LocationSubTypeId)
                .Index(t => t.TXSiteTypeId)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.UpdatedByUserId)
                .Index(t => t.PartialApprovedByUserId)
                .Index(t => t.ApprovedByUserId);
            
            CreateTable(
                "dbo.ElectricityMeter",
                c => new
                    {
                        ElectricityMeterId = c.Int(nullable: false, identity: true),
                        SiteId = c.Int(nullable: false),
                        ElectricityMeterCode = c.String(nullable: false, maxLength: 200),
                        ElectricityMeterTypeId = c.Int(nullable: false),
                        ElectricityMeterSubTypeId = c.Int(),
                        ElectricityMeterPaymentSubTypeId = c.Int(),
                        ElectricityMeterDistributionId = c.Int(),
                        EstimatePowerConsumption = c.Decimal(nullable: false, precision: 10, scale: 3, storeType: "numeric"),
                        IsActive = c.Boolean(nullable: false),
                        ActivationDate = c.DateTime(storeType: "date"),
                        MeterShield = c.Boolean(nullable: false),
                        BoxInstallation = c.Boolean(nullable: false),
                        PipesFilling = c.Boolean(nullable: false),
                        PipesFillingSupplierName = c.String(maxLength: 200),
                        USence = c.Boolean(nullable: false),
                        USenceSupplierName = c.String(maxLength: 200),
                        FlagDeleted = c.Boolean(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedByUserId = c.Int(nullable: false),
                        LastUpdatedDate = c.DateTime(nullable: false),
                        OriginalId = c.Int(),
                        ApprovalStatusId = c.Int(nullable: false),
                        PartialApprovedByUserId = c.Int(),
                        PartialApprovedDate = c.DateTime(),
                        ApprovedByUserId = c.Int(),
                        ApprovedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.ElectricityMeterId)
                .ForeignKey("dbo.User", t => t.ApprovedByUserId)
                .ForeignKey("dbo.User", t => t.CreatedByUserId)
                .ForeignKey("dbo.Lookup", t => t.ElectricityMeterDistributionId)
                .ForeignKey("dbo.Lookup", t => t.ElectricityMeterPaymentSubTypeId)
                .ForeignKey("dbo.Lookup", t => t.ElectricityMeterSubTypeId)
                .ForeignKey("dbo.Lookup", t => t.ElectricityMeterTypeId)
                .ForeignKey("dbo.User", t => t.PartialApprovedByUserId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .ForeignKey("dbo.User", t => t.UpdatedByUserId)
                .Index(t => t.SiteId)
                .Index(t => t.ElectricityMeterTypeId)
                .Index(t => t.ElectricityMeterSubTypeId)
                .Index(t => t.ElectricityMeterPaymentSubTypeId)
                .Index(t => t.ElectricityMeterDistributionId)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.UpdatedByUserId)
                .Index(t => t.PartialApprovedByUserId)
                .Index(t => t.ApprovedByUserId);
            
            CreateTable(
                "dbo.ElectricityBill",
                c => new
                    {
                        BillId = c.Int(nullable: false, identity: true),
                        ElectricityMeterId = c.Int(nullable: false),
                        ElectricityBillCode = c.String(nullable: false, maxLength: 200),
                        OldReading = c.String(nullable: false, maxLength: 200),
                        NewReading = c.String(nullable: false, maxLength: 200),
                        BillDate = c.DateTime(nullable: false, storeType: "date"),
                        Consumption = c.String(nullable: false, maxLength: 200),
                        Amount = c.Decimal(nullable: false, precision: 10, scale: 3, storeType: "numeric"),
                        FlagDeleted = c.Boolean(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedByUserId = c.Int(nullable: false),
                        LastUpdatedDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.BillId)
                .ForeignKey("dbo.User", t => t.CreatedByUserId)
                .ForeignKey("dbo.ElectricityMeter", t => t.ElectricityMeterId)
                .ForeignKey("dbo.User", t => t.UpdatedByUserId)
                .Index(t => t.ElectricityMeterId)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.UpdatedByUserId);
            
            CreateTable(
                "dbo.ElectricityMeterTechnology",
                c => new
                    {
                        ElectricityMeterId = c.Int(nullable: false),
                        TechnologyUsedId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.ElectricityMeterId, t.TechnologyUsedId })
                .ForeignKey("dbo.ElectricityMeter", t => t.ElectricityMeterId)
                .ForeignKey("dbo.TechnologyUsed", t => t.TechnologyUsedId)
                .Index(t => t.ElectricityMeterId)
                .Index(t => t.TechnologyUsedId);
            
            CreateTable(
                "dbo.TechnologyUsed",
                c => new
                    {
                        TechnologyUsedId = c.Int(nullable: false, identity: true),
                        DisplayName = c.String(nullable: false),
                        AverageCost = c.Decimal(nullable: false, precision: 10, scale: 3, storeType: "numeric"),
                    })
                .PrimaryKey(t => t.TechnologyUsedId);
            
            CreateTable(
                "dbo.ElectricityTheft",
                c => new
                    {
                        TheftId = c.Int(nullable: false),
                        SiteId = c.Int(nullable: false),
                        ImageFilePth = c.String(maxLength: 200),
                        TheftWay = c.String(),
                        TakenAction = c.String(),
                    })
                .PrimaryKey(t => t.TheftId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.SiteId);
            
            CreateTable(
                "dbo.Owner",
                c => new
                    {
                        OwnerId = c.Int(nullable: false, identity: true),
                        SiteId = c.Int(nullable: false),
                        FullName = c.String(nullable: false),
                        PrimaryPhone = c.String(nullable: false, maxLength: 50),
                        SecoundaryPhone = c.String(maxLength: 50),
                        Email = c.String(maxLength: 50),
                        BankName = c.String(maxLength: 200),
                        BankBranch = c.String(maxLength: 200),
                        AccountNumber = c.String(maxLength: 200),
                        IBAN = c.String(maxLength: 200),
                        IsPay = c.Boolean(nullable: false),
                        Percentage = c.Decimal(nullable: false, precision: 10, scale: 3, storeType: "numeric"),
                        Note = c.String(),
                        OwnerCode = c.String(maxLength: 200),
                        IsInventoryOfLegacy = c.Boolean(nullable: false),
                        InventoryOfLegacyFilePath = c.String(maxLength: 200),
                        PercentageOfMoneyReceived = c.Decimal(nullable: false, precision: 10, scale: 3),
                        NationalityId = c.Int(),
                        NationalId = c.String(),
                        FlagDeleted = c.Boolean(nullable: false),
                        CreatedByUserId = c.Int(nullable: false),
                        CreatedDate = c.DateTime(nullable: false),
                        UpdatedByUserId = c.Int(nullable: false),
                        LastUpdatedDate = c.DateTime(nullable: false),
                        OriginalId = c.Int(),
                        ApprovalStatusId = c.Int(nullable: false),
                        PartialApprovedByUserId = c.Int(),
                        PartialApprovedDate = c.DateTime(),
                        ApprovedByUserId = c.Int(),
                        ApprovedDate = c.DateTime(),
                    })
                .PrimaryKey(t => t.OwnerId)
                .ForeignKey("dbo.User", t => t.ApprovedByUserId)
                .ForeignKey("dbo.User", t => t.CreatedByUserId)
                .ForeignKey("dbo.Nationality", t => t.NationalityId)
                .ForeignKey("dbo.User", t => t.PartialApprovedByUserId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .ForeignKey("dbo.User", t => t.UpdatedByUserId)
                .Index(t => t.SiteId)
                .Index(t => t.NationalityId)
                .Index(t => t.CreatedByUserId)
                .Index(t => t.UpdatedByUserId)
                .Index(t => t.PartialApprovedByUserId)
                .Index(t => t.ApprovedByUserId);
            
            CreateTable(
                "dbo.Nationality",
                c => new
                    {
                        NationalityId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.NationalityId);
            
            CreateTable(
                "dbo.Group",
                c => new
                    {
                        GroupId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.GroupId);
            
            CreateTable(
                "dbo.SiteGroup",
                c => new
                    {
                        GroupId = c.Int(nullable: false),
                        SiteId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.GroupId, t.SiteId })
                .ForeignKey("dbo.Group", t => t.GroupId)
                .ForeignKey("dbo.Site", t => t.SiteId)
                .Index(t => t.GroupId)
                .Index(t => t.SiteId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SiteGroup", "SiteId", "dbo.Site");
            DropForeignKey("dbo.SiteGroup", "GroupId", "dbo.Group");
            DropForeignKey("dbo.Contract", "UpdatedByUserId", "dbo.User");
            DropForeignKey("dbo.Site", "UpdatedByUserId", "dbo.User");
            DropForeignKey("dbo.Site", "TXSiteTypeId", "dbo.Lookup");
            DropForeignKey("dbo.Site", "SiteTypeId", "dbo.Lookup");
            DropForeignKey("dbo.Site", "PartialApprovedByUserId", "dbo.User");
            DropForeignKey("dbo.Owner", "UpdatedByUserId", "dbo.User");
            DropForeignKey("dbo.Owner", "SiteId", "dbo.Site");
            DropForeignKey("dbo.Owner", "PartialApprovedByUserId", "dbo.User");
            DropForeignKey("dbo.Owner", "NationalityId", "dbo.Nationality");
            DropForeignKey("dbo.Owner", "CreatedByUserId", "dbo.User");
            DropForeignKey("dbo.Owner", "ApprovedByUserId", "dbo.User");
            DropForeignKey("dbo.Site", "LocationSubTypeId", "dbo.Lookup");
            DropForeignKey("dbo.ElectricityTheft", "SiteId", "dbo.Site");
            DropForeignKey("dbo.ElectricityMeter", "UpdatedByUserId", "dbo.User");
            DropForeignKey("dbo.ElectricityMeterTechnology", "TechnologyUsedId", "dbo.TechnologyUsed");
            DropForeignKey("dbo.ElectricityMeterTechnology", "ElectricityMeterId", "dbo.ElectricityMeter");
            DropForeignKey("dbo.ElectricityMeter", "SiteId", "dbo.Site");
            DropForeignKey("dbo.ElectricityMeter", "PartialApprovedByUserId", "dbo.User");
            DropForeignKey("dbo.ElectricityMeter", "ElectricityMeterTypeId", "dbo.Lookup");
            DropForeignKey("dbo.ElectricityMeter", "ElectricityMeterSubTypeId", "dbo.Lookup");
            DropForeignKey("dbo.ElectricityMeter", "ElectricityMeterPaymentSubTypeId", "dbo.Lookup");
            DropForeignKey("dbo.ElectricityMeter", "ElectricityMeterDistributionId", "dbo.Lookup");
            DropForeignKey("dbo.ElectricityBill", "UpdatedByUserId", "dbo.User");
            DropForeignKey("dbo.ElectricityBill", "ElectricityMeterId", "dbo.ElectricityMeter");
            DropForeignKey("dbo.ElectricityBill", "CreatedByUserId", "dbo.User");
            DropForeignKey("dbo.ElectricityMeter", "CreatedByUserId", "dbo.User");
            DropForeignKey("dbo.ElectricityMeter", "ApprovedByUserId", "dbo.User");
            DropForeignKey("dbo.Site", "CreatedByUserId", "dbo.User");
            DropForeignKey("dbo.Contract", "SiteId", "dbo.Site");
            DropForeignKey("dbo.Site", "ApprovedByUserId", "dbo.User");
            DropForeignKey("dbo.Contract", "PaymentTypeId", "dbo.Lookup");
            DropForeignKey("dbo.Contract", "PartialApprovedByUserId", "dbo.User");
            DropForeignKey("dbo.Contract", "CreatedByUserId", "dbo.User");
            DropForeignKey("dbo.ContractYearlyRent", "ContractId", "dbo.Contract");
            DropForeignKey("dbo.Contract", "ApprovedByUserId", "dbo.User");
            DropForeignKey("dbo.User", "RoleId", "dbo.Role");
            DropForeignKey("dbo.Contract", "AcquiredById", "dbo.Lookup");
            DropForeignKey("dbo.Lookup", "LookupTypeId", "dbo.LookupType");
            DropIndex("dbo.SiteGroup", new[] { "SiteId" });
            DropIndex("dbo.SiteGroup", new[] { "GroupId" });
            DropIndex("dbo.Owner", new[] { "ApprovedByUserId" });
            DropIndex("dbo.Owner", new[] { "PartialApprovedByUserId" });
            DropIndex("dbo.Owner", new[] { "UpdatedByUserId" });
            DropIndex("dbo.Owner", new[] { "CreatedByUserId" });
            DropIndex("dbo.Owner", new[] { "NationalityId" });
            DropIndex("dbo.Owner", new[] { "SiteId" });
            DropIndex("dbo.ElectricityTheft", new[] { "SiteId" });
            DropIndex("dbo.ElectricityMeterTechnology", new[] { "TechnologyUsedId" });
            DropIndex("dbo.ElectricityMeterTechnology", new[] { "ElectricityMeterId" });
            DropIndex("dbo.ElectricityBill", new[] { "UpdatedByUserId" });
            DropIndex("dbo.ElectricityBill", new[] { "CreatedByUserId" });
            DropIndex("dbo.ElectricityBill", new[] { "ElectricityMeterId" });
            DropIndex("dbo.ElectricityMeter", new[] { "ApprovedByUserId" });
            DropIndex("dbo.ElectricityMeter", new[] { "PartialApprovedByUserId" });
            DropIndex("dbo.ElectricityMeter", new[] { "UpdatedByUserId" });
            DropIndex("dbo.ElectricityMeter", new[] { "CreatedByUserId" });
            DropIndex("dbo.ElectricityMeter", new[] { "ElectricityMeterDistributionId" });
            DropIndex("dbo.ElectricityMeter", new[] { "ElectricityMeterPaymentSubTypeId" });
            DropIndex("dbo.ElectricityMeter", new[] { "ElectricityMeterSubTypeId" });
            DropIndex("dbo.ElectricityMeter", new[] { "ElectricityMeterTypeId" });
            DropIndex("dbo.ElectricityMeter", new[] { "SiteId" });
            DropIndex("dbo.Site", new[] { "ApprovedByUserId" });
            DropIndex("dbo.Site", new[] { "PartialApprovedByUserId" });
            DropIndex("dbo.Site", new[] { "UpdatedByUserId" });
            DropIndex("dbo.Site", new[] { "CreatedByUserId" });
            DropIndex("dbo.Site", new[] { "TXSiteTypeId" });
            DropIndex("dbo.Site", new[] { "LocationSubTypeId" });
            DropIndex("dbo.Site", new[] { "SiteTypeId" });
            DropIndex("dbo.ContractYearlyRent", new[] { "ContractId" });
            DropIndex("dbo.User", new[] { "RoleId" });
            DropIndex("dbo.Lookup", new[] { "LookupTypeId" });
            DropIndex("dbo.Contract", new[] { "ApprovedByUserId" });
            DropIndex("dbo.Contract", new[] { "PartialApprovedByUserId" });
            DropIndex("dbo.Contract", new[] { "UpdatedByUserId" });
            DropIndex("dbo.Contract", new[] { "CreatedByUserId" });
            DropIndex("dbo.Contract", new[] { "AcquiredById" });
            DropIndex("dbo.Contract", new[] { "PaymentTypeId" });
            DropIndex("dbo.Contract", new[] { "SiteId" });
            DropTable("dbo.SiteGroup");
            DropTable("dbo.Group");
            DropTable("dbo.Nationality");
            DropTable("dbo.Owner");
            DropTable("dbo.ElectricityTheft");
            DropTable("dbo.TechnologyUsed");
            DropTable("dbo.ElectricityMeterTechnology");
            DropTable("dbo.ElectricityBill");
            DropTable("dbo.ElectricityMeter");
            DropTable("dbo.Site");
            DropTable("dbo.ContractYearlyRent");
            DropTable("dbo.Role");
            DropTable("dbo.User");
            DropTable("dbo.LookupType");
            DropTable("dbo.Lookup");
            DropTable("dbo.Contract");
        }
    }
}
