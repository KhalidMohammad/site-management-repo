namespace SiteManagement.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class addsitegroup : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Site", "SiteGroup", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Site", "SiteGroup");
        }
    }
}
