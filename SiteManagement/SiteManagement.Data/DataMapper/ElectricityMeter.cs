using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteManagement.Data
{
    [Table("ElectricityMeter")]
    public partial class ElectricityMeter : ISavedEntity, IApprovalEntity, IAuditEntity
    {
        [Key]
        public int ElectricityMeterId { get; set; }

        public int SiteId { get; set; }

        public virtual Site Site { get; set; }

        [Required]
        [StringLength(200)]
        public string ElectricityMeterCode { get; set; }

        public int ElectricityMeterTypeId { get; set; }

        public virtual Lookup ElectricityMeterType { get; set; }

        public int? ElectricityMeterSubTypeId { get; set; }

        public virtual Lookup ElectricityMeterSubType { get; set; }

        public int? ElectricityMeterPaymentSubTypeId { get; set; }

        public virtual Lookup ElectricityMeterPaymentSubType { get; set; }

        public int? ElectricityMeterDistributionId { get; set; }

        public virtual Lookup ElectricityMeterDistribution { get; set; }

        [Column(TypeName = "numeric")]
        public decimal EstimatePowerConsumption { get; set; }

        public bool IsActive { get; set; }

        [Column(TypeName = "date")]
        public DateTime? ActivationDate { get; set; }

        public bool MeterShield { get; set; }

        public bool BoxInstallation { get; set; }

        public bool PipesFilling { get; set; }

        [StringLength(200)]
        public string PipesFillingSupplierName { get; set; }

        public bool USence { get; set; }

        [StringLength(200)]
        public string USenceSupplierName { get; set; }

        public virtual ICollection<ElectricityBill> ElectricityBills { get; set; }

        public virtual IList<ElectricityMeterTechnology> TechnologiesUsed { get; set; }


        #region ISavedEntity Properties

        public bool FlagDeleted { get; set; }

        public int CreatedByUserId { get; set; }

        public virtual User CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UpdatedByUserId { get; set; }

        public virtual User UpdatedBy { get; set; }

        public DateTime LastUpdatedDate { get; set; }

        #endregion ISavedEntity Properties


        #region IApprovalEntity Properties

        public int? OriginalId { get; set; }

        public int ApprovalStatusId { get; set; }

        public int? PartialApprovedByUserId { get; set; }

        public virtual User PartialApprovedBy { get; set; }

        public DateTime? PartialApprovedDate { get; set; }

        public int? ApprovedByUserId { get; set; }

        public virtual User ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int Id
        {
            get
            {
                return ElectricityMeterId;
            }
        }

        #endregion IApprovalEntity Properties
    }
}
