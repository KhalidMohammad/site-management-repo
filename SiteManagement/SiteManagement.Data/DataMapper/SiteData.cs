using System;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Data.Entity.Infrastructure;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SiteManagement.Data
{
    public partial class SiteData : DbContext
    {
        public SiteData()
            : base("name=SiteData")
        {
        }



        // This is overridden to prevent someone from calling SaveChanges without specifying the user making the change
        public override int SaveChanges()
        {
            return SaveChanges(SharedEvent.GetCurrentUser().UserId.ToString());
        }

        public int SaveChanges(string userId)
        {

            //get changeSetId 


            Lazy<int> lazyChangeSetId = new Lazy<int>(() =>
            {
                int changeSetId = 0;
                SiteDataManager.CallDBContext(db => { changeSetId = db.AuditLogs.Any() ? db.AuditLogs.Max(a => a.ChangeSetId) : 0; });
                changeSetId++;
                return changeSetId;
            });

            DateTime changeSetDate = DateTime.Now;
            List<Tuple<AuditLog, DbEntityEntry>> addedLogs = new List<Tuple<AuditLog, DbEntityEntry>>();
            List<AuditLog> allLogsExceptAddLogs = new List<AuditLog>();
            // Get all Deleted/Modified entities (not Unmodified or Detached)
            foreach (var ent in this.ChangeTracker.Entries().Where(p => (p.Entity is IAuditEntity) &&
                (p.State == EntityState.Deleted || p.State == EntityState.Modified || p.State == EntityState.Added)))
            {
                // For each changed record, get the audit record entries and add them
                foreach (AuditLog x in GetAuditRecordsForChange(ent, userId, lazyChangeSetId.Value, changeSetDate))
                {
                    if (x.EventType == "A")
                    {
                        addedLogs.Add(new Tuple<AuditLog, DbEntityEntry>(x, ent));
                    }
                    else
                    {
                        allLogsExceptAddLogs.Add(x);
                    }
                }
            }

            int returnValue = base.SaveChanges();


            bool isLogsContains = false;
            if (addedLogs.Count > 0)
            {
                isLogsContains = true;
                // Get all Added entities (not Unmodified or Detached)
                foreach (var x in addedLogs)
                {
                    x.Item1.RecordId = GetPrimaryKey(x.Item2);
                    this.AuditLogs.Add(x.Item1);
                }
            }
            if (allLogsExceptAddLogs.Count > 0)
            {
                isLogsContains = true;
                this.AuditLogs.AddRange(allLogsExceptAddLogs);
            }

            if (isLogsContains) base.SaveChangesAsync();


            // Call the original SaveChanges(), which will save both the changes made and the audit records
            return returnValue;
        }

        private string GetPrimaryKey(DbEntityEntry dbEntry, bool isDelete = false)
        {
            // Get primary key value (If you have more than one key column, this will need to be adjusted)
            string keyName = String.Empty;


            var keyNameProp = dbEntry.Entity.GetType().GetProperties().FirstOrDefault(p => p.GetCustomAttributes(typeof(KeyAttribute), false).Count() > 0);
            if (keyNameProp != null)
            {
                keyName = keyNameProp.Name;
            }

            return String.IsNullOrWhiteSpace(keyName) ? null : (isDelete ? dbEntry.OriginalValues : dbEntry.CurrentValues).GetValue<object>(keyName).ToString();
        }
        private List<AuditLog> GetAuditRecordsForChange(DbEntityEntry dbEntry, string userId, int changeSetId, DateTime changeSetDate)
        {
            List<AuditLog> result = new List<AuditLog>();


            // Get the Table() attribute, if one exists
            TableAttribute tableAttr = dbEntry.Entity.GetType().GetCustomAttributes(typeof(TableAttribute), false).SingleOrDefault() as TableAttribute;

            // Get table name (if it has a Table attribute, use that, otherwise get the pluralized name)
            string tableName = tableAttr != null ? tableAttr.Name : dbEntry.Entity.GetType().Name;


            if (dbEntry.State == EntityState.Added)
            {

                foreach (string propertyName in dbEntry.CurrentValues.PropertyNames)
                {
                    // For Inserts, just add the whole record

                    result.Add(new AuditLog
                    {
                        UserID = userId,
                        EventDate = changeSetDate,
                        EventType = "A",    // added
                        TableName = tableName,
                        RecordId = GetPrimaryKey(dbEntry),
                        ColumnName = propertyName,
                        NewValue = dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString(),
                        ChangeSetId = changeSetId
                    });
                }

            }
            else if (dbEntry.State == EntityState.Deleted)
            {
                // Same with deletes, do the whole record, and use either the description from Describe() or ToString()
                result.Add(new AuditLog()
                {
                    //AuditLogID = Guid.NewGuid(),
                    UserID = userId,
                    EventDate = changeSetDate,
                    EventType = "D", // Deleted
                    TableName = tableName,
                    RecordId = GetPrimaryKey(dbEntry, true),
                    ColumnName = "*ALL",
                    NewValue = dbEntry.OriginalValues.ToObject().ToString(),
                    ChangeSetId = changeSetId

                }
                    );
            }
            else if (dbEntry.State == EntityState.Modified)
            {
                foreach (string propertyName in dbEntry.OriginalValues.PropertyNames)
                {
                    // For updates, we only want to capture the columns that actually changed
                    if (!object.Equals(dbEntry.OriginalValues.GetValue<object>(propertyName), dbEntry.CurrentValues.GetValue<object>(propertyName)))
                    {
                        result.Add(new AuditLog
                        {
                            //AuditLogID = Guid.NewGuid(),
                            UserID = userId,
                            EventDate = changeSetDate,
                            EventType = "M",    // Modified
                            TableName = tableName,
                            RecordId = GetPrimaryKey(dbEntry),
                            ColumnName = propertyName,
                            OriginalValue = dbEntry.OriginalValues.GetValue<object>(propertyName) == null ? null : dbEntry.OriginalValues.GetValue<object>(propertyName).ToString(),
                            NewValue = dbEntry.CurrentValues.GetValue<object>(propertyName) == null ? null : dbEntry.CurrentValues.GetValue<object>(propertyName).ToString(),
                            ChangeSetId = changeSetId

                        });
                    }
                }
            }
            // Otherwise, don't do anything, we don't care about Unchanged or Detached entities

            return result;
        }

        public virtual DbSet<AuditLog> AuditLogs { get; set; }

        public virtual DbSet<Contract> Contracts { get; set; }
        public virtual DbSet<ContractYearlyRent> ContractYearlyRents { get; set; }
        public virtual DbSet<ElectricityBill> ElectricityBills { get; set; }
        public virtual DbSet<ElectricityMeter> ElectricityMeters { get; set; }
        public virtual DbSet<TechnologyUsed> TechnologiesUsed { get; set; }
        public virtual DbSet<ElectricityMeterTechnology> ElectricityMeterTechnologies { get; set; }
        public virtual DbSet<ElectricityTheft> ElectricityThefts { get; set; }
        public virtual DbSet<Lookup> Lookups { get; set; }
        public virtual DbSet<LookupType> LookupTypes { get; set; }
        public virtual DbSet<Owner> Owners { get; set; }
        public virtual DbSet<Site> Sites { get; set; }
        public virtual DbSet<SiteGroup> SitesGroup { get; set; }
        public virtual DbSet<Group> Groups { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Nationality> Nationalities { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<SiteData>(null);

            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            //modelBuilder. = false;
            //modelBuilder.Entity<Contract>()
            //    .Property(e => e.YearlyRent)
            //    .HasPrecision(10, 3);

            modelBuilder.Entity<Contract>()
                .Property(e => e.IncrementalPercentage)
                .HasPrecision(10, 3);

            modelBuilder.Entity<ContractYearlyRent>()
                .Property(e => e.Amount)
                .HasPrecision(10, 3);

            modelBuilder.Entity<Contract>()
                .Property(e => e.MaarefTax)
                .HasPrecision(10, 3);

            modelBuilder.Entity<Owner>()
                .Property(e => e.Percentage)
                .HasPrecision(10, 3);

            modelBuilder.Entity<Owner>()
                .Property(e => e.PercentageOfMoneyReceived)
                .HasPrecision(10, 3);

            modelBuilder.Entity<ElectricityBill>()
                .Property(e => e.Amount)
                .HasPrecision(10, 3);

            modelBuilder.Entity<ElectricityMeter>()
                .Property(e => e.EstimatePowerConsumption)
                .HasPrecision(10, 3);

            modelBuilder.Entity<TechnologyUsed>()
                .Property(e => e.AverageCost)
                .HasPrecision(10, 3);

            //modelBuilder.Entity<ElectricityMeter>()
            //    .HasMany(e => e.ElectricityBills)
            //    .WithRequired(e => e.ElectricityMeter)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<TechnologyUsed>()
            //    .HasMany(e => e.ElectricityMeterTechnologies)
            //    .WithRequired(e => e.TechnologyUsed)
            //    .HasForeignKey(e => e.TechnologyUsedId)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<TechnologyUsed>()
            //    .Property(e => e.DisplayName).IsRequired();

            //modelBuilder.Entity<Lookup>()
            //    .HasMany(e => e.ElectricityMeters)
            //    .WithRequired(e => e.ElectricityMeterTypeLookup)
            //    .HasForeignKey(e => e.ElectricityMeterType)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Lookup>()
            //    .HasMany(e => e.ElectricityMeters1)
            //    .WithRequired(e => e.TechnologyUsedLookup)
            //    .HasForeignKey(e => e.TechnologyUsed)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Lookup>()
            //    .HasMany(e => e.Sites)
            //    .WithRequired(e => e.Lookup)
            //    .HasForeignKey(e => e.SiteType)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Site>()
            //    .HasMany(e => e.Contracts)
            //    .WithRequired(e => e.Site)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Site>()
            //    .HasMany(e => e.ElectricityThefts)
            //    .WithRequired(e => e.Site)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Site>()
            //    .HasMany(e => e.ElectricityMeters)
            //    .WithRequired(e => e.Site)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<Site>()
            //    .HasMany(e => e.Owners)
            //    .WithRequired(e => e.Site)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Contracts)
            //    .WithRequired(e => e.CreatedBy)
            //    .HasForeignKey(e => e.CreatedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Contracts1)
            //    .WithRequired(e => e.UpdatedBy)
            //    .HasForeignKey(e => e.UpdatedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.ElectricityBills)
            //    .WithRequired(e => e.CreatedBy)
            //    .HasForeignKey(e => e.CreatedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.ElectricityBills1)
            //    .WithRequired(e => e.UpdatedBy)
            //    .HasForeignKey(e => e.UpdatedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.ElectricityMeters)
            //    .WithRequired(e => e.CreatedBy)
            //    .HasForeignKey(e => e.CreatedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.ElectricityMeters1)
            //    .WithRequired(e => e.UpdatedBy)
            //    .HasForeignKey(e => e.UpdatedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Owners)
            //    .WithRequired(e => e.CreatedBy)
            //    .HasForeignKey(e => e.CreatedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Owners1)
            //    .WithRequired(e => e.UpdatedBy)
            //    .HasForeignKey(e => e.UpdatedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Sites)
            //    .WithRequired(e => e.UpdatedBy)
            //    .HasForeignKey(e => e.UpdatedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Sites)
            //    .WithRequired(e => e.PartialApprovedBy)
            //    .HasForeignKey(e => e.PartialApprovedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Sites)
            //    .WithRequired(e => e.ApprovedBy)
            //    .HasForeignKey(e => e.ApprovedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Contracts)
            //    .WithRequired(e => e.PartialApprovedBy)
            //    .HasForeignKey(e => e.PartialApprovedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Contracts)
            //    .WithRequired(e => e.ApprovedBy)
            //    .HasForeignKey(e => e.ApprovedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Owners)
            //    .WithRequired(e => e.PartialApprovedBy)
            //    .HasForeignKey(e => e.PartialApprovedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Owners)
            //    .WithRequired(e => e.ApprovedBy)
            //    .HasForeignKey(e => e.ApprovedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.ElectricityMeters)
            //    .WithRequired(e => e.PartialApprovedBy)
            //    .HasForeignKey(e => e.PartialApprovedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.ElectricityMeters)
            //    .WithRequired(e => e.ApprovedBy)
            //    .HasForeignKey(e => e.ApprovedById)
            //    .WillCascadeOnDelete(false);

            //modelBuilder.Entity<User>()
            //    .HasMany(e => e.Sites1)
            //    .WithRequired(e => e.CreatedBy)
            //    .HasForeignKey(e => e.CreatedById)
            //    .WillCascadeOnDelete(false);
        }
    }
}
