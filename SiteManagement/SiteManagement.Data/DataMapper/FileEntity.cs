﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    public class FileEntity
    {
        public string Name { get; set; }

        public string GeneratedName { get; set; }

        public string Path { get; set; }

        public string Content { get; set; }
    }
}
