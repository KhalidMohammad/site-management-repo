﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    [Table("TechnologyUsed")]
    public class TechnologyUsed
    {
        [Key]
        public int TechnologyUsedId { get; set; }

        [Required]
        public string DisplayName { get; set; }

        [Column(TypeName = "numeric")]
        public decimal AverageCost { get; set; }

        public virtual ICollection<ElectricityMeterTechnology> ElectricityMeterTechnologies { get; set; }
    }
}
