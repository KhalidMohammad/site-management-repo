using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteManagement.Data
{
    [Table("Site")]
    public partial class Site : ISavedEntity, IApprovalEntity, IAuditEntity
    {
        [Key]
        public int SiteId { get; set; }

        /// <summary>
        /// save site group for logging only
        /// </summary>
        public string SiteGroup { get; set; }

        [Required]
        [StringLength(100)]
        public string SiteCode { get; set; }

        [StringLength(200)]
        public string SiteName { get; set; }

        [StringLength(200)]
        public string SiteCity { get; set; }

        [StringLength(200)]
        public string SiteGovernorates { get; set; }

        [Required]
        [StringLength(50)]
        public string Latitude { get; set; }

        [Required]
        [StringLength(50)]
        public string Longitude { get; set; }

        [StringLength(200)]
        public string StreetName { get; set; }

        [StringLength(200)]
        public string SiteAddress { get; set; }

        public bool IsActive { get; set; }

        public int SiteTypeId { get; set; }

        public virtual Lookup SiteType { get; set; }

        public string SiteNote { get; set; }

        public bool MunicipalityPermission { get; set; }

        public int? LocationSubTypeId { get; set; }

        public virtual Lookup LocationSubType { get; set; }

        public string LocationSubTypeNote { get; set; }

        [Column(TypeName = "date")]
        public DateTime? MunicipalityPermissionDate { get; set; }

        public string MunicipalityPermissionPicturePath { get; set; }

        [NotMapped]
        public List<FileEntity> MunicipalityPermissionPicture { get; set; }

        [NotMapped]
        public List<string> PendingFields { get; set; }

        public int? TXSiteTypeId { get; set; }

        public virtual Lookup TXSiteType { get; set; }

        public bool PayRent { get; set; }

        public string PhaseNumber { get; set; }

        [Column(TypeName = "date")]
        public DateTime? OnAirDate { get; set; }

        public string SiteCodePrefix { get; set; }

        public virtual ICollection<Contract> Contracts { get; set; }

        public virtual ICollection<Owner> Owners { get; set; }

        public virtual ICollection<ElectricityMeter> ElectricityMeters { get; set; }

        public virtual ICollection<ElectricityTheft> ElectricityThefts { get; set; }


        #region ISavedEntity Properties

        public bool FlagDeleted { get; set; }

        public int CreatedByUserId { get; set; }

        public virtual User CreatedBy { get; set; }

        public DateTime CreatedDate { get; set; }

        public int UpdatedByUserId { get; set; }

        public virtual User UpdatedBy { get; set; }

        public DateTime LastUpdatedDate { get; set; }

        #endregion ISavedEntity Properties


        #region IApprovalEntity Properties

        public int? OriginalId { get; set; }

        public int ApprovalStatusId { get; set; }

        public int? PartialApprovedByUserId { get; set; }

        public virtual User PartialApprovedBy { get; set; }

        public DateTime? PartialApprovedDate { get; set; }

        public int? ApprovedByUserId { get; set; }

        public virtual User ApprovedBy { get; set; }

        public DateTime? ApprovedDate { get; set; }

        public int Id
        {
            get
            {
                return SiteId;
            }
        }

        #endregion IApprovalEntity Properties
    }
}
