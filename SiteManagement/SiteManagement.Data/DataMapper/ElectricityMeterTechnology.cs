﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteManagement.Data
{
    [Table("ElectricityMeterTechnology")]
    public class ElectricityMeterTechnology
    {
        [Key]
        [Column(Order = 1)]
        [Required]
        public int ElectricityMeterId { get; set; }

        public virtual ElectricityMeter ElectricityMeter { get; set; }

        [Key]
        [Column(Order = 2)]
        [Required]
        public int TechnologyUsedId { get; set; }

        public virtual TechnologyUsed TechnologyUsed { get; set; }
    }
}
