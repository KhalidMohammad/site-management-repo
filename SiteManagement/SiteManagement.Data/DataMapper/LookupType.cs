using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteManagement.Data
{
    [Table("LookupType")]
    public partial class LookupType
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int LookupTypeId { get; set; }

        [Required]
        [StringLength(50)]
        public string LookupTypeName { get; set; }
    }
}
