using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SiteManagement.Data
{
    [Table("Lookup")]
    public partial class Lookup
    {
        [Key]
        public int Id { get; set; }

        [Required]
        [StringLength(200)]
        public string DisplayName { get; set; }

        public int LookupTypeId { get; set; }

        public virtual LookupType LookupType { get; set; }
    }
}
