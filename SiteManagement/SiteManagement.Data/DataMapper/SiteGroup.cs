﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    [Table("SiteGroup")]
    public class SiteGroup
    {
        [Key]
        [Column(Order = 1)]
        [Required]
        public int GroupId { get; set; }

        public virtual Group Group { get; set; }

        [Key]
        [Column(Order = 2)]
        [Required]
        public int SiteId { get; set; }

        public virtual Site Site { get; set; }
    }
}
