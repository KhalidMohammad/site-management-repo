﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    [Table("ContractYearlyRent")]
    public class ContractYearlyRent
    {
        [Key, Column(Order = 0)]
        public int ContractId { get; set; }

        public virtual Contract Contract { get; set; }

        [Key, Column(Order = 1, TypeName = "date")]
        public DateTime FromDate { get; set; }

        [Key, Column(Order = 2, TypeName = "date")]
        public DateTime ToDate { get; set; }

        [Column(TypeName = "numeric")]
        public decimal Amount { get; set; }
    }
}
