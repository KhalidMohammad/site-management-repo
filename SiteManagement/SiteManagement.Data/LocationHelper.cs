﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SiteManagement.Data
{
    public static class LocationHelper
    {
        static string baseUri = @"http://maps.google.com/maps/api/geocode/xml?latlng={0},{1}&sensor=false&creditntial=AIzaSyAyyPZ_62snfVRtTCgBTrUt7JJiPUzEK2U";


        private static locationInfo RetrieveFormatedAddress(string lat, string lng, bool callRecursive = false)
        {
            locationInfo info = new locationInfo();
            try
            {
                string requestUri = string.Format(baseUri, lat, lng);
                string statusCode = String.Empty;
                using (WebClient wc = new WebClient())
                {
                    string result = wc.DownloadString(requestUri);
                    var xmlElm = XElement.Parse(result);
                    var status = (from elm in xmlElm.Descendants()
                                  where
                                            elm.Name == "status"
                                  select elm).FirstOrDefault();

                    statusCode = status.Value.ToLower();
                    if (statusCode == "ok")
                    {
                        var address = (from elm in xmlElm.Descendants()
                                       where elm.Name == "formatted_address"
                                       select elm).FirstOrDefault();

                        var city = (from elm in xmlElm.Descendants()
                                    where elm.Name == "type" &&
                                          elm.Value.Equals("locality", StringComparison.InvariantCultureIgnoreCase)
                                    select elm).FirstOrDefault();

                        if (city != null) city = city.Parent.Descendants().FirstOrDefault(p => p.Name == "long_name");

                        var route = (from elm in xmlElm.Descendants()
                                     where
                                        elm.Name == "type" &&
                                        elm.Value.Equals("route", StringComparison.InvariantCultureIgnoreCase)
                                     select elm).FirstOrDefault();
                        if (route != null) route = route.Parent.Descendants().FirstOrDefault(p => p.Name == "long_name");


                        var governorate = (from elm in xmlElm.Descendants()
                                           where
                                              elm.Name == "type" &&
                                              elm.Value.Equals("administrative_area_level_1", StringComparison.InvariantCultureIgnoreCase)
                                           select elm).FirstOrDefault();

                        if (governorate != null) governorate = governorate.Parent.Descendants().FirstOrDefault(p => p.Name == "long_name");

                        info.Address = address == null ? null : address.Value;
                        info.CityName = city == null ? null : city.Value;
                        info.StreetName = route == null ? null : route.Value;
                        info.Governorate = governorate == null ? null : governorate.Value;
                    }
                }
                if (callRecursive &&
                    statusCode.ToUpperInvariant() == "OVER_QUERY_LIMIT")
                {
                    Thread.Sleep(Convert.ToInt32(TimeSpan.FromSeconds(5).TotalMilliseconds));
                    return RetrieveFormatedAddress(lat, lng);
                }
            }
            catch { }
            return info;
        }

        private static bool IsEmptyOrRabish(string val)
        {
            return String.IsNullOrWhiteSpace(val) || val.Equals("-") || val.Equals("_");
        }

        public static void FillSiteInfo(Site site, bool callRecursive = false)
        {
            var info = new Lazy<locationInfo>(() => RetrieveFormatedAddress(site.Latitude, site.Longitude, callRecursive));
            if (IsEmptyOrRabish(site.SiteCity) && info.Value != null)
            {
                site.SiteCity = info.Value.CityName;
            }
            if (IsEmptyOrRabish(site.SiteAddress) && info.Value != null)
            {
                site.SiteAddress = info.Value.Address;
            }
            if (IsEmptyOrRabish(site.StreetName) && info.Value != null)
            {
                site.StreetName = info.Value.StreetName;
            }
            if (IsEmptyOrRabish(site.SiteGovernorates) && info.Value != null)
            {
                site.SiteGovernorates = info.Value.Governorate;
            }
        }
    }


    public class locationInfo
    {
        public string StreetName { get; set; }

        public string CityName { get; set; }

        public string Address { get; set; }

        public string Governorate { get; set; }

    }
}
