﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    public static class DataManager
    {

        public static IEnumerable<TReturnValue> CallDBContextEnumerable<TContext, TReturnValue>(Func<TContext, IEnumerable<TReturnValue>> dbCall)
                     where TContext : DbContext, new()
        {
            IEnumerable<TReturnValue> values = null;
            using (TContext context = new TContext())
            {
                values = dbCall(context).AsEnumerable();;
            }

            return values;
        }

        public static void CallDBContext<TContext>(Action<TContext> dbCall)
            where TContext : DbContext, new()
        {
            using (TContext context = new TContext())
            {
                dbCall(context);
            }
        }

    }
}
