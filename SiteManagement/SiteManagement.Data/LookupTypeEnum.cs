﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiteManagement.Data
{
    //public enum LookupTypeEnum
    //{
    //    Unknown = 0,

    //    SiteType = 1,

    //    ElectricityMeterType = 2,

    //    //TechnologyUsed = 3,

    //    LocationSubType = 4,

    //    TXSiteType = 5,

    //    PaymentType = 6,

    //    ElectricityMeterDistribution = 7,

    //    ElectricityMeterSubType = 8,

    //    ElectricityMeterPaymentSubType = 9,

    //    AcquiredBy = 10
    //}

    public enum LookupTypeEnum
    {
        Unknown = 0,

        SiteType = 1,

        ElectricityMeterType = 2,

        LocationSubType = 3,

        TXSiteType = 4,

        PaymentType = 5,

        ElectricityMeterDistribution = 6,

        ElectricityMeterSubType = 7,

        ElectricityMeterPaymentSubType = 8,

        AcquiredBy = 9
    }
}
