﻿using SiteManagement.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace SiteManagement.Helpers
{
    public static class ExtensionHelper
    {
        private static string GetBaseUrl()
        {
            var request = HttpContext.Current.Request;
            var appUrl = HttpRuntime.AppDomainAppVirtualPath;

            if (appUrl != "/")
                appUrl = "/" + appUrl;

            var baseUrl = string.Format("{0}://{1}{2}", request.Url.Scheme, request.Url.Authority, appUrl);

            return baseUrl;
        }

        public static string ToMapLocationPath(this Enum enumValue)
        {
            string path = string.Empty;

            Type type = enumValue.GetType();

            if (!type.IsEnum)
            {
                throw new ArgumentException("enumValue must be of Enum type", "enumValue");
            }

            FieldInfo fieldInfo = type.GetField(enumValue.ToString());

            if (fieldInfo == null || fieldInfo.GetCustomAttribute(typeof(MapLocationAttribute)) == null)
            {
                throw new ArgumentException("enumValue must contains the MapLocationAttribute", "enumValue");
            }

            MapLocationAttribute attribute = (MapLocationAttribute)fieldInfo.GetCustomAttribute(typeof(MapLocationAttribute));

            if (!String.IsNullOrEmpty(attribute.Path))
            {
                path = string.Format("{0}/{1}", GetBaseUrl(), attribute.Path);
            }

            return path;
        }

        public static string ToCustomString<T>(this T enumValue) where T : struct
        {
            string description = string.Empty;

            Type type = enumValue.GetType();

            if (!type.IsEnum)
            {
                throw new ArgumentException("enumValue must be of Enum type", "enumValue");
            }

            if (type.GetCustomAttribute(typeof(FlagsAttribute)) != null)
            {
                string[] values = enumValue.ToString().Split(',');

                description = String.Join(",", values);
            }

            return description;
        }
    }
}