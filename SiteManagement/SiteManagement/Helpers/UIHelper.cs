﻿using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Helpers
{
    public static class UIHelper
    {
        public static string RenderRazorViewToHtml(ControllerContext context, string viewName, object model)
        {
            context.Controller.ViewData.Model = model;

            using (StringWriter stringWriter = new StringWriter())
            {
                ViewEngineResult viewResult =
                    ViewEngines.Engines.FindPartialView(context, viewName);

                ViewContext viewContext =
                    new ViewContext(context, viewResult.View, context.Controller.ViewData, context.Controller.TempData, stringWriter);

                viewResult.View.Render(viewContext, stringWriter);
                viewResult.ViewEngine.ReleaseView(context, viewResult.View);

                return stringWriter.GetStringBuilder().ToString();
            }
        }

        public static IEnumerable<SelectListItem> ReflectSelectItemFromLookup(IEnumerable<Lookup> lookups)
        {
            return lookups.Select(ReflectSelectItemFromLookup).AsEnumerable();
        }

        public static SelectListItem ReflectSelectItemFromLookup(Lookup lookup)
        {
            return new SelectListItem
            {
                Value = lookup.Id.ToString(),
                Text = lookup.DisplayName
            };
        }

    }
}