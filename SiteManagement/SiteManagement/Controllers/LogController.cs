﻿using SiteManagement.Attributes.Authorization;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.SuperAdmin)]
    public class LogController : AuthenticatedController
    {
        [HttpPost]
        public ActionResult GetLog(LogType logType, int id)
        {
            ActionResult result = null;

            try
            {
                LogModel model = new LogModel();

                SiteDataManager.CallDBContext(db =>
                {
                    switch (logType)
                    {
                        case LogType.Site:
                            model = db.Sites.Where(s => s.SiteId == id).First().ToLog();
                            break;

                        case LogType.Contract:
                            model = db.Contracts.Where(s => s.ContractId == id).First().ToLog();
                            break;

                        case LogType.Owner:
                            model = db.Owners.Where(s => s.OwnerId == id).First().ToLog();
                            break;

                        case LogType.ElectricityMeter:
                            model = db.ElectricityMeters.Where(s => s.ElectricityMeterId == id).First().ToLog();
                            break;
                    }
                });

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Shared/_PartialLog.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }
    }
}