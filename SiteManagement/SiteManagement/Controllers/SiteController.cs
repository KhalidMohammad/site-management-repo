﻿using Newtonsoft.Json;
using SiteManagement.Attributes.Authorization;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.Admin, RoleTypeEnum.SuperAdmin, RoleTypeEnum.Guest)]
    public class SiteController : AuthenticatedController
    {
        // GET: Site
        [HttpGet]
        public ActionResult Index()
        {
            ActionResult result = null;

            try
            {
                List<MapModel> list = new List<MapModel>();

                SiteDataManager.CallDBContext(db =>
                {
                    list = db.Sites.Where(s => !s.FlagDeleted && s.ApprovalStatusId != (int)ApprovalStatus.Rejected).ToMapListModel();
                });

                FillDropdownValues();
                ViewData["mapList"] = JsonConvert.SerializeObject(list);
                result = View();
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;
        }

        [HttpPost]
        public ActionResult GetAllMapLocations()
        {
            JsonResult result = null;

            try
            {
                List<MapModel> list = new List<MapModel>();

                SiteDataManager.CallDBContext(db =>
                {
                    list = db.Sites.Where(s => !s.FlagDeleted && s.ApprovalStatusId != (int)ApprovalStatus.Rejected).ToMapListModel();
                });

                ViewData["mapList"] = JsonConvert.SerializeObject(list);

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Site/_PartialMap.cshtml", null),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);

                result.MaxJsonLength = int.MaxValue;
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }


        [HttpPost]
        public ActionResult NewSite()
        {
            ActionResult result = null;

            try
            {
                FillDropdownValues();

                SiteModel model = new SiteModel();

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Site/_PartialFullSite.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult GetPendingSiteById(int siteId)
        {
            return GetSiteResultById(siteId, true, true);
        }

        [HttpPost]
        public ActionResult GetSiteById(int siteId)
        {
            return GetSiteResultById(siteId, false, false);
        }

        [HttpPost]
        public ActionResult ViewSiteById(int siteId)
        {
            return GetSiteResultById(siteId, true, false);
        }

        //private bool ValidateLongitude(string val)
        //{
        //    double dval;

        //    if (Double.TryParse(val, out dval) && dval < -180 || dval > 180)
        //    {
        //        return true;
        //    }

        //    return false;

        //}

        //private bool ValidateLatitude(string val)
        //{
        //    double dval;

        //    if (Double.TryParse(val, out dval) && dval > -90 && dval < 90)
        //    {
        //        return true;
        //    }

        //    return false;

        //}

        [HttpPost]
        public ActionResult SaveSite(SiteModel model)
        {
            ActionResult result = null;

            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    List<int> relatedSites;
                    Site site = SiteConverter.ToDatabaseEntity(model, out relatedSites);
                    //site.ApprovlStatusId = SiteManager.GetInsertStatusId();
                    SiteManager.FillApprovalStatus(site, site);

                    //fill related site code 
                    List<string> relatedSiteCodes = null;
                    if (relatedSites != null && relatedSites.Count > 0)
                    {
                        if (!relatedSites.Contains(site.SiteId))
                            relatedSites.Add(site.SiteId);

                        SiteDataManager.CallDBContext(db =>
                        {
                            relatedSiteCodes = db.Sites.Where(s => relatedSites.Contains(s.SiteId)).Select(s => s.SiteCode).ToList();
                        });

                        site.SiteGroup = string.Join(", ", relatedSiteCodes.ToArray());
                    }
                    else
                    {
                        site.SiteGroup = null;
                    }

                    //SiteManager.FillApprovalStatus(site, site);

                    if (model.SiteId == 0)
                    {
                        FillEntitySavedData(site, false);
                        model.SiteId = SiteManager.AddSite(site);
                    }
                    else
                    {
                        if (site.ApprovalStatusId != (int)ApprovalStatus.Approved)
                        {
                            FillEntitySavedData(site, false);
                            site.OriginalId = site.SiteId;
                            site.SiteId = 0;
                            model.SiteId = SiteManager.AddSite(site);
                        }
                        else
                        {
                            FillEntitySavedData(site, true);
                            SiteManager.UpdateSite(site);
                        }
                    }
                    if (relatedSites != null)
                    {
                        if (!relatedSites.Contains(site.SiteId))
                            relatedSites.Add(site.SiteId);

                        SiteManager.AddUpdateSiteGroup(relatedSites);
                    }
                    result = Json(new
                    {
                        MessageType = MessageType.Success,
                        SiteId = model.SiteId
                    }, JsonRequestBehavior.AllowGet);
                }

            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error, MessageText = ex.Message }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        //[HttpGet]
        //public FileResult DownloadFile(string filePath)
        //{
        //    try
        //    {
        //        byte[] fileContent = FileManager.GetFile(filePath);
        //        if (fileContent != null)
        //        {
        //            string fileName = FileManager.GetFileNameFromPath(filePath);
        //            string contentType = MimeMapping.GetMimeMapping(fileName);
        //            return File(fileContent, contentType, fileName);
        //        }
        [HttpGet]
        [RoleAuthorize(RoleTypeEnum.Admin, RoleTypeEnum.SuperAdmin)]
        public ActionResult GetAllPendingApprovalSites()
        {
            ActionResult result = null;

            try
            {
                List<SiteApprovalModel> list = new List<SiteApprovalModel>();

                SiteDataManager.CallDBContext(db =>
                {

                    list = db.Sites.SqlQuery(String.Format(@"select distinct s.* 
                                                        from
	                                                         [site] s 
                                                        left join [Owner] o 
	                                                        on o.SiteId = s.SiteId 
                                                        left join [Contract] c
	                                                        on c.SiteId = s.SiteId
                                                        left join ElectricityMeter e 
	                                                        on e.SiteId = s.SiteId
	                                                        where s.ApprovalStatusId = {0} OR 
		                                                        o.ApprovalStatusId = {0} OR 
		                                                        c.ApprovalStatusId = {0} OR 
		                                                        e.ApprovalStatusId = {0} ", SiteManager.GetSelectStatusId())).AsEnumerable()
                            //list = db.Sites.AsEnumerable().Where(s =>
                            //        {
                            //            SiteManager.FillPendingApprovalFields(s, db);

                            //            return s.PendingFields.Any();
                            //        }

                            .Select(s =>
                            {
                                //SiteManager.FillPendingApprovalFields(s, db);
                                return SiteApprovalConverter.FromDatabaseApprovalEntity(s);
                            }).ToList();


                });

                //FillDropdownValues();

                ViewData["pendingSiteList"] = JsonConvert.SerializeObject(list);
                result = View("~/Views/Site/SitePendingApproval.cshtml");
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return result;

        }



        [RoleAuthorize(RoleTypeEnum.Admin, RoleTypeEnum.SuperAdmin)]
        [HttpPost]
        public ActionResult RejectSite(int id)
        {
            ActionResult result = null;

            try
            {
                SiteManager.ChangeSiteApprovalStatus(id, (int)ApprovalStatus.Rejected);
                //SiteDataManager.UpdateApprovalState<Site>(id, ApprovalStatus.Rejected);
                result = Json(new
                {
                    MessageType = MessageType.Success,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [RoleAuthorize(RoleTypeEnum.Admin, RoleTypeEnum.SuperAdmin)]
        [HttpPost]
        public ActionResult ApproveSite(int id)
        {
            ActionResult result = null;

            try
            {
                int stateid = (int)SiteManager.GetInsertStatusId();

                SiteManager.ChangeSiteApprovalStatus(id, stateid);
                //SiteDataManager.CallDBContext(db =>
                //{
                //    string commandtext = String.Format(@"
                //                        update [site] set ApprovlStatusId = {0} where siteid = {1} and ApprovlStatusId = {2} AND FlagDeleted = 0
                //                        update [Owner] set ApprovlStatusId = {0} where siteid = {1} and ApprovlStatusId = {2} AND FlagDeleted = 0
                //                        update [Contract] set ApprovlStatusId = {0} where siteid = {1} and ApprovlStatusId = {2} AND FlagDeleted = 0
                //                        update [ElectricityMeter] set ApprovlStatusId = {0} where siteid = {1} and ApprovlStatusId = {2} AND FlagDeleted = 0", stateid, id, SiteManager.GetSelectStatusId());

                //    db.Database.ExecuteSqlCommand(commandtext);
                //    db.SaveChanges();
                //});
                //SiteDataManager.UpdateApprovalState<Site>(id, GetApprovalStatusByCurrentUser());
                result = Json(new
                {
                    MessageType = MessageType.Success,
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }


        [HttpPost]
        public ActionResult GetFilterMapLocations(SiteFilterModel siteFilterModel)
        {
            JsonResult result = null;

            try
            {
                List<MapModel> list = MapSearchCriteria.GetSiteSearch(siteFilterModel);


                ViewData["mapList"] = JsonConvert.SerializeObject(list);

                result = Json(new
                {
                    IsHaveResult = list.Any(),
                    Content = list.Any() ? UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Site/_PartialMap.cshtml", null) : "",
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);

                result.MaxJsonLength = int.MaxValue;
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult GetAdvanceFilterView()
        {
            ActionResult result = null;

            try
            {
                // Contract Form Lookups
                ViewData["PaymentTypes"] = GetLookupSelectedItems(LookupTypeEnum.PaymentType);
                // Electricity Meter Form Lookups
                ViewData["ElectricityMeterTypes"] = GetLookupSelectedItems(LookupTypeEnum.ElectricityMeterType);

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Site/AdvanceFilter.cshtml", null),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult ExportFilterMapLocations(SiteFilterModel siteFilterModel)
        {
            ActionResult result = null;

            try
            {
                byte[] fileContent = MapSearchCriteria.ExportSiteSearch(siteFilterModel);
                TempData["ExportSheetResult"] = fileContent;

                result = Json(new
                {
                    Url = Url.Action("DownloadExportSheet"),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpGet]
        public FileResult DownloadExportSheet()
        {
            byte[] fileContent = TempData["ExportSheetResult"] as byte[];
            return File(fileContent, System.Net.Mime.MediaTypeNames.Application.Octet, "Filter Map By Site.xlsx");
        }

        #region Private Methods

        private ActionResult GetSiteResultById(int siteId, bool isReadOnly, bool loadPendingChanges = false)
        {
            ActionResult result = null;

            ViewData["IsReadOnly"] = isReadOnly;

            try
            {
                FillDropdownValues();

                SiteModel model = new SiteModel();

                SiteDataManager.CallDBContext(db =>
                {
                    Site site = db.Sites.Where(s => s.SiteId == siteId).SingleOrDefault();
                    if (site != null)
                    {
                        List<SiteGroup> siteGroups = null;
                        SiteGroup siteGroup = db.SitesGroup.FirstOrDefault(sg => sg.SiteId == site.SiteId);
                        if (siteGroup != null)
                            siteGroups = db.SitesGroup.Where(sg => sg.GroupId == siteGroup.GroupId).ToList();

                        if (loadPendingChanges)
                            SiteManager.FillPendingApprovalFields(site, db);

                        model = site.FromDatabaseEntity(siteGroups);
                    }
                });

                result = Json(new
                {
                    SiteName = model.SiteName,
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Site/_PartialFullSite.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        private IEnumerable<SelectListItem> GetAllSite()
        {
            IEnumerable<SelectListItem> allSite = null;
            SiteDataManager.CallDBContext(db =>
            {
                allSite = db.Sites.Select(s => new SelectListItem { Text = s.SiteCode, Value = s.SiteId.ToString() }).ToList();
            });
            return allSite;
        }

        private IEnumerable<SelectListItem> GetSiteTypes()
        {
            return GetLookupSelectedItems(LookupTypeEnum.SiteType);
        }

        private IEnumerable<SelectListItem> GetLocationSubTypes()
        {
            return GetLookupSelectedItems(LookupTypeEnum.LocationSubType);
        }

        private IEnumerable<SelectListItem> GetTXSiteTypes()
        {
            return GetLookupSelectedItems(LookupTypeEnum.TXSiteType);
        }

        private void FillDropdownValues()
        {
            ViewData["SiteTypes"] = GetSiteTypes();
            ViewData["LocationSubTypes"] = GetLocationSubTypes();
            ViewData["TXSiteTypes"] = GetTXSiteTypes();
            ViewData["Sites"] = GetAllSite();
        }

        #endregion Private Methods
    }
}