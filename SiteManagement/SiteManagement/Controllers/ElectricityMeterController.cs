﻿using Newtonsoft.Json;
using SiteManagement.Attributes.Authorization;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.Admin, RoleTypeEnum.SuperAdmin, RoleTypeEnum.Guest)]
    public class ElectricityMeterController : AuthenticatedController
    {
        private void FillDropdpownData()
        {
            IEnumerable<TechnologyUsed> technologyUsedList = GetAllTechnologyUsed();

            Dictionary<int, decimal> technologyUsedIdToAverageCost = ReflectTechnologyUsedToDictionary(technologyUsedList);
            ViewData["AllTechnologyUsed"] = JsonConvert.SerializeObject(technologyUsedIdToAverageCost);

            ViewData["ElectricityMeterTypes"] = GetElectricityMeterTypes();
            ViewData["TechnologyUsedTypes"] = ReflectTechnologyUsedToSelectItems(technologyUsedList);

            ViewData["ElectricityMeterDistributions"] = GetLookupSelectedItems(LookupTypeEnum.ElectricityMeterDistribution);
            ViewData["ElectricityMeterSubTypes"] = GetLookupSelectedItems(LookupTypeEnum.ElectricityMeterSubType);
            ViewData["ElectricityMeterPaymentSubTypes"] = GetLookupSelectedItems(LookupTypeEnum.ElectricityMeterPaymentSubType);
        }

        [HttpPost]
        public ActionResult GetSiteElectricityMeters(int siteId)
        {
            return GetSiteElectricityMetersById(siteId, false);
        }

        [HttpPost]
        public ActionResult ViewSiteElectricityMeters(int siteId)
        {
            return GetSiteElectricityMetersById(siteId, true);
        }

        [HttpPost]
        public ActionResult SaveElectricityMeter(ElectricityMeterModel model)
        {
            ActionResult result = null;

            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    ElectricityMeter electricityMeter = ElectricityMeterConverter.ToDatabaseEntity(model);
                    SiteManager.FillApprovalStatus(electricityMeter, electricityMeter.Site);

                    //electricityMeter.ApprovlStatusId = SiteManager.GetInsertStatusId();
                    if (model.ElectricityMeterId == 0)
                    {
                        FillEntitySavedData(electricityMeter, false);
                        model.ElectricityMeterId = ElectricityMeterManager.AddElectricityMeter(electricityMeter);
                    }
                    else
                    {
                        if (electricityMeter.ApprovalStatusId != (int)ApprovalStatus.Approved)
                        {
                            FillEntitySavedData(electricityMeter, false);
                            electricityMeter.OriginalId = electricityMeter.ElectricityMeterId;
                            electricityMeter.ElectricityMeterId = 0;
                            model.ElectricityMeterId = ElectricityMeterManager.AddElectricityMeter(electricityMeter);
                        }
                        else
                        {
                            FillEntitySavedData(electricityMeter, true);
                            ElectricityMeterManager.UpdateElectricityMeter(electricityMeter);
                        }
                    }

                    string electricityMeterJsonData = JsonConvert.SerializeObject(model);

                    result = Json(new
                    {
                        ElectricityMeterJsonData = electricityMeterJsonData,
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks on cancel button of edit electricity meter
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAddElectricityMeterView()
        {
            ActionResult result = null;

            try
            {
                FillDropdpownData();

                ElectricityMeterModel model = new ElectricityMeterModel();

                result = Json(new
                {
                    AddElectricityMeterContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/ElectricityMeter/AddElectricityMeter.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks edit button on electricity meters grid
        /// </summary>
        /// <param name="electricityMeterId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetEditElectricityMeterView(int electricityMeterId)
        {
            return GetElectricityMeterById(electricityMeterId, false);
        }

        /// <summary>
        /// This method is called when user clicks view button on electricity meters grid
        /// </summary>
        /// <param name="electricityMeterId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ViewElectricityMeter(int electricityMeterId)
        {
            return GetElectricityMeterById(electricityMeterId, true);
        }

        /// <summary>
        /// This method is called when user clicks on delete button on electricity meters grid
        /// </summary>
        /// <param name="electricityMeterId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteElectricityMeter(int electricityMeterId)
        {
            ActionResult result = null;

            try
            {
                SiteDataManager.CallDBContext(db =>
                {
                    ElectricityMeter oldElectricityMeter = db.ElectricityMeters.Find(electricityMeterId);
                    oldElectricityMeter.FlagDeleted = true;
                    db.SaveChanges();
                });

                result = Json(new { MessageType = MessageType.Success }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult GetFilterMapLocations(ElectricityMeterFilterModel electricityMeterFilterModel)
        {
            JsonResult result = null;

            try
            {
                List<MapModel> list = MapSearchCriteria.GetElectricityMeterSearch(electricityMeterFilterModel);

                ViewData["mapList"] = JsonConvert.SerializeObject(list);

                result = Json(new
                {
                    IsHaveResult = list.Any(),
                    Content = list.Any() ? UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Site/_PartialMap.cshtml", null) : "",
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);

                result.MaxJsonLength = int.MaxValue;
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult ExportFilterMapLocations(ElectricityMeterFilterModel electricityMeterFilterModel)
        {
            ActionResult result = null;

            try
            {
                byte[] fileContent = MapSearchCriteria.ExportElectricityMeterSearch(electricityMeterFilterModel);
                TempData["ExportSheetResult"] = fileContent;

                result = Json(new
                {
                    Url = Url.Action("DownloadExportSheet"),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpGet]
        public FileResult DownloadExportSheet()
        {
            byte[] fileContent = TempData["ExportSheetResult"] as byte[];
            return File(fileContent, System.Net.Mime.MediaTypeNames.Application.Octet, "Filter Map By Electricity Meter.xlsx");
        }

        #region Private Methods

        private ActionResult GetSiteElectricityMetersById(int siteId, bool isReadOnly)
        {
            ActionResult result = null;

            ViewData["IsReadOnly"] = isReadOnly;

            try
            {
                FillDropdpownData();

                List<ElectricityMeterModel> siteElectricityMeterModels = new List<ElectricityMeterModel>();

                SiteDataManager.CallDBContext(db =>
                {
                    List<ElectricityMeter> electricityMeters = db.ElectricityMeters.Where(o => o.SiteId == siteId && !o.FlagDeleted).ToList();
                    if (electricityMeters != null)
                    {
                        siteElectricityMeterModels = electricityMeters.Select(o => o.FromDatabaseEntity()).ToList();
                    }
                });

                ViewData["electricityMeterGridData"] = JsonConvert.SerializeObject(siteElectricityMeterModels);

                if (isReadOnly)
                {
                    result = Json(new
                    {
                        ElectricityMeterGridContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/ElectricityMeter/ElectricityMeterGrid.cshtml", null),
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ElectricityMeterModel electricityMeterModel = new ElectricityMeterModel();

                    result = Json(new
                    {
                        AddElectricityMeterContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/ElectricityMeter/AddElectricityMeter.cshtml", electricityMeterModel),
                        ElectricityMeterGridContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/ElectricityMeter/ElectricityMeterGrid.cshtml", null),
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        private ActionResult GetElectricityMeterById(int electricityMeterId, bool isReadOnly)
        {
            ActionResult result = null;

            ViewData["IsReadOnly"] = isReadOnly;

            try
            {
                FillDropdpownData();

                ElectricityMeterModel model = new ElectricityMeterModel();

                SiteDataManager.CallDBContext(db =>
                {
                    ElectricityMeter electricityMeter = db.ElectricityMeters.Where(o => o.ElectricityMeterId == electricityMeterId).FirstOrDefault();
                    if (electricityMeter != null)
                    {
                        model = electricityMeter.FromDatabaseEntity();
                    }
                });

                result = Json(new
                {
                    EditElectricityMeterContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/ElectricityMeter/EditElectricityMeter.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        private IEnumerable<SelectListItem> GetElectricityMeterTypes()
        {
            return GetLookupSelectedItems(LookupTypeEnum.ElectricityMeterType);
        }

        //private IEnumerable<SelectListItem> GetTechnologyUsedTypes()
        //{
        //    return GetLookupSelectedItems(LookupTypeEnum.TechnologyUsed);
        //}

        private IEnumerable<TechnologyUsed> GetAllTechnologyUsed()
        {
            IEnumerable<TechnologyUsed> technologiesUsedList = new List<TechnologyUsed>();

            SiteDataManager.CallDBContext(db =>
            {
                technologiesUsedList = db.TechnologiesUsed.ToList();
            });

            return technologiesUsedList;
        }

        private IEnumerable<SelectListItem> ReflectTechnologyUsedToSelectItems(IEnumerable<TechnologyUsed> technologiesUsedList)
        {
            IEnumerable<SelectListItem> items = new List<SelectListItem>();

            if (technologiesUsedList != null)
            {
                items = technologiesUsedList.Select(t => new SelectListItem
                {
                    Value = t.TechnologyUsedId.ToString(),
                    Text = t.DisplayName
                });
            }

            return items;
        }

        private Dictionary<int, decimal> ReflectTechnologyUsedToDictionary(IEnumerable<TechnologyUsed> technologiesUsedList)
        {
            Dictionary<int, decimal> technologyUsedIdToAverageCost = new Dictionary<int, decimal>();

            if (technologiesUsedList != null)
            {
                foreach(TechnologyUsed technologyUsed in technologiesUsedList)
                {
                    technologyUsedIdToAverageCost.Add(technologyUsed.TechnologyUsedId, technologyUsed.AverageCost);
                }
            }

            return technologyUsedIdToAverageCost;
        }

        #endregion Private Methods
    }
}