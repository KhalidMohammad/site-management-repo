﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    public class BaseController : Controller
    {
        protected override void OnException(ExceptionContext filterContext)
        {
            Exception exception = filterContext.Exception;
            TempData["ErrorType"] = ErrorType.Generic;


            System.Diagnostics.Debugger.Break();

            StringBuilder errorMessage = new StringBuilder();

            errorMessage.AppendLine("\n----------------------------------------------- Message -----------------------------------------------\n");
            errorMessage.AppendLine(exception.Message);

            Exception inner = exception;
            while (inner != null)
            {
                errorMessage.AppendLine(inner.ToString());

                errorMessage.AppendLine("\n----------------------------------------------- InnerException -----------------------------------------------");

                inner = inner.InnerException;
            }

            errorMessage.AppendLine("\n----------------------------------------------- StackTrace -----------------------------------------------\n");
            errorMessage.AppendLine(exception.StackTrace);

            TempData["ErrorMessage"] = errorMessage.ToString();

            filterContext.ExceptionHandled = true;


            if (filterContext.Exception is HttpAntiForgeryException)
            {
                if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
                {
                    TempData["ErrorType"] = ErrorType.InvalidRequest;
                }
                else
                {
                    TempData["ErrorType"] = ErrorType.AnotherUserLoggedIn;
                }

            }

            //TODO_: Add log
            //exception.ToLog();
            LogManager.Write(exception);

            if (!Request.IsAjaxRequest())
            {
                filterContext.Result = RedirectToAction("Index", "Error");
            }
            else
            {
                filterContext.Result = JavaScript(string.Format("window.location = '{0}'", Url.Action("Index", "Error")));
            }

            base.OnException(filterContext);
        }

        protected ActionResult HandleModelErrors()
        {
            var errorList = ModelState.Where(ms => ms.Value.Errors.Any())
                                      .ToDictionary(kvp => kvp.Key, kvp => kvp.Value.Errors.Select(e => e.ErrorMessage).ToArray());

            var allErrors = JsonConvert.SerializeObject(errorList);

            return Json(new { errors = allErrors, MessageType = MessageType.ValidateError }, JsonRequestBehavior.AllowGet);
        }


        protected static IEnumerable<SelectListItem> GetLookupSelectedItems(LookupTypeEnum lookupType)
        {
            IEnumerable<Lookup> technologiesLookups = LookupHelper.GetLookupByType(lookupType);

            IEnumerable<SelectListItem> items = new List<SelectListItem>();
            if (technologiesLookups != null)
            {
                items = UIHelper.ReflectSelectItemFromLookup(technologiesLookups);
            }

            return items;
        }
    }
}