﻿using Newtonsoft.Json;
using SiteManagement.Attributes.Authorization;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.Admin, RoleTypeEnum.SuperAdmin, RoleTypeEnum.Guest)]
    public class OwnerController : AuthenticatedController
    {
        [HttpPost]
        public ActionResult GetSiteOwners(int siteId)
        {
            return GetSiteOwnersById(siteId, false);
        }

        [HttpPost]
        public ActionResult ViewSiteOwners(int siteId)
        {
            return GetSiteOwnersById(siteId, true);
        }

        [HttpPost]
        public ActionResult SaveOwner(OwnerModel model)
        {
            ActionResult result = null;

            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    Owner owner = OwnerConverter.ToDatabaseEntity(model);
                    SiteManager.FillApprovalStatus(owner, owner.Site);

                    //owner.ApprovlStatusId = SiteManager.GetInsertStatusId();
                    if (model.OwnerId == 0)
                    {
                        FillEntitySavedData(owner, false);
                        model.OwnerId = OwnerManager.AddOwner(owner);
                    }
                    else
                    {
                        if (owner.ApprovalStatusId != (int)ApprovalStatus.Approved)
                        {
                            FillEntitySavedData(owner, false);
                            owner.OriginalId = owner.OwnerId;
                            owner.OwnerId = 0;
                            model.OwnerId = OwnerManager.AddOwner(owner);
                        }
                        else
                        {
                            FillEntitySavedData(owner, true);
                            OwnerManager.UpdateOwner(owner);
                        }
                    }

                    string ownerJsonData = JsonConvert.SerializeObject(model);

                    result = Json(new
                    {
                        OwnerJsonData = ownerJsonData,
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks on cancel button of edit owner
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAddOwnerView()
        {
            ActionResult result = null;

            try
            {
                OwnerModel model = new OwnerModel();
                SiteDataManager.CallDBContext(db =>
                {
                    ViewData["Nationaities"] = db.Nationalities.Select(nationality =>
                       new SelectListItem
                       {
                           Text = nationality.Name,
                           Value = nationality.NationalityId.ToString(),
                           Selected = nationality.NationalityId == 1
                       }).ToList();
                });

                result = Json(new
                {
                    AddOwnerContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Owner/AddOwner.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks edit button on owners grid
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetEditOwnerView(int ownerId)
        {
            return GetOwnerById(ownerId, false);
        }

        /// <summary>
        /// This method is called when user clicks view button on owners grid
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ViewOwner(int ownerId)
        {
            return GetOwnerById(ownerId, true);
        }

        /// <summary>
        /// This method is called when user clicks on delete button on owners grid
        /// </summary>
        /// <param name="ownerId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteOwner(int ownerId)
        {
            ActionResult result = null;

            try
            {
                SiteDataManager.CallDBContext(db =>
                {
                    Owner oldOwner = db.Owners.Find(ownerId);
                    oldOwner.FlagDeleted = true;
                    db.SaveChanges();
                });

                result = Json(new { MessageType = MessageType.Success }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult GetFilterMapLocations(OwnerFilterModel ownerFilterModel)
        {
            JsonResult result = null;

            try
            {
                List<MapModel> list = MapSearchCriteria.GetOwnerSearch(ownerFilterModel);

                ViewData["mapList"] = JsonConvert.SerializeObject(list);

                result = Json(new
                {
                    IsHaveResult = list.Any(),
                    Content = list.Any() ? UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Site/_PartialMap.cshtml", null) : "",
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);

                result.MaxJsonLength = int.MaxValue;
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult ExportFilterMapLocations(OwnerFilterModel ownerFilterModel)
        {
            ActionResult result = null;

            try
            {
                byte[] fileContent = MapSearchCriteria.ExportOwnerSearch(ownerFilterModel);
                TempData["ExportSheetResult"] = fileContent;

                result = Json(new
                {
                    Url = Url.Action("DownloadExportSheet"),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpGet]
        public FileResult DownloadExportSheet()
        {
            byte[] fileContent = TempData["ExportSheetResult"] as byte[];
            return File(fileContent, System.Net.Mime.MediaTypeNames.Application.Octet, "Filter Map By Owner.xlsx");
        }

        #region Private Methods

        private ActionResult GetSiteOwnersById(int siteId, bool isReadOnly)
        {
            ActionResult result = null;

            ViewData["IsReadOnly"] = isReadOnly;

            try
            {

                List<OwnerModel> siteOwnerModels = new List<OwnerModel>();

                SiteDataManager.CallDBContext(db =>
                {
                    ViewData["Nationaities"] = db.Nationalities.Select(nationality =>
                      new SelectListItem
                      {
                          Text = nationality.Name,
                          Value = nationality.NationalityId.ToString(),
                          Selected = nationality.NationalityId == 1
                      }).ToList();

                    List<Owner> owners = db.Owners.Where(o => o.SiteId == siteId && !o.FlagDeleted).ToList();
                    if (owners != null)
                    {
                        siteOwnerModels = owners.Select(o => o.FromDatabaseEntity()).ToList();
                    }
                });

                ViewData["ownerGridData"] = JsonConvert.SerializeObject(siteOwnerModels);

                if (isReadOnly)
                {
                    result = Json(new
                    {
                        OwnerGridContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Owner/OwnerGrid.cshtml", null),
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    OwnerModel ownerModel = new OwnerModel();

                    result = Json(new
                    {
                        AddOwnerContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Owner/AddOwner.cshtml", ownerModel),
                        OwnerGridContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Owner/OwnerGrid.cshtml", null),
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }


            return result;
        }

        private ActionResult GetOwnerById(int ownerId, bool isReadOnly)
        {
            ActionResult result = null;

            ViewData["IsReadOnly"] = isReadOnly;

            try
            {
                OwnerModel model = new OwnerModel();

                SiteDataManager.CallDBContext(db =>
                {
                    ViewData["Nationaities"] = db.Nationalities.Select(nationality =>
                     new SelectListItem
                     {
                         Text = nationality.Name,
                         Value = nationality.NationalityId.ToString(),
                         Selected = nationality.NationalityId == 1
                     }).ToList();

                    Owner owner = db.Owners.Where(o => o.OwnerId == ownerId).FirstOrDefault();
                    if (owner != null)
                    {
                        model = owner.FromDatabaseEntity();
                    }
                });

                result = Json(new
                {
                    EditOwnerContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Owner/EditOwner.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new
                {
                    MessageType = MessageType.Error
                }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }


        #endregion Private Methods
    }
}