﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using SiteManagement.Models;
using SiteManagement.Data;
using System.Web.Security;
using SiteManagement.Business;

namespace SiteManagement.Controllers
{
    public class AccountController : BaseController
    {
        [AllowAnonymous]
        public ActionResult Login(string returnUrl, string isReturnUrl)
        {
            try
            {
                ActionResult result = null;

                if (SessionManager.CurrentUser == null)
                {
                    result = View();
                }
                else
                {
                    result = RedirectToAction("Index", "Site");
                }

                return result;
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(UserLoginModel model, string returnUrl)
        {
            ActionResult result = null;
            
            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    User user = null;
                    SignInState signInState = SignInManager.SignIn(model.Username, model.Password, out user);

                    switch(signInState)
                    {
                        case SignInState.Success:
                            Session["user"] = user;
                            FormsAuthentication.SetAuthCookie(user.FullName, false);

                            string urlAction = Url.Action("Index", "Site");

                            result = JavaScript(string.Format("window.location = '{0}'", string.IsNullOrEmpty(returnUrl) ? urlAction : returnUrl));
                            break;

                        case SignInState.DeactivatedUser:
                            result = Json(new { MessageType = MessageType.SecurityValidation, MessageText = "Your account is deactivated" }, JsonRequestBehavior.AllowGet);
                            break;

                        case SignInState.Failure:
                            result = Json(new { MessageType = MessageType.SecurityValidation, MessageText = "Invalid username or password" }, JsonRequestBehavior.AllowGet);
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        public ActionResult Logout()
        {
            SessionManager.CurrentUser = null;
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
        }

    }
}