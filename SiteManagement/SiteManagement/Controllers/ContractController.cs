﻿using Newtonsoft.Json;
using SiteManagement.Attributes.Authorization;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.Admin, RoleTypeEnum.SuperAdmin, RoleTypeEnum.Guest)]
    public class ContractController : AuthenticatedController
    {
        [HttpPost]
        public ActionResult GetSiteContracts(int siteId)
        {
            return GetSiteContractsById(siteId, false);
        }

        [HttpPost]
        public ActionResult ViewSiteContracts(int siteId)
        {
            return GetSiteContractsById(siteId, true);
        }

        [HttpPost]
        public ActionResult SaveContract(ContractModel model)
        {
            ActionResult result = null;

            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    Contract contract = ContractConverter.ToDatabaseEntity(model);
                    SiteManager.FillApprovalStatus(contract, contract.Site);

                    if (model.ContractId == 0)
                    {
                        FillEntitySavedData(contract, false);
                        model.ContractId = ContractManager.AddContract(contract);
                    }
                    else
                    {
                        if (contract.ApprovalStatusId != (int)ApprovalStatus.Approved)
                        {
                            FillEntitySavedData(contract, false);
                            contract.OriginalId = contract.ContractId;
                            contract.ContractId = 0;
                            model.ContractId = ContractManager.AddContract(contract);
                        }
                        else
                        {
                            FillEntitySavedData(contract, true);
                            ContractManager.UpdateContract(contract);
                        }
                    }

                    // Used only in contract grid
                    model.StartDateString = contract.ContractStartDate.ToShortDateString();
                    // Used only in contract grid
                    model.EndDateString = contract.ContractEndDate.HasValue ? contract.ContractEndDate.Value.ToShortDateString() : string.Empty;

                    string contractJsonData = JsonConvert.SerializeObject(model);

                    result = Json(new
                    {
                        ContractJsonData = contractJsonData,
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks on cancel button of edit contract
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetAddContractView()
        {
            ActionResult result = null;

            try
            {
                ViewData["PaymentTypes"] = GetPaymentTypes();

                ContractModel model = new ContractModel();

                result = Json(new
                {
                    AddContractContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/AddContract.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks edit button on contracts grid
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetEditContractView(int contractId)
        {
            return GetContractById(contractId, false);
        }

        /// <summary>
        /// This method is called when user clicks view button on contracts grid
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult ViewContract(int contractId)
        {
            return GetContractById(contractId, true);
        }

        [HttpPost]
        public ActionResult GetContractRents(ContractModel model)
        {
            ActionResult result = null;

            try
            {
                if (model.IsIncremental)
                {
                    FillRents(model);
                    //if (model.Rents == null || model.Rents.Count == 0)
                    //{
                    //    FillRents(model);
                    //}
                    //else
                    //{
                    //    UpdateRents(model);
                    //}
                }

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/_PartialRents.cshtml", model),
                    RentCount = model.Rents != null ? model.Rents.Count : 0,
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        /// <summary>
        /// This method is called when user clicks on delete button on contracts grid
        /// </summary>
        /// <param name="contractId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult DeleteContract(int contractId)
        {
            ActionResult result = null;

            try
            {
                SiteDataManager.CallDBContext(db =>
                {
                    Contract oldContract = db.Contracts.Find(contractId);
                    oldContract.FlagDeleted = true;
                    db.SaveChanges();
                });

                result = Json(new { MessageType = MessageType.Success }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult GetFilterMapLocations(ContractFilterModel contractFilterModel)
        {
            JsonResult result = null;

            try
            {
                List<MapModel> list = MapSearchCriteria.GetContractSearch(contractFilterModel);

                ViewData["mapList"] = JsonConvert.SerializeObject(list);

                result = Json(new
                {
                    IsHaveResult = list.Any(),
                    Content = list.Any() ? UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Site/_PartialMap.cshtml", null) : "",
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);

                result.MaxJsonLength = int.MaxValue;
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult ExportFilterMapLocations(ContractFilterModel contractFilterModel)
        {
            ActionResult result = null;

            try
            {
                byte[] fileContent = MapSearchCriteria.ExportContractSearch(contractFilterModel);
                TempData["ExportSheetResult"] = fileContent;

                result = Json(new
                {
                    Url = Url.Action("DownloadExportSheet"),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpGet]
        public FileResult DownloadExportSheet()
        {
            byte[] fileContent = TempData["ExportSheetResult"] as byte[];
            return File(fileContent, System.Net.Mime.MediaTypeNames.Application.Octet, "Filter Map By Contract.xlsx");
        }

        #region Private Methods

        private ActionResult GetSiteContractsById(int siteId, bool isReadOnly)
        {
            ActionResult result = null;

            ViewData["IsReadOnly"] = isReadOnly;

            try
            {
                List<ContractModel> siteContractModels = new List<ContractModel>();

                SiteDataManager.CallDBContext(db =>
                {
                    List<Contract> contracts = db.Contracts.Where(o => o.SiteId == siteId && !o.FlagDeleted).ToList();
                    if (contracts != null)
                    {
                        //siteContractModels = contracts.Select(o => o.FromDatabaseEntity(
                        //    db.ContractYearlyRents.Where(C => C.ContractId == o.ContractId).ToList())).ToList();

                        // Important Note: No need to get contract yearly rents, coz we need the minimam data to show on contract grid
                        siteContractModels = contracts.Select(o => o.FromDatabaseEntity()).ToList();
                    }
                });

                ViewData["PaymentTypes"] = GetPaymentTypes();
                ViewData["Acquiredby"] = GetAcquiredby();

                ViewData["contractGridData"] = JsonConvert.SerializeObject(siteContractModels);

                if (isReadOnly)
                {
                    result = Json(new
                    {
                        ContractGridContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/ContractGrid.cshtml", null),
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    ContractModel contractModel = new ContractModel();

                    result = Json(new
                    {
                        AddContractContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/AddContract.cshtml", contractModel),
                        ContractGridContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/ContractGrid.cshtml", null),
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        private IEnumerable<SelectListItem> GetPaymentTypes()
        {
            return GetLookupSelectedItems(LookupTypeEnum.PaymentType);
        }

        private IEnumerable<SelectListItem> GetAcquiredby()
        {
            return GetLookupSelectedItems(LookupTypeEnum.AcquiredBy);
        }

        private static void FillRents(ContractModel model)
        {
            model.Rents = new List<ContractYearlyRentModel>();

            DateTime startDate = CommonHelper.GetDateFromModel(model.Date.Start);
            DateTime endDate = CommonHelper.GetDateFromModel(model.Date.End);

            DateTime from = startDate;
            DateTime to;
            while (from < endDate)
            {
                to = from.AddYears(1) <= endDate ? from.AddYears(1) : endDate;

                model.Rents.Add(new ContractYearlyRentModel
                {
                    StartDate = from,
                    EndDate = to,
                    Label = String.Format("{0}-{1}", from.Year, to.Year),
                    Amount = 0,
                });

                from = to;
            }
        }

        private ActionResult GetContractById(int contractId, bool isReadOnly)
        {
            ActionResult result = null;

            ViewData["IsReadOnly"] = isReadOnly;

            try
            {
                ViewData["PaymentTypes"] = GetPaymentTypes();

                ContractModel model = new ContractModel();

                SiteDataManager.CallDBContext(db =>
                {
                    Contract contract = db.Contracts.Where(o => o.ContractId == contractId).FirstOrDefault();
                    if (contract != null)
                    {
                        contract.ContractYearlyRents = db.ContractYearlyRents.Where(C => C.ContractId == contract.ContractId).ToList();
                        model = contract.FromDatabaseEntity();
                    }
                });

                result = Json(new
                {
                    EditContractContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Contract/EditContract.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        //private static void UpdateRents(ContractModel contractModel)
        //{
        //    DateTime firstStartDate = contractModel.Rents.First().StartDate;
        //    DateTime lastEndDate = contractModel.Rents.Last().EndDate;

        //    DateTime startDate = Convert.ToDateTime(contractModel.StartDate);
        //    DateTime endDate = Convert.ToDateTime(contractModel.EndDate);

        //    //TODO_N

        //    //DateTime from = startDate;
        //    //DateTime to;
        //    //while (from < endDate)
        //    //{
        //    //    to = from.AddYears(1) <= endDate ? from.AddYears(1) : endDate;

        //    //    contractModel.Rents.Add(new ContractYearlyRentModel
        //    //    {
        //    //        StartDate = from,
        //    //        EndDate = to,
        //    //        Label = String.Format("{0}-{1}", from.Year, to.Year),

        //    //    });

        //    //    from = to;
        //    //}
        //}

        #endregion Private Methods
    }
}