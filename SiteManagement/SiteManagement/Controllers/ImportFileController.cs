﻿using Newtonsoft.Json;
using SiteManagement.Attributes.Authorization;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.SuperAdmin)]
    public class ImportFileController : AuthenticatedController
    {
        [HttpPost]
        public ActionResult Index()
        {
            ActionResult result = null;

            try
            {
                ViewData["ImportFileTypes"] = GetImportFileTypes();

                ImportFileModel model = new ImportFileModel();
                //TODO_: Remove this line after add other types
                model.ImportType = (int)ImportType.ElectricityBill; // Set ElectricityBill selected by default cause this is the only item inside the dropdown

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/ImportFile/Index.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpGet]
        public ActionResult DownloadTemplate(ImportType importType)
        {
            try
            {
                byte[] fileContent = null;
                string fileName = string.Empty;

                switch (importType)
                {
                    case ImportType.ElectricityBill:
                        fileContent = ImportFileManager.GetElectricityBillTemplate();
                        fileName = "Electricity Bill.xlsx";
                        break;
                }

                string contentType = MimeMapping.GetMimeMapping(fileName);
                return File(fileContent, contentType, fileName);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPost]
        public ActionResult Import(ImportFileModel model)
        {
            ActionResult result = null;

            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    model.ImportFile = JsonConvert.DeserializeObject<FileModel>(model.ImportFileJson);
                    byte[] fileContnt = Convert.FromBase64String(model.ImportFile.Content.Split(",".ToCharArray())[1]);

                    try
                    {
                        switch ((ImportType)model.ImportType)
                        {
                            case ImportType.ElectricityBill:
                                List<ElectricityBill> electricityBills = ImportFileManager.ReadElectricityBillSheet(fileContnt);

                                List<string> electricityMeterCodes = electricityBills.Select(e => e.ElectricityMeterCode).ToList();

                                SiteDataManager.CallDBContext(db =>
                                {
                                    Dictionary<string, int> ElectricityMeterCodeToId = db.ElectricityMeters
                                                                            .Where(em => electricityMeterCodes.Contains(em.ElectricityMeterCode))
                                                                            .Select(em => new
                                                                            {
                                                                                ElectricityMeterId = em.ElectricityMeterId,
                                                                                ElectricityMeterCode = em.ElectricityMeterCode
                                                                            })
                                                                            .AsEnumerable()
                                                                            .ToDictionary(em => em.ElectricityMeterCode, em => em.ElectricityMeterId);

                                    bool isElectricityMeterCodesValid = true;
                                    List<string> invalidElectricityMeterCodes = new List<string>();

                                    foreach (ElectricityBill electricityBill in electricityBills)
                                    {
                                        if (!ElectricityMeterCodeToId.ContainsKey(electricityBill.ElectricityMeterCode))
                                        {
                                            isElectricityMeterCodesValid = false;
                                            invalidElectricityMeterCodes.Add(electricityBill.ElectricityMeterCode);
                                        }

                                        // If still valid get ElectricityMeterId by ElectricityMeterCode and build the insert query
                                        if (isElectricityMeterCodesValid)
                                        {
                                            FillEntitySavedData(electricityBill, false);
                                            electricityBill.ElectricityMeterId = ElectricityMeterCodeToId[electricityBill.ElectricityMeterCode];
                                            // Build db query
                                            db.ElectricityBills.Add(electricityBill);
                                        }
                                    }

                                    if (!isElectricityMeterCodesValid)
                                    {
                                        string invalidCodes = string.Join(",", invalidElectricityMeterCodes);
                                        throw new ArgumentException(string.Format("The following electricity meter codes '{0}' are invalid", invalidCodes));
                                    }
                                    else
                                    {
                                        db.SaveChanges();
                                    }
                                });
                                break;
                        }

                        result = Json(new { MessageType = MessageType.Success }, JsonRequestBehavior.AllowGet);
                    }
                    catch (Exception ex)
                    {
                        result = Json(new { MessageType = MessageType.ImportSheetFailure, MessageText = ex.Message }, JsonRequestBehavior.AllowGet);
                    }
                }

                return result;
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        #region Private Methods

        private IEnumerable<SelectListItem> GetImportFileTypes()
        {
            //TODO_: Get lookup from ImportFileType enum (from custom string) instead of the bellow code
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Value = "1",
                Text = "Electricity Bill"
            });

            return items;
        }

        #endregion Private Methods
    }
}