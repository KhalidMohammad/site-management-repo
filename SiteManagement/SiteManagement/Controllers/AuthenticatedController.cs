﻿using SiteManagement.Attributes.Authorization;
using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    public class AuthenticatedController : BaseController
    {
        protected override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            base.OnActionExecuting(filterContext);
        }

        protected void FillEntitySavedData(ISavedEntity entity, bool editMode)
        {
            if (!editMode)
            {
                entity.CreatedByUserId = SessionManager.CurrentUser.UserId;
                entity.CreatedDate = DateTime.Now;
            }
          
            entity.UpdatedByUserId = SessionManager.CurrentUser.UserId;
            entity.LastUpdatedDate = DateTime.Now;
        }

        [RoleAuthorize(RoleTypeEnum.SuperAdmin)]
        public ActionResult AddNewLookup(int lookupType, string text)
        {
            ActionResult result = null;

            try
            {
                int lookupId = 0;
                Lookup lookup = new Lookup
                {
                    DisplayName = text,
                    LookupTypeId = lookupType
                };

                SiteDataManager.CallDBContext(db =>
                {
                    db.Lookups.Add(lookup);
                    db.SaveChanges();
                    //Important Note: This line should be after save changes
                    lookupId = lookup.Id;
                });

                result = Json(new { OptionId = lookupId, MessageType = MessageType.Success }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [RoleAuthorize(RoleTypeEnum.SuperAdmin)]
        public ActionResult EditLookup(int lookupType, int id, string text)
        {
            ActionResult result = null;

            try
            {
                Lookup lookup = new Lookup
                {
                    Id = id,
                    DisplayName = text,
                    LookupTypeId = lookupType
                };

                SiteDataManager.CallDBContext(db =>
                {
                    Lookup oldLookup = db.Lookups.Find(lookup.Id);
                    db.Entry(oldLookup).CurrentValues.SetValues(lookup);
                    db.SaveChanges();
                });

                result = Json(new { MessageType = MessageType.Success }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        protected ApprovalStatus GetApprovalStatusByCurrentUser()
        {
            ApprovalStatus state = ApprovalStatus.Pending;

            switch ((RoleTypeEnum)SessionManager.CurrentUser.RoleId)
            {
                case RoleTypeEnum.SuperAdmin:
                    state = ApprovalStatus.Approved;
                    break;
                case RoleTypeEnum.Admin:
                    state = ApprovalStatus.PartialApprove;
                    break;
            }

            return state;
        }
    }
}