﻿using Newtonsoft.Json;
using SiteManagement.Attributes.Authorization;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.SuperAdmin)]
    public class ReportController : AuthenticatedController
    {
        [HttpPost]
        public ActionResult Index()
        {
            ActionResult result = null;

            try
            {
                ViewData["ReportTypes"] = GetReportTypes();

                ReportModel model = new ReportModel();

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Report/Index.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult Export(ReportModel model)
        {
            ActionResult result = null;

            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    byte[] fileContent = null;
                    switch ((ReportType)model.ReportType)
                    {
                        case ReportType.OutOfAverageElectricityBill:
                            fileContent = ReportManager.ExportOutOfAverageElectricityBills();
                            break;
                        case ReportType.DuplicateElectricityBill:
                            fileContent = ReportManager.ExportDuplicateElectricityBills();
                            break;
                    }

                    TempData["ReportSheetResult"] = fileContent;

                    result = Json(new
                    {
                        Url = Url.Action("DownloadReportSheet"),
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }

                return result;
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpGet]
        public FileResult DownloadReportSheet()
        {
            byte[] fileContent = TempData["ReportSheetResult"] as byte[];
            return File(fileContent, System.Net.Mime.MediaTypeNames.Application.Octet, "Report Result.xlsx");
        }

        #region Private Methods

        private IEnumerable<SelectListItem> GetReportTypes()
        {
            //TODO_: Get lookup from GetReportType enum (from custom string) instead of the bellow code
            List<SelectListItem> items = new List<SelectListItem>();
            items.Add(new SelectListItem
            {
                Value = "1",
                Text = "Out of Average Electricity Bill"
            });

            items.Add(new SelectListItem
            {
                Value = "2",
                Text = "Duplicate Electricity Bill"
            });

            return items;
        }

        #endregion Private Methods
    }
}