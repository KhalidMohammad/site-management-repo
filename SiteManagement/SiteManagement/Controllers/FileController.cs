﻿using SiteManagement.Attributes.Authorization;
using SiteManagement.Business;
using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    public class FileController : AuthenticatedController
    {
        [HttpGet]
        public FileResult DownloadFile(string filePath)
        {
            try
            {
                byte[] fileContent = FileManager.GetFile(filePath);
                if (fileContent != null)
                {
                    string fileName = FileManager.GetFileNameFromPath(filePath);
                    string contentType = MimeMapping.GetMimeMapping(fileName);
                    return File(fileContent, contentType, fileName);
                }

                return null;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}