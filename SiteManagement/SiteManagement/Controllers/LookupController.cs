﻿using Newtonsoft.Json;
using SiteManagement.Attributes.Authorization;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Controllers
{
    [RoleAuthorize(RoleTypeEnum.SuperAdmin)]
    public class LookupController : AuthenticatedController
    {
        // GET: Lookup
        public ActionResult ManageLookups()
        {
            try
            {
                List<LookupModel> lookupModels = new List<LookupModel>();

                SiteDataManager.CallDBContext(db =>
                {
                    List<Lookup> lookups = db.Lookups.ToList();
                    if (lookups != null)
                    {
                        lookupModels = lookups.Select(u => u.FromDatabaseEntity()).OrderBy(l => l.TypeString).ToList();
                    }
                });

                ViewData["LookupTypes"] = GetLookupTypes();
                ViewData["LookupData"] = JsonConvert.SerializeObject(lookupModels);

                return View();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ActionResult SaveLookup(LookupModel model)
        {
            ActionResult result = null;
            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    SiteDataManager.CallDBContext(db =>
                    {
                        Lookup lookup = model.ToDatabaseEntity();

                        if (db.Lookups.Any(l =>
                                l.LookupTypeId == lookup.LookupTypeId &&
                                l.DisplayName.Equals(lookup.DisplayName, StringComparison.InvariantCultureIgnoreCase)))
                        {
                            result = Json(new { errors = "lookup already exists", MessageType = MessageType.ValidateError }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            if (model.Id == 0)
                            {
                                db.Lookups.Add(lookup);
                                db.SaveChanges();
                                //Important Note: This line should be after save changes
                                model.Id = lookup.Id;
                            }
                            else
                            {
                                Lookup oldLookup = db.Lookups.Find(lookup.Id);
                                oldLookup.DisplayName = model.DisplayName;

                                //db.Entry(oldLookup).CurrentValues.SetValues(lookup);
                                db.SaveChanges();
                            }
                        }
                    });

                    string lookupJsonData = JsonConvert.SerializeObject(model);

                    result = Json(new
                    {
                        LookupJsonData = lookupJsonData,
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult NewLookup()
        {
            ActionResult result = null;

            try
            {
                ViewData["LookupTypes"] = GetLookupTypes();

                LookupModel model = new LookupModel();

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Lookup/AddLookup.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult EditLookup(int lookupId)
        {
            ActionResult result = null;

            try
            {
                ViewData["LookupTypes"] = GetLookupTypes();

                LookupModel model = new LookupModel();

                SiteDataManager.CallDBContext(db =>
                {
                    Lookup lookup = db.Lookups.Where(u => u.Id == lookupId).First();
                    if (lookup != null)
                    {
                        model = lookup.FromDatabaseEntity();
                    }
                });

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Lookup/EditLookup.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        public ActionResult ManageTechnologiesUsed()
        {
            ActionResult result = null;

            try
            {
                List<TechnologyUsedModel> technologyUsedModels = new List<TechnologyUsedModel>();

                SiteDataManager.CallDBContext(db =>
                {
                    List<TechnologyUsed> technologiesUsed = db.TechnologiesUsed.ToList();
                    if (technologiesUsed != null)
                    {
                        technologyUsedModels = technologiesUsed.Select(u => u.FromDatabaseEntity()).ToList();
                    }
                });

                ViewData["TechnologyUsedData"] = JsonConvert.SerializeObject(technologyUsedModels);

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Lookup/ManageTechnologiesUsed.cshtml", null),
                    //GridContent = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Lookup/TechnologiesUsedGrid.cshtml", null),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        public ActionResult SaveTechnologyUsed(TechnologyUsedModel model)
        {
            ActionResult result = null;
            try
            {
                if (!ModelState.IsValid)
                {
                    result = HandleModelErrors();
                }
                else
                {
                    SiteDataManager.CallDBContext(db =>
                    {
                        TechnologyUsed technologyUsed = model.ToDatabaseEntity();

                        if (model.TechnologyUsedId == 0)
                        {
                            db.TechnologiesUsed.Add(technologyUsed);
                            db.SaveChanges();
                            //Important Note: This line should be after save changes
                            model.TechnologyUsedId = technologyUsed.TechnologyUsedId;
                        }
                        else
                        {
                            TechnologyUsed oldTechnologyUsed = db.TechnologiesUsed.Find(technologyUsed.TechnologyUsedId);
                            db.Entry(oldTechnologyUsed).CurrentValues.SetValues(technologyUsed);
                            db.SaveChanges();
                        }
                    });

                    string technologyUsedJsonData = JsonConvert.SerializeObject(model);

                    result = Json(new
                    {
                        TechnologyUsedJsonData = technologyUsedJsonData,
                        MessageType = MessageType.Success
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            catch
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult NewTechnologyUsed()
        {
            ActionResult result = null;

            try
            {
                TechnologyUsedModel model = new TechnologyUsedModel();

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Lookup/AddTechnologyUsed.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }

        [HttpPost]
        public ActionResult EditTechnologyUsed(int technologyUsedId)
        {
            ActionResult result = null;

            try
            {
                TechnologyUsedModel model = new TechnologyUsedModel();

                SiteDataManager.CallDBContext(db =>
                {
                    TechnologyUsed technologyUsed = db.TechnologiesUsed.Where(u => u.TechnologyUsedId == technologyUsedId).First();
                    if (technologyUsed != null)
                    {
                        model = technologyUsed.FromDatabaseEntity();
                    }
                });

                result = Json(new
                {
                    Content = UIHelper.RenderRazorViewToHtml(this.ControllerContext, "~/Views/Lookup/EditTechnologyUsed.cshtml", model),
                    MessageType = MessageType.Success
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                result = Json(new { MessageType = MessageType.Error }, JsonRequestBehavior.AllowGet);
            }

            return result;
        }



        #region Private Methods

        private IEnumerable<SelectListItem> GetLookupTypes()
        {
            IEnumerable<SelectListItem> allLoopupTypes = null;
            SiteDataManager.CallDBContext(db =>
            {
                allLoopupTypes = db.LookupTypes.Select(s => new SelectListItem { Text = s.LookupTypeName, Value = s.LookupTypeId.ToString() }).ToList();
            });
            return allLoopupTypes;
        }

        #endregion Private Methods
    }
}