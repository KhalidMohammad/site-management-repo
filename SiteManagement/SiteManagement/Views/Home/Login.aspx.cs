﻿using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;

namespace SiteManagement
{
    public partial class Login : System.Web.UI.Page
    {
        public string ApplicationURL = "";
        public string ApplicationVersion = "1.000";
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnLogin_Click(object sender, EventArgs e)
        {

            string userName = txtUserName.Text;
            string password = txtPassword.Text;

            if (!String.IsNullOrWhiteSpace(userName) && !String.IsNullOrWhiteSpace(password))
            {
                Data.SiteDataManager.CallDBContext(db =>
                {
                    User user = db.Users.FirstOrDefault(u => u.UserName.Equals(userName, StringComparison.InvariantCultureIgnoreCase) && u.Password.Equals(password));
                    if (user != null)
                    {
                        Session["user"] = user;

                        Response.Redirect("/Site/Index.aspx"); 
                    }
                    else
                    {
                        lblError.Text = "invalid username or password";
                        lblError.Visible = true;
                    }

                });

            }
        }
    }
}