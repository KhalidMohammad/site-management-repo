﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="SiteManagement.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Site -Login</title>
    <link type="text/css" rel="stylesheet" href="../../Content/bootstrap/css/bootstrap.min.css" />
    <script type="text/javascript" src="../../Scripts/jQuery/jquery-3.2.1.min.js"></script>
    <style>
        .form-control {
width:100%;
}
    </style>
</head>
<body>
    <div class="container" style="padding-left: 25%; padding-top: 10%;">
        <div class="col-lg-6">
            <div class="well">
                <form id="form1" runat="server" class="form-signin" autocomplete="off">


                    <h2 class="form-signin-heading">Login</h2>
                    <asp:TextBox runat="server" ID="txtUserName" CssClass="form-control" placeholder="Username" autofocus></asp:TextBox>

                    <asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="form-control" placeholder="Password"></asp:TextBox>

                    <label class="checkbox">

                        <asp:Label runat="server" ID="lblError" Text="" Visible="false" ForeColor="Red"></asp:Label>

                    </label>
                    <asp:Button runat="server" ID="btnLogin" CssClass="btn btn-lg btn-primary btn-block" Text="Login" OnClick="btnLogin_Click" />

                </form>
            </div>
        </div>

    </div>

</body>
</html>
