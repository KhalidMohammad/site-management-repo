﻿using Newtonsoft.Json;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Attributes
{
    public class RequiredFileAttribute : RequiredAttribute, IClientValidatable
    {
        #region Methods

        public RequiredFileAttribute(string message)
        {
            ErrorMessage = message;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //if (value == null)
            //    return new ValidationResult(this.ErrorMessage, new[] { validationContext.MemberName });

            //string jsonFiles = value.ToString();
            //List<FileModel> files = JsonConvert.DeserializeObject<List<FileModel>>(jsonFiles);
            //bool isValid = false;

            //if (files.Count == 0)
            //    return new ValidationResult(this.ErrorMessage, new[] { validationContext.MemberName });

            //foreach (var file in files)
            //{
            //    if (!file.IsDeleted)
            //    {
            //        isValid = true;
            //        break;
            //    }
            //}

            //if (!isValid)
            //{
            //    return new ValidationResult(this.ErrorMessage, new[] { validationContext.MemberName });
            //}

            return ValidationResult.Success;
        }

        #endregion Methods

        #region IClientValidatable Members

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule()
            {
                ErrorMessage = ErrorMessage,
                ValidationType = "filerequired"
            };

            yield return rule;
        }
        public override string FormatErrorMessage(string name)
        {
            return name;
        }

        #endregion IClientValidatable Members
    }
}