﻿using Newtonsoft.Json;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Attributes
{
    public class ExtensionFileAttribute : ValidationAttribute, IClientValidatable
    {
        #region Methods
        //private string _errorMessage;
        public ExtensionFileAttribute(string errorMessage, params string[] extensions)
        {
            Extensions = extensions;
            ErrorMessage = errorMessage;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            //bool isValid = true;
            //if (value != null)
            //{
            //    List<FileModel> files = JsonConvert.DeserializeObject<List<FileModel>>(value.ToString()).Where(f => !f.IsDeleted).ToList();

            //    foreach (var file in files)
            //    {
            //        string fileExtension = file.Extension; //ex: .jpeg, .docx, .pdf, .mp3

            //        string[] splitedFileMimeType = file.MimeType.Split('/'); //ex: image/*, video/*, audio/*, image/png, application/pdf
            //        string fileMimeType = splitedFileMimeType[0];
            //        string fileMimeTypeExtension = splitedFileMimeType[1];

            //        //Important Note: The Extensions list may consist of file 'extension' or file 'mime type'
            //        for (int i = 0; i < Extensions.Count(); i++)
            //        {
            //            string currentExtension = Extensions[i];
            //            if (currentExtension.StartsWith("."))
            //            {
            //                // Check file extension (ex: .jpeg, .docx)
            //                if (fileExtension == currentExtension)
            //                {
            //                    isValid = true;
            //                    break;
            //                }
            //            }
            //            else if (currentExtension.IndexOf('/') != -1)
            //            {
            //                // Check file mime type (ex: image/*, video/*, audio/*, image/png, application/pdf)
            //                string[] splitedMimeType = currentExtension.Split('/');
            //                string currentMimeType = splitedMimeType[0];
            //                string currentMimeTypeExtension = splitedMimeType[1];

            //                if (fileMimeType == currentMimeType) // Check the first part of mime type
            //                {
            //                    if (currentMimeTypeExtension == "*" // if the second part is *, this means that all files of this type is accepted
            //                        || fileMimeTypeExtension == currentMimeTypeExtension) // otherwise check if the second part of mime type is equals to the file mime type
            //                    {
            //                        isValid = true;
            //                        break;
            //                    }
            //                }
            //            }
            //        }

            //        if (!isValid)
            //        {
            //            break;
            //        }
            //    }

            //    if (!isValid)
            //        return new ValidationResult(this.ErrorMessage, new[] { validationContext.MemberName });

            //    return ValidationResult.Success;
            //}
            return ValidationResult.Success;
        }

        public string[] Extensions { get; set; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule()
            {
                ErrorMessage = ErrorMessage,
                ValidationType = "extensionfile"
            };

            rule.ValidationParameters.Add("extensions", JsonConvert.SerializeObject(Extensions));

            yield return rule;
        }

        #endregion Methods


    }
}