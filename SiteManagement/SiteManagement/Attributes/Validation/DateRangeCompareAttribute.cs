﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement.Attributes
{
    public class DateRangeCompareAttribute : ValidationAttribute, IClientValidatable
    {
        #region Methods

        public DateRangeCompareAttribute(string errorMessage, params string[] extensions)
        {
            Extensions = extensions;
            ErrorMessage = errorMessage;
        }

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            bool isValid = true;

            if (value != null)
            {
                DateRangeModel dateRangeModel = value as DateRangeModel;

                if (dateRangeModel == null)
                {
                    throw new Exception("The value should be of type DateRangeModel");
                }

                if (!String.IsNullOrEmpty(dateRangeModel.Start) && !String.IsNullOrEmpty(dateRangeModel.End))
                {
                    DateTime startDateTime = CommonHelper.GetDateFromModel(dateRangeModel.Start);
                    DateTime endDateTime = CommonHelper.GetDateFromModel(dateRangeModel.End);

                    if (endDateTime < startDateTime)
                    {
                        isValid = false;
                    }
                }
            }

            if (!isValid)
                return new ValidationResult(this.ErrorMessage, new[] { validationContext.MemberName });

            return ValidationResult.Success;
        }

        public string[] Extensions { get; set; }

        public IEnumerable<ModelClientValidationRule> GetClientValidationRules(ModelMetadata metadata, ControllerContext context)
        {
            var rule = new ModelClientValidationRule()
            {
                ErrorMessage = ErrorMessage,
                ValidationType = "daterangecompare"
            };

            rule.ValidationParameters.Add("extensions", JsonConvert.SerializeObject(Extensions));

            yield return rule;
        }

        #endregion Methods
    }
}