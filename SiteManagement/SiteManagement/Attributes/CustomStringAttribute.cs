﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement.Attributes
{
    public class CustomStringAttribute : Attribute
    {
        private string _text;

        public CustomStringAttribute(string text)
        {
            this._text = text;
        }
    }
}