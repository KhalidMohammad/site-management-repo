﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement.Attributes
{
    public class MapLocationAttribute : Attribute
    {
        public string Path { get; set; }

        public MapLocationAttribute()
        {

        }
    }
}