﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace SiteManagement.Attributes.Authorization
{
    /// <summary>
    /// It is used to prevent access any action required a logged-in user.
    /// User is required to do handle any non anonymous request.
    /// </summary>
    public class UserAuthorizeAttribute : AuthorizeAttribute
    {
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return (SessionManager.CurrentUser != null);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var request = filterContext.RequestContext.HttpContext.Request;

            FormsAuthentication.SignOut();

            if (!request.IsAjaxRequest())
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Home", action = "Index", returnUrl = request.RawUrl, isReturnUrl = true }));
            }
            else
            {
                var basePath = new Uri(string.Concat(request.Url.Scheme, "://", request.Url.Authority, request.ApplicationPath));
                string script = string.Format("window.location = '{0}/Home/Index?returnUrl={1}&isReturnUrl=true'", basePath, request.UrlReferrer);

                filterContext.Result = new JavaScriptResult() { Script = script };

            }
        }
    }
}