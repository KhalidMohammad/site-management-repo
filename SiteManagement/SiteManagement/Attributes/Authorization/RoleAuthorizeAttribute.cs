﻿using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace SiteManagement.Attributes.Authorization
{
    /// <summary>
    /// Role is required to handle accessing a controller request.
    /// </summary>
    public class RoleAuthorizeAttribute : AuthorizeAttribute
    {
        private RoleTypeEnum[] _RoleType;
        public RoleAuthorizeAttribute(params RoleTypeEnum[] roleType)
        {
            _RoleType = roleType;
        }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            return _RoleType.Any(r => r == (RoleTypeEnum)SessionManager.CurrentUser.RoleId);
        }

        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            var request = filterContext.RequestContext.HttpContext.Request;

            string value = (string)filterContext.Controller.TempData["IsReturnUrl"];

            if (value != null)
            {
                filterContext.Controller.TempData.Remove("IsReturnUrl");

                //TODO_N
                if (!request.IsAjaxRequest())
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Home", action = "Index" }));
                }
                else
                {
                    var basePath = new Uri(string.Concat(request.Url.Scheme, "://", request.Url.Authority, request.ApplicationPath));
                    filterContext.Result = new JavaScriptResult() { Script = string.Format("window.location = '{0}/Home/Index'", basePath) };
                }
            }
            else
            {
                if (!request.IsAjaxRequest())
                {
                    filterContext.Result = new RedirectToRouteResult(new RouteValueDictionary(new { controller = "Error", action = "Error403" }));
                }
                else
                {
                    var basePath = new Uri(string.Concat(request.Url.Scheme, "://", request.Url.Authority, request.ApplicationPath));
                    filterContext.Result = new JavaScriptResult() { Script = string.Format("window.location = '{0}/Error/Error403'", basePath) };
                }
            }     
        }
    }
}