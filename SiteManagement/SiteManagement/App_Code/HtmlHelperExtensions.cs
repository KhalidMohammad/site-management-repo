﻿using Microsoft.Ajax.Utilities;
using Newtonsoft.Json;
using SiteManagement.Attributes;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;
using System.Web.Routing;

namespace SiteManagement
{
    public static class HtmlHelperExtensions
    {
        public static MvcHtmlString JsMinify(this HtmlHelper helper, Func<object, object> markup)
        {
            string notMinifiedJs =
                (markup.DynamicInvoke(helper.ViewContext) ?? "").ToString();
#if DEBUG
            return new MvcHtmlString(notMinifiedJs);
#else
            var minifier = new Minifier();
            var minifiedJs = minifier.MinifyJavaScript(notMinifiedJs);
            return new MvcHtmlString(minifiedJs);
#endif
        }

        public static MvcHtmlString CustomLabelFor<TModel, TValue>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        {
            var memberExpression = expression.Body as MemberExpression;
            var validate = memberExpression.Member.GetCustomAttributes(typeof(RequiredAttribute), true);

            if (validate.Length > 0)
            {
                return helper.LabelFor(expression, helper.MergeHtmlAttributes(htmlAttributes, new { @class = "required-label" }));
            }
            return helper.LabelFor(expression, htmlAttributes);
        }

        public static MvcHtmlString NumericInputFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null)
        {
            return helper.TextBoxFor(expression, helper.MergeHtmlAttributes(new { @min = "0", @onkeydown = "return CommonHelper.setNumberMask(event);" }, htmlAttributes));
        }

        public static MvcHtmlString DecimalInputFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression, object htmlAttributes = null)
        {
            return helper.TextBoxFor(expression, helper.MergeHtmlAttributes(new { @min = "0", @onkeydown = "return CommonHelper.setDecimalMask(event);" }, htmlAttributes));
        }

        public static MvcHtmlString Select2For<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> selectList, string optionLabel = null, bool allowClear = false, object htmlAttributes = null)
        {
            if (selectList == null)
            {
                selectList = new List<SelectListItem>();
            }

            optionLabel = !String.IsNullOrEmpty(optionLabel) ? optionLabel : "Choose";

            var memberExpression = expression.Body as MemberExpression;

            var value = ModelMetadata.FromLambdaExpression(expression, helper.ViewData).Model;

            // Convert the value to string
            string selectedValue = GetSelect2StringValue(value, false);

            // Create select dropdown
            string dropdownList = helper.DropDownListFor(expression, selectList, optionLabel, htmlAttributes)
                                        .ToString()
                                        .Replace(Environment.NewLine, "");

            // Get the property name
            var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            var propertyName = metadata.PropertyName;

            string jsContainerVariableName = string.Format("{0}SingleSelect2Container", propertyName);
            string jsDropdownListVariableName = string.Format("{0}SingleSelect2", propertyName);

            StringBuilder sp = new StringBuilder();

            string id = Guid.NewGuid().ToString();

            sp.Append(string.Format("<div id='{0}'></div>", id));

            //IMPORTANT NOTE: We add a css class 'ui-control-select2' for select2 dropdown to use it inside html-helper-extensions.css file.
            sp.Append(string.Format(@"
                <script>
                    var {0} = $('<div>').addClass('ui-control-single-select2-container');
                    $('#{7}').append({0});

                    var {1} = $('{2}').addClass('ui-control-select2');
                    {0}.append({1});

                    {1}.select2({{
                        placeholder: '{3}',
                        allowClear: '{4}'.toLowerCase() == 'true',
                        dropdownAutoWidth: true,
                        width: '100%',
                        theme: 'bootstrap',
                        tags: false, //Important Note: Prevent select2 from dynamically create new options from text input by the user in the search box. 
                        dir: '{6}'
                    }});

                    // Important Note: If there is a value from server side, set this value before register the control change event.
                    if ('{5}') {{
                        //{1}.select2('val', '{5}');
                        {1}.val('{5}').trigger('change');
                    }}

                    //***********************
                    {1}.bind('onSuccess', function () {{
                        $(this).removeClass('control-validation-error');
                    }});

                    {1}.bind('onError', function() {{
                        $(this).addClass('control-validation-error');
                    }});

                    {1}.change(function () {{
                        // Check if state of document to avoid call valid() method for the first time on setValue,
                        //   if we don't set this check and there is a value for the first time so this will cause to stop the whole form validation.
                        if (document.readyState === 'complete') {{
                            $(this).valid();
                        }}
                    }});
                    //***********************
                </script>
                ",
                    jsContainerVariableName, jsDropdownListVariableName, dropdownList, optionLabel, allowClear, selectedValue, "ltr", id));

            return MvcHtmlString.Create(sp.ToString());
        }

        private static string GetSelect2StringValue(Object value, bool isMultiple)
        {
            string selectedValue;

            if (!isMultiple)
            {
                selectedValue = value != null ? value.ToString() : string.Empty;
                selectedValue = selectedValue == "0" ? "" : selectedValue;
            }
            else
            {
                selectedValue = "[]";
                if (value != null)
                {
                    List<int> valueIntList = value as List<int>;
                    if (valueIntList != null)
                    {
                        selectedValue = string.Format("[{0}]", string.Join(",", valueIntList.Select(v => string.Format("\"{0}\"", v))));
                    }
                    else
                    {
                        List<string> valueStringList = value as List<string>;
                        if (valueStringList != null)
                        {
                            selectedValue = string.Format("[{0}]", string.Join(",", valueStringList.Select(v => string.Format("\"{0}\"", v))));
                        }
                    }
                }
            }

            return selectedValue;
        }

        public static MvcHtmlString MultipleSelect2For<TModel, TProperty>(
            this HtmlHelper<TModel> helper,
            Expression<Func<TModel, TProperty>> expression,
            IEnumerable<SelectListItem> selectList,
            string optionLabel = null,
            object htmlAttributes = null)
        {
            if (selectList == null)
            {
                selectList = new List<SelectListItem>();
            }

            optionLabel = !String.IsNullOrEmpty(optionLabel) ? optionLabel : "Choose";

            var memberExpression = expression.Body as MemberExpression;

            var value = ModelMetadata.FromLambdaExpression(expression, helper.ViewData).Model;

            // Convert the value to string
            string selectedValue = GetSelect2StringValue(value, true);

            // Create select dropdown
            string dropdownList = helper.DropDownListFor(expression, selectList, optionLabel, htmlAttributes)
                                        .ToString()
                                        .Replace(Environment.NewLine, "");

            // Get the property name
            var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);
            var propertyName = metadata.PropertyName;

            string jsContainerVariableName = string.Format("{0}MultipleSelect2Container", propertyName);
            string jsDropdownListVariableName = string.Format("{0}MultipleSelect2", propertyName);

            StringBuilder sp = new StringBuilder();

            string id = Guid.NewGuid().ToString();

            sp.Append(string.Format("<div id='{0}'></div>", id));

            //IMPORTANT NOTE: We add a css class 'ui-control-select2' for select2 dropdown to use it inside html-helper-extensions.css file.
            sp.Append(string.Format(@"
                <script>
                    var {0} = $('<div>').addClass('.ui-control-multiple-select2-container');
                    $('#{7}').append({0});

                    var {1} = $('{2}').addClass('ui-control-select2');
                    {0}.append({1});

                    {1}.select2({{
                        placeholder: '{3}',
                        allowClear: false,
                        dropdownAutoWidth: true,
                        width: '100%',
                        theme: 'bootstrap',
                        multiple: true,
                        tags: false, //Important Note: Prevent select2 from dynamically create new options from text input by the user in the search box. 
                        dir: '{6}'
                    }});

                    var {5} = {1}.val();
                    if ({5} && {5}.length > 0) {{
                        if (!{5}[0]) {{
                            {5}.shift();
                            {1}.val({5}).trigger('change');
                        }}
                    }}

                    // Important Note: If there are values from server side, set those values before register the control change event.
                    if ({4} && {4}.length) {{
                        {1}.val({4}).trigger('change');
                    }}

                    //***********************
                    {1}.bind('onSuccess', function () {{
                        $(this).removeClass('control-validation-error');
                    }});

                    {1}.bind('onError', function() {{
                        $(this).addClass('control-validation-error');
                    }});

                    {1}.change(function () {{
                        // Check if state of document to avoid call valid() method for the first time on setValue,
                        //   if we don't set this check and there is a value for the first time so this will cause to stop the whole form validation.
                        if (document.readyState === 'complete') {{
                            $(this).valid();
                        }}
                    }});
                    //***********************
                </script>
                ",
                    jsContainerVariableName, jsDropdownListVariableName, dropdownList, optionLabel, selectedValue,
                    string.Format("{0}Values", jsDropdownListVariableName), "ltr", id));

            return MvcHtmlString.Create(sp.ToString());
        }

        public static MvcHtmlString SwitchFor<TModel>(this HtmlHelper<TModel> helper, Expression<Func<TModel, bool>> expression, bool isReadOnly = false, object htmlAttributes = null)
        {
            StringBuilder sp = new StringBuilder();

            string checkbox = helper.CheckBoxFor(expression, helper.MergeHtmlAttributes(htmlAttributes, new { @class = "switch-input switch-6 form-control" })).ToString();

            sp.Append(string.Format(@"
                <label class='switch'>
                    {0}
                    <span class='lbl'></span>
                </label>"
            , checkbox));

            return MvcHtmlString.Create(sp.ToString());
        }

        public static MvcHtmlString DatePickerFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression,
            bool isReadOnly = false, string onChangeCallbackFunction = null, object htmlAttributes = null)
        {
            StringBuilder sp = new StringBuilder();

            string id = Guid.NewGuid().ToString();

            string textBox = helper.TextBoxFor(expression, helper.MergeHtmlAttributes(htmlAttributes, 
                                new { @class = "form-control ui-control-datetime-picker date-picker" })).ToString();

            sp.Append(string.Format(@"
                <div id='{1}' class='input-group'>
                    {0}
                    <span class='input-group-addon'>
                        <span class='glyphicon glyphicon-calendar'></span>
                    </span>
                </div>
                <script>
                    $(document).ready(function () {{
                        $('#{1} .date-picker').datetimepicker({{
                            format: 'DD/MM/YYYY'
                        }});

                        $('#{1} date-picker').on('dp.change', function (e) {{
                            var dateChangeCallback = {2};
                            if (dateChangeCallback) {{
                                dateChangeCallback();
                            }}
                        }});
                    }});
                </script>
            ", textBox, id, onChangeCallbackFunction?? "null"));

            return MvcHtmlString.Create(sp.ToString());
        }

        private static MvcHtmlString DateRangePickerBaseFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression,
            bool isRequired = false, bool isReadOnly = false, string startInputId = null, string endInputId = null, 
            string onStartDateChangeCallbackFunction = null, string onEndDateChangeCallbackFunction = null,
            object htmlAttributes = null)
        {
            StringBuilder sp = new StringBuilder();

            string id = Guid.NewGuid().ToString();

            var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

            DateRangeModel dateRange = metadata.Model != null ? (DateRangeModel)metadata.Model : new DateRangeModel();
            string propertyName = helper.NameFor(expression).ToString();
            string startName = string.Format("{0}.Start", propertyName);
            string endName = string.Format("{0}.End", propertyName);

            string classNames = "form-control ui-control-datetime-picker date-picker";
            string startDateClassNames = string.Format("{0} start-date-picker", classNames);
            string endDateClassNames = string.Format("{0} end-date-picker", classNames);

            object startHtmlAttributes = null;
            if (!string.IsNullOrEmpty(startInputId))
                startHtmlAttributes = new { @Name = startName, @class = startDateClassNames, @Placeholder = "Start", id = startInputId };
            else
                startHtmlAttributes = new { @Name = startName, @class = startDateClassNames, @Placeholder = "Start" };

            object endHtmlAttributes = null;
            if (!string.IsNullOrEmpty(endInputId))
                endHtmlAttributes = new { @Name = endName, @class = endDateClassNames, @Placeholder = "End", id = endInputId };
            else
                endHtmlAttributes = new { @Name = endName, @class = endDateClassNames, @Placeholder = "End" };


            string startInput = helper.TextBoxFor(m => dateRange.Start, startHtmlAttributes).ToString();
            string endInput = helper.TextBoxFor(m => dateRange.End, endHtmlAttributes).ToString();
            

            //var memberExpression = expression.Body as MemberExpression;
            //var validate = memberExpression.Member.GetCustomAttributes(typeof(RequiredAttribute), true);

            string endValidation = string.Empty, startValidation = string.Empty;
            //if (validate.Length > 0)
            if (isRequired)
            {
                endValidation = helper.ValidationMessage(endName, "", new { @data_valmsg_for = endName, @class = "text-danger" }).ToString();
                startValidation = helper.ValidationMessage(startName, "", new { @data_valmsg_for = startName, @class = "text-danger" }).ToString();
            }

            sp.Append(string.Format(@"
                <div id='{0}' class='date-range-control'>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                        <div class='row start-date-row'>
                            <div class='input-group'>
                                {1}
                                <span class='input-group-addon'>
                                    <span class='glyphicon glyphicon-calendar'></span>
                                </span>
                            </div>
                        </div>
                        <div class='row start-date-row'>
                            {3}
                        </div>
                    </div>
                    <div class='col-lg-6 col-md-6 col-sm-6 col-xs-6'>
                        <div class='row end-date-row'>
                            <div class='input-group'>
                                {2}
                                <span class='input-group-addon'>
                                    <span class='glyphicon glyphicon-calendar'></span>
                                </span>
                            </div>
                        </div>
                        <div class='row end-date-row'>
                            {4}
                        </div>
                    </div>
                </div>
                <script>
                    $(document).ready(function () {{
                        $('#{0} .date-picker').datetimepicker({{
                            format: 'DD/MM/YYYY'
                        }});

                        $('#{0} .start-date-picker').on('dp.change', function (e) {{
                            var startDateChangeCallback = {5};
                            if (startDateChangeCallback) {{
                                startDateChangeCallback();
                            }}
                        }});

                        $('#{0} .end-date-picker').on('dp.change', function (e) {{
                            var endDateChangeCallback = {6};
                            if (endDateChangeCallback) {{
                                endDateChangeCallback();
                            }}
                        }});

                    }});
                </script>
            ", id, startInput, endInput, startValidation, endValidation, onStartDateChangeCallbackFunction?? "null", onEndDateChangeCallbackFunction ?? "null"));

            return MvcHtmlString.Create(sp.ToString());
        }

        public static MvcHtmlString DateRangePickerFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression,
            bool isReadOnly = false, string startInputId = null, string endInputId = null,
            string onStartDateChangeCallbackFunction = null, string onEndDateChangeCallbackFunction = null,
            object htmlAttributes = null)
        {
            return helper.DateRangePickerBaseFor(expression, false, isReadOnly, startInputId, endInputId, onStartDateChangeCallbackFunction, onEndDateChangeCallbackFunction, htmlAttributes);
        }

        public static MvcHtmlString RequiredDateRangePickerFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression,
             bool isReadOnly = false, string startInputId = null, string endInputId = null,
             string onStartDateChangeCallbackFunction = null, string onEndDateChangeCallbackFunction = null,
             object htmlAttributes = null)
        {
            return helper.DateRangePickerBaseFor(expression, true, isReadOnly, startInputId, endInputId, onStartDateChangeCallbackFunction, onEndDateChangeCallbackFunction, htmlAttributes);
        }

        public static MvcHtmlString UploadFileFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression,
            bool isMultiple = false, string downloadUrl = null, bool isReadOnly = false, object htmlAttributes = null)
        {
            StringBuilder sp = new StringBuilder();

            string id = Guid.NewGuid().ToString();
            string extensions = string.Empty;

            var memberExpression = expression.Body as MemberExpression;
            var extentionFile = memberExpression.Member.GetCustomAttributes(typeof(ExtensionFileAttribute), true);

            if (extentionFile.Length == 1)
            {
                var ext = ((ExtensionFileAttribute)(extentionFile[0])).Extensions;

                extensions = string.Join(",", ext);
            }

            var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

            //var value = metadata.Model != null && metadata.Model.ToString() != "" ? metadata.Model : "{}";
            var value = metadata.Model != null ? metadata.Model : "[]";

            string hiddenField = helper.HiddenFor(expression).ToString();

            string htmlJsonAttributesObject = ConvertHtmlAttributesToJsonObject(htmlAttributes);

            sp.Append(string.Format("<div id='{0}'></div>", id));

            sp.Append(string.Format(@"
                    <script>
                        var uploadFile = new Ui.UploadFile({{ downloadUrl: '{0}', files: {1}, hiddenField:'{2}', accept:'{3}', isReadOnly: '{4}', 
                                            attr: {5}, dir: '{6}', multiple: '{8}', }});
                        uploadFile.buildControl($('#{7}'));
                    </script>
                    ",
                    downloadUrl, value, hiddenField, extensions, isReadOnly, htmlJsonAttributesObject, "ltr", id, isMultiple));

            return MvcHtmlString.Create(sp.ToString());
        }

        public static MvcHtmlString UploadPictureFor<TModel, TProperty>(this HtmlHelper<TModel> helper, Expression<Func<TModel, TProperty>> expression,
            string downloadUrl = null, bool isReadOnly = false, object htmlAttributes = null)
        {
            StringBuilder sp = new StringBuilder();

            string id = Guid.NewGuid().ToString();
            string extensions = string.Empty;

            var memberExpression = expression.Body as MemberExpression;
            var extentionFile = memberExpression.Member.GetCustomAttributes(typeof(ExtensionFileAttribute), true);

            if (extentionFile.Length == 1)
            {
                var ext = ((ExtensionFileAttribute)(extentionFile[0])).Extensions;

                extensions = string.Join(",", ext);
            }

            var metadata = ModelMetadata.FromLambdaExpression(expression, helper.ViewData);

            //var value = metadata.Model != null && metadata.Model.ToString() != "" ? metadata.Model : "{}";
            var value = metadata.Model != null ? metadata.Model : "[]";

            string hiddenField = helper.HiddenFor(expression).ToString();

            string htmlJsonAttributesObject = ConvertHtmlAttributesToJsonObject(htmlAttributes);

            sp.Append(string.Format("<div id='{0}'></div>", id));

            sp.Append(string.Format(@"
                    <script>
                        var uploadPictureFile = new Ui.UploadPictureFile({{ downloadUrl: '{0}', files: {1}, hiddenField:'{2}', accept:'{3}', isReadOnly: '{4}', 
                                            attr: {5}, dir: '{6}' }});
                        uploadPictureFile.buildControl($('#{7}'));
                    </script>
                    ",
                    downloadUrl, value, hiddenField, extensions, isReadOnly, htmlJsonAttributesObject, "ltr", id));

            return MvcHtmlString.Create(sp.ToString());
        }


        public static string ConvertHtmlAttributesToJsonObject(object htmlAttributesObject)
        {
            string htmlJsonAttributesObject = null;

            if (htmlAttributesObject != null)
            {
                RouteValueDictionary htmlAttributes = new RouteValueDictionary(htmlAttributesObject);

                List<string> htmlJsonAttributes = new List<string>();
                foreach (var item in htmlAttributes)
                {
                    htmlJsonAttributes.Add(string.Format("'{0}': '{1}'", item.Key, item.Value));
                }

                htmlJsonAttributesObject = string.Join(", ", htmlJsonAttributes.ToArray());
            }

            return string.Format("{{{0}}}", htmlJsonAttributesObject);
        }

        public static IDictionary<string, object> MergeHtmlAttributes(this HtmlHelper helper, object htmlAttributesObject, object defaultHtmlAttributesObject)
        {
            var concatKeys = new string[] { "class" };

            var htmlAttributesDict = htmlAttributesObject as IDictionary<string, object>;
            var defaultHtmlAttributesDict = defaultHtmlAttributesObject as IDictionary<string, object>;

            RouteValueDictionary htmlAttributes = (htmlAttributesDict != null)
                ? new RouteValueDictionary(htmlAttributesDict)
                : HtmlHelper.AnonymousObjectToHtmlAttributes(htmlAttributesObject);
            RouteValueDictionary defaultHtmlAttributes = (defaultHtmlAttributesDict != null)
                ? new RouteValueDictionary(defaultHtmlAttributesDict)
                : HtmlHelper.AnonymousObjectToHtmlAttributes(defaultHtmlAttributesObject);

            foreach (var item in htmlAttributes)
            {
                if (concatKeys.Contains(item.Key))
                {
                    defaultHtmlAttributes[item.Key] = (defaultHtmlAttributes[item.Key] != null)
                        ? string.Format("{0} {1}", defaultHtmlAttributes[item.Key], item.Value)
                        : item.Value;
                }
                else
                {
                    defaultHtmlAttributes[item.Key] = item.Value;
                }
            }

            return defaultHtmlAttributes;
        }
    }
}