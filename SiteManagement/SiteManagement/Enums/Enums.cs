﻿using SiteManagement.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public enum ErrorType
    {
        Generic = 0,
        AnotherUserLoggedIn = 1,
        InvalidRequest = 2
    }

    public enum MessageType
    {
        Success = 1,
        Error = 2,
        ValidateError = 3,
        SecurityValidation = 4,
        ImportSheetFailure = 5,
    }

    //TODO_: Add custom string
    public enum ImportFileType
    {
        ElectricityBill = 1
    }

    public enum SignInState
    {
        Success,
        DeactivatedUser,
        Failure,
    }

    public enum LogType
    {
        Site = 1, 
        Contract = 2,
        Owner = 3,
        ElectricityMeter = 4,
    }

    public enum ImportType
    {
        ElectricityBill = 1,
    }

    public enum ReportType
    {
        OutOfAverageElectricityBill = 1,
        DuplicateElectricityBill = 2
    }

    public enum SiteMarkerType
    {
        [MapLocation(Path = "images/map-location-blue-aqua.png")]
        Blue_Aqua,

        [MapLocation(Path = "images/map-location-blue-a.png")]
        Blue_A,

        [MapLocation(Path = "images/map-location-blue-b.png")]
        Blue_B,

        [MapLocation(Path = "images/map-location-blue-c.png")]
        Blue_C,

        [MapLocation(Path = "images/map-location-green-a.png")]
        Green_A,

        [MapLocation(Path = "images/map-location-green-b.png")]
        Green_B,

        [MapLocation(Path = "images/map-location-orange.png")]
        Orange,

        [MapLocation(Path = "images/map-location-pink.png")]
        Pink,

        [MapLocation(Path = "images/map-location-purple-a.png")]
        Purple_A,

        [MapLocation(Path = "images/map-location-purple-b.png")]
        Purple_B,

        [MapLocation(Path = "images/map-location-red-a.png")]
        Red_A,

        [MapLocation(Path = "images/map-location-red-b.png")]
        Red_B,

        [MapLocation(Path = "images/map-location-red-c.png")]
        Red_C,

        [MapLocation(Path = "images/map-location-yellow-a.png")]
        Yellow_A,

        [MapLocation(Path = "images/map-location-yellow-b.png")]
        Yellow_B,
    }
}