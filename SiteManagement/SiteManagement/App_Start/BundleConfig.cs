﻿using System.Web;
using System.Web.Optimization;

namespace SiteManagement
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            //jQuery
            //bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
            //            "~/Scripts/jQuery/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jQuery").Include(
                            "~/Scripts/jQuery/jquery-3.2.1.js",
                            "~/Scripts/jQuery/js/jquery-ui.js",
                            "~/Scripts/jQuery/jquery.dataTables.min.js"));

            bundles.Add(new StyleBundle("~/Content/jQuery/css").Include(
                            "~/Content/jQuery/jquery-ui.css",
                            "~/Content/jQuery/jquery-ui.structure.css",
                            "~/Content/jQuery/jquery-ui.theme.css"));

            //jQueryValidation
            bundles.Add(new ScriptBundle("~/bundles/jQueryValidation").Include(
                            "~/Scripts/jQueryValidation/jquery.validate*"));

            //// Use the development version of Modernizr to develop with and learn from. Then, when you're
            //// ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            //bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
            //                "~/Scripts/modernizr-*"));

            //bootstrap
            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                          "~/Scripts/moment-with-locales.js",
                          "~/Scripts/bootstrap/bootstrap.js",
                          "~/Scripts/bootstrap/bootstrap-datetimepicker.js",
                          "~/Scripts/responsiveDesign/respond.js"));

            bundles.Add(new StyleBundle("~/Content/bootstrap/css/style").Include(
                            "~/Content/bootstrap/css/bootstrap.css",
                            "~/Content/bootstrap/css/bootstrap-theme.css",
                            "~/Content/bootstrap/css/bootstrap-datetimepicker.css"));

            //fontAwesome
            bundles.Add(new StyleBundle("~/Content/fontAwesome/css/style").Include(
                            "~/Content/fontAwesome/css/font-awesome.css"));

            //plugins
            bundles.Add(new ScriptBundle("~/bundles/plugins").Include(
                            "~/Scripts/plugins/select2/select2.full.js",
                            "~/Scripts/plugins/jLoadingOverlay/jloading-overlay.js",
                            "~/Scripts/plugins/maplace/maplace.js",
                            //"~/Scripts/plugins/richmarker/richmarker-compiled.js",
                            "~/Scripts/plugins/modal/iziModal-master/iziModal.js",
                            "~/Scripts/plugins/modal/iziToast-master/iziToast.js",
                            "~/Scripts/plugins/modal/iziModalManager.js"));

            bundles.Add(new StyleBundle("~/Content/plugins/css/style").Include(
                            "~/Content/plugins/select2/select2-bootstrap.css",
                            "~/Content/plugins/select2/select2.css",
                            "~/Content/plugins/jLoadingOverlay/jloading-overlay.css",
                            "~/Content/plugins/modal/iziModal-master/iziModal.css",
                            "~/Content/plugins/modal/iziToast-master/iziToast.css",
                            "~/Content/plugins/modal/izi-modal-manager.css"));

            //common
            bundles.Add(new ScriptBundle("~/bundles/common").Include(
                            "~/Scripts/common/CommonHelper.js",
                            "~/Scripts/common/ajaxManager.js",
                            "~/Scripts/common/CustomValidation.js",
                            "~/Scripts/common/UploadBase.js",
                            "~/Scripts/common/UploadFile.js",
                            "~/Scripts/common/UploadPictureFile.js"));

            bundles.Add(new StyleBundle("~/Content/common/css/style").Include(
                            "~/Content/common/upload-file.css",
                            "~/Content/common/upload-picture-file.css",
                            "~/Content/common/form.css",
                            "~/Content/common/htmlHelperExtensions/switch.css",
                            "~/Content/common/html-helper-extensions.css"));

            //site
            bundles.Add(new StyleBundle("~/Content/css").Include(
                          "~/Content/site.css",
                          "~/Content/umniah-style.css"));

#if DEBUG
            BundleTable.EnableOptimizations = false;
#else
            BundleTable.EnableOptimizations = true;
#endif
        }
    }
}
