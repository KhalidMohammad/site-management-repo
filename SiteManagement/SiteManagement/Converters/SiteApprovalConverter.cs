﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class SiteApprovalConverter
    {

        public static SiteApprovalModel FromDatabaseApprovalEntity(this Site entity)
        {
            if (entity != null)
            {
                return new SiteApprovalModel
                {
                    Id = entity.SiteId,
                    ApprovalStatusId = entity.ApprovalStatusId,
                    ApprovalStatus = ((ApprovalStatus)entity.ApprovalStatusId).ToString(),
                    EditedMemberName = entity.UpdatedBy.FullName,
                    SiteCode = entity.SiteCode,
                    SiteName = entity.SiteName,
                    SiteType = entity.SiteType.DisplayName,
                    //PendingFields = entity.PendingFields,
                    //PendingFieldsString = String.Join(",", entity.PendingFields),

                };
            }

            return null;
        }

        //public enum GoogleIcon
        //{
        //    //'http://maps.google.com/mapfiles/markerA.png'
        //    MarkerA = 1,
        //    //'http://maps.google.com/mapfiles/markerB.png'
        //    MarkerB = 2,
        //    //'http://maps.google.com/mapfiles/markerC.png'
        //    MarkerC = 3,
        //}
    }
}