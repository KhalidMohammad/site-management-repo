﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class UserConverter
    {
        public static User ToDatabaseEntity(this UserModel model)
        {
            if (model != null)
            {
                User entity = new User
                {
                    UserId = model.Id,
                    FullName = model.FullName,
                    UserEmail = model.UserEmail,
                    UserName = model.UserName,
                    Password = model.Password,
                    RoleId = model.UserRole,
                    IsActive = model.IsActive,
                };

                return entity;
            }

            return null;
        }

        public static UserModel FromDatabaseEntity(this User entity)
        {
            if (entity != null)
            {
                UserModel model = new UserModel
                {
                    Id = entity.UserId,
                    FullName = entity.FullName,
                    UserName = entity.UserName,
                    UserEmail = entity.UserEmail,
                    Password = entity.Password,
                    UserRole = entity.RoleId,
                    IsActive = entity.IsActive
                };

                return model;
            }

            return null;
        }
    }
}