﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class ContractConverter
    {

        public static Contract ToDatabaseEntity(this ContractModel model)
        {
            if (model != null)
            {
                model.DocumentFile = !String.IsNullOrEmpty(model.DocumentFileJson) ? 
                                        JsonConvert.DeserializeObject<List<FileModel>>(model.DocumentFileJson) : null;

                model.QOSHAN = !String.IsNullOrEmpty(model.QOSHANJson) ? 
                                    JsonConvert.DeserializeObject<List<FileModel>>(model.QOSHANJson) : null;

                model.TerminationLetter = !String.IsNullOrEmpty(model.TerminationLetterJson) ? 
                                            JsonConvert.DeserializeObject<List<FileModel>>(model.TerminationLetterJson) : null;

                Contract entity = new Contract
                {
                    //ContractStartDate = CommonHelper.GetDateFromModel(model.Date.StartDate),
                    //ContractEndDate = CommonHelper.GetDateFromModel(model.EndDate),
                    ContractStartDate = CommonHelper.GetDateFromModel(model.Date.Start),
                    ContractEndDate = CommonHelper.GetDateFromModel(model.Date.End),
                    ContractId = model.ContractId,
                    MunicipalityPermission = model.MunicipalityPermission,
                    SiteId = model.ContractSiteId,
                    //YearlyRent = model.YearlyRent,
                    PaymentTypeId = model.PaymentType,
                    IsMadeByUmniah = model.IsMadeByUmniah,
                    IsColocationClose = model.IsColocationClose,
                    ColocationCloseNote = model.ColocationCloseNote,
                    IsActive = model.IsActive,
                    AcquiredById = model.AcquiredbyId,
                    AquariumName = model.AquariumName,
                    AquariumNumber = model.AquariumNumber,
                    PieceNumber = model.PieceNumber,
                    Directorate = model.Directorate,
                    MaarefTax = model.MaarefTax,
                    PlateNumber = model.PlateNumber,
                    Town = model.Town,
                    IsIncremental = model.IsIncremental,
                    IncrementalPercentage = model.YearlyIncremental,
                    SignatureDate = String.IsNullOrWhiteSpace(model.SigntuerDate) ? null : (DateTime?)CommonHelper.GetDateFromModel(model.SigntuerDate),
                    ActiveDate = !String.IsNullOrEmpty(model.ActiveDate) ?
                                                    CommonHelper.GetDateFromModel(model.ActiveDate) : (DateTime?)null,
                    ActiveNote = model.ActiveNote,
                    IsTerminated = model.IsTerminated,
                    TerminatedDate = !String.IsNullOrEmpty(model.TerminatedDate) ?
                                                    CommonHelper.GetDateFromModel(model.TerminatedDate) : (DateTime?)null,
                    TerminatedNote = model.TerminatedNote,
                    ContractYearlyRents = model.Rents != null ? model.Rents.Select(r => new ContractYearlyRent
                    {
                        Amount = r.Amount,
                        ContractId = model.ContractId,
                        FromDate = r.StartDate,
                        ToDate = r.EndDate
                    }).ToList() : null,
                    ContractDocumentFile = model.DocumentFile != null ? model.DocumentFile.Select(f => new FileEntity
                    {
                        Name = f.Name,
                        GeneratedName = f.GeneratedName,
                        Path = f.Path,
                        Content = f.Content
                    }).ToList() : null,
                    QoshanFile = model.QOSHAN != null ? model.QOSHAN.Select(f => new FileEntity
                    {
                        Name = f.Name,
                        GeneratedName = f.GeneratedName,
                        Path = f.Path,
                        Content = f.Content
                    }).ToList() : null,
                    TerminationLetterFile = model.TerminationLetter != null ? model.TerminationLetter.Select(f => new FileEntity
                    {
                        Name = f.Name,
                        GeneratedName = f.GeneratedName,
                        Path = f.Path,
                        Content = f.Content
                    }).ToList() : null
                };

                entity.ContractYearlyRentsString = entity.ContractYearlyRents != null && entity.ContractYearlyRents.Count > 0 ?
                    String.Join(Environment.NewLine, entity.ContractYearlyRents.Select(r =>  String.Format("{0} - {1} : {2}", 
                        r.FromDate.ToShortDateString() , r.ToDate.ToShortDateString() , r.Amount.ToString("0.000")))) : null;

                return entity;
            }

            return null;
        }

        public static ContractModel FromDatabaseEntity(this Contract entity)
        {
            if (entity != null)
            {
                ContractModel model = new ContractModel
                {
                    // Used only in contract grid
                    StartDateString = entity.ContractStartDate.ToShortDateString(),
                    // Used only in contract grid
                    EndDateString = entity.ContractEndDate.HasValue ? entity.ContractEndDate.Value.ToShortDateString() : string.Empty,
                    Date = new DateRangeModel
                    {
                        Start = Convert.ToString(entity.ContractStartDate),
                        End = Convert.ToString(entity.ContractEndDate),
                    },
                    ContractId = entity.ContractId,
                    MunicipalityPermission = entity.MunicipalityPermission,
                    ContractSiteId = entity.SiteId,
                    //YearlyRent = entity.YearlyRent,
                    PaymentType = entity.PaymentTypeId != null ? entity.PaymentTypeId.Value : 0,
                    IsMadeByUmniah = entity.IsMadeByUmniah,
                    IsColocationClose = entity.IsColocationClose,
                    ColocationCloseNote = entity.ColocationCloseNote,
                    IsActive = entity.IsActive,
                    ActiveDate = entity.ActiveDate != null ? Convert.ToString(entity.ActiveDate) : null,
                    ActiveNote = entity.ActiveNote,
                    IsTerminated = entity.IsTerminated,
                    TerminatedDate = entity.TerminatedDate != null ? Convert.ToString(entity.TerminatedDate) : null,
                    TerminatedNote = entity.TerminatedNote,
                    IsIncremental = entity.IsIncremental,
                    YearlyIncremental = entity.IncrementalPercentage,

                    AcquiredbyId = entity.AcquiredById,
                    AquariumName = entity.AquariumName,
                    AquariumNumber = entity.AquariumNumber,
                    PieceNumber = entity.PieceNumber,
                    Directorate = entity.Directorate,
                    MaarefTax = entity.MaarefTax,
                    PlateNumber = entity.PlateNumber,
                    Town = entity.Town,
                    SigntuerDate = entity.SignatureDate == null ? null : Convert.ToString(entity.SignatureDate),
                    Rents = entity.ContractYearlyRents != null ? entity.ContractYearlyRents.Select(r => new ContractYearlyRentModel
                    {
                        Amount = r.Amount,
                        EndDate = r.ToDate,
                        StartDate = r.FromDate,
                        Label = String.Format("{0}-{1}", r.FromDate.Year, r.ToDate.Year)
                    }).ToList() : null,
                    DocumentFile = !String.IsNullOrEmpty(entity.ContractDocumentFilePath) ? entity.ContractDocumentFilePath.Split('|').Select(p => new FileModel()
                    {
                        Name = FileManager.GetFileNameFromPath(p),
                        GeneratedName = FileManager.GetGeneratedFileNameFromPath(p),
                        Path = p,
                        Content = null
                    }).ToList() : null,
                    QOSHAN = !String.IsNullOrEmpty(entity.QoshanFilePath) ? entity.QoshanFilePath.Split('|').Select(p => new FileModel()
                    {
                        Name = FileManager.GetFileNameFromPath(p),
                        GeneratedName = FileManager.GetGeneratedFileNameFromPath(p),
                        Path = p,
                        Content = null
                    }).ToList() : null,
                    TerminationLetter = !String.IsNullOrEmpty(entity.TerminationLetterFilePath) ? entity.TerminationLetterFilePath.Split('|').Select(p => new FileModel()
                    {
                        Name = FileManager.GetFileNameFromPath(p),
                        GeneratedName = FileManager.GetGeneratedFileNameFromPath(p),
                        Path = p,
                        Content = null
                    }).ToList() : null,
                };

                if (model.DocumentFile != null)
                {
                    model.DocumentFileJson = JsonConvert.SerializeObject(model.DocumentFile);
                }

                if (model.QOSHAN != null)
                {
                    model.QOSHANJson = JsonConvert.SerializeObject(model.QOSHAN);
                }

                if (model.TerminationLetter != null)
                {
                    model.TerminationLetterJson = JsonConvert.SerializeObject(model.TerminationLetter);
                }

                return model;
            }

            return null;
        }
    }
}