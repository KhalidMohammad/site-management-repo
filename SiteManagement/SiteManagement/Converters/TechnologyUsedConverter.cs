﻿using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class TechnologyUsedConverter
    {

        public static TechnologyUsed ToDatabaseEntity(this TechnologyUsedModel model)
        {
            if (model != null)
            {
                return new TechnologyUsed
                {
                    TechnologyUsedId = model.TechnologyUsedId,
                    DisplayName = model.DisplayName,
                    AverageCost = model.AverageCost
                };
            }

            return null;
        }

        public static TechnologyUsedModel FromDatabaseEntity(this TechnologyUsed entity)
        {
            if (entity != null)
            {
                return new TechnologyUsedModel
                {
                    TechnologyUsedId = entity.TechnologyUsedId,
                    DisplayName = entity.DisplayName,
                    AverageCost = entity.AverageCost
                };
            }

            return null;
        }
    }
}