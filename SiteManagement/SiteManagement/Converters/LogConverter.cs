﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class LogConverter
    {
        public static LogModel ToLog(this Site entity)
        {
            if (entity != null)
            {
                LogModel model = new LogModel
                {
                    CreatedBy = entity.CreatedBy.FullName,
                    CreatedDate = entity.CreatedDate.ToString(),
                    LastUpdatedBy = entity.UpdatedBy.FullName,
                    LastUpdateDate = entity.LastUpdatedDate.ToString(),
                    PartialApprovedBy = entity.PartialApprovedByUserId.HasValue && entity.PartialApprovedByUserId.Value > 0 ? entity.PartialApprovedBy.FullName : string.Empty,
                    PartialApprovedDate = entity.PartialApprovedDate.HasValue ? entity.PartialApprovedDate.ToString() : string.Empty,
                    ApprovedBy = entity.ApprovedByUserId.HasValue && entity.ApprovedByUserId.Value > 0 ? entity.ApprovedBy.FullName : string.Empty,
                    ApprovedDate = entity.ApprovedDate.HasValue ? entity.ApprovedDate.ToString() : string.Empty
                };

                return model;
            }

            return null;
        }

        public static LogModel ToLog(this Contract entity)
        {
            if (entity != null)
            {
                LogModel model = new LogModel
                {
                    CreatedBy = entity.CreatedBy.FullName,
                    CreatedDate = entity.CreatedDate.ToString(),
                    LastUpdatedBy = entity.UpdatedBy.FullName,
                    LastUpdateDate = entity.LastUpdatedDate.ToString(),
                    PartialApprovedBy = entity.PartialApprovedByUserId.HasValue && entity.PartialApprovedByUserId.Value > 0 ? entity.PartialApprovedBy.FullName : string.Empty,
                    PartialApprovedDate = entity.PartialApprovedDate.HasValue ? entity.PartialApprovedDate.ToString() : string.Empty,
                    ApprovedBy = entity.ApprovedByUserId.HasValue && entity.ApprovedByUserId.Value > 0 ? entity.ApprovedBy.FullName : string.Empty,
                    ApprovedDate = entity.ApprovedDate.HasValue ? entity.ApprovedDate.ToString() : string.Empty
                };

                return model;
            }

            return null;
        }

        public static LogModel ToLog(this Owner entity)
        {
            if (entity != null)
            {
                LogModel model = new LogModel
                {
                    CreatedBy = entity.CreatedBy.FullName,
                    CreatedDate = entity.CreatedDate.ToString(),
                    LastUpdatedBy = entity.UpdatedBy.FullName,
                    LastUpdateDate = entity.LastUpdatedDate.ToString(),
                    PartialApprovedBy = entity.PartialApprovedByUserId.HasValue && entity.PartialApprovedByUserId.Value > 0 ? entity.PartialApprovedBy.FullName : string.Empty,
                    PartialApprovedDate = entity.PartialApprovedDate.HasValue ? entity.PartialApprovedDate.ToString() : string.Empty,
                    ApprovedBy = entity.ApprovedByUserId.HasValue && entity.ApprovedByUserId.Value > 0 ? entity.ApprovedBy.FullName : string.Empty,
                    ApprovedDate = entity.ApprovedDate.HasValue ? entity.ApprovedDate.ToString() : string.Empty
                };

                return model;
            }

            return null;
        }

        public static LogModel ToLog(this ElectricityMeter entity)
        {
            if (entity != null)
            {
                LogModel model = new LogModel
                {
                    CreatedBy = entity.CreatedBy.FullName,
                    CreatedDate = entity.CreatedDate.ToString(),
                    LastUpdatedBy = entity.UpdatedBy.FullName,
                    LastUpdateDate = entity.LastUpdatedDate.ToString(),
                    PartialApprovedBy = entity.PartialApprovedByUserId.HasValue && entity.PartialApprovedByUserId.Value > 0 ? entity.PartialApprovedBy.FullName : string.Empty,
                    PartialApprovedDate = entity.PartialApprovedDate.HasValue ? entity.PartialApprovedDate.ToString() : string.Empty,
                    ApprovedBy = entity.ApprovedByUserId.HasValue && entity.ApprovedByUserId.Value > 0 ? entity.ApprovedBy.FullName : string.Empty,
                    ApprovedDate = entity.ApprovedDate.HasValue ? entity.ApprovedDate.ToString() : string.Empty
                };

                return model;
            }

            return null;
        }
    }
}