﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class OwnerConverter
    {

        public static Owner ToDatabaseEntity(this OwnerModel model)
        {
            if (model != null)
            {
                model.InventoryofLegacy = !String.IsNullOrEmpty(model.InventoryofLegacyJson) ? 
                                                JsonConvert.DeserializeObject<List<FileModel>>(model.InventoryofLegacyJson) : null;

                return new Owner
                {
                    AccountNumber = model.AccountNumber,
                    BankBranch = model.BankBranch,
                    BankName = model.BankName,
                    Email = model.Email,
                    FullName = model.FullName,
                    IsPay = model.IsPay,
                    IBAN = model.IBAN,
                    OwnerId = model.OwnerId,
                    PrimaryPhone = model.PrimaryPhone,
                    SecoundaryPhone = model.SecondaryPhone,
                    SiteId = model.OwnerSiteId,
                    Note = model.Note,
                    IsInventoryOfLegacy = model.IsInventoryofLegacy,
                    Percentage = model.Percentage,
                    PercentageOfMoneyReceived = model.PercentageOfMonyReseaved,
                    OwnerCode = model.OwnerCode,
                    NationalityId = model.Nationaity,
                    NationalId = model.NationalId,
                    InventoryOfLegacyFile = model.InventoryofLegacy != null ? model.InventoryofLegacy.Select(f => new FileEntity
                    {
                        Name = f.Name,
                        GeneratedName = f.GeneratedName,
                        Path = f.Path,
                        Content = f.Content
                    }).ToList() : null
                };
            }

            return null;
        }

        public static OwnerModel FromDatabaseEntity(this Owner entity)
        {
            if (entity != null)
            {
                OwnerModel owner = new OwnerModel
                {
                    AccountNumber = entity.AccountNumber,
                    BankBranch = entity.BankBranch,
                    BankName = entity.BankName,
                    Email = entity.Email,
                    FullName = entity.FullName,
                    IsPay = entity.IsPay,
                    IBAN = entity.IBAN,
                    OwnerId = entity.OwnerId,
                    PrimaryPhone = entity.PrimaryPhone,
                    SecondaryPhone = entity.SecoundaryPhone,
                    OwnerSiteId = entity.SiteId,
                    OwnerCode = entity.OwnerCode,
                    PercentageOfMonyReseaved = entity.PercentageOfMoneyReceived,
                    Percentage = entity.Percentage,
                    IsInventoryofLegacy = entity.IsInventoryOfLegacy,
                    Note = entity.Note,
                    InventoryofLegacy = !String.IsNullOrEmpty(entity.InventoryOfLegacyFilePath) ? entity.InventoryOfLegacyFilePath.Split('|').Select(p => new FileModel()
                    {
                        Name = FileManager.GetFileNameFromPath(p),
                        GeneratedName = FileManager.GetGeneratedFileNameFromPath(p),
                        Path = p,
                        Content = null
                    }).ToList() : null
                };

                owner.InventoryofLegacyJson = owner.InventoryofLegacy != null ? JsonConvert.SerializeObject(owner.InventoryofLegacy) : null;

                return owner;
            }

            return null;
        }

    }
}