﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Helpers;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SiteManagement
{
    public static class SiteConverter
    {

        public static Site ToDatabaseEntity(this SiteModel model, out List<int> relatedSites)
        {
            relatedSites = null;

            if (model != null)
            {
                relatedSites = model.RelatedSite;
                model.MunicipalityPermissionPicture = !String.IsNullOrEmpty(model.MunicipalityPermissionPictureJson) ?
                                                        JsonConvert.DeserializeObject<List<FileModel>>(model.MunicipalityPermissionPictureJson) : null;

                Site entity = new Site
                {
                    SiteCode = model.SiteCode,
                    IsActive = model.IsActive,
                    Latitude = model.Latitude,
                    Longitude = model.Longitude,
                    MunicipalityPermission = model.MunicipalityPermission,
                    MunicipalityPermissionDate = !String.IsNullOrEmpty(model.MunicipalityPermissionDate) ?
                                                    CommonHelper.GetDateFromModel(model.MunicipalityPermissionDate) : (DateTime?)null,
                    SiteCity = model.SiteCity,
                    SiteGovernorates = model.SiteGovernorates,
                    SiteId = model.SiteId,
                    SiteName = model.SiteName,
                    SiteNote = model.SiteNote,
                    SiteTypeId = model.SiteType,
                    StreetName = model.StreetName,
                    SiteAddress = model.SiteAddress,
                    LocationSubTypeId = model.LocationSubType,
                    LocationSubTypeNote = model.LocationSubTypeNote,
                    TXSiteTypeId = model.TXSiteType,
                    PhaseNumber = model.PhaseNumber,
                    OnAirDate = !String.IsNullOrEmpty(model.OnAirDate) ?
                                  (DateTime?)CommonHelper.GetDateFromModel(model.OnAirDate) : null,
                    PayRent = model.PayRent,
                    SiteCodePrefix = model.SiteCodePrefix,
                    MunicipalityPermissionPicture = model.MunicipalityPermissionPicture != null ? model.MunicipalityPermissionPicture.Select(f => new FileEntity
                    {
                        Name = f.Name,
                        GeneratedName = f.GeneratedName,
                        Path = f.Path,
                        Content = f.Content
                    }).ToList() : null,
                };

                return entity;
            }

            return null;
        }

        public static SiteModel FromDatabaseEntity(this Site entity, List<SiteGroup> siteGroups)
        {
            if (entity != null)
            {
                SiteModel model = new SiteModel
                {
                    SiteCode = entity.SiteCode,
                    IsActive = entity.IsActive,
                    Latitude = entity.Latitude,
                    Longitude = entity.Longitude,
                    MunicipalityPermission = entity.MunicipalityPermission,
                    MunicipalityPermissionDate = entity.MunicipalityPermissionDate != null ? Convert.ToString(entity.MunicipalityPermissionDate) : null,
                    SiteCity = entity.SiteCity,
                    SiteAddress = entity.SiteAddress,

                    SiteGovernorates = entity.SiteGovernorates,
                    SiteId = entity.SiteId,
                    SiteName = entity.SiteName,
                    SiteNote = entity.SiteNote,
                    SiteType = entity.SiteTypeId,
                    StreetName = entity.StreetName,
                    LocationSubType = entity.LocationSubTypeId != null ? entity.LocationSubTypeId.Value : 0,
                    LocationSubTypeNote = entity.LocationSubTypeNote,
                    TXSiteType = entity.TXSiteTypeId != null ? entity.TXSiteTypeId.Value : 0,
                    PhaseNumber = entity.PhaseNumber,
                    OnAirDate = entity.OnAirDate != null ? Convert.ToString(entity.OnAirDate) : null,
                    PayRent = entity.PayRent,
                    SiteCodePrefix = entity.SiteCodePrefix,
                    RelatedSite = siteGroups != null ? siteGroups.Select(s => s.SiteId).ToList() : null,
                    PendingFields = entity.PendingFields,
                    MunicipalityPermissionPicture = !String.IsNullOrEmpty(entity.MunicipalityPermissionPicturePath) ? entity.MunicipalityPermissionPicturePath.Split('|').Select(p => new FileModel()
                    {
                        Name = FileManager.GetFileNameFromPath(p),
                        GeneratedName = FileManager.GetGeneratedFileNameFromPath(p),
                        Path = p,
                        Content = FileManager.GetFileBase64String(p),
                    }).ToList() : null,
                };

                if (model.MunicipalityPermissionPicture != null)
                {
                    model.MunicipalityPermissionPictureJson = JsonConvert.SerializeObject(model.MunicipalityPermissionPicture);
                }

                return model;
            }

            return null;
        }

        private static string GetMarkerPath(Site site)
        {
            string markerPath = string.Empty;

            if (site.IsActive)
            {
                markerPath = SiteMarkerType.Blue_A.ToMapLocationPath();
            }
            else
            {
                markerPath = SiteMarkerType.Red_A.ToMapLocationPath();
            }

            return markerPath;
        }

        public static MapModel ToMapModel(this Site site)
        {
            if (site != null)
            {
                MapModel mapModel = new MapModel();
                mapModel.Id = site.SiteId;
                mapModel.Code = site.SiteCode;
                mapModel.Latitude = site.Latitude;
                mapModel.Longitude = site.Longitude;
                mapModel.Title = site.SiteName;
                //mapModel.Html = string.Format(@"{0}, {1}, {2}", site.SiteName, site.SiteCity, site.StreetName);
                mapModel.Html = string.Format(@"
                        <div>
                            <h4 class='site-title'>{0}</h4>
                            <h5>City: {2}</h5>
                            <h5>Street: {3}</h5>
                        </div>
                        <br />
                        <div class='maps-popup-buttons-container'>
                            <button type='button' class='btn btn-primary' onclick='editSite({1});'>Edit Site</button>
                            <button type='button' class='btn btn-primary' onclick='viewSite({1});'>View Site</button>
                        </div>
                    ", site.SiteName, site.SiteId, site.SiteCity, site.StreetName);

                //mapModel.Icon = "http://maps.google.com/mapfiles/markerA.png";
                //mapModel.Icon = "http://localhost/SiteManagement/images/map-location-pink.png";
                mapModel.Icon = GetMarkerPath(site);

                //mapModel.Zoom = 8;

                return mapModel;
            }

            return null;
        }

        public static List<MapModel> ToMapListModel(this IEnumerable<Site> entityList)
        {
            if (entityList != null)
            {
                List<MapModel> mapListModel = new List<MapModel>();

                foreach (Site site in entityList)
                {
                    mapListModel.Add(site.ToMapModel());
                }

                return mapListModel;
            }

            return null;
        }

        //public enum GoogleIcon
        //{
        //    //'http://maps.google.com/mapfiles/markerA.png'
        //    MarkerA = 1,
        //    //'http://maps.google.com/mapfiles/markerB.png'
        //    MarkerB = 2,
        //    //'http://maps.google.com/mapfiles/markerC.png'
        //    MarkerC = 3,
        //}
    }
}