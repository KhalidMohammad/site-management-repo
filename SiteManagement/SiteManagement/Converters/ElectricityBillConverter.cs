﻿using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class ElectricityBillConverter
    {

        public static ElectricityBill ToDatabaseEntity(this ElectricityBillModel model)
        {
            if (model != null)
            {
                return new ElectricityBill
                {
                    Amount = model.Amount,
                    //TODO_khalid convert to date
                    //BillDate = model.BillDate,
                    BillId = model.Id,
                    Consumption = model.Consumption,
                    ElectricityMeterId = model.ElectricityMeterId,
                    NewReading = model.NewReading,
                    OldReading = model.OldReading,
                    ElectricityBillCode = model.ElectricityBillCode
                };
            }

            return null;
        }

        public static ElectricityBillModel FromDatabaseEntity(this ElectricityBill entity)
        {
            if (entity != null)
            {
                return new ElectricityBillModel
                {
                    Amount = entity.Amount,
                    //TODO_khalid convert to date
                    //BillDate = entity.BillDate,
                    Id = entity.BillId,
                    Consumption = entity.Consumption,
                    ElectricityMeterId = entity.ElectricityMeterId,
                    NewReading = entity.NewReading,
                    OldReading = entity.OldReading,
                    ElectricityBillCode = entity.ElectricityBillCode
                };
            }

            return null;
        }

    }
}