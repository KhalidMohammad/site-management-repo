﻿using Newtonsoft.Json;
using SiteManagement.Business;
using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement
{
    public static class LookupConverter
    {
        public static Lookup ToDatabaseEntity(this LookupModel model)
        {
            if (model != null)
            {
                Lookup entity = new Lookup
                {
                    Id = model.Id,
                    DisplayName = model.DisplayName,
                    LookupTypeId = model.Type
                };

                return entity;
            }

            return null;
        }

        public static LookupModel FromDatabaseEntity(this Lookup entity)
        {
            if (entity != null)
            {
                LookupModel model = new LookupModel
                {
                    Id = entity.Id,
                    DisplayName = entity.DisplayName,
                    Type = entity.LookupTypeId,
                    TypeString = ((LookupTypeEnum)entity.LookupTypeId).ToString()
                };

                return model;
            }

            return null;
        }
    }
}