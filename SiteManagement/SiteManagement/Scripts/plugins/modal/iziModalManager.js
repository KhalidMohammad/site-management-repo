﻿(function () {
    this.ModalManager = this.ModalManager || {};

    /*
        This manager is created to customize the iziModal plugin

        iziModal documentation
        http://izimodal.marcelodolce.com/


        our custom options: 
        {
            onModalCloseButtonClick:.., 
        }

        iziModal options:
        {
            title: '',
            subtitle: '',
            headerColor: '#88A0B9',
            background: null,
            theme: '',  // light
            icon: null,
            iconText: null,
            iconColor: '',
            rtl: false,
            width: 600,
            top: null,
            bottom: null,
            borderBottom: true,
            padding: 0,
            radius: 3,
            zindex: 999,
            iframe: false,
            iframeHeight: 400,
            iframeURL: null,
            focusInput: true,
            group: '',
            loop: false,
            arrowKeys: true,
            navigateCaption: true,
            navigateArrows: true, // Boolean, 'closeToModal', 'closeScreenEdge'
            history: false,
            restoreDefaultContent: false,
            autoOpen: 0, // Boolean, Number
            bodyOverflow: false,
            fullscreen: false,
            openFullscreen: false,
            closeOnEscape: true,
            closeButton: true,
            appendTo: 'body', // or false
            appendToOverlay: 'body', // or false
            overlay: true,
            overlayClose: true,
            overlayColor: 'rgba(0, 0, 0, 0.4)',
            timeout: false,
            timeoutProgressbar: false,
            pauseOnHover: false,
            timeoutProgressbarColor: 'rgba(255,255,255,0.5)',
            transitionIn: 'comingIn',
            transitionOut: 'comingOut',
            transitionInOverlay: 'fadeIn',
            transitionOutOverlay: 'fadeOut',
            onFullscreen: function(){},
            onResize: function(){},
            onOpening: function(){},
            onOpened: function(){},
            onClosing: function(){},
            onClosed: function(){},
            afterRender: function(){}
        }

        buttons: 
        [
            { 
                label:.., 
                onClick:.., 
                classes: "btn-default/btn-primary/btn-success/btn-info/btn-warning/btn-danger/....", 
            }
        ]
    */

    var createModal = function (content, title, buttons, optOptions) {
        optOptions = optOptions || {};

        var modalDiv = $("<div>").addClass("custom-modal");

        if (optOptions.hasOwnProperty("modalClassName")) {
            modalDiv.addClass(optOptions.modalClassName);
        }

        var modalBody = $("<div>")
                            .addClass("custom-modal-body")
                            //.addClass("row")
                            .append(content);

        modalDiv.append(modalBody);

        var modalActions = {
            close: function () {
                modalDiv.iziModal('close');
            }
        };

        var options = $.extend(
        {
            title: title,
            padding: 5,
            closeOnEscape: false,
            overlayClose: false,
            group: 'alert',
            arrowKeys: false,
            navigateCaption: false,
            navigateArrows: false,
            //top: 10,
            //bottom: 10,
        }, optOptions, {
            onClosed: function () {
                // Override any user onClosed event to distroy the modal bafore callback

                modalDiv.iziModal('destroy');
                modalDiv.remove();

                if (optOptions.onClosed && optOptions.onClosed instanceof Function) {
                    optOptions.onClosed();
                }
            },
            onFullscreen: function (iziModalObject) {
                var isFullScreen = iziModalObject.$element.hasClass("isFullscreen");

                if (optOptions.onFullscreen && optOptions.onFullscreen instanceof Function) {
                    optOptions.onFullscreen(isFullScreen);
                }
            },
            onResize: function (iziModalObject) {
                // Resize the modal to include our custom footer, coz the iziModal did not add any customization for footers

                var hasFooter = iziModalObject.$element.hasClass("hasFooter");

                if (hasFooter) {
                    var elementHeight = iziModalObject.$element.outerHeight();
                    
                    var headerHeight = iziModalObject.$header.outerHeight();
                    var wrapHeight = iziModalObject.$wrap.outerHeight();
                    var footerHeight = iziModalObject.$element.find(".modal-footer").outerHeight(true);

                    var outerHeight = headerHeight + wrapHeight + footerHeight;

                    // Important Note: For the first time the modal is opened, this function is fired and the modal is not completely created,
                    //  so we will not find any class name 'hasScroll', coz the scrollable is not applied yet to the modal,
                    //  so we add another check to make sure if the whole modal height is grater than window height or not
                    var hasScroll = iziModalObject.$element.hasClass("hasScroll") || outerHeight > $(window).height();
                    
                    if (hasScroll) {
                        // If the modal is scrollable, that means the modal height is fit to window and there is no space for modal footer,
                        //  then, we should reduce the wrap height to give a space for modal footer to be shown on window
                        //  so, here we should recalculate the wrap height
                        var maxWrapHeight = elementHeight - (headerHeight + footerHeight);
                        iziModalObject.$wrap.css("max-height", maxWrapHeight);

                        // Return back the modal to its default
                        iziModalObject.$element.css("min-height", "none");
                    }
                    else {
                        // If the modal is NOT scrollable, we should expand/fit the modal height to include the footer, 
                        //  coz the modal height represents the height of (headerHeight + wrapHeight)
                        //  so, here we should recalculate the modal height to be (headerHeight + wrapHeight + footerHeight)
                        var minModalHeight = outerHeight;
                        iziModalObject.$element.css("min-height", minModalHeight);

                        // Return back the wrap to its default
                        iziModalObject.$wrap.css("max-height", "none");
                    }
                }

                if (optOptions.onResize && optOptions.onResize instanceof Function) {
                    optOptions.onResize();
                }
            }
        });

        modalDiv.iziModal(options);

        // Important Note: Bind click event to modal close button after create iziModal object
        if (optOptions.onModalCloseButtonClick && optOptions.onModalCloseButtonClick instanceof Function) {
            modalDiv.find(".iziModal-header .iziModal-button-close").click(function () {
                optOptions.onModalCloseButtonClick.call(this, modalActions);
            });
        }

        // Important Note: Create modal footer and its buttons after create iziModal object
        //  coz, when we bind click events to modal buttons, the events is not applied, 
        //   maybe coz when create iziModal object, the iziModal internally removes any created event inside it,
        //   so for this reason we create the buttons and bind events on it after create iziModal
        if (buttons && buttons.length > 0) {
            modalDiv.addClass("hasFooter");

            var modalFooter = $("<div>")
                                    .addClass("modal-footer")
                                    .addClass("custom-modal-footer");
            //.addClass("row");

            //modalDiv.find(".iziModal-content").append(modalFooter);
            modalDiv.append(modalFooter);

            var bindButtonClick = function (buttonElement, onClickCallback) {
                buttonElement.click(function () {
                    if (onClickCallback && onClickCallback instanceof Function) {
                        onClickCallback(modalActions);
                    }
                    else {
                        modalActions.close();
                    }
                });
            };

            for (var i = 0; i < buttons.length; i++) {
                var buttonOptions = buttons[i];

                var buttonClasses = buttonOptions.hasOwnProperty("classes") ? buttonOptions.classes : "btn-primary";

                var buttonElement = $("<button type='button'>")
                                            .addClass("btn")
                                            .addClass(buttonClasses)
                                            .append(buttonOptions.label);

                modalFooter.append(buttonElement);

                bindButtonClick(buttonElement, buttonOptions.onClick);
            }
        }

        modalDiv.iziModal("open");

        return modalActions;
    };

    ModalManager.showModal = function (content, title, buttons, optOptions) {
        return createModal(content, title, buttons, optOptions);
    };

    /*
    var createMessageModal = function (content, title, buttons, optOptions, iconClass) {
        var modalButtons = [];

        for (var i = 0; i < buttons.length; i++) {
            var button = buttons[i];

            var buttonElement = '<button>' + button.label + '</button>';

            modalButtons.push([buttonElement, function (instance, toast) {
                if (button.onClick && button.onClick instanceof Function) {
                    button.onClick(instance, toast);
                }
                else {
                    instance.hide(toast, { transitionOut: 'fadeOut' }, 'button');
                }
            }
            ]);
        }

        iziToast.show({
            title: title,
            message: content,
            progressBar: false,
            position: 'center',
            layout: 2,
            timeout: false,
            drag: false,
            overlay: true,
            buttons: modalButtons,
        });
    };
    */

    var createMessageModal = function (content, title, buttons, optOptions, iconClass) {
        var options = $.extend({}, optOptions);

        var modalMessageContainer = $("<div>")
                                        .addClass("custom-modal-msg");

        // a. Create message icon
        var iconContainer = $("<div>")
                                .addClass("custom-msg-icon-container")
                                .addClass("col-lg-1 col-md-1 col-sm-1 col-xs-2");

        var icon = $("<i>")
                        .addClass("custom-msg-icon")
                        .addClass(iconClass);

        iconContainer.append(icon);

        // b. Create message content
        var contentContainer = $("<div>")
                                    .addClass("custom-msg-content-container")
                                    .addClass("col-lg-11 col-md-11 col-sm-11 col-xs-10");

        var contentTable = $("<div>")
                                .addClass("custom-msg-content-table");

        contentContainer.append(contentTable);

        var contentCell = $("<div>")
                                .addClass("custom-msg-content-table-cell")
                                .append(content);

        contentTable.append(contentCell);


        modalMessageContainer.append(iconContainer).append(contentContainer);

        return createModal(modalMessageContainer, title, buttons, options);
    }

    ModalManager.showConfirmModal = function (content, optOptions, optYesCallback, optNoCallback) {
        var options = $.extend({ modalClassName: "custom-confirm-modal", onModalCloseButtonClick: optNoCallback, fullscreen: false, width: 560 }, optOptions);
        var buttons = [
            { label: "Yes", onClick: optYesCallback, classes: "btn-success" },
            { label: "No", onClick: optNoCallback, classes: "btn-default" }
        ];

        // Create modal direct
        return createModal(content, "Confirmation", buttons, options);
    };

    ModalManager.showErrorModal = function (content, optOptions, optOkCallback) {
        //icon: "fa fa-times-circle", iconText: "Error Message", iconColor: "red"
        var options = $.extend({ modalClassName: "custom-error-modal", onModalCloseButtonClick: optOkCallback, fullscreen: false, width: 560 }, optOptions);
        var buttons = [
            { label: "OK", onClick: optOkCallback, classes: "btn-danger" },
        ];

        return createMessageModal(content, "Error", buttons, options, "custom-modal-error-icon fa fa-times-circle");
    };

    ModalManager.showWarningModal = function (content, optOptions, optOkCallback) {
        var options = $.extend({ modalClassName: "custom-warning-modal", onModalCloseButtonClick: optOkCallback, fullscreen: false, width: 560 }, optOptions);
        var buttons = [
            { label: "OK", onClick: optOkCallback, classes: "btn-warning" },
        ];

        return createMessageModal(content, "Warning", buttons, options, "custom-modal-warning-icon fa fa-exclamation-triangle");
    };

    ModalManager.showInformationModal = function (content, optOptions, optOkCallback) {
        var options = $.extend({ modalClassName: "custom-info-modal", onModalCloseButtonClick: optOkCallback, fullscreen: false, width: 560 }, optOptions);
        var buttons = [
            { label: "OK", onClick: optOkCallback, classes: "btn-info" },
        ];

        return createMessageModal(content, "Information", buttons, options, "custom-modal-info-icon fa fa-info-circle");
    };

})();