﻿(function () {
    this.Ui = this.Ui || {};

    var ns = this.Ui;

    /***************************************************************************************************/
    /******************************************** Upload File ******************************************/

    /*
        options: { 
                    id:.., 
                    files: [{ fileID:.., content:.., name:.., extension:.., size:.., thumbnail:.. }], 
                    maximumFileSize: 524000000, 
                    accept: "file_extension,audio/*,video/*,image/*,media_type", 
                    saveUrl:.., 
                    downloadUrl:.., 
                    hiddenField:.., 
                    validationField:.., 
                    isReadOnly: true/false, 
                    localizer: {
                        BROWSER_UPLOAD_SUPPORT_MESSAGE: "Can\'t upload files in this browser", 
                        REMOVE_FILE_CONFIRMATION_MESSAGE: "Are you sure you want to remove the file", 
                        WARNING: "Warning", 
                        EMPTY_FILE_VALIDATION_MESSAGE: "Empty file is not accepted", 
                        FILE_SIZE_VALIDATION_MESSAGE_1: "This file is too large, the maximum size is", 
                        FILE_SIZE_VALIDATION_MESSAGE_2: "you must select a smaller file", 
                        FILE_FORMAT_VALIDATION_MESSAGE: "File format is not supported", 
                        WAITING_READ_FILE_MESSAGE: "Please wait while read the file", 
                        UPLOAD_ERROR_MESSAGE: "Error while uploading the file", 
                        CHOOSE: "Choose", 
                        CHANGE: "Change", 
                    }
                }

        accept: "file_extension,audio/*,video/*,image/*,media_type"
            file_extension: A file extension starting with the STOP character, e.g: .gif, .jpg, .png, .doc
            media_type: A valid media type, with no parameters. Look at IANA Media Types for a complete list of standard media types

        IMPORTANT NOTE: (The needed js files to import before use this class)
          ajaxManager.js
          CommonHelper.js
          ModalHelper.js

    */
    ns.UploadFile = (function (parent) {
        UploadFile.prototype = new ns.UploadBase();
        UploadFile.prototype.constructor = UploadFile;

        function UploadFile(options) {
            options.localizer = {
                BROWSER_UPLOAD_SUPPORT_MESSAGE: "Can\'t upload files in this browser",
                REMOVE_FILE_CONFIRMATION_MESSAGE: "Are you sure you want to remove the file",
                WARNING: "Warning",
                EMPTY_FILE_VALIDATION_MESSAGE: "Empty file is not accepted",
                FILE_SIZE_VALIDATION_MESSAGE_1: "This file is too large, the maximum size is",
                FILE_SIZE_VALIDATION_MESSAGE_2: "you must select a smaller file",
                FILE_FORMAT_VALIDATION_MESSAGE: "File format is not supported",
                WAITING_READ_FILE_MESSAGE: "Please wait while read the file",
                UPLOAD_ERROR_MESSAGE: "Error while uploading the file",
                CHOOSE: "Choose",
                CHANGE: "Change",
            };

            this.downloadIcon = $("<span>")
                                    .addClass("upload-file-download-file-icon")
                                    .addClass("fa fa-cloud-download");

            this.downloadLinkContainer = $("<div>")
                                                .addClass("upload-file-download-link-container");

            this.removeFileIcon = $("<span>")
                                        .addClass("upload-file-remove-file")
                                        .addClass("fa fa-times");

            this.uploadButton = $("<button type='button'>")
                                    .addClass("upload-file-upload-button")
                                    .addClass("btn btn-primary");

            this.fileBlock = $("<div>")
                                .addClass("upload-file-block");

            parent.call(this, options);

            if (this.options.dir == "rtl") {
                applyRightToLeftDirection.call(this);
            }

            changeButtonTextToChoose.call(this);
        }

        var applyRightToLeftDirection = function () {
            this.downloadIcon.addClass("dir-rtl");
            this.downloadLinkContainer.addClass("dir-rtl");
            this.removeFileIcon.addClass("dir-rtl");
            this.uploadButton.addClass("dir-rtl");
        };

        var setValue = function (file) {
            var me = this;

            this.downloadLinkContainer.empty();

            var downloadFileLink = $("<a>")
                                        .addClass("upload-file-download-link")
                                        .html(file.name);

            if (this.options.downloadUrl) {
                this.downloadLinkContainer.addClass("allow-download");
                downloadFileLink.addClass("allow-download");

                downloadFileLink.click(function () {
                    me.onDownloadFileClick(file);
                });
            }

            this.downloadLinkContainer.append(downloadFileLink);

            showFile.call(this);
        };

        var removeFileFromUi = function () {
            this.downloadLinkContainer.empty();

            hideFile.call(this);
            changeButtonTextToChoose.call(this);
        };

        var showFile = function () {
            this.downloadIcon.css("display", "inline-block");
            this.downloadLinkContainer.css("display", "inline-block");

            if (!this.options.isReadOnly) {
                this.removeFileIcon.css("display", "inline-block");
            }
        };

        var hideFile = function () {
            this.downloadIcon.css("display", "none");
            this.downloadLinkContainer.css("display", "none");

            if (!this.options.isReadOnly) {
                this.removeFileIcon.css("display", "none");
            }
        };

        var changeButtonTextToChange = function () {
            this.uploadButton.html(this.options.localizer["CHANGE"]);
        };

        var changeButtonTextToChoose = function () {
            this.uploadButton.html(this.options.localizer["CHOOSE"] + "...");
        };

        var onValidationSuccess = function () {
            this.fileBlock.removeClass("control-validation-error");
        };

        var onValidationFail = function () {
            this.fileBlock.addClass("control-validation-error");
        };

        //#region ___________________________ Overridden Methods ___________________________

        UploadFile.prototype.onBeforeUploadClick = function () {
            this.uploadButton.addClass("disabled");
        };

        UploadFile.prototype.onAfterUploadClick = function () {
            this.uploadButton.removeClass("disabled");
        };

        UploadFile.prototype.renderFile = function (file) {
            setValue.call(this, file);
            changeButtonTextToChange.call(this);
        };

        UploadFile.prototype.showValidationErrorMessage = function (errorMessage) {
            showLocalValidation.call(this, errorMessage);
        };

        UploadFile.prototype.renderFileWithProgress = function (file) {
            var events = {};

            setValue.call(this, file);

            //TODO: Create progress bar

            var me = this;
            var onSaveSuccess = function () {
                hideLocalValidation.call(me);
                changeButtonTextToChange.call(me);
            };

            var onSaveFail = function (textStatus, error) {
                showLocalValidation.call(me, me.options.localizer["UPLOAD_ERROR_MESSAGE"]);
            };

            events["onSaveSuccess"] = onSaveSuccess;
            events["onSaveFail"] = onSaveFail;

            return events;
        };

        UploadFile.prototype.onAfterChange = function () {
            if (this.hiddenField) {
                var hiddenFiles = this.getFiles();

                this.hiddenField.val(JSON.stringify(hiddenFiles));
                this.hiddenField.valid();
            }
        };

        UploadFile.prototype.removeFileFromUi = function () {

        };

        UploadFile.prototype.previewFile = function (fileContent) {

        };

        //#endregion ___________________________ Overridden Methods ___________________________


        //#region ___________________________ File Local Validation ___________________________

        var createLocalValidationInput = function (controlContainer) {
            this.localValidationField = $("<span>")
                                            .addClass("text-danger")
                                            .css("display", "none");

            controlContainer.append(this.localValidationField);
        };

        var showLocalValidation = function (validationMessage) {
            this.localValidationField.empty();

            this.localValidationField
                        .css("display", "block")
                        .append(validationMessage);

        };

        var hideLocalValidation = function () {
            this.localValidationField.css("display", "none");
        };

        //#endregion ___________________________ File Local Validation ___________________________

        //#region ___________________________ File Block Creation ___________________________

        var createFileBlock = function (container) {
            var me = this;

            container.append(this.fileBlock);

            // 1. Render the file input field (this field will be hidden on the screen)
            container.append(this.uploadField);

            // 2. Create download icon
            if (this.options.downloadUrl) {
                this.fileBlock.append(this.downloadIcon);
            }

            // 3. Create download link
            this.fileBlock.append(this.downloadLinkContainer);

            if (!this.options.isReadOnly) {
                // 4. Create remove file icon
                this.fileBlock.append(this.removeFileIcon);

                this.removeFileIcon.click(function () {
                    me.onRemoveFileClick(me.options.files[me.options.files.length - 1], function () {
                        removeFileFromUi.call(me);
                    });
                });

                // 5. Customize our upload file button (Create an input field and a button to upload)
                this.fileBlock.append(this.uploadButton);

                // When user click on our customize button, internally the method fires the click event of uploadField control
                this.uploadButton.click(function () {
                    me.onUploadFileButtonClick(); // fire upload base method
                });
            }
        };

        //#endregion ___________________________ File Block Creation ___________________________

        UploadFile.prototype.buildControl = function (controlContainer) {
            var container = $("<div>")
                                .addClass("upload-file-upload-container");

            // 1. Create file block
            createFileBlock.call(this, container);
            controlContainer.append(container);

            // 2. Create local hidden field
            createLocalValidationInput.call(this, controlContainer);

            // 3. Create hidden field
            if (this.options.hiddenField) {
                this.hiddenField = $(this.options.hiddenField);
                controlContainer.append(this.hiddenField);

                var me = this;
                this.hiddenField.bind("onSuccess", function () {
                    onValidationSuccess.call(me);
                });

                this.hiddenField.bind("onError", function () {
                    onValidationFail.call(me);
                });
            }

            // 4. Create validation field
            if (this.options.validationField) {
                this.validationField = $(this.options.validationField);
                controlContainer.append(this.validationField);
            }
        };

        return UploadFile;
    })(ns.UploadBase);
})();
