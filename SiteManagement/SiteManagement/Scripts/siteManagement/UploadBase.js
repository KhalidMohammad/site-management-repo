﻿(function () {
    this.Ui = this.Ui || {};

    var ns = this.Ui;

    /***************************************************************************************************/
    /******************************************** Upload Base ******************************************/

    ns.UploadBase = (function () {
        function UploadBase(options) {
            this.options = options;

            var me = this;
            this.uploadField = $("<input type='file' name='files[]'>")
                                    .addClass("upload-input-file")
                                    .css("display", "none");

            // Add on change event for upload field control
            this.uploadField.change(function (e) {
                var uploadedFiles = $.extend(true, [], this.files);
                onUploadFieldChange.call(me, uploadedFiles);
            });

            this.uploadField.click(function (e) {
                me.onBeforeUploadClick();
                e.stopPropagation();
                setTimeout(function () {
                    me.onAfterUploadClick();
                }, 1000);
            });

            this.extensions;

            if (options) {
                this.options.dir = this.options.dir || "ltr";

                options.maximumFileSize = options.maximumFileSize || 524000000; //3145728 (3 MB), 524000000 (500 MB)

                if (options.id) {
                    this.uploadField.attr("id", options.id);
                }

                if (options.multiple) {
                    options.multiple = options.multiple.toString().toLowerCase() == "true";
                    this.uploadField.attr("multiple", options.multiple);
                }

                if (options.isReadOnly) {
                    options.isReadOnly = options.isReadOnly.toString().toLowerCase() == "true";
                }

                if (options.accept) {
                    this.uploadField.attr("accept", options.accept);
                    this.extensions = options.accept.split(",");
                }

                options.files = options.files || [];
                if (options.filePath) {
                    setFiles.call(this);
                }
            }
        }

        //#region ___________________________ Base Methods ___________________________

        UploadBase.prototype.onUploadFileButtonClick = function () {
            if (isUploadFilesSupportedByBrowser.call(this)) {
                this.uploadField.click();
            }
            else {
                ModalHelper.showErrorModal(this.options.localizer["BROWSER_UPLOAD_SUPPORT_MESSAGE"]);
            }
        };

        UploadBase.prototype.onRemoveFileClick = function (file, onRemoveCallback) {
            var me = this;
            var message = this.options.localizer["REMOVE_FILE_CONFIRMATION_MESSAGE"] + " " + file.name + " ?";

            ModalHelper.showConfirmModal(message, function (modal) {
                // 1. Remove the file itself from ui
                if (onRemoveCallback && onRemoveCallback instanceof Function) {
                    onRemoveCallback();
                }

                // 2. Delete file from files list
                var id = file.id;
                $.each(me.options.files, function (index) {
                    if (me.options.files[index].id == id) {
                        deleteFile.call(me, index);
                        return false;
                    }
                });

                // 3. Reflect the files changes
                me.onAfterChange();

                modal.close();
            });
        };

        UploadBase.prototype.onPreviewFileClick = function (file) {
            if (file.content) {
                this.previewFile(file.content);
            }
        };

        UploadBase.prototype.onDownloadFileClick = function (file) {
            //window.location.href = this.options.downloadUrl + "?fileID=" + encodeURIComponent(file.fileID);
            if (this.options.filePath) {
                window.location.href = this.options.downloadUrl + "?filePath=" + this.options.filePath;
            }
        };

        UploadBase.prototype.isEmpty = function () {
            return !this.options.files || this.options.files.length == 0;
        };

        UploadBase.prototype.resetFile = function () {
            // Delete the previous file
            deleteFile.call(this, this.options.files.length - 1);
            this.removeFileFromUi();
        };

        UploadBase.prototype.getFileTypeFromContent = function (file) {
            var type = "";

            var content = file.content || file.thumbnail;
            if (content) {
                type = content.substring(0, content.indexOf(";")).replace("data:", "");
            }

            return type;
        };

        UploadBase.prototype.getFiles = function () {
            var filesList = [];

            for (var i = 0; i < this.options.files.length; i++) {
                var file = this.options.files[i];
                filesList.push(file.content);
            }

            return filesList;
        };

        //#endregion ___________________________ Base Methods ___________________________

        //#region ___________________________ Override Methods ___________________________

        UploadBase.prototype.onBeforeUploadClick = function () {
            throw "You should implement the \'onBeforeUploadClick\' method";
        };

        UploadBase.prototype.onAfterUploadClick = function () {
            throw "You should implement the \'onAfterUploadClick\' method";
        };

        UploadBase.prototype.renderFile = function (file) {
            throw "You should implement the \'renderFile\' method";
        };

        UploadBase.prototype.showValidationErrorMessage = function (errorMessage) {
            throw "You should implement the \'showValidationErrorMessage\' method";
        };

        UploadBase.prototype.renderFileWithProgress = function (file) {
            throw "You should implement the \'renderFileWithProgress\' method";

            var events = {};

            var onSaveSuccess = function () {

            };

            var onSaveFail = function (textStatus, error) {

            };

            events["onSaveSuccess"] = onSaveSuccess;
            events["onSaveFail"] = onSaveFail;

            return events;
        };

        UploadBase.prototype.onAfterChange = function () {
            throw "You should implement the \'onAfterChange\' method";
        };

        UploadBase.prototype.removeFilesFromUi = function () {
            throw "You should implement the \'removeFilesFromUi\' method";
        };

        UploadBase.prototype.previewFile = function (fileContent) {
            throw "You should implement the \'previewFile\' method";
        };

        //#endregion ___________________________ Override Methods ___________________________


        //#region ___________________________ Set Files ___________________________

        var setFiles = function () {
            var file = createFileObjectFromUploadedFile.call(this, { name: this.options.fileName || "File" }, null);

            this.renderFile(file);

            this.options.files.push(file);

            this.onAfterChange();
        };

        //#endregion ___________________________ Set Value ___________________________

        //#region ___________________________ On Upload File Click ___________________________

        var isUploadFilesSupportedByBrowser = function () {
            var isSupportedByBrowser = false;

            // Check for the various File API support.
            if (window.File && window.FileReader && window.FileList && window.Blob) {
                // Great success! All the File APIs are supported.
                isSupportedByBrowser = true;
            }

            return isSupportedByBrowser;
        };

        var validateFile = function (file) {
            var errorMessage;

            if (file.size == 0) {
                errorMessage = this.options.localizer["EMPTY_FILE_VALIDATION_MESSAGE"];
            }
            else if (file.size >= this.options.maximumFileSize) {
                var formatBytes = CommonHelper.formatBytes(this.options.maximumFileSize);
                errorMessage = this.options.localizer["FILE_SIZE_VALIDATION_MESSAGE_1"] + " " + formatBytes + ", " + this.options.localizer["FILE_SIZE_VALIDATION_MESSAGE_2"];
            }
            else if (this.options.checkImageDimension) {
                var _URL = window.URL || window.webkitURL;

                var img = new Image();
                img.onload = function () {
                    alert(this.width + " " + this.height);
                };
                img.onerror = function () {
                    alert("not a valid file: " + file.type);
                };
                img.src = _URL.createObjectURL(file);
            }
            else {
                if (this.extensions && this.extensions.length > 0) {
                    var isValid = false;

                    var fileExtension = CommonHelper.getExtension(file.name); //ex: .jpeg, .docx, .pdf, .mp3

                    var splitedFileMimeType = file.type.split("/"); //ex: image/*, video/*, audio/*, image/png, application/pdf
                    var fileMimeType = splitedFileMimeType[0];
                    var fileMimeTypeExtension = splitedFileMimeType[1];

                    //Important Note: The this.extensions list may consist of file 'extension' or file 'mime type'
                    for (var i = 0; i < this.extensions.length; i++) {
                        var currentExtension = this.extensions[i];

                        //if (currentExtension.startsWith(".")) {
                        //Important Note: Use indexOf instead of startsWith coz startsWith does not work on IE
                        if (currentExtension.indexOf(".") == 0) {
                            // Check file extension (ex: .jpeg, .docx)
                            if (fileExtension == currentExtension) {
                                isValid = true;
                                break;
                            }
                        }
                        else if (currentExtension.indexOf("/") != -1) {
                            // Check file mime type (ex: image/*, video/*, audio/*, image/png, application/pdf)
                            var splitedMimeType = currentExtension.split("/");
                            var currentMimeType = splitedMimeType[0];
                            var currentMimeTypeExtension = splitedMimeType[1];

                            if (fileMimeType == currentMimeType) { // Check the first part of mime type
                                if (currentMimeTypeExtension == "*" // if the second part is *, this means that all files of this type is accepted
                                        || fileMimeTypeExtension == currentMimeTypeExtension) { // otherwise check if the second part of mime type is equals to the file mime type
                                    isValid = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (!isValid) {
                        errorMessage = this.options.localizer["FILE_FORMAT_VALIDATION_MESSAGE"] + " (" +
                            this.options.localizer["FILE_EXTENSIONS_VALIDATION_MESSAGE"] + ": " + this.extensions.join(", ").replace(/\./g, "") + ")";
                    }
                }
            }

            return errorMessage;
        };

        var onUploadFieldChange = function (uploadedFiles) {
            var me = this;

            if (uploadedFiles.length > 0) {
                for (var i = 0; i < uploadedFiles.length; i++) {
                    var uploadedFile = uploadedFiles[i];

                    // Check if the uploaded file is valid
                    var errorMessage = validateFile.call(this, uploadedFile);
                    if (errorMessage) {
                        // if the uploaded file is single file then reset the previous value
                        if (!this.options.multiple) {
                            this.resetFile();
                        }

                        this.showValidationErrorMessage(errorMessage);
                    } else {
                        // Create a file reader
                        var reader = new FileReader();
                        reader.uploadedFile = uploadedFile; // Hack: Cache each uploaded file inside its reader to save the file reference object
                        reader.readAsDataURL(uploadedFile);

                        reader.onload = function (e) {
                            var uploadedFile = this.uploadedFile;

                            // Check if the browser completed reading the file
                            if (e.target.readyState == FileReader.DONE) {
                                // Get the file bytes/result from e.target.result
                                var uploadedFileContent = e.target.result;
                                if (uploadedFileContent) {
                                    renderAndSaveFile.call(me, uploadedFile, uploadedFileContent);
                                }
                            } else {
                                ModalHelper.showWarningModal(me.options.localizer["WAITING_READ_FILE_MESSAGE"]);
                            }
                        }
                    }
                }

                // Fix a bug after upload all files (Remove all files from the input field control after draw and save all of them).
                this.uploadField.val('');
            }
        };

        var renderAndSaveFile = function (uploadedFile, uploadedFileContent) {
            // if the uploaded file is single file then reset the previous value
            if (!this.options.multiple) {
                this.resetFile();
            }

            var file = createFileObjectFromUploadedFile.call(this, uploadedFile, uploadedFileContent);
            this.options.files.push(file);

            this.renderFileWithProgress(file);

            // Reflect the files changes
            this.onAfterChange();
        };

        var createFileObjectFromUploadedFile = function (uploadedFile, uploadedFileContent) {
            var file = {
                id: CommonHelper.generateRandomIdFromArrayOfObject("file", this.options.files, "id"),
                fileID: 0,
                content: uploadedFileContent,
                name: uploadedFile.name,
                extension: CommonHelper.getExtension(uploadedFile.name),
                size: uploadedFile.size,
                type: uploadedFile.type,
            };

            return file;
        };

        //#endregion ___________________________ On Upload File Click ___________________________

        //#region ___________________________ On After Change on File ___________________________

        var deleteFile = function (index) {
            if (this.options.files && this.options.files.length > 0) {
                this.options.files.splice(index, 1);
            }
        };

        //#endregion ___________________________ On After Change on File ___________________________


        return UploadBase;
    })();
})();
