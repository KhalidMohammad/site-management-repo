﻿(function () {
    //*******************************************************
    // Very Important Note:
    // We create this helper to show modals that is already created and cached on the DOM
    // So for the first time on _MasterLayout we should call some methods from HtmlHelperExtension.cs that returns a partial views containing the following modals:
    // (Error Modal, Warning Modal, Information Modal, Confirm Modal) and these modals contans a unique id on the DOM.
    // Then, on everywhere we call the any of ModalHelper functions the appopriate modal will be shown. 
    // So when use this helper for the first time, it is important to check if the modals above is created, coz this helper depends on specific ids to show its.
    //*******************************************************

    this.ModalHelper = this.ModalHelper || {};

    var showBaseModal = function (modalId, content, contentContainerClass, buttons) {
        // This function is shared between all type of modals (MessageModals or ConfirmMessageModal)

        // Get the appropriate modal from the DOM
        var modalDialog = $("#" + modalId);

        // Prepare modal actions
        var modalActions = {
            close: function () {
                modalDialog.modal("hide");
            },
            closeAll: function () {
                $(".modal").modal("hide");
            },
        };

        // Set modal content
        modalDialog.find("." + contentContainerClass).html(content);

        var bindButtonClick = function (buttonElement, callback) {
            buttonElement.click(function () {
                if (callback && callback instanceof Function) {
                    callback(modalActions);
                }
                else {
                    modalActions.close();
                }
            });
        };

        if (buttons && buttons.length > 0) {
            for (var i = 0; i < buttons.length; i++) {
                var button = buttons[i];

                // Find button element by class name
                var buttonElement = modalDialog.find("." + button.buttonClassName);

                // Add click event to button element
                bindButtonClick(buttonElement, button.callback);
            }
        }

        modalDialog.modal("show");

        return modalActions;
    };

    // This function is shared between all modals that contains a simple message with OK button
    //  like: ErrorModal, WarningModal, InformationModal
    var showMessageModal = function (modalId, content, optOkCallback) {
        var buttons = [
            { buttonClassName: "ok-btn", callback: optOkCallback },
            { buttonClassName: "modal-close-button", callback: optOkCallback },
        ];

        return showBaseModal(modalId, content, "modal-custom-content-table-cell", buttons);
    };

    ModalHelper.showErrorModal = function (content, optOkCallback) {
        return showMessageModal("errorDefaultModal", content, optOkCallback);
    };

    ModalHelper.showWarningModal = function (content, optOkCallback) {
        return showMessageModal("warningDefaultModal", content, optOkCallback);
    };

    ModalHelper.showInformationModal = function (content, optOkCallback) {
        return showMessageModal("informationDefaultModal", content, optOkCallback);
    };

    ModalHelper.showConfirmModal = function (content, optYesCallback, optNoCallback) {
        var buttons = [
            { buttonClassName: "yes-btn", callback: optYesCallback },
            { buttonClassName: "no-btn", callback: optNoCallback },
            { buttonClassName: "modal-close-button", callback: optNoCallback },
        ];

        return showBaseModal("confirmDefaultModal", content, "modal-body", buttons);
    };

    ModalHelper.showDefaultModal = function (content, title, onModalClosing) {
        var buttons = [
            { buttonClassName: "modal-close-button", callback: onModalClosing },
        ];

        var modalId = "defaultModal";
        $("#" + modalId).find(".modal-title").text(title);

        return showBaseModal(modalId, content, "modal-body", buttons);
    };
})();