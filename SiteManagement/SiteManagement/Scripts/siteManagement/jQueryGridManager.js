﻿(function () {
    this.Ui = this.Ui || {};

    var ns = this.Ui;

    ns.JQueryGridManager = (function () {
        function JQueryGridManager(options) {
            this.options = options;

        }

        var createGrid = function (tableDiv) {
            var table = $("<table>")
                            .attr("id", "ownerTable")
                            .addClass("hover");

            createHeader.call(this, table);
            
            tableDiv.append(table);

            $(document).ready(function () {

            });
        };
        
        var createHeader = function (table) {
            var thead = $("<thead>");

            var tr = $("<tr>");

            for (var i = 0; i < this.options.header.length; i++) {
                var header = this.options.header[i];
                var th = $("<th>").append(header);

                tr.append(th);
            }

            thead.append(tr);
            table.append(thead);
        };

        JQueryGridManager.prototype.buildControl = function (container) {
            var div = $("<div>").addClass("row");
            var tableDiv = $("<div>").addClass("table-responsive");

            createGrid.call(this, tableDiv);

            div.append(tableDiv);
            container.append(div);
        };

        return JQueryGridManager;
    })();
})();
