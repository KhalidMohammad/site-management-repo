﻿(function () {
    this.Ui = this.Ui || {};

    var ns = this.Ui;

    /***************************************************************************************************/
    /***************************************** DataTable Manager ***************************************/

    /*
        options: { 
                    header: [{ id:.., title }], 
                    data: [{ headerId: data, ..... }],
                    editRecordCallback:..,
                    deleteRecordCallback:..,
                    actionButtons: []
                 }
    */
    ns.JQueryGridManager = (function () {
        function JQueryGridManager(options) {
            this.options = options;
            this.options.localizer = {
                ACTIONS: "Actions"
            };

            this.hasButtons = this.options.editRecordCallback || this.options.deleteRecordCallback ||
                                (actionButtons && actionButtons.length > 0);
        }

        var createGrid = function (tableDiv) {
            var table = $("<table>")
                            .attr("id", "ownerTable")
                            .addClass("hover");

            createHeader.call(this, table);
            
            tableDiv.append(table);

            $(document).ready(function () {

            });
        };
        
        var createHeader = function (table) {
            var thead = $("<thead>");

            var tr = $("<tr>");

            for (var i = 0; i < this.options.header.length; i++) {
                var header = this.options.header[i];
                var th = $("<th>").append(header.title);

                tr.append(th);
            }

            if (this.hasButtons) {
                var th = $("<th>").append(this.options.localizer["ACTIONS"]);
                tr.append(th);
            }

            thead.append(tr);
            table.append(thead);
        };

        var getGridColumns = function () {
            var columns = [];

            for (var i = 0; i < this.options.header.length; i++) {
                var header = this.options.header[i];
                columns.push({ data: header.id });
            }

            if (this.hasButtons) {
                columns.push({ data: "button" });
            }

            return columns;
        };

        var getActionButtons = function () {
            this.hasButtons = this.options.editRecordCallback || this.options.deleteRecordCallback ||
                    (actionButtons && actionButtons.length > 0);

            var buttons = [];
            if (this.options.editRecordCallback) {
                buttons.push("<div class='btn btn-primary btn-edit'>Edit</div>");
            }

            if (this.options.deleteRecordCallback) {
                buttons.push("<div class='btn btn-primary btn-delete'>Delete</div>");
            }
        };

        var convertGridToDataTable = function (table, columns, data) {
            var dataTableOptions = {
                responsive: true,
                data: data,
                columns: columns,
                "columnDefs": [{
                    "targets": -1,
                    "data": null,
                    "defaultContent": "<div class='btn btn-primary btn-edit'>Edit</div> <div class='btn btn-primary btn-delete'>Delete</div>"
                }]
            };

            var dataTable = table.dataTable(dataTableOptions);
        };

        JQueryGridManager.prototype.buildControl = function (container) {
            var div = $("<div>").addClass("row");
            var tableDiv = $("<div>").addClass("table-responsive");

            createGrid.call(this, tableDiv);

            div.append(tableDiv);
            container.append(div);
        };

        return JQueryGridManager;
    })();
})();
