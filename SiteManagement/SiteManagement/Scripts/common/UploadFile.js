﻿(function () {
    this.Ui = this.Ui || {};

    var ns = this.Ui;

    /***************************************************************************************************/
    /******************************************** Upload File ******************************************/

    /*
        options: { 
                    id:.., 
                    maximumFileSize: 524000000, 
                    files: [{ fileID:.., name:.., path:.., content:.. }, ...], 
                    accept: "file_extension,audio/*,video/*,image/*,media_type", 
                    downloadUrl:.., 
                    hiddenField:.., 
                    isReadOnly: true/false, 
                }

        accept: "file_extension,audio/*,video/*,image/*,media_type"
            file_extension: A file extension starting with the STOP character, e.g: .gif, .jpg, .png, .doc
            media_type: A valid media type, with no parameters. Look at IANA Media Types for a complete list of standard media types

        IMPORTANT NOTE: (The needed js files to import before use this class)
          ajaxManager.js
          CommonHelper.js
          ModalManager.js

    */
    ns.UploadFile = (function (parent) {
        UploadFile.prototype = new ns.UploadBase();
        UploadFile.prototype.constructor = UploadFile;

        function UploadFile(options) {
            options.localizer = {
                BROWSER_UPLOAD_SUPPORT_MESSAGE: "Can\'t upload files in this browser",
                REMOVE_FILE_CONFIRMATION_MESSAGE: "Are you sure you want to remove the file",
                //WARNING: "Warning",
                EMPTY_FILE_VALIDATION_MESSAGE: "Empty file is not accepted",
                FILE_SIZE_VALIDATION_MESSAGE_1: "This file is too large, the maximum size is",
                FILE_SIZE_VALIDATION_MESSAGE_2: "you must select a smaller file",
                FILE_FORMAT_VALIDATION_MESSAGE: "File format is not supported",
                WAITING_READ_FILE_MESSAGE: "Please wait while read the file",
                //UPLOAD_ERROR_MESSAGE: "Error while uploading the file",
                CHOOSE: "Choose",
            };

            this.filesBlock = $("<div>").addClass("upload-files-blocks").addClass("col-lg-9 col-md-9 col-sm-9 col-xs-9");

            this.uploadButton = $("<button type='button'>")
                                    .addClass("upload-file-upload-button")
                                    .addClass("btn btn-primary")
                                    .addClass("col-lg-3 col-md-3 col-sm-3 col-xs-3")
                                    .html(options.localizer["CHOOSE"]);

            parent.call(this, options);

            if (this.options.dir == "rtl") {
                applyRightToLeftDirection.call(this);
            }
        }

        var applyRightToLeftDirection = function () {
            this.downloadIcon.addClass("dir-rtl");
            this.downloadLinkContainer.addClass("dir-rtl");
            this.removeFileIcon.addClass("dir-rtl");
            this.uploadButton.addClass("dir-rtl");
        };

        var onValidationSuccess = function () {
            this.filesBlock.removeClass("control-validation-error");
        };

        var onValidationFail = function () {
            this.filesBlock.addClass("control-validation-error");
        };

        //#region ___________________________ Overridden Methods ___________________________

        UploadFile.prototype.renderFile = function (file) {
            createFileBlock.call(this, file);
            hideLocalValidation.call(this);
        };

        UploadFile.prototype.showValidationErrorMessage = function (errorMessage) {
            showLocalValidation.call(this, errorMessage);
        };

        UploadFile.prototype.onAfterChange = function () {
            if (this.hiddenField) {
                var hiddenFiles = this.getFiles();
                this.hiddenField.val(JSON.stringify(hiddenFiles));
                this.hiddenField.valid();
            }
        };

        UploadFile.prototype.emptyFiles = function () {
            this.filesBlock.empty();
        };

        //#endregion ___________________________ Overridden Methods ___________________________


        //#region ___________________________ File Local Validation ___________________________

        var createLocalValidationInput = function (controlContainer) {
            this.localValidationField = $("<span>")
                                            .addClass("text-danger")
                                            .css("display", "none");

            controlContainer.append(this.localValidationField);
        };

        var showLocalValidation = function (validationMessage) {
            this.localValidationField.empty();

            this.localValidationField
                        .css("display", "block")
                        .append(validationMessage);

        };

        var hideLocalValidation = function () {
            this.localValidationField.css("display", "none");
        };

        //#endregion ___________________________ File Local Validation ___________________________

        //#region ___________________________ File Block Creation ___________________________

        var createFileBlock = function (file) {
            var me = this;

            var isDownloadable = this.options.downloadUrl && file.path;

            var fileBlock = $("<div>").addClass("upload-file-block");
            this.filesBlock.append(fileBlock);

            // 1. Create download icon
            if (isDownloadable) {
                var downloadIcon = $("<span>")
                                        .addClass("upload-file-download-file-icon")
                                        .addClass("fa fa-download");

                fileBlock.append(downloadIcon);
            }

            // 2. Create file name
            var fileNameContainer = $("<div>").addClass("upload-file-name-container");
            fileBlock.append(fileNameContainer);

            var fileNameLinkLink = $("<a>")
                                        .addClass("upload-file-name-link")
                                        .html(file.name);

            fileNameContainer.append(fileNameLinkLink);

            // Make the file name downloadable
            if (isDownloadable) {
                fileNameContainer.addClass("allow-download");
                fileNameLinkLink.addClass("allow-download");

                fileNameLinkLink.click(function () {
                    me.onDownloadFileClick(file);
                });
            }

            // 3. Create remove file icon
            if (!this.options.isReadOnly) {
                var removeFileIcon = $("<span>")
                                            .addClass("upload-file-remove-file")
                                            .addClass("fa fa-times");

                fileBlock.append(removeFileIcon);

                removeFileIcon.click(function () {
                    me.onRemoveFileClick(file, function () {
                        fileBlock.remove();
                    });
                });
            }
        };

        //#endregion ___________________________ File Block Creation ___________________________

        UploadFile.prototype.buildControl = function (controlContainer) {
            var me = this;

            var container = $("<div>")
                                .addClass("upload-file-container")
                                .addClass("row");

            controlContainer.append(container);

            // 1. Create file block
            container.append(this.filesBlock);
            //createFileBlock.call(this, container);
            

            if (!this.options.isReadOnly) {
                // 1. Render the file input field (this field will be hidden on the screen)
                container.append(this.uploadField);

                // 2. Customize our upload file button (Create an input field and a button to upload)
                container.append(this.uploadButton);

                // When user click on our customize button, internally the method fires the click event of uploadField control
                this.uploadButton.click(function () {
                    me.onUploadFileButtonClick(); // fire upload base method
                });
            }

            // 3. Create local hidden field
            createLocalValidationInput.call(this, controlContainer);

            // 4. Create hidden field
            if (this.options.hiddenField) {
                this.hiddenField = $(this.options.hiddenField);
                controlContainer.append(this.hiddenField);

                this.hiddenField.bind("onSuccess", function () {
                    onValidationSuccess.call(me);
                });

                this.hiddenField.bind("onError", function () {
                    onValidationFail.call(me);
                });
            }

            if (this.options.files.length > 0) {
                this.setFiles();
            }
        };

        return UploadFile;
    })(ns.UploadBase);
})();
