﻿(function () {


    function CustomappendModelPrefix(value, prefix) {
        if (value.indexOf("*.") === 0) {
            value = value.replace("*.", prefix);
        }
        return value;
    }

    function CustomescapeAttributeValue(value) {
        // As mentioned on http://api.jquery.com/category/selectors/
        return value.replace(/([!"#$%&'()*+,./:;<=>?@\[\\\]^`{|}~])/g, "\\$1");
    }


    $.validator.unobtrusive.options = {
        errorPlacement: function ($error, $element) {
            if ($error.text().length === 0) {
                $element.triggerHandler("onSuccess");
            } else {
                $element.triggerHandler("onError");
            }
        }
    };

    $.validator.unobtrusive.adapters.addBool("mandatorycheckbox", "required");

    $.validator.unobtrusive.adapters.add(
                    'filerequired',
                    function (options) {
                        options.rules['filerequired'] = {
                            errormessages: options.params['errormessages']
                        };
                        options.messages['filerequired'] = options.message;
                    });

    $.validator.addMethod('filerequired',
        function (value, element, parameters) {
            if (!value)
                return false;

            var files = $.parseJSON(value);
            var isValid = false;

            isValid = files.some(function (item, index, array) {
                return !item.isDeleted;
            });

            if (!isValid)
                return false;

            return true;
        }
    );

    $.validator.unobtrusive.adapters.add(
                   'extensionfile', ['extensions'],
                   function (options) {
                       options.rules['extensionfile'] = {
                           errormessages: options.params['errormessages'],
                           extensions: options.params['extensions']
                       };
                       options.messages['extensionfile'] = options.message;
                   });

    $.validator.addMethod('extensionfile',
        function (value, element, parameters) {
            if (!value)
                return true;

            var isValid = true;
            var files = $.parseJSON(value);
            var extensions = $.parseJSON(parameters["extensions"]);

            if ((files && files.length > 0) && (extensions && extensions.length > 0)) {
                var uploadedFiles = files.filter(function (file) {
                    return !file.isDeleted;
                });

                if (uploadedFiles && uploadedFiles.length > 0) {
                    var isFileExtensionMatched = false;

                    $.each(uploadedFiles, function (index, file) {
                        var fileExtension = file.extension; //ex: .jpeg, .docx, .pdf, .mp3

                        var splitedFileMimeType = file.type.split("/"); //ex: image/*, video/*, audio/*, image/png, application/pdf
                        var fileMimeType = splitedFileMimeType[0];
                        var fileMimeTypeExtension = splitedFileMimeType[1];

                        //Important Note: The this.extensions list may consist of file 'extension' or file 'mime type'
                        for (var i = 0; i < extensions.length; i++) {
                            var currentExtension = extensions[i];
                            //Important Note: Use indexOf instead of startsWith coz startsWith does not work on IE
                            if (currentExtension.indexOf(".") == 0) {
                                // Check file extension (ex: .jpeg, .docx)
                                if (fileExtension == currentExtension) {
                                    isFileExtensionMatched = true;
                                    break;
                                }
                            }
                            else if (currentExtension.indexOf("/") != -1) {
                                // Check file mime type (ex: image/*, video/*, audio/*, image/png, application/pdf)
                                var splitedMimeType = currentExtension.split("/");
                                var currentMimeType = splitedMimeType[0];
                                var currentMimeTypeExtension = splitedMimeType[1];

                                if (fileMimeType == currentMimeType) { // Check the first part of mime type
                                    if (currentMimeTypeExtension == "*" // if the second part is *, this means that all files of this type is accepted
                                            || fileMimeTypeExtension == currentMimeTypeExtension) { // otherwise check if the second part of mime type is equals to the file mime type
                                        isFileExtensionMatched = true;
                                        break;
                                    }
                                }
                            }
                        }

                        if (!isFileExtensionMatched) {
                            return false;
                        }
                    });

                    isValid = isFileExtensionMatched;

                }
            }

            return isValid;
        }
    );

})();


(function ($) {

    $.fn.resetValidation = function () {

        var $form = this.closest('form');

        //reset jQuery Validate's internals
        $form.validate().resetForm();

        //reset unobtrusive validation summary, if it exists
        $form.find("[data-valmsg-summary=true]")
            .removeClass("validation-summary-errors")
            .addClass("validation-summary-valid")
            .find("ul").empty();

        //reset unobtrusive field level, if it exists
        $form.find("[data-valmsg-replace]")
            .removeClass("field-validation-error")
            .addClass("field-validation-valid")
            .empty();

        return $form;
    };

    $.fn.setError = function (message) {
        var span = $(this);
        if (span && span.length > 0) {
            $(span).html(message);
            if (message && message != "") {
                $(span).removeClass("field-validation-valid");
                $(span).addClass("field-validation-no-valid");
            } else {
                $(span).removeClass("field-validation-no-valid");
                $(span).addClass("field-validation-valid");
            }
        }
    }
}(jQuery));
