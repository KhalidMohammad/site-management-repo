﻿(function () {
    this.AjaxManager = this.AjaxManager || {};

    var ajax = function (type, url, data, successCallback, failCallback, options) {
        options = options || {};

        $.ajax({
            type: type,
            url: url,
            data: data,
            success: function (data) {
                if (successCallback) {
                    successCallback(data);
                }
                //switch (data.MessageType) {
                //    case 1:
                //        if (successCallback) {
                //            successCallback(data);
                //        }
                //        break;
                //    case 2:
                //        //TODO_N
                //        //ModalManager.showErrorModal(data.MessageText);
                //        break;
                //    case 3:
                //        //TODO_N
                //        //var validator = $("#frm").data("validator");
                //        //var errors = $.parseJSON(data.errors);
                //        //validator.showErrors(errors);
                //        break;
                //}
            },
            error: function (jqXHR, textStatus, error) {
                if (failCallback) {
                    failCallback(textStatus, error);
                }
            },
            beforeSend: function () {
                if (!options.hideLoading) {
                    $('body').jLoadingOverlay('');
                }
            },
            complete: function () {
                if (!options.hideLoading) {
                    $('body').jLoadingOverlay('close');
                }
            }
        });
    };

    AjaxManager = {
        post: function (url, data, successCallback, failCallback, options) {
            ajax("post", url, data, successCallback, failCallback, options);
        },
        get: function (url, data, successCallback, failCallback, options) {
            ajax("get", url, data, successCallback, failCallback, options);
        }
    }
})();
