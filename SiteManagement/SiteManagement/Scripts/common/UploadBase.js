﻿(function () {
    this.Ui = this.Ui || {};

    var ns = this.Ui;

    /***************************************************************************************************/
    /******************************************** Upload Base ******************************************/

    ns.UploadBase = (function () {
        function UploadBase(options) {
            this.options = options;

            var me = this;
            this.uploadField = $("<input type='file' name='files[]'>")
                                    .addClass("upload-input-file")
                                    .css("display", "none");

            // Add on change event for upload field control
            this.uploadField.change(function (e) {
                var uploadedFiles = $.extend(true, [], this.files);
                onUploadFieldChange.call(me, uploadedFiles);
            });

            this.extensions;

            if (options) {
                this.options.dir = this.options.dir || "ltr";

                options.maximumFileSize = options.maximumFileSize || 524000000; //3145728 (3 MB), 524000000 (500 MB)

                if (options.id) {
                    this.uploadField.attr("id", options.id);
                }

                if (options.multiple) {
                    options.multiple = options.multiple.toString().toLowerCase() == "true";
                    this.uploadField.attr("multiple", options.multiple);
                }

                if (options.isReadOnly) {
                    options.isReadOnly = options.isReadOnly.toString().toLowerCase() == "true";
                }

                if (options.accept) {
                    this.uploadField.attr("accept", options.accept);
                    this.extensions = options.accept.split(",");
                }

                options.files = options.files || [];
            }
        }

        UploadBase.prototype.onUploadFileButtonClick = function () {
            this.uploadField.click();
        };

        UploadBase.prototype.onRemoveFileClick = function (file, onRemoveCallback) {
            var me = this;
            var message = this.options.localizer["REMOVE_FILE_CONFIRMATION_MESSAGE"] + " " + file.name + " ?";

            ModalManager.showConfirmModal(message, null, function (modal) {
                // 1. Remove the file itself from ui
                if (onRemoveCallback && onRemoveCallback instanceof Function) {
                    onRemoveCallback();
                }

                // 2. Delete file from files list
                var id = file.id;
                $.each(me.options.files, function (index) {
                    if (me.options.files[index].id == id) {
                        deleteFile.call(me, index);
                        return false;
                    }
                });

                // 3. Reflect the files changes
                me.onAfterChange();

                modal.close();
            });
        };

        UploadBase.prototype.onDownloadFileClick = function (file) {
            //window.location.href = this.options.downloadUrl + "?fileID=" + encodeURIComponent(file.fileID);
            if (file.path) {
                window.location.href = this.options.downloadUrl + "?filePath=" + file.path;
            }
        };

        UploadBase.prototype.isEmpty = function () {
            return !this.options.files || this.options.files.length == 0;
        };

        UploadBase.prototype.resetFile = function () {
            // Delete the previous file
            deleteFile.call(this, this.options.files.length - 1);
            this.emptyFiles();
        };

        UploadBase.prototype.getFiles = function () {
            var filesList = [];

            for (var i = 0; i < this.options.files.length; i++) {
                var file = this.options.files[i];
                filesList.push({
                    name: file.name,
                    generatedName: file.generatedName,
                    path: "",
                    content: file.content
                });
            }

            return filesList;
        };

        UploadBase.prototype.setFiles = function () {
            if (this.options.files.length > 0) {
                for (var i = 0; i < this.options.files.length; i++) {
                    this.options.files[i].id = CommonHelper.generateRandomIdFromArrayOfObject("file", this.options.files, "id");

                    var file = this.options.files[i];
                    this.renderFile(file);
                }
            }

            this.onAfterChange();
        };

        UploadBase.prototype.deleteAllFiles = function () {
            this.options.files = [];
            this.emptyFiles();
        };

        var validateFile = function (file) {
            var errorMessage;

            if (file.size == 0) {
                errorMessage = this.options.localizer["EMPTY_FILE_VALIDATION_MESSAGE"];
            }
            else if (file.size >= this.options.maximumFileSize) {
                var formatBytes = CommonHelper.formatBytes(this.options.maximumFileSize);
                errorMessage = this.options.localizer["FILE_SIZE_VALIDATION_MESSAGE_1"] + " " + formatBytes + ", " + this.options.localizer["FILE_SIZE_VALIDATION_MESSAGE_2"];
            }
            else if (this.options.checkImageDimension) {
                var _URL = window.URL || window.webkitURL;

                var img = new Image();
                img.onload = function () {
                    alert(this.width + " " + this.height);
                };
                img.onerror = function () {
                    alert("not a valid file: " + file.type);
                };
                img.src = _URL.createObjectURL(file);
            }
            else {
                if (this.extensions && this.extensions.length > 0) {
                    var isValid = false;

                    var fileExtension = CommonHelper.getExtension(file.name); //ex: .jpeg, .docx, .pdf, .mp3

                    var splitedFileMimeType = file.type.split("/"); //ex: image/*, video/*, audio/*, image/png, application/pdf
                    var fileMimeType = splitedFileMimeType[0];
                    var fileMimeTypeExtension = splitedFileMimeType[1];

                    //Important Note: The this.extensions list may consist of file 'extension' or file 'mime type'
                    for (var i = 0; i < this.extensions.length; i++) {
                        var currentExtension = this.extensions[i];

                        //if (currentExtension.startsWith(".")) {
                        //Important Note: Use indexOf instead of startsWith coz startsWith does not work on IE
                        if (currentExtension.indexOf(".") == 0) {
                            // Check file extension (ex: .jpeg, .docx)
                            if (fileExtension == currentExtension) {
                                isValid = true;
                                break;
                            }
                        }
                        else if (currentExtension.indexOf("/") != -1) {
                            // Check file mime type (ex: image/*, video/*, audio/*, image/png, application/pdf)
                            var splitedMimeType = currentExtension.split("/");
                            var currentMimeType = splitedMimeType[0];
                            var currentMimeTypeExtension = splitedMimeType[1];

                            if (fileMimeType == currentMimeType) { // Check the first part of mime type
                                if (currentMimeTypeExtension == "*" // if the second part is *, this means that all files of this type is accepted
                                        || fileMimeTypeExtension == currentMimeTypeExtension) { // otherwise check if the second part of mime type is equals to the file mime type
                                    isValid = true;
                                    break;
                                }
                            }
                        }
                    }

                    if (!isValid) {
                        errorMessage = this.options.localizer["FILE_FORMAT_VALIDATION_MESSAGE"] + " (" +
                            this.options.localizer["FILE_EXTENSIONS_VALIDATION_MESSAGE"] + ": " + this.extensions.join(", ").replace(/\./g, "") + ")";
                    }
                }
            }

            return errorMessage;
        };

        var onUploadFieldChange = function (uploadedFiles) {
            var me = this;

            if (uploadedFiles.length > 0) {
                for (var i = 0; i < uploadedFiles.length; i++) {
                    var uploadedFile = uploadedFiles[i];

                    // Check if the uploaded file is valid
                    var errorMessage = validateFile.call(this, uploadedFile);
                    if (errorMessage) {
                        // if the uploaded file is single file then reset the previous value
                        if (!this.options.multiple) {
                            this.resetFile();
                        }

                        this.showValidationErrorMessage(errorMessage);
                    } else {
                        // Create a file reader
                        var reader = new FileReader();
                        reader.uploadedFile = uploadedFile; // Hack: Cache each uploaded file inside its reader to save the file reference object
                        reader.readAsDataURL(uploadedFile);

                        reader.onload = function (e) {
                            var uploadedFile = this.uploadedFile;

                            // Check if the browser completed reading the file
                            if (e.target.readyState == FileReader.DONE) {
                                // Get the file bytes/result from e.target.result
                                var uploadedFileContent = e.target.result;
                                if (uploadedFileContent) {
                                    renderAndSaveFile.call(me, uploadedFile, uploadedFileContent);
                                }
                            } else {
                                ModalManager.showWarningModal(me.options.localizer["WAITING_READ_FILE_MESSAGE"]);
                            }
                        }
                    }
                }

                // Fix a bug after upload all files (Remove all files from the input field control after draw and save all of them).
                this.uploadField.val('');
            }
        };

        var renderAndSaveFile = function (uploadedFile, uploadedFileContent) {
            // if the uploaded file is single file then reset the previous value
            if (!this.options.multiple) {
                this.resetFile();
            }

            var file = {
                id: CommonHelper.generateRandomIdFromArrayOfObject("file", this.options.files, "id"),
                //fileID: 0,
                content: uploadedFileContent,
                extension: CommonHelper.getExtension(uploadedFile.name),
                size: uploadedFile.size,
                type: uploadedFile.type,
                name: uploadedFile.name,
                path: uploadedFile.path || "",
            };

            this.options.files.push(file);

            this.renderFile(file);

            // Reflect the files changes
            this.onAfterChange();
        };

        var deleteFile = function (index) {
            if (this.options.files && this.options.files.length > 0) {
                this.options.files.splice(index, 1);
            }
        };

        return UploadBase;
    })();
})();
