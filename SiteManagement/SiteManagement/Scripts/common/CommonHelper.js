﻿(function () {
    this.CommonHelper = this.CommonHelper || {};

    CommonHelper.generateRandomIdFromArrayOfObject = function (startIdString, uniqueGeneratedIDs, arrayKey) {
        uniqueGeneratedIDs = uniqueGeneratedIDs || [];

        var generetedID;

        if (startIdString) {
            startIdString = startIdString.replace(/\s+/g, '').replace(/[^a-zA-Z 0-9]+/g, '');

            var isUnique = false;
            do {
                var randomNumber = Math.floor(Math.random() * 1000000);
                generetedID = startIdString + randomNumber;

                isUnique = !uniqueGeneratedIDs.some(function (element, index, array) {
                    return element[arrayKey] == generetedID;
                });
            } while (!isUnique);

            return generetedID;
        }
    };

    CommonHelper.setNumberMask = function (e) {
        // 0-9 = 48-57
        // 0-9 = 96-105 (numpad)
        // left = 37, up = 38, right = 39, down = 40
        // backspace = 8, tab = 9, delete = 46
        // ctrl + c = ctrl + 67
        // ctrl + v = ctrl + 86
        // ctrl + x = ctrl + 88

        var ctrlDown = e.ctrlKey || e.metaKey; // Mac support

        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) ||
                e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 46 || 
                (ctrlDown && e.keyCode == 67) || // c
                (ctrlDown && e.keyCode == 86) || // v
                (ctrlDown && e.keyCode == 88) ||// x
                (e.keyCode === 109 || e.keyCode === 189) // negative sign
            )) {
            // Prevent type negative sign or any other key
            return false;
        }
    };

    CommonHelper.setDecimalMask = function (e) {
        // 0-9 = 48-57
        // 0-9 = 96-105 (numpad)
        // left = 37, up = 38, right = 39, down = 40
        // backspace = 8, tab = 9, delete = 46
        // period = 190 (.)
        // period = 110 (.) from numpad
        // ctrl + c = ctrl + 67
        // ctrl + v = ctrl + 86
        // ctrl + x = ctrl + 88

        var ctrlDown = e.ctrlKey || e.metaKey; // Mac support

        if (!((e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 96 && e.keyCode <= 105) || (e.keyCode >= 37 && e.keyCode <= 40) ||
                e.keyCode == 8 || e.keyCode == 9 || e.keyCode == 46 || 
                e.keyCode == 190 || e.keyCode == 110 ||
                (ctrlDown && e.keyCode == 67) || // c
                (ctrlDown && e.keyCode == 86) || // v
                (ctrlDown && e.keyCode == 88) ||// x
                (e.keyCode === 109 || e.keyCode === 189) // negative sign
            )) {
            // Prevent type negative sign or any other key
            return false;
        }
    };

    CommonHelper.getExtension = function (fileName) {
        return "." + fileName.split(".").pop();
    };

    CommonHelper.formatBytes = function (bytes, optDecimal) {
        optDecimal = optDecimal || 0;

        if (bytes < 1024) return bytes + " Bytes";
        else if (bytes < 1048576) return (bytes / 1024).toFixed(optDecimal) + " KB";
        else if (bytes < 1073741824) return (bytes / 1048576).toFixed(optDecimal) + " MB";
        else return (bytes / 1073741824).toFixed(optDecimal) + " GB";
    };

})();