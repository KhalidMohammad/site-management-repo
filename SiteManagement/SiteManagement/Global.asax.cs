﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Threading;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace SiteManagement
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            System.Data.Entity.Database.SetInitializer<SiteManagement.Data.SiteData>(null);
        }
        protected void Application_PostAuthenticateRequest(Object sender, EventArgs e)
        {
            var authCookie = HttpContext.Current.Request.Cookies[FormsAuthentication.FormsCookieName];

            if (authCookie != null)
            {
                try
                {
                    FormsAuthenticationTicket authTicket = FormsAuthentication.Decrypt(authCookie.Value);
                    if (authTicket != null && !authTicket.Expired)
                    {
                        var roles = authTicket.UserData.Split(',');
                        HttpContext.Current.User = new System.Security.Principal.GenericPrincipal(new FormsIdentity(authTicket), roles);
                    }
                }

                catch (CryptographicException cex)
                {
                    FormsAuthentication.SignOut();
                }
            }
        }

        protected void Application_AcquireRequestState(Object sender, EventArgs e)
        {
            ChangeCurrentThreadDateFormat();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
            ChangeCurrentThreadDateFormat();
        }

        private void ChangeCurrentThreadDateFormat()
        {
            string dateFormat = "dd/MM/yyyy";

            CultureInfo cultureInfo = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
            cultureInfo.DateTimeFormat.ShortDatePattern = dateFormat;

            Thread.CurrentThread.CurrentCulture = cultureInfo;
            Thread.CurrentThread.CurrentUICulture = cultureInfo;
        }

        //protected void Application_Error(object sender, EventArgs e)
        //{
        //    Exception exception = Server.GetLastError();
        //    Response.Clear();

        //    HttpException httpException = exception as HttpException;

        //    if (httpException != null)
        //    {
        //        string action;

        //        switch (httpException.GetHttpCode())
        //        {
        //            case 404:
        //                // page not found
        //                action = "HttpError404";
        //                break;
        //            case 500:
        //                // server error
        //                action = "HttpError500";
        //                break;
        //            default:
        //                action = "General";
        //                break;
        //        }

        //        // clear error on server
        //        Server.ClearError();

        //        Response.Redirect(String.Format("~/Error/{0}/?message={1}", action, exception.Message));
        //    }

        //    //if (HttpContext.Current.Request.IsAjaxRequest())
        //    {
        //        HttpContext ctx = HttpContext.Current;
        //        ctx.Response.Clear();
        //        RequestContext rc = ((MvcHandler)ctx.CurrentHandler).RequestContext;
        //        rc.RouteData.Values["action"] = "AjaxGlobalError";

        //        // TODO: distinguish between 404 and other errors if needed
        //        rc.RouteData.Values["newActionName"] = "WrongRequest";

        //        rc.RouteData.Values["controller"] = "ErrorPages";
        //        IControllerFactory factory = ControllerBuilder.Current.GetControllerFactory();
        //        IController controller = factory.CreateController(rc, "ErrorPages");
        //        controller.Execute(rc);
        //        ctx.Server.ClearError();
        //    }
        //}
    }
}