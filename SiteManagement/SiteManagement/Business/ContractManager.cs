﻿using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace SiteManagement.Business
{
    public static class ContractManager
    {
        /// <summary>
        ///  Update contract on database
        /// </summary>
        /// <param name="contractModel"></param>
        /// <returns>void</returns>
        public static void UpdateContract(Contract contract)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                SiteDataManager.CallDBContext(db =>
                {
                    Contract oldContract = db.Contracts.Find(contract.ContractId);

                    if (oldContract != null)
                    {
                        contract.ContractDocumentFilePath = FileManager.SaveOrUpdateFile(oldContract.ContractDocumentFilePath, contract.ContractDocumentFile, FileType.ContractDocument);
                        contract.QoshanFilePath = FileManager.SaveOrUpdateFile(oldContract.QoshanFilePath, contract.QoshanFile, FileType.ContractQushan);
                        contract.TerminationLetterFilePath = FileManager.SaveOrUpdateFile(oldContract.TerminationLetterFilePath, contract.TerminationLetterFile, FileType.ContractTerminationLetter);

                        // TODO
                        contract.CreatedByUserId = oldContract.CreatedByUserId;
                        contract.CreatedDate = oldContract.CreatedDate;

                        UpdateContractYearlyRents(contract, db);

                        db.Entry(oldContract).CurrentValues.SetValues(contract);
                        db.SaveChanges();
                    }
                });

                scope.Complete();
            }
        }

        /// <summary>
        /// Add contract to database
        /// </summary>
        /// <param name="contract"></param>
        /// <returns></returns>
        public static int AddContract(Contract contract)
        {
            int contractId = 0;

            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                SiteDataManager.CallDBContext(db =>
                {
                    contract.ContractDocumentFilePath = string.Join("|", FileManager.SaveFiles(FileType.ContractDocument, contract.ContractDocumentFile));
                    contract.QoshanFilePath = string.Join("|", FileManager.SaveFiles(FileType.ContractQushan, contract.QoshanFile));
                    contract.TerminationLetterFilePath = string.Join("|", FileManager.SaveFiles(FileType.ContractTerminationLetter, contract.TerminationLetterFile));

                    //UpdateContractYearlyRents(contract, db);

                    db.Contracts.Add(contract);

                    db.SaveChanges();
                    //Important Note: This line should be after save changes
                    contractId = contract.ContractId;
                });

                scope.Complete();
            }

            return contractId;
        }

        private static void UpdateContractYearlyRents(Contract contract, SiteData db)
        {
            db.ContractYearlyRents.RemoveRange(db.ContractYearlyRents.Where(c => c.ContractId == contract.ContractId));

            if (contract.ContractYearlyRents != null)
            {
                foreach (ContractYearlyRent rent in contract.ContractYearlyRents)
                {
                    rent.ContractId = contract.ContractId;
                    db.ContractYearlyRents.Add(rent);
                }
            }
        }

        //private static void UpdateContractYearlyRents(Contract contract, SiteData db)
        //{
        //    if (contract.ContractId > 0)
        //    {
        //        if (db.ContractYearlyRents.Any(y => y.ContractId == contract.ContractId))
        //            db.ContractYearlyRents.RemoveRange(db.ContractYearlyRents.Where(y => y.ContractId == contract.ContractId));

        //        if (contract.IsIncremental && contract.IncrementalPercentage.HasValue)
        //        {
        //            DateTime from = contract.ContractStartDate;
        //            DateTime to;
        //            while (from < contract.ContractEndDate.Value)
        //            {
        //                to = from.AddYears(1) <= contract.ContractEndDate ?
        //                            from.AddYears(1) : contract.ContractEndDate.Value;

        //                db.ContractYearlyRents.Add(new Data.ContractYearlyRent
        //                {
        //                    FromDate = from,
        //                    ContractId = contract.ContractId,
        //                    ToDate = to
        //                });

        //                from = to;
        //            }
        //        }
        //    }
        //}
    }
}