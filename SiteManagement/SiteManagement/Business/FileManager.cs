﻿using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SiteManagement.Business
{
    public enum FileType
    {
        ContractDocument = 1,
        Owner = 2,
        ContractQushan = 3,
        ContractTerminationLetter = 4,
        Site = 5,
    }

    public static class FileManager
    {
        private static readonly string BaseDirectory = Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Files");

        #region Private Methods

        private static string GetFileBase64String(string fileName, byte[] content)
        {
            string contentType = MimeMapping.GetMimeMapping(fileName);
            return string.Format("data:{0};base64,{1}", contentType, Convert.ToBase64String(content));
        }


        public static string GetFileBase64String(string fileSubPath)
        {
            string base64String = string.Empty;

            byte[] fileContent = GetFile(fileSubPath);
            if (fileContent != null)
            {
                string contentType = MimeMapping.GetMimeMapping(Path.GetFileName(fileSubPath));
                base64String = string.Format("data:{0};base64,{1}", contentType, Convert.ToBase64String(fileContent));
            }

            return base64String;
        }


        private static byte[] GetFileBytes(string base64)
        {
            return Convert.FromBase64String(base64.Split(",".ToCharArray())[1]);
        }

        #endregion Private Methods

        public static string SaveFile(FileType type, string fileName, string base64)
        {
            string currentDate = DateTime.Now.ToString("yyyyMMddHHmmss");
            string fileSubPath = Path.Combine(type.ToString(), String.Format("{0}-{1}", currentDate, fileName));

            try
            {
                string fileFullPath = Path.Combine(BaseDirectory, fileSubPath);

                string directory = Path.GetDirectoryName(fileFullPath);

                if (!System.IO.Directory.Exists(directory))
                    System.IO.Directory.CreateDirectory(directory);

                using (FileStream stream = new FileStream(fileFullPath, FileMode.Create, FileAccess.Write))
                {
                    byte[] fileByets = GetFileBytes(base64);
                    stream.Write(fileByets, 0, fileByets.Length);
                }

            }
            catch
            {
                throw;
                fileSubPath = null;
            }

            return fileSubPath;

        }

        public static byte[] GetFile(string fileSubPath)
        {
            byte[] fileContent = null;

            string fileFullPath = Path.Combine(BaseDirectory, fileSubPath);
            if (File.Exists(fileFullPath))
            {
                fileContent = System.IO.File.ReadAllBytes(fileFullPath);
            }

            return fileContent;
        }

        public static void DeleteFile(string fileSubPath)
        {
            string fileFullPath = Path.Combine(BaseDirectory, fileSubPath);
            if (File.Exists(fileFullPath))
            {
                File.Delete(fileFullPath);
            }
        }

        /// <summary>
        /// Get the name that is upload by user on ui
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetFileNameFromPath(string filePath)
        {
            string fileName = string.Empty;

            if (!string.IsNullOrEmpty(filePath))
            {
                int fileNameIndex = filePath.IndexOf('-');
                if (fileNameIndex > -1)
                {
                    fileName = filePath.Substring(fileNameIndex + 1);
                }
            }

            return fileName;
        }

        /// <summary>
        /// Get the generated file name by the system (that is stored on the disk and its part of the file path) 
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public static string GetGeneratedFileNameFromPath(string filePath)
        {
            string realFileName = string.Empty;

            if (!string.IsNullOrEmpty(filePath))
            {
                int fileNameIndex = filePath.LastIndexOf('\\');
                if (fileNameIndex > -1)
                {
                    realFileName = filePath.Substring(fileNameIndex + 1);
                }
            }

            return realFileName;
        }

        /// <summary>
        /// Delete all files
        /// </summary>
        /// <param name="filesSubPaths">this variable contains multi files separated by |</param>
        public static void DeleteAllFiles(string filesSubPaths)
        {
            if (!String.IsNullOrWhiteSpace(filesSubPaths))
            {
                string[] filesPaths = filesSubPaths.Split('|');

                for (int i = 0; i < filesPaths.Count(); i++)
                {
                    FileManager.DeleteFile(filesPaths[i]);
                }
            }
        }

        /// <summary>
        /// Check if there are removed files from ui then delete those files from the disk and return the all un-removed files
        /// </summary>
        /// <param name="oldFilesSubPaths"></param>
        /// <param name="newFiles"></param>
        /// <returns></returns>
        public static List<string> DeleteRemovedFiles(string oldFilesSubPaths, List<FileEntity> newFiles)
        {
            List<string> unRemovedFilesSubPaths = new List<string>();

            if (!String.IsNullOrWhiteSpace(oldFilesSubPaths))
            {
                List<string> oldFiles = oldFilesSubPaths.Split('|').ToList();

                if (newFiles != null && newFiles.Count > 0)
                {
                    for (int i = oldFiles.Count - 1; i >= 0; i--)
                    {
                        string oldFileSubPath = oldFiles[i];

                        // if old path is not exist on new files list, rhis means that this file is removed from ui
                        FileEntity file = newFiles.FirstOrDefault(f => !String.IsNullOrEmpty(f.GeneratedName) && oldFileSubPath.EndsWith(f.GeneratedName));
                        if (file == null)
                        {
                            DeleteFile(oldFileSubPath);
                            oldFiles.Remove(oldFileSubPath);
                        }
                    }
                }

                unRemovedFilesSubPaths = oldFiles;
            }

            return unRemovedFilesSubPaths;
        }

        /// <summary>
        /// Save more than one file and return its paths joined by | separator
        /// </summary>
        /// <param name="fileType"></param>
        /// <param name="files"></param>
        /// <returns></returns>
        public static List<string> SaveFiles(FileType fileType, List<FileEntity> files)
        {
            List<string> filesSubPaths = new List<string>();

            if (files != null && files.Count > 0)
            {
                foreach (FileEntity file in files)
                {
                    if (!String.IsNullOrWhiteSpace(file.Content))
                    {
                        filesSubPaths.Add(FileManager.SaveFile(fileType, file.Name, file.Content));
                    }
                }
            }

            return filesSubPaths;
        }

        public static string SaveOrUpdateFile(string oldFilesPaths, List<FileEntity> newFiles, FileType fileType)
        {
            List<string> newFilesPaths = new List<string>();

            // if there are old files stored on db
            if (!String.IsNullOrWhiteSpace(oldFilesPaths))
            {
                // Check if all files deleted from ui then delete all of them from disk
                if (newFiles == null || newFiles.Count == 0)
                {
                    FileManager.DeleteAllFiles(oldFilesPaths);
                }
                else
                {
                    newFilesPaths = FileManager.DeleteRemovedFiles(oldFilesPaths, newFiles);
                }
            }

            // Save the new uploaded files
            if (newFiles != null && newFiles.Count > 0)
            {
                newFilesPaths = newFilesPaths.Concat(FileManager.SaveFiles(fileType, newFiles)).ToList();
            }

            return newFilesPaths != null && newFilesPaths.Count > 0 ? string.Join("|", newFilesPaths) : null;
        }
    }
}