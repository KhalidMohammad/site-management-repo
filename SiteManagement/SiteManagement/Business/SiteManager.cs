﻿using Khalid.Framework.Email;
using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace SiteManagement.Business
{
    public static class SiteManager
    {
        /// <summary>
        ///  Update site on database
        /// </summary>
        /// <param name="siteModel"></param>
        /// <returns>void</returns>
        public static void UpdateSite(Site site)
        {
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                SiteDataManager.CallDBContext(db =>
                {
                    Site oldSite = db.Sites.Find(site.SiteId);

                    if (oldSite != null)
                    {
                        // Important Note: Every time, we should delete the picture file and save the new one, coz we don't have any information
                        //  if the uploaded file is new or not, coz the picture upload control has file content either it is new one or updated one.
                        FileManager.DeleteAllFiles(oldSite.MunicipalityPermissionPicturePath);
                        List<string> newFiles = FileManager.SaveFiles(FileType.Site, site.MunicipalityPermissionPicture);
                        site.MunicipalityPermissionPicturePath = string.Join("|", newFiles);

                        // TODO
                        site.CreatedByUserId = oldSite.CreatedByUserId;
                        site.CreatedDate = oldSite.CreatedDate;

                        LocationHelper.FillSiteInfo(site);

                        db.Entry(oldSite).CurrentValues.SetValues(site);
                        db.SaveChanges();
                    }
                });

                scope.Complete();
            }
        }

        /// <summary>
        /// Add site to database
        /// </summary>
        /// <param name="site"></param>
        /// <returns></returns>
        public static int AddSite(Site site)
        {
            int siteId = 0;

            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                SiteDataManager.CallDBContext(db =>
                {
                    List<string> newFiles = FileManager.SaveFiles(FileType.Site, site.MunicipalityPermissionPicture);
                    site.MunicipalityPermissionPicturePath = string.Join("|", newFiles);

                    LocationHelper.FillSiteInfo(site);
                    db.Sites.Add(site);
                    db.SaveChanges();
                    //Important Note: This line should be after save changes
                    siteId = site.SiteId;
                });

                scope.Complete();
            }

            return siteId;
        }

        internal static void AddUpdateSiteGroup(List<int> relatedSites)
        {
            SiteDataManager.CallDBContext(db =>
            {
                SiteGroup oldGroup = db.SitesGroup.FirstOrDefault(s => relatedSites.Contains(s.SiteId));
                if (oldGroup != null)
                {
                    db.SitesGroup.RemoveRange(db.SitesGroup.Where(sg => sg.GroupId == oldGroup.GroupId));
                    db.SaveChanges();
                    foreach (int siteId in relatedSites)
                    {
                        if (!db.SitesGroup.Any(s => s.SiteId == siteId))
                        {
                            db.SitesGroup.Add(new SiteGroup { GroupId = oldGroup.GroupId, SiteId = siteId });
                        }
                    }

                }
                else
                {
                    Group newGroup = db.Groups.Add(new Group { Name = "" });

                    foreach (int siteId in relatedSites)
                    {
                        db.SitesGroup.Add(new SiteGroup { GroupId = newGroup.GroupId, SiteId = siteId });
                    }
                }
                db.SaveChanges();
            });
        }

        public static int GetSelectStatusId()
        {
            return GetSelectStatusId(SessionManager.CurrentUser);
        }

        public static int GetSelectStatusId(User user)
        {
            int statusId = 0;
            if (user.RoleId == (int)RoleTypeEnum.SuperAdmin)
            {
                statusId = (int)ApprovalStatus.PartialApprove;
            }
            else
            {
                statusId = (int)ApprovalStatus.Pending;
            }
            return statusId;
        }

        public static void FillApprovalStatus(IApprovalEntity entity, Site site)
        {

            entity.ApprovalStatusId = GetInsertStatusId();
            switch ((ApprovalStatus)entity.ApprovalStatusId)
            {
                case ApprovalStatus.Approved:
                    entity.ApprovedByUserId = SessionManager.CurrentUser.UserId;
                    break;
                case ApprovalStatus.PartialApprove:
                    entity.PartialApprovedByUserId = SessionManager.CurrentUser.UserId;
                    
                    break;
                case ApprovalStatus.Pending:
                    break;
            }
            SendApprovalEmail(site, (ApprovalStatus)entity.ApprovalStatusId);

        }

        public static void SendApprovalEmail(Site site, ApprovalStatus state)
        {
            List<string> userMails = new List<string>();
            RoleTypeEnum role = RoleTypeEnum.Unknown;
            switch (state)
            {
                case ApprovalStatus.Pending:
                    role = RoleTypeEnum.Admin;
                    break;
                case ApprovalStatus.PartialApprove:
                    role = RoleTypeEnum.SuperAdmin;
                    break;
            }
            if (role != RoleTypeEnum.Unknown)
            {
                SiteDataManager.CallDBContext(db =>
                {
                    userMails = db.Users.Where(u =>
                              u.RoleId == (int)role &&
                              u.UserEmail != null)
                           .Select(u => u.UserEmail).ToList();
                });

                foreach (string mail in userMails)
                {
                    EmailManager.SendEmail(mail, "Site Pending Approval",
                        string.Format("'{0}' Site is pendig your approval", site.SiteCode));
                }

            }
        }


        public static int GetInsertStatusId()
        {
            int statusId = 0;

            switch ((RoleTypeEnum)SessionManager.CurrentUser.RoleId)
            {
                case RoleTypeEnum.Admin:
                    statusId = (int)ApprovalStatus.PartialApprove;
                    break;

                case RoleTypeEnum.SuperAdmin:
                    statusId = (int)ApprovalStatus.Approved;
                    break;
                case RoleTypeEnum.Guest:
                    statusId = (int)ApprovalStatus.Pending;
                    break;
            }

            //if (SessionManager.CurrentUser.RoleId == (int)RoleTypeEnum.SuperAdmin)
            //{
            //    statusId = (int)ApprovalStatus.Approved;
            //}
            //else if
            //{
            //    statusId = (int)ApprovalStatus.Pending;
            //}
            return statusId;
        }

        public static void FillPendingApprovalFields(Site site, SiteData db)
        {
            int statusId = (int)ApprovalStatus.Approved; //GetSelectStatusId();

            Func<object, bool> checkDelegate = s => !(s as ISavedEntity).FlagDeleted &&
                     (s as IApprovalEntity).ApprovalStatusId != statusId;
            site.PendingFields = new List<string>();
            if (!site.FlagDeleted && site.ApprovalStatusId != statusId)
                site.PendingFields.Add("Site");
            if (site.Owners.Any(checkDelegate))
                site.PendingFields.Add("Owner");
            if (site.Contracts.Any(checkDelegate))
                site.PendingFields.Add("Contract");
            if (db.ElectricityMeters.Any(e => !e.FlagDeleted && e.SiteId == site.SiteId && e.ApprovalStatusId != statusId))
                site.PendingFields.Add("ElectricityMeter");
        }

        public static void ChangeSiteApprovalStatus(int siteId, int statusId)
        {
            Site site = null;
            SiteDataManager.CallDBContext(db =>
            {
                string commandtext = String.Format(@"
                    update [site] set ApprovlStatusId = {0} where siteid = {1} and ApprovlStatusId = {2} AND FlagDeleted = 0
                    update [Owner] set ApprovlStatusId = {0} where siteid = {1} and ApprovlStatusId = {2} AND FlagDeleted = 0
                    update [Contract] set ApprovlStatusId = {0} where siteid = {1} and ApprovlStatusId = {2} AND FlagDeleted = 0
                    update [ElectricityMeter] set ApprovlStatusId = {0} where siteid = {1} and ApprovlStatusId = {2} AND FlagDeleted = 0",
                    statusId,
                    siteId,
                    SiteManager.GetSelectStatusId());

                site = db.Sites.Find(siteId);

                db.Database.ExecuteSqlCommand(commandtext);
                db.SaveChanges();
            });

            SiteManager.SendApprovalEmail(site, (ApprovalStatus)site.ApprovalStatusId);
        }
    }
}