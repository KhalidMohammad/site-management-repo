﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement.Business
{
    public static class SpreadsheetGradersHelper
    {
        internal readonly static char[] AlphabeticLetters = new char[] {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I',
                                                                        'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R',
                                                                        'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'};
        #region Constants
        private const int HLSMAX = 255;
        public const string CHART_TITLE_PARAMETER_NAME = "ChartTitle";
        public const int CHART_FONT_SIZE_RATE = 100; //Chart Legend,Title and Axes Font Size Rate
        public const string CURRENT_CELL_BACKGROUND_HEX_COLOR_PARAMETER_NAME = "CurrentCellBackgroundHexColor";
        public const string CELL_FONT_STYLE_PARAMETER_NAME = "CellFontStyle";
        #endregion

        public static WorksheetPart GetWorksheetByName(SpreadsheetDocument document, string worksheetName)
        {
            WorksheetPart worksheetPart = null;

            WorkbookPart workbookPart = document.WorkbookPart;
            if (workbookPart != null)
            {
                Sheet sheet = workbookPart.Workbook.Descendants<Sheet>().FirstOrDefault(shet =>
                                                                               shet.Name != null &&
                                                                               shet.Name.Value != null &&
                                                                               shet.Name.Value.Equals(worksheetName, StringComparison.InvariantCultureIgnoreCase));
                if (sheet != null)
                {
                    worksheetPart = (WorksheetPart)(workbookPart.GetPartById(sheet.Id));
                }
            }
            return worksheetPart;
        }

        public static Cell GetCellByAddress(WorksheetPart worksheetPart, string cellAddress)
        {
            Cell cell = null;
            cell = worksheetPart.Worksheet.Descendants<Cell>().
                         FirstOrDefault(myCell => myCell.CellReference.ToString().Equals(cellAddress, StringComparison.InvariantCultureIgnoreCase));
            return cell;
        }

        /// <summary>
        /// get all addresses which included in a range
        /// </summary>
        /// <param name="cellRangeAddresses"></param>
        /// <returns></returns>
        public static List<string> GetCellRangeAddresses(string cellRangeAddresses)
        {
            List<string> wantedCellRangeAddressesList = new List<string>();
            cellRangeAddresses = cellRangeAddresses.ToUpperInvariant();

            List<string> firstAndLastAddresses = new List<string>();
            firstAndLastAddresses = cellRangeAddresses.Split(':').ToList<string>();
            string firstColumnRowAddress = firstAndLastAddresses[0];
            string lastColumnRowAddress = firstAndLastAddresses[1];

            int firstLetterIndex = Array.IndexOf(AlphabeticLetters, firstColumnRowAddress.First());
            int lastLetterIndex = Array.IndexOf(AlphabeticLetters, lastColumnRowAddress.First());

            int firstRowAddress = Convert.ToInt32(firstColumnRowAddress.TrimStart(firstColumnRowAddress.First()));
            int lastRowAddress = Convert.ToInt32(lastColumnRowAddress.TrimStart(lastColumnRowAddress.First()));

            for (int i = firstLetterIndex; i <= lastLetterIndex; i++)
            {
                for (int j = firstRowAddress; j <= lastRowAddress; j++)
                {
                    wantedCellRangeAddressesList.Add(String.Format("{0}{1}", AlphabeticLetters[i], j));
                }
            }
            return wantedCellRangeAddressesList;
        }

        /// <summary>
        /// get all cells in one range of addresses
        /// </summary>
        /// <param name="worksheetPart"></param>
        /// <param name="cellRange"></param>
        /// <returns></returns>
        public static List<Cell> GetCellsByAddresses(WorksheetPart worksheetPart, string cellRange)
        {
            List<string> cellAddresses = SpreadsheetGradersHelper.GetCellRangeAddresses(cellRange);

            return cellAddresses.Select(cellAddress =>
                         SpreadsheetGradersHelper.GetCellByAddress(worksheetPart, cellAddress)).ToList();
        }

        /// <summary>
        /// get all cells in multiple ranges of addresses
        /// </summary>
        /// <param name="worksheetPart"></param>
        /// <param name="cellRanges"></param>
        /// <returns></returns>
        public static List<Cell> GetCellsByAddresses(WorksheetPart worksheetPart, List<string> cellRanges)
        {
            return cellRanges.SelectMany(cellRange => GetCellsByAddresses(worksheetPart, cellRange)).ToList();
        }

        public static CellFormat GetCellFormat(WorkbookStylesPart workbookStylesPart,
                                                            Cell theCell)
        {
            CellFormat cellFormat = null;
            int cellStyleIndex;

            if (theCell.StyleIndex == null)
            {
                cellStyleIndex = 0;
            }
            else
            {
                cellStyleIndex = (int)theCell.StyleIndex.Value;
            }

            cellFormat = (CellFormat)workbookStylesPart.Stylesheet.CellFormats.ChildElements[cellStyleIndex];
            return cellFormat;
        }
        //public static bool IsCellContentValid(SpreadsheetDocument document, WorksheetPart worksheet, string cellAddress, bool isMustExist, string text)
        //{
        //    Cell cell = SpreadsheetGradersHelper.GetCellByAddress(worksheet, cellAddress);
        //    if (cell == null)
        //        if (!isMustExist)
        //            return true;
        //        else
        //            return false;
        //    string cellContent = SpreadsheetGradersHelper.GetCellContent(document, cell);
        //    int result;
        //    bool isNumeric = int.TryParse(text, out result);
        //    return isMustExist ? isNumeric ? cellContent == text :
        //        cellContent.ComparePercentageString(text) :
        //        string.IsNullOrWhiteSpace(cellContent);
        //}

        public static bool IsCellContentMatch(SpreadsheetDocument document, Cell cell, List<string> text)
        {
            bool isMatch = false;
            if (cell != null)
            {
                string cellContent = SpreadsheetGradersHelper.GetCellContent(document, cell);
                isMatch = text.Any<string>(t => t == cellContent);
            }
            return isMatch;
        }
        public static string GetCellContent(SpreadsheetDocument document, Cell cell)
        {
            if (cell == null)
            {
                return null;
            }
            string value = "";
            WorkbookPart workbookPart = document.WorkbookPart;
            if (cell.DataType == null) // number & dates
            {
                if (cell.StyleIndex != null)
                {
                    int styleIndex = (int)cell.StyleIndex.Value;
                    CellFormat cellFormat = (CellFormat)workbookPart.WorkbookStylesPart.Stylesheet.CellFormats.ElementAt(styleIndex);
                    uint formatId = cellFormat.NumberFormatId.Value;

                    if (formatId == (uint)14)
                    {
                        double oaDate;
                        if (double.TryParse(cell.InnerText, out oaDate))
                        {
                            value = DateTime.FromOADate(oaDate).ToShortDateString();
                        }
                    }
                    else if (formatId == (uint)10) //Percentage
                    {
                        string oaMony = cell.InnerText;
                        String result = (Decimal.Parse(oaMony) / 100).ToString("C");
                    }
                    else
                        value = cell.InnerText;
                }
                else
                    value = cell.InnerText;
            }
            else // string or boolean
            {
                switch (cell.DataType.Value)
                {
                    case CellValues.SharedString:
                        SharedStringItem ssi = workbookPart.SharedStringTablePart.SharedStringTable.Elements<SharedStringItem>().ElementAt(int.Parse(cell.CellValue.InnerText));
                        value = ssi.Text.Text;
                        break;
                    case CellValues.Boolean:
                        value = cell.CellValue.InnerText == "0" ? "false" : "true";
                        break;
                    default:
                        value = cell.CellValue.InnerText;
                        break;
                }
            }
            return value;
        }
    }
}