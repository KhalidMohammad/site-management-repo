﻿using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace SiteManagement.Business
{
    public static class OwnerManager
    {
        /// <summary>
        ///  Update owner on database
        /// </summary>
        /// <param name="ownerModel"></param>
        /// <returns>void</returns>
        public static void UpdateOwner(Owner owner)
        {
            SiteDataManager.CallDBContext(db =>
            {
                Owner oldOwner = db.Owners.Find(owner.OwnerId);

                if (oldOwner != null)
                {
                    owner.InventoryOfLegacyFilePath = FileManager.SaveOrUpdateFile(oldOwner.InventoryOfLegacyFilePath, owner.InventoryOfLegacyFile, FileType.Owner);

                    // TODO
                    owner.CreatedByUserId = oldOwner.CreatedByUserId;
                    owner.CreatedDate = oldOwner.CreatedDate;

                    db.Entry(oldOwner).CurrentValues.SetValues(owner);
                    db.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Add owner to database
        /// </summary>
        /// <param name="owner"></param>
        /// <returns></returns>
        public static int AddOwner(Owner owner)
        {
            int ownerId = 0;
            SiteDataManager.CallDBContext(db =>
            {
                List<string> newFiles = FileManager.SaveFiles(FileType.Owner, owner.InventoryOfLegacyFile);
                owner.InventoryOfLegacyFilePath = string.Join("|", newFiles);

                db.Owners.Add(owner);
                db.SaveChanges();
                //Important Note: This line should be after save changes
                ownerId = owner.OwnerId;
            });

            return ownerId;
        }
    }
}