﻿using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace SiteManagement.Business
{
    public static class SignInManager
    {
        private static User GetUserByUsernameAndPassword(string username, string password)
        {
            User user = null;

            SiteDataManager.CallDBContext(db =>
            {
                user = db.Users.FirstOrDefault(u => u.UserName.Equals(username, StringComparison.InvariantCultureIgnoreCase) &&
                                u.Password.Equals(password));
            });

            return user;
        }

        public static SignInState SignIn(string username, string password, out User user)
        {
            user = GetUserByUsernameAndPassword(username, password);

            if (user == null)
            {
                return SignInState.Failure;
            }
            else if (!user.IsActive)
            {
                return SignInState.DeactivatedUser;
            }
            else
            {
                return SignInState.Success;
            }
        }
    }
}