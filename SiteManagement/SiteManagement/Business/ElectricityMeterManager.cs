﻿using SiteManagement.Data;
using SiteManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;

namespace SiteManagement.Business
{
    public static class ElectricityMeterManager
    {
        /// <summary>
        /// Update electricity meter technologies by deleting the removed ones and insert the added ones
        /// </summary>
        /// <param name="existingElectricityMeter"></param>
        /// <param name="newElectricityMeterTechnologies"></param>
        private static void UpdateElectricityMeterTechnologies(ElectricityMeter existingElectricityMeter, IEnumerable<ElectricityMeterTechnology> newElectricityMeterTechnologies)
        {
            // Find the deleted technologies
            List<ElectricityMeterTechnology> deletedTechnologies = new List<ElectricityMeterTechnology>();
            foreach (ElectricityMeterTechnology existTechnology in existingElectricityMeter.TechnologiesUsed)
            {
                if (!newElectricityMeterTechnologies.Any(t => t.TechnologyUsedId == existTechnology.TechnologyUsedId))
                {
                    deletedTechnologies.Add(existTechnology);
                }
            }

            // Find the added technologies
            List<ElectricityMeterTechnology> addedTechnologies = new List<ElectricityMeterTechnology>();
            foreach (ElectricityMeterTechnology newTechnology in newElectricityMeterTechnologies)
            {
                if (!existingElectricityMeter.TechnologiesUsed.Any(t => t.TechnologyUsedId == newTechnology.TechnologyUsedId))
                {
                    addedTechnologies.Add(newTechnology);
                }
            }

            //Remove the deleted technologies from the existing electricity meter technologies
            deletedTechnologies.ForEach(t => existingElectricityMeter.TechnologiesUsed.Remove(t));

            // Add new technologies to the existing electricity meter technologies
            addedTechnologies.ForEach(t => { existingElectricityMeter.TechnologiesUsed.Add(t); });
        }

        /// <summary>
        ///  Update electricity meter on database
        /// </summary>
        /// <param name="electricityMeterModel"></param>
        /// <returns>void</returns>
        public static void UpdateElectricityMeter(ElectricityMeter electricityMeter)
        {
            SiteDataManager.CallDBContext(db =>
            {
                ElectricityMeter oldElectricityMeter = db.ElectricityMeters.Find(electricityMeter.ElectricityMeterId);
                if (oldElectricityMeter != null)
                {
                    UpdateElectricityMeterTechnologies(oldElectricityMeter, electricityMeter.TechnologiesUsed);

                    // TODO
                    electricityMeter.CreatedByUserId = oldElectricityMeter.CreatedByUserId;
                    electricityMeter.CreatedDate = oldElectricityMeter.CreatedDate;

                    db.Entry(oldElectricityMeter).CurrentValues.SetValues(electricityMeter);
                    db.SaveChanges();
                }
            });
        }

        /// <summary>
        /// Add electricity meter to database
        /// </summary>
        /// <param name="electricityMeter"></param>
        /// <returns></returns>
        public static int AddElectricityMeter(ElectricityMeter electricityMeter)
        {
            int electricityMeterId = 0;
            using (TransactionScope scope = new TransactionScope(TransactionScopeAsyncFlowOption.Enabled))
            {
                SiteDataManager.CallDBContext(db =>
                {
                    // Cache the electricity meter technologies
                    IList<ElectricityMeterTechnology> electricityMeterTechnologies = electricityMeter.TechnologiesUsed;

                    // Remove the electricity meter technologies from electricity meter entity to avoid exceptions
                    electricityMeter.TechnologiesUsed = null;

                    // First insert the electricity meter on db
                    db.ElectricityMeters.Add(electricityMeter);
                    db.SaveChanges();
                    //Important Note: This line should be after save changes
                    electricityMeterId = electricityMeter.ElectricityMeterId;

                    // After insert the electricity meter and get its id, update the electricity meter technologies and then insert its on db
                    if (electricityMeterTechnologies != null)
                    {
                        foreach (ElectricityMeterTechnology technology in electricityMeterTechnologies)
                        {
                            technology.ElectricityMeterId = electricityMeterId;
                        }

                        db.ElectricityMeterTechnologies.AddRange(electricityMeterTechnologies);
                        db.SaveChanges();
                    }
                });

                scope.Complete();
            }

            return electricityMeterId;
        }

    }
}