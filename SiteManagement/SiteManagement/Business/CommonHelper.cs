﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace SiteManagement.Business
{
    public static class CommonHelper
    {
        public static DateTime GetDateFromModel(string date)
        {
            return DateTime.ParseExact(date, Constants.DateFormat, CultureInfo.InvariantCulture);   
        }

        //public static string GetDateFromString(DateTime date)
        //{
        //    //return DateTime.ParseExact(date, Constants.DateFormat, CultureInfo.InvariantCulture);
        //}
    }
}