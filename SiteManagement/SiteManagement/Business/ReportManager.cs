﻿using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;

namespace SiteManagement.Business
{
    public static class ReportManager
    {
        private static List<ElectricityBill> GetOutOfAvarageElectricityBills()
        {
            List<ElectricityBill> outOfAvarageElectricityBills = new List<ElectricityBill>();

            List<ElectricityBill> electricityBillList = new List<ElectricityBill>();

            SiteDataManager.CallDBContext(db =>
            {
                electricityBillList = db.ElectricityBills
                                        .Select(b => new
                                        {
                                            ElectricityBillCode = b.ElectricityBillCode,
                                            BillDate = b.BillDate,
                                            OldReading = b.OldReading,
                                            NewReading = b.NewReading,
                                            Consumption = b.Consumption,
                                            Amount = b.Amount,
                                            ElectricityMeter = new
                                            {
                                                Site = new
                                                {
                                                    SiteCode = b.ElectricityMeter.Site.SiteCode,
                                                },
                                                ElectricityMeterCode = b.ElectricityMeter.ElectricityMeterCode,
                                                EstimatePowerConsumption = b.ElectricityMeter.EstimatePowerConsumption,
                                                TechnologiesUsed = b.ElectricityMeter.TechnologiesUsed.Select(emt => new
                                                {
                                                    TechnologyUsed = new
                                                    {
                                                        AverageCost = emt.TechnologyUsed.AverageCost
                                                    },
                                                })
                                            }
                                        })
                                        .AsEnumerable()
                                        .Select(b => new ElectricityBill
                                        {
                                            ElectricityBillCode = b.ElectricityBillCode,
                                            BillDate = b.BillDate,
                                            OldReading = b.OldReading,
                                            NewReading = b.NewReading,
                                            Consumption = b.Consumption,
                                            Amount = b.Amount,
                                            ElectricityMeter = new ElectricityMeter
                                            {
                                                Site = new Site
                                                {
                                                    SiteCode = b.ElectricityMeter.Site.SiteCode,
                                                },
                                                ElectricityMeterCode = b.ElectricityMeter.ElectricityMeterCode,
                                                EstimatePowerConsumption = b.ElectricityMeter.EstimatePowerConsumption,
                                                TechnologiesUsed = b.ElectricityMeter.TechnologiesUsed.Select(emt => new ElectricityMeterTechnology
                                                {
                                                    TechnologyUsed = new TechnologyUsed
                                                    {
                                                        AverageCost = emt.TechnologyUsed.AverageCost
                                                    }
                                                }).ToList()
                                            }
                                        }).ToList();
            });

            foreach (ElectricityBill electricityBill in electricityBillList)
            {
                if (electricityBill.Amount > electricityBill.ElectricityMeter.EstimatePowerConsumption)
                {
                    outOfAvarageElectricityBills.Add(electricityBill);
                }
            }

            return outOfAvarageElectricityBills;
        }

        private static List<ElectricityBill> GetDuplicateElectricityBills()
        {
            List<ElectricityBill> duplicateElectricityBills = new List<ElectricityBill>();

            SiteDataManager.CallDBContext(db =>
            {
                duplicateElectricityBills = db.ElectricityBills
                                                .GroupBy(b => b.ElectricityBillCode)
                                                .Where(g => g.Count() > 1)
                                                .SelectMany(grp => grp.Select(sb => new
                                                {
                                                    ElectricityBillCode = sb.ElectricityBillCode,
                                                    BillDate = sb.BillDate,
                                                    
                                                    OldReading = sb.OldReading,
                                                    NewReading = sb.NewReading,
                                                    Consumption = sb.Consumption,
                                                    Amount = sb.Amount,
                                                    ElectricityMeter = new
                                                    {
                                                        Site = new
                                                        {
                                                            SiteCode = sb.ElectricityMeter.Site.SiteCode,
                                                        },
                                                        ElectricityMeterCode = sb.ElectricityMeter.ElectricityMeterCode
                                                    }
                                                }))
                                                .AsEnumerable()
                                                .Select(b => new ElectricityBill
                                                {
                                                    ElectricityBillCode = b.ElectricityBillCode,
                                                    BillDate = b.BillDate,
                                                    OldReading = b.OldReading,
                                                    NewReading = b.NewReading,
                                                    Consumption = b.Consumption,
                                                    Amount = b.Amount,
                                                    ElectricityMeter = new ElectricityMeter
                                                    {
                                                        Site = new Site
                                                        {
                                                            SiteCode = b.ElectricityMeter.Site.SiteCode
                                                        },
                                                        ElectricityMeterCode = b.ElectricityMeter.ElectricityMeterCode
                                                    }
                                                }).ToList();
            });

            return duplicateElectricityBills;
        }

        private static byte[] ExportElectricityBill(List<ElectricityBill> electricityBills, bool showElectricityBillRealAmount = false)
        {
            using (MemoryStream ms = new MemoryStream())
            {
                // Create excel sheet document
                using (SpreadsheetDocument excelDocument = SpreadsheetDocument.Create(ms, SpreadsheetDocumentType.Workbook))
                {
                    ms.Seek(0, SeekOrigin.Begin);

                    // Create workbook part
                    WorkbookPart workbookPart = excelDocument.AddWorkbookPart();
                    workbookPart.Workbook = new Workbook();

                    // Create sheets
                    Sheets sheets = excelDocument.WorkbookPart.Workbook.AppendChild<Sheets>(new Sheets());

                    // Create Sheet
                    WorksheetPart worksheetPart = workbookPart.AddNewPart<WorksheetPart>();
                    Worksheet workSheet = new Worksheet();
                    SheetData sheetData = new SheetData();

                    Sheet sheet = new Sheet()
                    {
                        Id = excelDocument.WorkbookPart.GetIdOfPart(worksheetPart),
                        SheetId = 1,
                        Name = "Electricity Bill"
                    };

                    sheets.Append(sheet);

                    workSheet.AppendChild(sheetData);
                    worksheetPart.Worksheet = workSheet;

                    // Create header row
                    Row headerRow = new Row();

                    // Prepare header titles
                    List<string> columnNames = new List<string>
                                {
                                    "Site Code",
                                    "Electricity Meter Code",
                                    "Electricity Bill Code",
                                    "Bill Date",
                                    "Old Reading",
                                    "New Reading",
                                    "Consumption",
                                    "Bill Amount"
                                };

                    if (showElectricityBillRealAmount)
                    {
                        columnNames.Add("Real Amount");
                    }

                    // Create header columns
                    foreach (string column in columnNames)
                    {
                        // Create header cell
                        Cell cell = new Cell
                        {
                            DataType = CellValues.String,
                            CellValue = new CellValue(column),
                            StyleIndex = 2
                        };

                        headerRow.AppendChild(cell);
                    }

                    sheetData.AppendChild(headerRow);

                    // Create data rows
                    for (int i = 0; i < electricityBills.Count(); i++)
                    {
                        Row dataRow = new Row();

                        // Prepare data row
                        IList<object> data = new List<object>
                        {
                            electricityBills[i].ElectricityMeter.Site.SiteCode,
                            electricityBills[i].ElectricityMeter.ElectricityMeterCode,
                            electricityBills[i].ElectricityBillCode,
                            electricityBills[i].BillDate,
                            electricityBills[i].OldReading,
                            electricityBills[i].NewReading,
                            electricityBills[i].Consumption,
                            electricityBills[i].Amount,
                        };

                        if (showElectricityBillRealAmount)
                        {
                            data.Add(electricityBills[i].ElectricityMeter.EstimatePowerConsumption);
                        }

                        // Create data row
                        foreach (object item in data)
                        {
                            Cell cell = new Cell
                            {
                                CellValue = new CellValue(item.ToString()),
                            };

                            dataRow.AppendChild(cell);
                        }

                        sheetData.AppendChild(dataRow);
                    }

                    excelDocument.WorkbookPart.Workbook.Save();

                    excelDocument.Close();

                    return ms.ToArray();
                }
            }
        }

        public static byte[] ExportOutOfAverageElectricityBills()
        {
            List<ElectricityBill> electricityBills = GetOutOfAvarageElectricityBills();
            return ExportElectricityBill(electricityBills, true);
        }

        public static byte[] ExportDuplicateElectricityBills()
        {
            List<ElectricityBill> electricityBills = GetDuplicateElectricityBills();
            return ExportElectricityBill(electricityBills, false);
        }
    }
}