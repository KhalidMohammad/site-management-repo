﻿using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.SessionState;

namespace SiteManagement
{
    public static class SessionManager
    {
        static SessionManager()
        {
            SharedEvent.GetCurrentUser = () => CurrentUser;
        }

        public static User CurrentUser
        {
            get
            {
                User user = null;
                HttpSessionState session = HttpContext.Current != null ? HttpContext.Current.Session : null;
                if (session != null)
                {
                    user = session["user"] as User;
                }

                return user;
            }
            set
            {
                HttpSessionState session = HttpContext.Current != null ? HttpContext.Current.Session : null;
                if (session != null)
                {
                    session["user"] = value;
                }
            }
        }
    }
}