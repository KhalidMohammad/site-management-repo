﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace SiteManagement.Business
{
    public static class LogManager
    {
        public static void Write(this Exception exception, string category = null)
        {
            StringBuilder errorMessage = new StringBuilder();

            errorMessage.AppendLine("\n----------------------------------------------- Message -----------------------------------------------\n");
            errorMessage.AppendLine(exception.Message);

            Exception inner = exception;
            while (inner != null)
            {
                errorMessage.AppendLine(inner.ToString());

                errorMessage.AppendLine("\n----------------------------------------------- InnerException -----------------------------------------------");

                inner = inner.InnerException;
            }

            errorMessage.AppendLine("\n----------------------------------------------- StackTrace -----------------------------------------------\n");
            errorMessage.AppendLine(exception.StackTrace);

            var dir = AppDomain.CurrentDomain.BaseDirectory;
            string path = Path.Combine(dir, "log", "log.txt");
            if (!File.Exists(path))
            {
                File.WriteAllText(path, errorMessage.ToString());
            }
            else
            {
                File.AppendAllText(path, errorMessage.ToString());
            }
        }
    }
}