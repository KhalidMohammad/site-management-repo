﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class SiteApprovalModel
    {
        public int Id { get; set; }

        public string SiteCode { get; set; }

        public string SiteName { get; set; }

        public string EditedMemberName { get; set; }

        public int ApprovalStatusId { get; set; }

        public string SiteType { get; set; }

        public string ApprovalStatus { get; set; }

        public List<string> PendingFields { get; set; }

        public string PendingFieldsString { get; set; }
    }
}