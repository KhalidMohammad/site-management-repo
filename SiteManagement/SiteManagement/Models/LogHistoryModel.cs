﻿using DocumentFormat.OpenXml.Wordprocessing;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class LogHistoryModel
    {
        [JsonProperty("Id")]
        public int ChangeId { get; set; }

        [Display(Name = "User")]
        public string User { get; set; }

        [Display(Name = "Change Date")]
        public string ChangeDate { get; set; }

        [Display(Name = "Type")]
        public string ChangeType { get; set; }
    }

    public class LogHistoryDetailModel
    {
        [JsonProperty("Id")]
        public int ChangeId { get; set; }

        [Display(Name = "Field Name")]
        public string FieldName { get; set; }

        [Display(Name = "Original Value")]
        public string OriginalValue { get; set; }


        [Display(Name = "New Value")]
        public string NewValue { get; set; }
    }
}