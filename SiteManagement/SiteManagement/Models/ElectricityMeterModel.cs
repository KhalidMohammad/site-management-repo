﻿using Newtonsoft.Json;
using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class ElectricityMeterModel
    {
        [JsonProperty("id")]
        public int ElectricityMeterId { get; set; }

        [JsonProperty("code")]
        [Required(ErrorMessage = "Enter a meter code")]
        [Display(Name = "Electricity Meter Code")]
        public string ElectricityMeterCode { get; set; }

        [Required(ErrorMessage = "Select an electricity meter type")]
        [Display(Name = "Electricity Meter Type")]
        public int ElectricityMeterType { get; set; }

        [JsonProperty("distribution")]
        [Required(ErrorMessage = "Select an electricity meter distribution")]
        [Display(Name = "Electricity Meter Distribution")]
        public int? ElectricityMeterDistribution { get; set; }

        [JsonProperty("electricityMeterSubType")]
        [Required(ErrorMessage = "Select an electricity meter subtype")]
        [Display(Name = "Electricity Meter Subtype")]
        public int? ElectricityMeterSubType { get; set; }

        [JsonProperty("electricityMeterPaymentSubType")]
        [Required(ErrorMessage = "Select an electricity meter payment subtype")]
        [Display(Name = " Electricity Meter Payment Subtype")]
        public int? ElectricityMeterPaymentSubType { get; set; }

        [JsonProperty("pipesFilling")]
        [Display(Name = "Pipes Filling")]
        public bool PipesFilling { get; set; }

        [Required(ErrorMessage = "Select technologies used")]
        [Display(Name = "Technology Used")]
        public List<int> TechnologyUsed { get; set; }

        [Required(ErrorMessage = "Enter an estimate power consumption")]
        [Display(Name = "Estimate Power Consumption")]
        public decimal EstimatePowerConsumption { get; set; }

        [JsonProperty("isActive")]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [JsonProperty("activationDate")]
        [Display(Name = "Activation Date")]
        public string ActivationDate { get; set; }

        [JsonProperty("meterShield")]
        [Display(Name = "Meter Shield")]
        public bool MeterShield { get; set; }

        [JsonProperty("uSence")]
        [Display(Name = "U Sence")]
        public bool USence { get; set; }

        [JsonProperty("boxInstallaion")]
        [Display(Name = "Box Installaion")]
        public bool BoxInstallaion { get; set; }

        public int ElectricityMeterSiteId { get; set; }


        [JsonProperty("pipesFillingSupplierName")]
        [Display(Name = "Supplier Name")]
        public string PipesFillingSupplierName { get; set; }


        [JsonProperty("uSenceSupplierName")]
        [Display(Name = "Supplier Name")]
        public string USenceSupplierName { get; set; }
    }

}