﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class UserModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("fullName")]
        [Required(ErrorMessage = "Enter a full name")]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [JsonProperty("userName")]
        [Required(ErrorMessage = "Enter a user name")]
        [Display(Name = "UserName")]
        public string UserName { get; set; }

        [JsonProperty("email")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        [Required(ErrorMessage = "Enter a email")]
        [Display(Name = "Email")]
        public string UserEmail { get; set; }

        [Required(ErrorMessage = "Enter a password")]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Required(ErrorMessage = "Select a role")]
        [Display(Name = "Role")]
        public int UserRole { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }
    }
}