﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class SiteModel
    {
        public int SiteId { get; set; }

        [Required(ErrorMessage = "Enter a code")]
        [Display(Name = "Code")]
        public string SiteCode { get; set; }


        //[Required(ErrorMessage = "Select a related site")]
        [Display(Name = "Related Sites")]
        public List<int> RelatedSite { get; set; }

        [Required(ErrorMessage = "Enter a name")]
        [Display(Name = "Name")]
        public string SiteName { get; set; }

        [Required(ErrorMessage = "Enter a latitude")]
        [Display(Name = "Latitude")]
        public string Latitude { get; set; }

        [Required(ErrorMessage = "Enter a longitude")]
        [Display(Name = "Longitude")]
        public string Longitude { get; set; }

        //[Required(ErrorMessage = "Enter a city")]
        [Display(Name = "City")]
        public string SiteCity { get; set; }

        //[Required(ErrorMessage = "Enter a Site Governorates")]
        [Display(Name = "Site Governorates")]
        public string SiteGovernorates { get; set; }

        //[Required(ErrorMessage = "Enter a street")]
        [Display(Name = "Street")]
        public string StreetName { get; set; }

        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [Required(ErrorMessage = "Select a type")]
        [Display(Name = "Type")]
        public int SiteType { get; set; }

        //[Required(ErrorMessage = "Enter a note")]
        [Display(Name = "Note")]
        public string SiteNote { get; set; }

        [Display(Name = "Address")]
        public string SiteAddress{ get; set; }

        [Display(Name = "Municipality Permission")]
        public bool MunicipalityPermission { get; set; }

        [Display(Name = "Municipality Permission Date")]
        public string MunicipalityPermissionDate { get; set; }

        [Display(Name = "Municipality Permission Picture")]
        public string MunicipalityPermissionPictureJson { get; set; }

        public List<FileModel> MunicipalityPermissionPicture { get; set; }

        [Display(Name = "Location SubType")]
        public int? LocationSubType { get; set; }

        [Display(Name = "Location SubType Note")]
        public string LocationSubTypeNote { get; set; }

        [Display(Name = "TX SiteType")]
        public int? TXSiteType { get; set; }

        [Display(Name = "Pay Rent")]
        public bool PayRent { get; set; }

        [Display(Name = "Phase Number")]
        public string PhaseNumber { get; set; }

        [Display(Name = "On Air Date")]
        public string OnAirDate { get; set; }

        [Display(Name = "Site Code Prefix")]
        public string SiteCodePrefix { get; set; }

        public List<string> PendingFields { get; set; }

        public bool IsSitePendingApproval { get { return PendingFields != null && PendingFields.Contains("Site"); } }

        public bool IsOwnerPendingApproval { get { return PendingFields != null && PendingFields.Contains("Owner"); } }

        public bool IsContractPendingApproval { get { return PendingFields != null && PendingFields.Contains("Contract"); } }

        public bool IsElectricityMeterPendingApproval { get { return PendingFields != null && PendingFields.Contains("ElectricityMeter"); } }

        public SiteModel()
        {
            PendingFields = new List<string>();
        }
    }


    public class MapModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        public string Code { get; set; }

        [JsonProperty("lat")]
        public string Latitude { get; set; }

        [JsonProperty("lon")]
        public string Longitude { get; set; }

        [JsonProperty("title")]
        public string Title { get; set; }

        [JsonProperty("html")]
        public string Html { get; set; }

        [JsonProperty("icon")]
        public string Icon { get; set; }

        [JsonProperty("zoom")]
        public int Zoom { get; set; }
    }
}