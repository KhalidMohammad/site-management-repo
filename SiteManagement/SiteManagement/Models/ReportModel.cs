﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class ReportModel
    {
        [Required(ErrorMessage = "Select a report type")]
        [Display(Name = "Report Type")]
        public int ReportType { get; set; }
    }
}