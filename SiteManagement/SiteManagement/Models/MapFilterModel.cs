﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class SiteFilterModel
    {
        [Display(Name = "Site Code")]
        public string SiteCode { get; set; }

        [Display(Name = "City")]
        public string SiteCity { get; set; }

        [Display(Name = "Street")]
        public string StreetName { get; set; }

        [Display(Name = "Type")]
        public int SiteType { get; set; }
    }

    public class ContractFilterModel
    {
        //[Display(Name = "Start Date")]
        //public string StartDate { get; set; }

        //[Display(Name = "End Date")]
        //public string EndDate { get; set; }

        public DateRangeModel Date { get; set; }

        [Display(Name = "Active Date")]
        public string ActiveDate { get; set; }

        [Display(Name = "Terminated Date")]
        public string TerminatedDate { get; set; }
    }

    public class OwnerFilterModel
    {
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Display(Name = "Owner Code")]
        public string OwnerCode { get; set; }

        [Display(Name = "Primary Phone")]
        public string PrimaryPhone { get; set; }

        [Display(Name = "Secondary Phone")]
        public string SecondaryPhone { get; set; }

        [Display(Name = "Email")]
        public string Email { get; set; }

        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }
    }

    public class ElectricityMeterFilterModel
    {
        [Display(Name = "Electricity Meter Code")]
        public string ElectricityMeterCode { get; set; }

        [Display(Name = "Electricity Meter Type")]
        public int ElectricityMeterType { get; set; }

        //[Display(Name = "Technology Used")]
        //public List<int> TechnologyUsed { get; set; }
    }
}