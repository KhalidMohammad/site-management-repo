﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class LookupModel
    {
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("displayName")]
        [Required(ErrorMessage = "Enter a name")]
        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }

        /// <summary>
        /// This property is used inside the _PartialLookup (select2 control)
        /// </summary>
        [Required(ErrorMessage = "Select a type")]
        [Display(Name = "Type")]
        public int Type { get; set; }

        /// <summary>
        /// This property is used only to show type string on lookup grid
        /// </summary>
        [JsonProperty("type")]
        public string TypeString { get; set; }
    }
}