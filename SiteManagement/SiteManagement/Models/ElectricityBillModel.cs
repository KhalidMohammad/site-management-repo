﻿using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class ElectricityBillModel
    {
        public int Id { get; set; }

        public int ElectricityMeterId { get; set; }

        [Required(ErrorMessage = "Enter an old reading")]
        [Display(Name = "Old Reading")]
        public string OldReading { get; set; }

        [Required(ErrorMessage = "Enter a new reading")]
        [Display(Name = "New Reading")]
        public string NewReading { get; set; }

        //TODO_khalid change to date
        [Required(ErrorMessage = "Enter a bill date")]
        [Display(Name = "Bill Date")]
        public string BillDate { get; set; }

        [Required(ErrorMessage = "Enter a consumption")]
        [Display(Name = "Consumption")]
        public string Consumption { get; set; }

        //TODO_khalid change to decimal
        [Required(ErrorMessage = "Enter an amount")]
        [Display(Name = "Amount")]
        public decimal Amount { get; set; }

        [Display(Name = "Electricity Bill Code")]
        public string ElectricityBillCode { get;  set; }
    }

}