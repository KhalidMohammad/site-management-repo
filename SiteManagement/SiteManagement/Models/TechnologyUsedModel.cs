﻿using Newtonsoft.Json;
using SiteManagement.Data;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class TechnologyUsedModel
    {
        [JsonProperty("id")]
        public int TechnologyUsedId { get; set; }

        [JsonProperty("displayName")]
        [Required(ErrorMessage = "Enter a dispaly name")]
        [Display(Name = "Display Name")]
        public string DisplayName { get; set; }

        [JsonProperty("averageCost")]
        [Range(1, int.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [Required(ErrorMessage = "Enter an average cost")]
        [Display(Name = "Average Cost")]
        public decimal AverageCost { get; set; }
    }

}