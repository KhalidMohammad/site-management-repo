﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class ImportFileModel
    {
        [Required(ErrorMessage = "Select an import type")]
        [Display(Name = "Import Type")]
        public int ImportType { get; set; }

        [Required(ErrorMessage = "Upload a file")]
        [Display(Name = "File")]
        public string ImportFileJson { get; set; }

        public FileModel ImportFile { get; set; }
    }
}