﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class OwnerModel
    {
        [JsonProperty("id")]
        public int OwnerId { get; set; }

        [JsonProperty("fullName")]
        [Required(ErrorMessage = "Enter a full name")]
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [JsonProperty("primaryPhone")]
        [Required(ErrorMessage = "Enter a primary phone")]
        [Display(Name = "Primary Phone")]
        public string PrimaryPhone { get; set; }

        [JsonProperty("secondaryPhone")]
        //[Required(ErrorMessage = "Enter a secondary phone")]
        [Display(Name = "Secondary Phone")]
        public string SecondaryPhone { get; set; }

        [JsonProperty("email")]
        [EmailAddress(ErrorMessage = "Invalid email address")]
        //[Required(ErrorMessage = "Enter a email")]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [JsonProperty("bankName")]
        //[Required(ErrorMessage = "Enter a bank name")]
        [Display(Name = "Bank Name")]
        public string BankName { get; set; }

        [JsonProperty("bankBranch")]
        //[Required(ErrorMessage = "Enter a bank branch")]
        [Display(Name = "Bank Branch")]
        public string BankBranch { get; set; }

        [JsonProperty("accountNumber")]
        //[Required(ErrorMessage = "Enter a account number")]
        [Display(Name = "Account Number")]
        public string AccountNumber { get; set; }

        [Display(Name = "Chuque or Transfer")]
        public bool IsPay { get; set; }

        [JsonProperty("iban")]
        //[Required(ErrorMessage = "Enter a IBAN")]
        [Display(Name = "IBAN")]
        public string IBAN { get; set; }

        [JsonProperty("percentage")]
        [Range(0, 100, ErrorMessage = "The value must be greater than or equal 0 and less than 100")]
        //[Required(ErrorMessage = "Enter owner perventage")]
        [Display(Name = "Owner Percentage")]
        public decimal Percentage { get; set; }


        [JsonProperty("percentageOfMonyReseaved")]
        [Range(0, 100, ErrorMessage = "The value must be greater than or equal 0 and less than 100")]
        [Display(Name = "Percentage of Money Received")]
        public decimal PercentageOfMonyReseaved { get; set; }


        [JsonProperty("note")]
        [Display(Name = "Note")]
        public string Note { get; set; }


        [JsonProperty("ownerCode")]
        [Display(Name = "ERP Code")]
        public string OwnerCode { get; set; }


        [JsonProperty("isInventoryofLegacy")]
        [Display(Name = "Is Inventory of Legacy (7aser Erth)")]
        public bool IsInventoryofLegacy { get; set; }

        [JsonIgnore]
        [Display(Name = "inventory of Legacy")]
        public string InventoryofLegacyJson { get; set; }

        [JsonIgnore]
        public List<FileModel> InventoryofLegacy { get; set; }

        public int OwnerSiteId { get; set; }

        [JsonProperty("nationaity")]
        [Display(Name = "Nationaity")]
        public int? Nationaity { get; set; }

        [JsonProperty("nationalId")]
        [Display(Name = "National Id")]
        public string NationalId { get; set; }
    }
}