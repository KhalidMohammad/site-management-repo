﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class UserLoginModel
    {
        [Required(ErrorMessage = "Enter the username")]
        [Display(Name = "Username")]
        public string Username
        {
            get;
            set;
        }

        [Required(ErrorMessage = "Enter the password")]
        [Display(Name = "Password")]
        public string Password
        {
            get;
            set;
        }
    }
}