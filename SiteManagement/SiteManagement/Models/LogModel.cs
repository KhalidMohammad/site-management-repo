﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class LogModel
    {
        [Display(Name = "Created By")]
        public string CreatedBy { get; set; }

        [Display(Name = "Created Date")]
        public string CreatedDate { get; set; }

        [Display(Name = "Last Updated By")]
        public string LastUpdatedBy { get; set; }

        [Display(Name = "Last Updated Date")]
        public string LastUpdateDate { get; set; }

        [Display(Name = "Partial Approved By")]
        public string PartialApprovedBy { get; set; }

        [Display(Name = "Partial Approved Date")]
        public string PartialApprovedDate { get; set; }

        [Display(Name = "Approved By")]
        public string ApprovedBy { get; set; }

        [Display(Name = "Approved Date")]
        public string ApprovedDate { get; set; }
    }
}