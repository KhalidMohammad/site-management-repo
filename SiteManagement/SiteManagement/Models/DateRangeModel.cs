﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class DateRangeModel
    {
        [Required(ErrorMessage = "Enter a start date")]
        [DataType(DataType.Date)]
        public string Start { get; set; }

        [Required(ErrorMessage = "Enter a end date")]
        //[DateCompare("StartDate", Operation.GreaterThan, "Admin.School.EndDateCompare")]
        [DataType(DataType.Date)]
        public string End { get; set; }
    }
}