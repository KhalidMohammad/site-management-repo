﻿using Newtonsoft.Json;
using SiteManagement.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SiteManagement.Models
{
    public class ContractModel
    {
        public ContractModel()
        {
            Rents = new List<ContractYearlyRentModel>();
        }

        [JsonProperty("id")]
        public int ContractId { get; set; }

        //[JsonProperty("startDate")]
        //[Required(ErrorMessage = "Enter a start date")]
        //[Display(Name = "Start Date")]
        //public string StartDate { get; set; }

        //[JsonProperty("endDate")]
        //[Required(ErrorMessage = "Enter an end date")]
        //[Display(Name = "End Date")]
        //public string EndDate { get; set; }

        [JsonIgnore]
        [Display(Name = "Date")]
        public DateRangeModel Date { get; set; }

        /// <summary>
        /// This only used in contract grid
        /// </summary>
        [JsonProperty("startDate")]
        public string StartDateString { get; set; }

        /// <summary>
        /// This only used in contract grid
        /// </summary>
        [JsonProperty("endDate")]
        public string EndDateString { get; set; }

        //[JsonProperty("yearlyRent")]
        //[Range(1, int.MaxValue, ErrorMessage = "The value must be greater than 0")]
        //[Required(ErrorMessage = "Enter a yearly rent")]
        //[Display(Name = "Yearly Rent")]
        //public decimal YearlyRent { get; set; }

        [JsonProperty("maarefTax")]
        //[Range(1, int.MaxValue, ErrorMessage = "The value must be greater than 0")]
        [Required(ErrorMessage = "Enter a maaref tax")]
        [Display(Name = "Ma'aref Tax")]
        public decimal MaarefTax { get; set; }

        [JsonProperty("municipalityPermission")]
        [Display(Name = "Municipality Permission")]
        public bool MunicipalityPermission { get; set; }

        [JsonIgnore]
        //[Required(ErrorMessage = "Upload a document file")]
        [Display(Name = "Document File Path")]
        public string DocumentFileJson { get; set; }

        [JsonIgnore]
        [Display(Name = "Document File Path")]
        public List<FileModel> DocumentFile { get; set; }

        [JsonIgnore]
        public int ContractSiteId { get; set; }

        [JsonIgnore]
        [Display(Name = "Payment Type")]
        public int? PaymentType { get; set; }

        [JsonIgnore]
        [Display(Name = "Termination Letter")]
        public string TerminationLetterJson { get; set; }

        [JsonIgnore]
        [Display(Name = "Termination Letter")]
        public List<FileModel> TerminationLetter { get; set; }

        [JsonIgnore]
        [Display(Name = "Owner cerificate letter")]
        public string QOSHANJson { get; set; }

        [JsonIgnore]
        public List<FileModel> QOSHAN { get; set; }

        [JsonIgnore]
        [Display(Name = "Umniah Template")]
        public bool IsMadeByUmniah { get; set; }

        [JsonIgnore]
        [Display(Name = "Co location clause")]
        public bool IsColocationClose { get; set; }

        [Display(Name = "Colocation Close Note")]
        public string ColocationCloseNote { get; set; }

        [JsonIgnore]
        [Display(Name = "Is Active")]
        public bool IsActive { get; set; }

        [JsonIgnore]
        [Display(Name = "Active Date")]
        public string ActiveDate { get; set; }

        [JsonIgnore]
        [Display(Name = "Active Note")]
        public string ActiveNote { get; set; }

        [JsonIgnore]
        [Display(Name = "Is Terminated")]
        public bool IsTerminated { get; set; }

        [JsonIgnore]
        [Display(Name = "Terminated Date")]
        public string TerminatedDate { get; set; }

        [JsonIgnore]
        [Display(Name = "Terminated Note")]
        public string TerminatedNote { get; set; }


        [JsonProperty("aquariumNumber")]
        [Display(Name = "7ood number")]
        public string AquariumNumber { get; set; }


        [JsonProperty("aquariumName")]
        [Display(Name = "7ood Name")]
        public string AquariumName { get; set; }

        [JsonProperty("pieceNumber")]
        [Display(Name = "Piece Number")]
        public string PieceNumber { get; set; }


        [JsonProperty("directorate")]
        [Display(Name = "Directorate Name")]
        public string Directorate { get; set; }

        [JsonProperty("town")]
        [Display(Name = "Town Name")]
        public string Town { get; set; }


        [JsonProperty("plateNumber")]
        [Display(Name = "Plate Number")]
        public string PlateNumber { get; set; }

        [JsonProperty("signtuerDate")]
        //[Required(ErrorMessage = "Enter a start date")]
        [Display(Name = "Signtuer Date")]
        public string SigntuerDate { get; set; }

        [Display(Name = "Acquired by")]
        public int? AcquiredbyId { get; set; }

        [Display(Name = "Is Incremental")]
        public bool IsIncremental { get; set; }

        [JsonProperty("yearlyIncremental")]
        [Display(Name = "Yearly Incremental")]
        public decimal? YearlyIncremental { get; set; }

        //[Display(Name = "Yearly Rents")]
        //public string YearlyRents { get; set; }

        public List<ContractYearlyRentModel> Rents { get; set; }
    }

    public class ContractYearlyRentModel
    {
        //public int ContractId { get; set; }

        public DateTime StartDate { get; set; }

        public DateTime EndDate { get; set; }

        public string Label { get; set; }

        [JsonProperty("rent")]
        [Display(Name = "Rent")]
        public decimal Amount { get; set; }
    }
}