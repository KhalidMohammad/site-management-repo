﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(SiteManagement.Startup))]
namespace SiteManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
